#ifndef THREADSLEEP_HPP
#define THREADSLEEP_HPP

#include <QWaitCondition>
#include <QMutex>

namespace kernel {
  namespace utils {

    class ThreadSleep
    {
    public:
      static void msleep(unsigned long msecs)
      {
        QMutex mutex;
        mutex.lock();

        QWaitCondition waitCondition;
        waitCondition.wait(&mutex, msecs);

        mutex.unlock();
      }
    };

  }
}

#endif // THREADSLEEP_HPP
