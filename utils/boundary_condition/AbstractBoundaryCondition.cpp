#include "AbstractBoundaryCondition.hpp"

#include <cassert>

/*!
 * \brief AbstractBoundaryCondition::operator ==
 * \param rhs
 * \return
 */
bool AbstractBoundaryCondition::operator ==(const AbstractBoundaryCondition& rhs) const
{
  return ( conditionType == rhs.conditionType ) && ( name == rhs.name );
}

/*!
 * \brief AbstractBoundaryCondition::operator !=
 * \param rhs
 * \return
 */
bool AbstractBoundaryCondition::operator!=(const AbstractBoundaryCondition &rhs) const
{
  return ! operator ==( rhs );
}

/*!
 * \brief AbstractBoundaryCondition::getBoundaryConditionType
 * \return
 */
BOUNDARY_CONDITION_TYPE AbstractBoundaryCondition::getBoundaryConditionType() const
{
  return conditionType;
}

/*!
 * \brief AbstractBoundaryCondition::getName
 * \return
 */
std::string AbstractBoundaryCondition::getName() const
{
  return name;
}

/*!
 * \brief AbstractBoundaryCondition::setName
 * \param value
 */
void AbstractBoundaryCondition::setName(const std::string &value)
{
  name = value;
}



#include <DirihletBoundaryCondition.hpp>
#include <NeumannBoundaryCondition.hpp>

bool test::AbstractBoundaryConditionComparisonTest()
{
  std::shared_ptr<AbstractBoundaryCondition> dirihlet( new DirihletBoundaryCondition );
  std::shared_ptr<AbstractBoundaryCondition> dirihlet1( new DirihletBoundaryCondition );
  std::shared_ptr<AbstractBoundaryCondition> neumann( new NeumannBoundaryCondition );
  std::shared_ptr<AbstractBoundaryCondition> neumann1( new NeumannBoundaryCondition );

  static_cast< DirihletBoundaryCondition * > ( dirihlet1.get() )->setCondition( 42 );
  static_cast< NeumannBoundaryCondition * > ( neumann1.get() )->setCondition( 42, 2 );

  assert( *(dirihlet.get()) == *(dirihlet.get()) );
  assert( *(neumann.get()) == *(neumann.get()) );

  assert( *(dirihlet.get()) != *(dirihlet1.get()) );
  assert( *(neumann.get()) != *(neumann1.get()) );

  assert( *(dirihlet.get()) != *(neumann.get()) );
  assert( *(neumann.get()) != *(dirihlet.get()) );

  return true;
}
