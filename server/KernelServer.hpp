#ifndef TERMETSERVER_HPP
#define TERMETSERVER_HPP

#include <QtNetwork/QTcpServer>

#include <QVector>
#include <../client/ServerDescription.hpp>

namespace kernel {
  namespace server {

    class KernelWorkgroupMaintenancer;

    /*!
     * \brief The KernelServer class
     */
    class KernelServer : public QTcpServer
    {
      Q_OBJECT

    public:
      static KernelServer* getInstance();

      static QHostAddress serverAddress();
      static void setServerAddress(const QHostAddress &serverAddress);

      static quint32 portNumber();
      static void setPortNumber(const quint32 &portNumber);

      static QHostAddress mongodbServerAddress();
      static void setMongodbServerAddress(const QHostAddress &mongodbServerAddress);

      static quint32 mongodbPortNumber();
      static void setMongodbPortNumber(const quint32 &mongodbPortNumber);

      static QString getWorkgroupTag();
      static void setWorkgroupTag(const QString &value);

      quint64 connections() const;
      void stopServer();

    signals:
      void signal_interrupt();

    protected:
      void incomingConnection(int socketDescriptor);

    private:
      KernelServer(QObject *parent = nullptr) : QTcpServer(parent) {}

    private:
      // Server related info
      static bool activated;
      static QHostAddress serverAddress_;
      static quint32 portNumber_;

      quint64 connections_;

      // MongoDB relater info
      static QHostAddress mongodbServerAddress_;
      static quint32 mongodbPortNumber_;
      static QString workgroupTag_;

      static KernelWorkgroupMaintenancer* workgroupMaintenancerPtr;

    };

  }
}


#endif // TERMETSERVER_HPP
