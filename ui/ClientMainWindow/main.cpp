#include <QApplication>

#include "ClientMainWindow.hpp"

int main(int argc, char *argv[])
{
  QApplication a( argc, argv );

  ClientMainWindow mainWindow;
  mainWindow.show();

  return a.exec();
}
