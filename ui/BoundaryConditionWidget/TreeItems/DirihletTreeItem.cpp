#include "DirihletTreeItem.hpp"

#include <QDebug>

/*!
 * \brief DirihletTreeItem::DirihletTreeItem
 * \param name
 * \param value
 * \param color
 * \param parent
 */
DirihletTreeItem::DirihletTreeItem(const QString &name, qreal value, const QColor color, RootTreeItem *parent) :
  AbstractTreeItem( parent )
{
  std::shared_ptr< AbstractBoundaryCondition > boundaryCondition( new DirihletBoundaryCondition );
  static_cast<DirihletBoundaryCondition*>( boundaryCondition.get() )->setCondition( value );
  applyedCondition.setBoundaryCondition( boundaryCondition );
  applyedCondition.setColor( color );
  applyedCondition.setName( name );
}

/*!
 * \brief DirihletTreeItem::columnCount
 * \return
 */
int DirihletTreeItem::columnCount() const
{
  return 6;
}

/*!
 * \brief DirihletTreeItem::data
 * \param column
 * \return
 */
QVariant DirihletTreeItem::data(int column) const
{
  auto boundaryCondition = applyedCondition.getBoundaryCondition();

  switch ( column ) {
    case 0:
      return qVariantFromValue<QString>( applyedCondition.getName() );
      break;

    case 1:
      return qVariantFromValue<qreal>( static_cast<DirihletBoundaryCondition*>( boundaryCondition.get() )->getCondition() );
      break;

    case 5:
      return qVariantFromValue<QColor>( applyedCondition.getColor() );
      break;

    default:
      return QVariant();
      break;
  }

}

/*!
 * \brief DirihletTreeItem::setData
 * \param column
 * \param value
 * \return
 */
bool DirihletTreeItem::setData(int column, const QVariant &value)
{
  auto boundaryCondition = applyedCondition.getBoundaryCondition();

  switch ( column ) {
    case 0:
      {
        applyedCondition.setName( value.value<QString>() );
        return true;
        break;
      }

    case 1:
      {
        static_cast<DirihletBoundaryCondition*>( boundaryCondition.get() )->setCondition( value.value<qreal>() );
        return true;
        break;
      }

    case 5:
      {
        applyedCondition.setColor( value.value<QColor>() );
        return true;
        break;
      }

    default:
      return false;
      break;
  }

}

/*!
 * \brief DirihletTreeItem::getBcValue
 * \return
 */
qreal DirihletTreeItem::getBcValue() const
{
  auto boundaryCondition = applyedCondition.getBoundaryCondition();
  return static_cast<DirihletBoundaryCondition*>( boundaryCondition.get() )->getCondition();
}

/*!
 * \brief DirihletTreeItem::setBcValue
 * \param value
 */
void DirihletTreeItem::setBcValue(const qreal &value)
{
  auto boundaryCondition = applyedCondition.getBoundaryCondition();
  static_cast<DirihletBoundaryCondition*>( boundaryCondition.get() )->setCondition( value );
}

