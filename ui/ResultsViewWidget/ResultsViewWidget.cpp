#include "ResultsViewWidget.hpp"
#include "ui_ResultsViewWidget.h"

#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkRenderWindow.h>
#include <cassert>
#include <vtkDataSetMapper.h>
#include <vtkScalarBarActor.h>
#include <vtkPointData.h>
#include <vtkDataArray.h>
#include <vtkLookupTable.h>
#include <vtkProperty.h>
#include <vtkTextProperty.h>

#include <QShortcut>
#include <QDebug>

/*!
 * \brief ResultsViewWidget::ResultsViewWidget
 * \param parent
 */
ResultsViewWidget::ResultsViewWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::ResultsViewWidget),
  currentResult( -1 )
{
  ui->setupUi(this);

  renderer = vtkSmartPointer< vtkRenderer >::New();
  renderer->GradientBackgroundOn();
  renderer->SetBackground( 0.0, 0.0, 0.0 );
  renderer->SetBackground2( 0.9, 0.7, 0.2 );
  ui->qvtkWidget->GetRenderWindow()->AddRenderer( renderer );

  vtkSmartPointer< vtkInteractorStyleTrackballCamera > style = vtkSmartPointer< vtkInteractorStyleTrackballCamera >::New();
  style->SetDefaultRenderer( renderer );

  ui->qvtkWidget->GetInteractor()->SetInteractorStyle( style );

  QShortcut *shortcutNext = new QShortcut( QKeySequence( Qt::Key_Left ), this );
  QObject::connect( shortcutNext, SIGNAL( activated() ), this, SLOT( on_prevToolButton_clicked() ) );

  QShortcut *shortcutPrev = new QShortcut( QKeySequence( Qt::Key_Right ), this );
  QObject::connect( shortcutPrev, SIGNAL( activated() ), this, SLOT( on_nextToolButton_clicked() ) );

  QShortcut *shortcutPlayPause = new QShortcut( QKeySequence( Qt::Key_Space ), this );
  QObject::connect( shortcutPlayPause, SIGNAL( activated() ), this, SLOT( keypressSpaceSlot() ) );

  QShortcut *shortcutStop = new QShortcut( QKeySequence( Qt::Key_Escape ), this );
  QObject::connect( shortcutStop, SIGNAL( activated() ), this, SLOT( on_stopToolButton_clicked() ) );

  timer = new QTimer( this );
  QObject::connect( timer, SIGNAL( timeout() ), this, SLOT( timerTimeout() ) );
}

/*!
 * \brief ResultsViewWidget::~ResultsViewWidget
 */
ResultsViewWidget::~ResultsViewWidget()
{
  delete ui;
}

/*!
 * \brief ResultsViewWidget::setResults
 * \param timeGrids
 */
void ResultsViewWidget::setResults(std::shared_ptr<TimeGrid> timeGrids)
{
  setTimeGrids( *timeGrids.get() );
}

/*!
 * \brief ResultsViewWidget::changeEvent
 * \param e
 */
void ResultsViewWidget::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}

/*!
 * \brief ResultsViewWidget::setTimeGrids
 * \param value
 */
void ResultsViewWidget::setTimeGrids(const TimeGrid &value)
{
  timeActors.clear();
  renderer->RemoveAllViewProps();
  currentResult = -1;

  std::vector< double > timeKeys;
  timeKeys.reserve( value.size() );

  std::for_each( value.begin(), value.end(), [&]( TimeGrid::const_reference v ) {
    timeKeys.push_back( v.first );
  } );
  std::sort( timeKeys.begin(), timeKeys.end() );

  std::for_each( timeKeys.begin(), timeKeys.end(), [&]( const double time ) {
    const auto grid = value.at( time );

    std::string results_name;
    double range[ 2 ];
    vtkSmartPointer< vtkPointData > pd( grid->GetPointData() );
    assert( pd );
    assert( pd->GetNumberOfArrays() );
    assert( pd->GetNumberOfArrays() == 1 );

    results_name = ( pd->GetArrayName( 0 ) ? pd->GetArrayName( 0 ) : "NULL" );
    vtkSmartPointer< vtkDataArray > arr( pd->GetArray( 0 ) );
    assert( arr );
    arr->GetRange( range );

    vtkSmartPointer< vtkDataSetMapper > mapper = vtkSmartPointer< vtkDataSetMapper >::New();
    mapper->SetInput( grid );

    // Scalar bar actor settings
    vtkSmartPointer< vtkScalarBarActor > scalarBarActor = vtkSmartPointer< vtkScalarBarActor >::New();
    scalarBarActor->SetLookupTable( mapper->GetLookupTable() );
    scalarBarActor->SetTitle( (results_name + " (" + std::to_string( time ) + " s)" ).c_str() );
    scalarBarActor->SetNumberOfLabels( 10 );

    vtkSmartPointer< vtkTextProperty > titleProperty = vtkSmartPointer< vtkTextProperty >::New();
    titleProperty->SetFontFamilyToArial();
    titleProperty->BoldOn();
    titleProperty->ItalicOff();
    titleProperty->ShadowOff();
    titleProperty->SetColor( 0.9, 0.9, 0.9 );

    scalarBarActor->SetTitleTextProperty( titleProperty );

    vtkSmartPointer< vtkTextProperty > labelsProperty = vtkSmartPointer< vtkTextProperty >::New();
    labelsProperty->SetFontFamilyToArial();
    labelsProperty->BoldOff();
    labelsProperty->ItalicOff();
    labelsProperty->ShadowOff();
    labelsProperty->SetColor( 0.9, 0.9, 0.9 );

    scalarBarActor->SetLabelFormat( "%6.5f" );
    scalarBarActor->SetLabelTextProperty( labelsProperty );


    // Create a lookup table to share between the mapper and the scalarbar
    vtkSmartPointer< vtkLookupTable > lookupTable = vtkSmartPointer< vtkLookupTable >::New();
    lookupTable->SetTableRange( range );
    lookupTable->SetHueRange( 0.7, 0.0 );
    lookupTable->Build();

    mapper->SetLookupTable( lookupTable );
    mapper->SetScalarRange( range );
    scalarBarActor->SetLookupTable( lookupTable );

    vtkSmartPointer< vtkActor > actor = vtkSmartPointer< vtkActor >::New();
    actor->SetMapper( mapper );
    actor->GetProperty()->EdgeVisibilityOn();

    timeActors.insert( std::make_pair( time, std::make_pair( actor, scalarBarActor ) ) );
  } );

  ui->resultSlider->setValue( 0 );
  ui->resultSlider->setMaximum( timeActors.size() - 1 );
  renderer->ResetCamera();
  on_resultSlider_valueChanged( 0 );
  ui->prevToolButton->setEnabled( false );

  settings.looped = false;
  ui->configToolButton->setEnabled( (timeKeys.size() > 1) );
  if ( !timeKeys.empty() ) {
    settings.dataTimeStep = timeKeys[ 1 ] - timeKeys[ 0 ];
  }
  else {
    settings.dataTimeStep = 0.0;
  }
  settings.timeStep = 0.25;
}

/*!
 * \brief ResultsViewWidget::on_resultSlider_valueChanged
 * \param value
 */
void ResultsViewWidget::on_resultSlider_valueChanged(int value)
{
  if ( timeActors.empty() ) return;
  if ( value == currentResult ) return;

  renderer->RemoveAllViewProps();
  auto it = timeActors.begin();
  std::advance( it, value );
  const auto& timeActor = *it;
  const auto& time = timeActor.first;
  const auto& actor = timeActor.second.first;
  const auto& barActor = timeActor.second.second;
  renderer->AddActor( actor );
  renderer->AddActor( barActor );

  ui->qvtkWidget->update();

  currentResult = value;

  ui->sampleLabel->setText( QString::number( currentResult ) + '/' + QString::number( timeActors.size() - 1 ) );
  ui->timeLabel->setText( QString::number( time ) + " s" );
  if ( currentResult > 0 ) { ui->prevToolButton->setEnabled( true ); }
  if ( (std::size_t)currentResult == (timeActors.size() - 1) ) { ui->nextToolButton->setEnabled( false ); }
  if ( (std::size_t)currentResult < (timeActors.size() - 1) ) { ui->nextToolButton->setEnabled( true ); }
  if ( currentResult == 0 ) { ui->prevToolButton->setEnabled( false ); }
}

/*!
 * \brief ResultsViewWidget::on_prevToolButton_clicked
 */
void ResultsViewWidget::on_prevToolButton_clicked()
{
  if ( ! ui->prevToolButton->isEnabled() ) return;
  ui->resultSlider->setValue( currentResult - 1 );
}

/*!
 * \brief ResultsViewWidget::on_playToolButton_clicked
 */
void ResultsViewWidget::on_playToolButton_clicked()
{
  if ( ! ui->playToolButton->isEnabled() ) return;
  on_nextToolButton_clicked();
  timer->setInterval( settings.timeStep * 1000 );
  timer->start();
  ui->stopToolButton->setEnabled( true );
  ui->pauseToolButton->setEnabled( true );
  ui->playToolButton->setEnabled( false );
}

/*!
 * \brief ResultsViewWidget::on_pauseToolButton_clicked
 */
void ResultsViewWidget::on_pauseToolButton_clicked()
{
  if ( ! ui->pauseToolButton->isEnabled() ) return;
  timer->stop();
  ui->stopToolButton->setEnabled( true );
  ui->pauseToolButton->setEnabled( false );
  ui->playToolButton->setEnabled( true );
}

/*!
 * \brief ResultsViewWidget::on_stopToolButton_clicked
 */
void ResultsViewWidget::on_stopToolButton_clicked()
{
  if ( ! ui->stopToolButton->isEnabled() ) return;
  timer->stop();
  ui->resultSlider->setValue( 0 );
  ui->stopToolButton->setEnabled( false );
  ui->pauseToolButton->setEnabled( false );
  ui->playToolButton->setEnabled( true );
}

/*!
 * \brief ResultsViewWidget::on_nextToolButton_clicked
 */
void ResultsViewWidget::on_nextToolButton_clicked()
{
  if ( ! ui->nextToolButton->isEnabled() ) return;
  ui->resultSlider->setValue( currentResult + 1 );
}

/*!
 * \brief ResultsViewWidget::on_configToolButton_clicked
 */
void ResultsViewWidget::on_configToolButton_clicked()
{
  SettingsDialog dialog;
  dialog.setDataTimeStep( settings.dataTimeStep );
  dialog.setTimeStep( settings.timeStep );
  dialog.setLooped( settings.looped );

  if ( QDialog::Accepted ==  dialog.exec() ) {
    settings.timeStep = dialog.getTimeStep();
    settings.looped = dialog.getLooped();
  }

}

/*!
 * \brief ResultsViewWidget::timerTimeout
 */
void ResultsViewWidget::timerTimeout()
{
  if ( settings.looped ) {
    if ( (std::size_t)currentResult == (timeActors.size() - 1) ) {
      ui->resultSlider->setValue( 0 );
      return;
    }
  }
  else {
    if ( (std::size_t)currentResult == (timeActors.size() - 1) ) {
      on_stopToolButton_clicked();
      return;
    }
  }

  on_nextToolButton_clicked();
}

/*!
 * \brief ResultsViewWidget::keypressSpaceSlot
 */
void ResultsViewWidget::keypressSpaceSlot()
{
  if ( ui->playToolButton->isEnabled() ) {
    on_playToolButton_clicked();
  }
  else {
    on_pauseToolButton_clicked();
  }
}
