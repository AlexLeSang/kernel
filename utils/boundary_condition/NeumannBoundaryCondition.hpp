#ifndef NeumannBoundaryCondition_HPP
#define NeumannBoundaryCondition_HPP

#include <boundary_condition/AbstractBoundaryCondition.hpp>

/*!
 * \brief The NeumannBoundaryCondition class
 */
class NeumannBoundaryCondition : public AbstractBoundaryCondition
{
public:
  NeumannBoundaryCondition();
  bool operator==(const AbstractBoundaryCondition& rhs) const;
  bool operator!=(const AbstractBoundaryCondition& rhs) const;

  virtual bgeot::small_vector< double > appyBoundaryCondition(const bgeot::base_node&) const;

  bgeot::small_vector<double> getCondition() const;
  double getCondition(int pos) const;
  void setCondition(const bgeot::small_vector<double> &value);
  void setCondition(double val, int pos);

private:
  bgeot::small_vector< double > condition;
};

QDataStream &operator<<(QDataStream &ds, const NeumannBoundaryCondition &bc);
QDataStream &operator>>(QDataStream &ds, NeumannBoundaryCondition &bc);

/*
namespace boost {
  namespace serialization {

    template<class Archive>
    void save(Archive & ar, const bgeot::small_vector< double > & vector, const unsigned int version)
    {
      assert( vector.size() == 3 );
      for ( auto i = 0; i < 3; ++ i ) {
        const double val = vector[ i ];
        ar & val;
      }
    }

    template<class Archive>
    void load(Archive & ar, bgeot::small_vector< double > & vector, const unsigned int version)
    {
      bgeot::small_vector< double > v( 0.0, 0.0, 0.0 );
      for ( auto i = 0; i < 3; ++ i ) {
        ar & v[ i ];
      }
      vector = v;
    }

    template <class Archive>
    void serialize(Archive & ar, bgeot::small_vector< double > & vector, const unsigned int version)
    {
      boost::serialization::split_free( ar, vector, version );
    }

  }
}
*/


namespace test {
  bool NeumannBoundaryConditionSerializationTest(int argc, char * argv[]);
}


#endif // NeumannBoundaryCondition_HPP
