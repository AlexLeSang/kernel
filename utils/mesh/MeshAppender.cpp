#include "MeshAppender.hpp"

#include <cassert>

#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXMLUnstructuredGridWriter.h>

#include <vtkActor.h>
#include <vtkDataSetMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkProperty.h>
#include <vtkAppendPolyData.h>
#include <vtkGeometryFilter.h>
#include <vtkAppendFilter.h>

namespace kernel {
  namespace utils {
    namespace mesh {

      vtkSmartPointer<vtkAppendFilter> createAppender(vtkSmartPointer<vtkUnstructuredGrid> resultGrid, vtkSmartPointer< vtkUnstructuredGrid > grid)
      {
        vtkSmartPointer< vtkAppendFilter > polydataAppender = vtkSmartPointer< vtkAppendFilter >::New();
        polydataAppender->AddInput( resultGrid );
        polydataAppender->AddInput( grid );
        polydataAppender->Update();

        return polydataAppender;
      }

      vtkSmartPointer<vtkUnstructuredGrid> AppendMeshes(const std::list<vtkSmartPointer<vtkUnstructuredGrid> >& grids)
      {
        vtkSmartPointer<vtkUnstructuredGrid> resultGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();
        if ( !grids.empty() ) {
          for ( auto it = grids.begin(); it != grids.end(); ++ it ) {
            vtkSmartPointer< vtkUnstructuredGrid > grid = ( *it );

            vtkSmartPointer< vtkAppendFilter > polydataAppender = createAppender(resultGrid, grid);
            resultGrid = polydataAppender->GetOutput();
          }

        }

        return resultGrid;
      }


    }
  }
}

void test::VisualizeVtkUnstructutedGrid(vtkSmartPointer< vtkUnstructuredGrid > grid)
{
  vtkSmartPointer<vtkDataSetMapper> mapper = vtkSmartPointer<vtkDataSetMapper>::New();
  mapper->SetInput( grid );
  mapper->ScalarVisibilityOn();
  mapper->SetScalarModeToUsePointData();

  vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
  actor->SetMapper( mapper );
  actor->GetProperty()->EdgeVisibilityOn();

  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
  vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();

  renderWindow->AddRenderer( renderer );
  renderWindow->SetSize( 1024, 768 );

  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow( renderWindow );

  vtkSmartPointer< vtkInteractorStyleTrackballCamera > style = vtkSmartPointer< vtkInteractorStyleTrackballCamera >::New(); //like paraview
  renderWindowInteractor->SetInteractorStyle( style );

  renderer->AddActor( actor );
  renderer->SetBackground(.0, .0, .0);

  renderWindowInteractor->Start();
}


bool test::AppendMeshesTest(int argc, char *argv[])
{
  using namespace kernel::utils::mesh;

  assert( argc == 4 );
  const std::string input1 = argv[ 1 ];
  const std::string input2 = argv[ 2 ];
  const std::string output = argv[ 3 ];

  std::list<vtkSmartPointer<vtkUnstructuredGrid> > grids;

  auto readAndAddGrid = [&](const std::string& name) {
    vtkSmartPointer< vtkXMLUnstructuredGridReader > reader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
    reader->SetFileName( name.c_str() );
    reader->Update();
    vtkSmartPointer< vtkUnstructuredGrid > grid = reader->GetOutput();
    assert( grid->GetNumberOfPoints() );
    assert( grid->GetNumberOfCells() );

    grids.push_back( grid );
  };

  readAndAddGrid( input1 );
  readAndAddGrid( input2 );

  vtkSmartPointer< vtkUnstructuredGrid > resultGrid = AppendMeshes( grids );
  VisualizeVtkUnstructutedGrid( resultGrid );
  {
    vtkSmartPointer< vtkXMLUnstructuredGridWriter > writer = vtkSmartPointer< vtkXMLUnstructuredGridWriter >::New();
    writer->SetFileName( output.c_str() );
    writer->SetInput( resultGrid );
    writer->Write();
  }

  return true;
}


