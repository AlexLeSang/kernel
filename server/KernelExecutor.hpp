#ifndef TERMETEXECUTOR_HPP
#define TERMETEXECUTOR_HPP

#include <atomic>

#include <QtNetwork/QTcpSocket>

#include <QObject>
#include <QRunnable>

namespace kernel {
  namespace server {

    /*!
     * \brief The KernelExecutor class
     */
    class KernelExecutor : public QObject, public QRunnable
    {
      Q_OBJECT
    public:
      explicit KernelExecutor(int socketDescriptor, QObject *parent = nullptr);
      void run();

    public slots:
      void interrupt();

    private:
      int socketDescriptor_;
      std::atomic<bool> interruptFlag_;
    };
  }
}

#endif // TERMETEXECUTOR_HPP
