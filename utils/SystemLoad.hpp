#ifndef SYSTEMLOAD_HPP
#define SYSTEMLOAD_HPP

#include <QList>
#include <QByteArray>
#include <QFile>
#include <QProcess>

#include "ThreadSleep.hpp"

namespace kernel {
  namespace utils {

    /*!
     * \brief The SystemLoad class
     */
    class SystemLoad
    {
    public:
      static qreal currentCPULoad();

      static quint64 totalRAM();
      static quint64 freeRAM();

    private:
      typedef QList< QByteArray > TimeList;
      static const quint32 SLEEP_INTERVAL = 250; // 250 ms

      static TimeList getTimeList();
    };
  }
}

/* Test

  #include <QDebug>

int main(int argc, char *argv[])
{
  Q_UNUSED(argc);
  Q_UNUSED(argv);


  auto count = 5;
  while ( count -- ) {
    qDebug() << "CPU load: " << SystemLoad::currentCPULoad();
    qDebug() << "Total RAM: " << SystemLoad::totalRAM();
    qDebug() << "Free RAM: " << SystemLoad::freeRAM();
    qDebug();

    ThreadSleep::msleep( 500 );
  }
  return 0;
}
*/

#endif // SYSTEMLOAD_HPP
