#ifndef GMRESILUSLAESOLVER_HPP
#define GMRESILUSLAESOLVER_HPP

#include <slae_solvers/AbstractSLAESolver.hpp>

class GmresILUSLAESolver : public AbstractSLAESolver
{
  // AbstractSLAESolver interface
public:
  virtual bool operator ()(getfem::model &model, gmm::iteration &iter);
};

#endif // GMRESILUSLAESOLVER_HPP
