#include "BoundaryConditionWidget.hpp"
#include "ui_BoundaryConditionWidget.h"

#include <vtkRendererCollection.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkBoundingBox.h>


#include <TreeModel.hpp>
#include <DirihletTreeItem.hpp>
#include <NeumannTreeItem.hpp>
#include <vtkGeometryFilter.h>

#include <CreateBoundaryConditionDialog.hpp>
#include <ColorColumnDelegate.hpp>

#include <QDebug>
#include <QMenu>
#include <QFile>
#include <QtGui>

#include <DirihletTreeItem.hpp>
#include <NeumannTreeItem.hpp>

/*!
 * \brief BoundaryConditionWidget::BoundaryConditionWidget
 * \param parent
 */
BoundaryConditionWidget::BoundaryConditionWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::BoundaryConditionWidget)
{
  ui->setupUi( this );

  treeModel = new TreeModel( this );
  ui->treeView->setModel( treeModel );
  ui->treeView->setSelectionBehavior( QTreeView::SelectRows );

  gridActor = vtkSmartPointer< vtkActor >::New();

  renderer = vtkSmartPointer< vtkRenderer >::New();
  ui->qvtkWidget->GetRenderWindow()->AddRenderer( renderer );

  renderer->GradientBackgroundOn();
  renderer->SetBackground( 0.0, 0.0, 0.0 );
  renderer->SetBackground2( 0.0, 0.6, 0.7 );

  vtkSmartPointer< vtkAreaPicker > areaPicker = vtkSmartPointer< vtkAreaPicker >::New();
  ui->qvtkWidget->GetInteractor()->SetPicker( areaPicker );

  style = vtkSmartPointer< VisibleSelectionInteractorStyle >::New();
  style->SetCurrentRenderer( renderer );

  ui->qvtkWidget->GetInteractor()->SetInteractorStyle( style );

  axesActor = vtkSmartPointer< vtkAxesActor >::New();
  axesActor->AxisLabelsOff();

  axesActor->SetConeRadius( 0.1 );

  //  double lenghts[3] = { 1.0, 1.0, 1.0 };
  //  axesActor->SetTotalLength(lenghts);
  //  renderer->AddActor( axesActor );

  auto selectionModel = ui->treeView->selectionModel();
  QObject::connect( selectionModel, SIGNAL( currentChanged(QModelIndex,QModelIndex) ), this, SLOT( selectionChanged(QModelIndex,QModelIndex) ) );

  delegate = new ColorColumnDelegate( this );
  ui->treeView->setItemDelegateForColumn( 5, delegate );

  ui->treeView->setContextMenuPolicy(Qt::CustomContextMenu);

  QObject::connect( delegate, SIGNAL(changeAppearance()), this, SLOT(updateAppearance()) );
  QObject::connect( ui->visibleSelectedcheckBox, SIGNAL(clicked()), this, SLOT( updateAppearance()) );
  QObject::connect( style.GetPointer(), SIGNAL(switchState()), this, SLOT(switchState()) );
  QObject::connect( ui->treeView, SIGNAL( customContextMenuRequested(const QPoint &) ), this, SLOT( onCustomContextMenu(const QPoint&) ) );

  QShortcut* deleteShortcut = new QShortcut( Qt::Key_Delete, this );
  QObject::connect( deleteShortcut, SIGNAL( activated() ), this, SLOT( removeCurrentItem() ) );
}

/*!
 * \brief BoundaryConditionWidget::~BoundaryConditionWidget
 */
BoundaryConditionWidget::~BoundaryConditionWidget()
{
  delete ui;
}

/*!
 * \brief BoundaryConditionWidget::changeEvent
 * \param e
 */
void BoundaryConditionWidget::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}

/*!
 * \brief BoundaryConditionWidget::getSurfacePolyData
 * \param value
 * \return
 */
vtkSmartPointer<vtkPolyData> BoundaryConditionWidget::getSurfacePolyData(vtkSmartPointer<vtkUnstructuredGrid>& value)
{
  vtkSmartPointer< vtkDataSetSurfaceFilter > filter = vtkSmartPointer< vtkDataSetSurfaceFilter >::New();
  filter->SetInput( value );
  filter->Update();
  vtkSmartPointer< vtkPolyData > polyData = filter->GetOutput();
  return polyData;
}

/*!
 * \brief BoundaryConditionWidget::calculateGlyphCoefficient
 * \param value
 */
void BoundaryConditionWidget::calculateGlyphCoefficient(vtkSmartPointer<vtkUnstructuredGrid>& value)
{
  constexpr double multiplier = 0.07;
  vtkBoundingBox box( value->GetBounds() );
  double lengths[ 3 ];
  box.GetLengths( lengths );
  const double minLength = std::min( std::min( box.GetLength( 0 ), box.GetLength( 1 ) ), box.GetLength( 2 ) );

  this->setGlyphCoefficient( minLength * multiplier );
}

/*!
 * \brief BoundaryConditionWidget::setGrid
 * \param value
 */
void BoundaryConditionWidget::setGrid(vtkSmartPointer<vtkUnstructuredGrid> grid)
{
  calculateGlyphCoefficient( grid );

  vtkSmartPointer< vtkPolyData > polyData = getSurfacePolyData( grid );

  style->SetPolyData( polyData );

  vtkSmartPointer< vtkPolyDataMapper > mapper = vtkSmartPointer< vtkPolyDataMapper >::New();
  mapper->SetInput( polyData );

  gridActor->SetMapper( mapper );
  gridActor->GetProperty()->EdgeVisibilityOn();

  //renderer->RemoveActor( gridActor );
  renderer->RemoveAllViewProps();
  renderer->AddActor( gridActor );
  ui->qvtkWidget->update();
  ui->pushButton->setEnabled( true );
  ui->acceptPushButton->setEnabled( true );
  ui->qvtkWidget->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->ResetCamera();
}

/*!
 * \brief BoundaryConditionWidget::setGlyphCoefficient
 * \param coefficient
 */
void BoundaryConditionWidget::setGlyphCoefficient(double coefficient)
{
  qDebug() << "coefficient: " << coefficient;
  style->SetGlyphScaleCoefficient(coefficient);
}

/*!
 * \brief BoundaryConditionWidget::addTestConditions
 * \param numberOfGenerators
 */
void BoundaryConditionWidget::addTestConditions(const std::size_t numberOfGenerators)
{
  for ( std::size_t insertCounter = 0; insertCounter < numberOfGenerators; ++ insertCounter ) {
    if ( insertCounter % 3 == 0 ) {
      DirihletTreeItem* dirihlet = new DirihletTreeItem( "Dirihlet " + QString::number( insertCounter ), insertCounter * M_PI, Qt::red );
      treeModel->appendItem( dirihlet );
    }
    else
      if ( insertCounter % 3 == 1 ) {
        NeumannTreeItem* neumann = new NeumannTreeItem( "Neumann " + QString::number( insertCounter ), insertCounter * M_PI, insertCounter * M_PI_2, insertCounter * M_PI_4, Qt::green );
        treeModel->appendItem( neumann );
      }
      else {//( insertCounter % 3 == 1 )
        NeumannTreeItem* neumann = new NeumannTreeItem( "Neumann " + QString::number( insertCounter ), insertCounter * 3 * M_PI, insertCounter * 3 * M_PI_2, insertCounter * 3 * M_PI_4, Qt::blue );
        treeModel->appendItem( neumann );
      }
  }
}

/*!
 * \brief BoundaryConditionWidget::fromMesherOrGeometry
 * \param grid
 */
void BoundaryConditionWidget::fromMesherOrGeometry(vtkSmartPointer<vtkUnstructuredGrid> grid)
{
  setGrid( grid );
}

/*!
 * \brief BoundaryConditionWidget::on_pushButton_clicked
 */
void BoundaryConditionWidget::on_pushButton_clicked()
{
  CreateBoundaryConditionDialog dialog;
  if ( dialog.exec() == QDialog::Accepted ) {
    AbstractTreeItem* treeItem = dialog.treeItem();
    treeModel->appendItem( treeItem );
    style->SetCurrentTreeItem( treeItem );
    ui->pushButton_2->setEnabled( true );
  }
  selectionChanged( QModelIndex(), QModelIndex() );
}

/*!
 * \brief BoundaryConditionWidget::removeCurrentItem
 */
void BoundaryConditionWidget::removeCurrentItem()
{
  QModelIndexList list = ui->treeView->selectionModel()->selectedIndexes();
  if ( !list.empty() ) {
    treeModel->removeItem( list.begin()->row() );
    if ( list.empty() ) {
      ui->pushButton_2->setEnabled( false );
    }
  }
  selectionChanged( QModelIndex(), QModelIndex() );
}

/*!
 * \brief BoundaryConditionWidget::on_pushButton_2_clicked
 */
void BoundaryConditionWidget::on_pushButton_2_clicked()
{
  removeCurrentItem();
}

/*!
 * \brief BoundaryConditionWidget::selectionChanged
 */
void BoundaryConditionWidget::selectionChanged(QModelIndex/*newSelection*/, QModelIndex/*oldSelection*/)
{
  const QModelIndex currentIndex = ui->treeView->currentIndex();
  AbstractTreeItem* currentTreeItem = treeModel->getTreeItem( currentIndex.row() );
  style->SetCurrentTreeItem( currentTreeItem );

  if ( ui->visibleSelectedcheckBox->isChecked() ) {
    QList< AbstractTreeItem* > allItems = treeModel->getAllTreeItem();
    allItems.removeOne( currentTreeItem );
    style->SetPrevSelected( allItems );
  }
  else {
    style->SetPrevSelected( QList< AbstractTreeItem* >() );
  }

}

/*!
 * \brief BoundaryConditionWidget::updateAppearance
 */
void BoundaryConditionWidget::updateAppearance()
{
  selectionChanged( QModelIndex(), QModelIndex() );
}

/*!
 * \brief BoundaryConditionWidget::switchState
 */
void BoundaryConditionWidget::switchState()
{
  if ( ui->stateLabel->text() == "Navigation" ) {
    ui->stateLabel->setText( "Selection" );
  }
  else {
    ui->stateLabel->setText( "Navigation" );
  }
}

/*!
 * \brief BoundaryConditionWidget::displayOnItemContextMenu
 * \param point
 * \param index
 */
void BoundaryConditionWidget::displayOnItemContextMenu(const QPoint& point, QModelIndex index)
{
  const auto row = index.row();
  QPoint globalPos = ui->treeView->mapToGlobal( point );
  AbstractTreeItem* treeItem = treeModel->getTreeItem( row );
  QMenu* onBCMenu = new QMenu( this );
  QAction* saveBC = onBCMenu->addAction( QString( "Save BC " ) + treeItem->getName() );
  QObject::connect( saveBC, SIGNAL( triggered() ), this, SLOT( saveBCAction() ) );
  QAction* removeBC = onBCMenu->addAction( QString( "Remove BC " ) + treeItem->getName() );
  QObject::connect( removeBC, SIGNAL( triggered() ), this, SLOT( removeCurrentItem() ) );
  onBCMenu->exec( globalPos );
}

/*!
 * \brief BoundaryConditionWidget::displayOnEmptyPlace
 * \param point
 */
void BoundaryConditionWidget::displayOnEmptyPlace(const QPoint& point)
{
  QPoint globalPos = ui->treeView->mapToGlobal( point );
  QMenu* emptySpaceMenu = new QMenu( this );
  QAction* loadBC = emptySpaceMenu->addAction( "Load BC" );
  QObject::connect( loadBC, SIGNAL(triggered()), this, SLOT( loadBCAction()) );
  emptySpaceMenu->exec( globalPos );
}

/*!
 * \brief BoundaryConditionWidget::onCustomContextMenu
 * \param point
 */
void BoundaryConditionWidget::onCustomContextMenu(const QPoint & point)
{
  if ( gridActor->GetMapper() == nullptr ) return;

  QModelIndex index = ui->treeView->indexAt( point );
  if ( index.isValid() ) {
    displayOnItemContextMenu(point, index);
  }
  else {
    displayOnEmptyPlace(point);
  }
}

/*!
 * \brief BoundaryConditionWidget::loadBCAction
 */
void BoundaryConditionWidget::loadBCAction()
{
  const QString openFileName = QFileDialog::getOpenFileName( this, QObject::tr( "Save Boundary Condition" ), "./",
                                                             QObject::tr("Boundary conditions (") + boundaryConditionExtension + ")" );

  if ( openFileName.isEmpty() ) return;

  AppliedBoundaryCondition applyedBoundaryCondition = AppliedBoundaryCondition::readFromFile( openFileName );

  AbstractTreeItem* treeItem = nullptr;
  switch ( applyedBoundaryCondition.getConditionType() ) {
    case BOUNDARY_CONDITION_TYPE::DIRICHLET_BOUNDARY_CONDITION:
      {
        treeItem = new DirihletTreeItem;
        break;
      }
    case BOUNDARY_CONDITION_TYPE::NEUMANN_BOUNDARY_CONDITION:
      {
        treeItem = new NeumannTreeItem;
        break;
      }
    case BOUNDARY_CONDITION_TYPE::NON_CONDITION:break;
    default:break;
  }

  assert( treeItem != nullptr );
  treeItem->setApplyedCondition( applyedBoundaryCondition );
  treeModel->appendItem( treeItem );
  style->SetCurrentTreeItem( treeItem );
  selectionChanged( QModelIndex(), QModelIndex() );
}

/*!
 * \brief BoundaryConditionWidget::saveBCAction
 */
void BoundaryConditionWidget::saveBCAction()
{
  const auto currentRow = ui->treeView->currentIndex().row();
  AbstractTreeItem* treeItem = treeModel->getTreeItem( currentRow );
  assert( treeItem != nullptr );

  const QString newFileName = QFileDialog::getSaveFileName( this, QObject::tr( "Save Boundary Condition" ),
                                                            QString("./") + treeItem->getName() + boundaryConditionExtension,
                                                            QObject::tr("Boundary conditions (") + boundaryConditionExtension + ")" );

  AppliedBoundaryCondition::writeToFile( treeItem->getApplyedCondition(), newFileName );
}

/*!
 * \brief BoundaryConditionWidget::on_acceptPushButton_clicked
 */
void BoundaryConditionWidget::on_acceptPushButton_clicked()
{
  auto applyedBoundaryConditions = std::make_shared< std::list< AppliedBoundaryCondition > >( std::list< AppliedBoundaryCondition >() );
  auto itemList = treeModel->getAllTreeItem();
  std::for_each( itemList.begin(), itemList.end(), [&](AbstractTreeItem* item) {
    AppliedBoundaryCondition& condition = item->getApplyedCondition();
    applyedBoundaryConditions->push_back( condition );
  } );

  emit sendBoundaryConditionsToSolver( applyedBoundaryConditions );
}


bool BoundaryConditionWidgetTest(int argc, char *argv[])
{
  assert( argc == 2 );

  vtkSmartPointer< vtkXMLUnstructuredGridReader > reader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
  reader->SetFileName( argv[ 1 ] );
  reader->Update();
  vtkSmartPointer< vtkUnstructuredGrid > grid = reader->GetOutput();

  QApplication a( argc, argv );

  BoundaryConditionWidget widget;
  widget.setGrid( grid );
  widget.addTestConditions( 3 );
  widget.show();
  a.exec();


  return true;
}

