#ifndef WORKGROUPGRAPHVISUALIZER_HPP
#define WORKGROUPGRAPHVISUALIZER_HPP

#include <vtkRandomGraphSource.h>
#include <vtkDataObjectToTable.h>
#include <vtkQtTableView.h>
#include <vtkRenderWindow.h>
#include <vtkGraphLayoutView.h>
#include <vtkDataRepresentation.h>
#include <vtkViewUpdater.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkUndirectedGraph.h>
#include <vtkMutableUndirectedGraph.h>
#include <vtkMutableDirectedGraph.h>

#include <vtkUnsignedCharArray.h>
#include <vtkIntArray.h>
#include <vtkFloatArray.h>
#include <vtkDoubleArray.h>
#include <vtkStringArray.h>

#include <vtkDataSetAttributes.h>
#include <vtkGraphLayout.h>

#include <vtkSimple2DLayoutStrategy.h>
#include <vtkForceDirectedLayoutStrategy.h>
#include <vtkAttributeClustering2DLayoutStrategy.h>
#include <vtkClustering2DLayoutStrategy.h>
#include <vtkCircularLayoutStrategy.h>

#include <vtkGraphToPolyData.h>
#include <vtkGlyphSource2D.h>
#include <vtkGlyph3D.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkViewTheme.h>
#include <vtkLookupTable.h>
#include <vtkGraphToGlyphs.h>
#include <vtkRenderedGraphRepresentation.h>
#include <vtkColorTransferFunction.h>

#include <vtkObjectFactory.h>
#include <vtkCallbackCommand.h>

#include <vtkInteractorStyleSwitch.h>
#include <vtkInteractorStyleRubberBand3D.h>
#include <vtkInteractorStyleRubberBand2D.h>
#include <vtkInteractorStyleRubberBandPick.h>
#include <vtkInteractorStyleTrackballCamera.h>

#include <vtkCellPicker.h>
#include <vtkProperty.h>

#include <../server/command/GetServerStatusCommand.hpp>
#include <../utils/RandomUtils.hpp>

#include <QVTKWidget.h>

#include <thread>
#include <future>
#include <chrono>

#include <QObject>
#include <QRunnable>

#include <QThreadPool>

/*!
 * \brief The GraphProducerBase class
 */
class GraphProducerBase
{
public:
  virtual ~GraphProducerBase() {}
  virtual bool done() = 0;
  virtual QList< kernel::server::command::ServerStatus::WorkgroupGraphEdge > produce() = 0;
};


class CustomSphereRepresentation;

/*!
 * \brief The WorkgroupGraphVisualizer class
 */
class WorkgroupGraphVisualizer : public QObject, public QRunnable
{
  Q_OBJECT
public:
  WorkgroupGraphVisualizer(std::shared_ptr< GraphProducerBase > proucer, const QString windowName = "", const quint32 updateInterval = 45 * 1000, QObject* parent = nullptr);
  virtual ~WorkgroupGraphVisualizer();

  void update();

  // QRunnable interface
  virtual void run();

private:
  void vtkUpdatePropertes(const QString& vertexLabel, const vtkIdType& vtkId, bool from, vtkStringArray *vertexLabels, vtkDoubleArray *scaledVerticies, vtkFloatArray *vertexColors);
  vtkSmartPointer< vtkMutableDirectedGraph > transformToVtkGraph(const QList< kernel::server::command::ServerStatus::WorkgroupGraphEdge >& graph);
  void displayGraphOnLayout(vtkMutableDirectedGraph* vtkGraph);
  void visualizeVtkGraph(vtkSmartPointer< vtkMutableDirectedGraph > vtkGraph);

private:
  vtkSmartPointer< vtkGraphLayoutView > graphLayoutView;
  std::shared_ptr< GraphProducerBase > graphProducer;
  int timerId;
  quint32 updateInterval;
};


class CustomSphereRepresentation : public vtkRenderedGraphRepresentation
{
public:
  static CustomSphereRepresentation* New();
  vtkTypeMacro(CustomSphereRepresentation, vtkRenderedGraphRepresentation);

  void SetVertexSize(int vertexSize);
};


namespace test {

  QList< kernel::server::command::ServerStatus::WorkgroupGraphEdge >
  create_graph(const quint32 vertexNumber = 5, const quint32 lowPing = 1, const quint32 highPing = 200);

  class RandomGraphProducer : public GraphProducerBase
  {
  public:
    RandomGraphProducer() : vertexNumber( 7 ), lowPing( 1 ), highPing( 200 ), graphCounter( 10 ) {}
    RandomGraphProducer(const quint32 vN, const quint32 lP, const quint32 hP) : vertexNumber( vN ), lowPing( lP ), highPing( hP ) {}

    // GraphProducerBase interface
    virtual bool done();
    virtual QList<kernel::server::command::ServerStatus::WorkgroupGraphEdge> produce();

    quint32 getVertexNumber() const;
    void setVertexNumber(const quint32 &value);

    quint32 getLowPing() const;
    void setLowPing(const quint32 &value);

    quint32 getHighPing() const;
    void setHighPing(const quint32 &value);

    quint32 getGraphCounter() const;
    void setGraphCounter(const quint32 &value);

  private:
    quint32 vertexNumber;
    quint32 lowPing;
    quint32 highPing;
    quint32 graphCounter;
  };

  bool VisualizeWorkgroupGraphTest();
}

#endif // WORKGROUPGRAPHVISUALIZER_HPP
