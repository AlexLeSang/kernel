#ifndef GETSERVERSTATUSCOMMAND_HPP
#define GETSERVERSTATUSCOMMAND_HPP

#include "Command.hpp"
#include "../server/KernelServer.hpp"

#include <QMutex>
#include <QWaitCondition>
#include <QMutexLocker>

#include <../client/ServerDescription.hpp>

#include <tuple>

namespace kernel {
  namespace server {
    namespace command {

      bool GetServerStatusCommand(QTcpSocket &socket, std::atomic<bool> &/*interruptFlag*/);

      class ServerStatus
      {
      public:

        struct WorkgroupGraphEdge
        {
          WorkgroupGraphEdge() {}
          WorkgroupGraphEdge(const QString& from, const QString& to, const quint32& ping) : edgeFrom( from ), edgeTo( to ), avgPing( ping ) {}

          QString edgeFrom;
          QString edgeTo;
          quint32 avgPing;
        };

        /*!
         * \brief ServerStatus
         */
        ServerStatus() {}

        /*!
         * \brief ServerStatus
         * \param mongodbAddress
         * \param mongoPort
         * \param workgroupTag
         */
        ServerStatus(const QHostAddress& mongodbAddress, const quint32 mongoPort, const QString& workgroupTag ) : mongodbAddress( mongodbAddress, mongoPort ), workgroupTag( workgroupTag ) {}

        /*!
         * \brief getMongodbAddress
         * \return
         */
        client::ServerDescription getMongodbAddress() const
        {
          return mongodbAddress;
        }

        /*!
         * \brief setMongodbAddress
         * \param value
         */
        void setMongodbAddress(const client::ServerDescription &value)
        {
          mongodbAddress = value;
        }

        /*!
         * \brief getWorkgroupTag
         * \return
         */
        QString getWorkgroupTag() const
        {
          return workgroupTag;
        }

        /*!
         * \brief setWorkgroupTag
         * \param value
         */
        void setWorkgroupTag(const QString &value)
        {
          workgroupTag = value;
        }

        /*!
         * \brief getWorkgroupGraph
         * \return
         */
        QList<WorkgroupGraphEdge> getWorkgroupGraph() const
        {
          return workgroupGraph;
        }

        /*!
         * \brief setWorkgroupGraph
         * \param value
         */
        void setWorkgroupGraph(const QList<WorkgroupGraphEdge> &value)
        {
          workgroupGraph = value;
        }

      private:
        client::ServerDescription mongodbAddress;

        QString workgroupTag;

        QList< WorkgroupGraphEdge > workgroupGraph;

        // Service components
        // bool mongodbAddressSet;
        // bool workgroupTagSet;
        // bool workgroupSet;

        // mutable QMutex mutex;

        // mutable QWaitCondition mongodbAddressCond;
        // mutable QWaitCondition workgroupSetCond;
        // mutable QWaitCondition workgroupCond;

      };


      /*!
       * \brief operator <<
       * \param stream
       * \param edge
       * \return
       */
      inline QDataStream & operator<< (QDataStream& stream, const ServerStatus::WorkgroupGraphEdge& edge)
      {
        stream << edge.edgeFrom;
        stream << edge.edgeTo;
        stream << edge.avgPing;
        return stream;
      }

      /*!
       * \brief operator <<
       * \param d
       * \param edge
       * \return
       */
      inline QDebug  operator<< (QDebug d, const ServerStatus::WorkgroupGraphEdge& edge)
      {
        d << "[" << edge.edgeFrom << " -> ";
        d << edge.edgeTo << " : ";
        d << edge.avgPing << "]\n";
        return d;
      }

      /*!
       * \brief operator >>
       * \param stream
       * \param edge
       * \return
       */
      inline QDataStream & operator>> (QDataStream& stream, ServerStatus::WorkgroupGraphEdge& edge)
      {
        stream >> edge.edgeFrom;
        stream >> edge.edgeTo;
        stream >> edge.avgPing;
        return stream;
      }

      /*!
       * \brief operator <<
       * \param stream
       * \param serverStatus
       * \return
       */
      inline QDataStream & operator<< (QDataStream& stream, const ServerStatus& serverStatus)
      {
        stream << serverStatus.getMongodbAddress();
        stream << serverStatus.getWorkgroupTag();
        stream << serverStatus.getWorkgroupGraph();
        return stream;
      }

      /*!
       * \brief operator <<
       * \param d
       * \param serverStatus
       * \return
       */
      inline QDebug operator<< (QDebug d, const ServerStatus& serverStatus)
      {
        d << "mongoAddress: " << serverStatus.getMongodbAddress();
        d << "workgroupTag: " << serverStatus.getWorkgroupTag();
        d << "workgoupGraph: " << serverStatus.getWorkgroupGraph();
        return d;
      }

      /*!
       * \brief operator >>
       * \param stream
       * \param serverStatus
       * \return
       */
      inline QDataStream & operator>> (QDataStream& stream, ServerStatus& serverStatus)
      {
        client::ServerDescription mongoDBAddress;
        stream >> mongoDBAddress;
        serverStatus.setMongodbAddress( mongoDBAddress );

        QString workgropTag;
        stream >> workgropTag;
        serverStatus.setWorkgroupTag( workgropTag );

        QList< ServerStatus::WorkgroupGraphEdge >  workgroupGraph;
        stream >> workgroupGraph;
        serverStatus.setWorkgroupGraph( workgroupGraph );

        return stream;
      }
    }
  }
}

#endif // GETSERVERSTATUSCOMMAND_HPP
