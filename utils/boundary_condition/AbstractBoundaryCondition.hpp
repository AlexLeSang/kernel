#ifndef AbstractBoundaryCondition_HPP
#define AbstractBoundaryCondition_HPP

#include <getfem/getfem_mesh.h>
#include <gmm/gmm.h>
#include <gmm/gmm_ref.h>

#include <tuple>
#include <string>
#include <unordered_set>

#include <boost/functional/hash.hpp>

#include <cmath>

#include <QColor>

/*!
 * \brief The BOUNDARY_CONDITION_TYPE enum
 */
enum struct BOUNDARY_CONDITION_TYPE : std::size_t
{
  NON_CONDITION = 0,
  DIRICHLET_BOUNDARY_CONDITION = 1,
  NEUMANN_BOUNDARY_CONDITION = 2
};

/*!
 * \brief The BoundaryConditionTypeHash struct
 */
struct BoundaryConditionTypeHash
{
  std::size_t operator()(const BOUNDARY_CONDITION_TYPE& conditionType) const
  {
    std::hash< std::size_t > hash_fn;
    return hash_fn( static_cast< std::size_t >( conditionType ) );
  }
};

/*!
 * \brief The AbstractBoundaryCondition class
 */
class AbstractBoundaryCondition
{
public:
  AbstractBoundaryCondition(BOUNDARY_CONDITION_TYPE conditionType) : conditionType( conditionType ) {}
  virtual ~AbstractBoundaryCondition() {}

  virtual bool operator==(const AbstractBoundaryCondition& rhs) const;
  virtual bool operator!=(const AbstractBoundaryCondition& rhs) const;

  // Interface
  virtual BOUNDARY_CONDITION_TYPE getBoundaryConditionType() const;
  bool applicableToFace(const std::vector< bgeot::base_node >& facePoints) const;

  std::string getName() const;
  void setName(const std::string &value);

protected:
  BOUNDARY_CONDITION_TYPE conditionType;
  std::string name;
};


namespace test {
  bool ApplicableNodesContainerTest();
  bool AbstractBoundaryConditionComparisonTest();
}

#endif // AbstractBoundaryCondition_HPP
