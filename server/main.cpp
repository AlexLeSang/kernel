#include <QCoreApplication>
#include <QStringList>
#include <QDir>
#include <QHostInfo>

#include "ServerCommands.hpp"

#include "KernelServer.hpp"
#include "../utils/ThreadSaveLogger.hpp"

#include "SignalHandler.hpp"

#include <cassert>


class Application : public SignalHandler
{
public:
  Application() : SignalHandler(SignalHandler::SIG_INT), server_ptr( nullptr ) {}

  void printUsage(std::ostream& stream) const
  {
    stream << "Usage: --address <address(hostname)> --port <port> --mongodbaddress <mongodbaddress(hostname)> --mongodbport <mongodbport> --workgrouptag <workgrouptag> --logpath <logpath> --logfilename <logfilenane>" << std::endl;
  }

  int main(int argc, char *argv[])
  {
    QCoreApplication a( argc, argv );
    QStringList args = a.arguments();
    auto printProvidedArgs = [&](std::ostream& stream, const QStringList& strList) {
      stream << "Provided arguments: ";
      for ( auto it = strList.begin(); it != strList.end(); ++ it ) {
        stream << (*it).toStdString() << " ";
      }
      stream << std::endl;
    };

    for ( auto it = args.begin() + 1; it != args.end(); ++ it ) {

      if ( *it == "--address" ) {
        ++ it;
        if ( it == args.end() ) {
          std::cerr << "Error: Address was not specified" << std::endl;
          printProvidedArgs( std::cerr, args );
          printUsage( std::cerr );
          return EXIT_FAILURE;
        }

        const QString address = *it;

        QHostInfo addressHostInfo = QHostInfo::fromName( address );
        if ( addressHostInfo.error() != QHostInfo::NoError ) {
          if ( addressHostInfo.error() == QHostInfo::HostNotFound ) {
            std::cerr << "Error: Host not found" << std::endl;
            printProvidedArgs( std::cerr, args );
            printUsage( std::cerr );
            return EXIT_FAILURE;
          }
          if ( addressHostInfo.error() == QHostInfo::UnknownError ) {
            std::cerr << "Error: Unknown host address error" << std::endl;
            printProvidedArgs( std::cerr, args );
            printUsage( std::cerr );
            return EXIT_FAILURE;
          }
          return EXIT_FAILURE;
        }

        kernel::server::KernelServer::setServerAddress( addressHostInfo.addresses().first() );
        continue;
      }

      if ( *it == "--port" ) {
        ++ it;
        if ( it == args.end() ) {
          std::cerr << "Error: Port number was not specified" << std::endl;
          printProvidedArgs( std::cerr, args );
          printUsage( std::cerr );
          return EXIT_FAILURE;
        }

        const QString portnumberStr = *it;
        bool ok = false;
        const auto portnumber = portnumberStr.toInt( &ok );
        if ( !ok ) {
          std::cerr << "Error: Port number shoudl be integer" << std::endl;
          printProvidedArgs( std::cerr, args );
          printUsage( std::cerr );
          return EXIT_FAILURE;
        }
        kernel::server::KernelServer::setPortNumber( portnumber );
        continue;
      }

      if ( *it == "--mongodbaddress" ) {
        ++ it;
        if ( it == args.end() ) {
          std::cerr << "Error: MongoDB address was not specified" << std::endl;
          printProvidedArgs( std::cerr, args );
          printUsage( std::cerr );
          return EXIT_FAILURE;
        }

        const QString mongodbAddress = *it;

        QHostInfo mongodbAddressHostInfo = QHostInfo::fromName( mongodbAddress );
        if ( mongodbAddressHostInfo.error() != QHostInfo::NoError ) {
          if ( mongodbAddressHostInfo.error() == QHostInfo::HostNotFound ) {
            std::cerr << "Error: Host not found" << std::endl;
            printProvidedArgs( std::cerr, args );
            printUsage( std::cerr );
            return EXIT_FAILURE;
          }
          if ( mongodbAddressHostInfo.error() == QHostInfo::UnknownError ) {
            std::cerr << "Error: Unknown host address error" << std::endl;
            printProvidedArgs( std::cerr, args );
            printUsage( std::cerr );
            return EXIT_FAILURE;
          }
          return EXIT_FAILURE;
        }

        kernel::server::KernelServer::setMongodbServerAddress( mongodbAddressHostInfo.addresses().first() );
        continue;
      }

      if ( *it == "--mongodbport" ) {
        ++ it;
        if ( it == args.end() ) {
          std::cerr << "Error: Port number was not specified" << std::endl;
          printProvidedArgs( std::cerr, args );
          printUsage( std::cerr );
          return EXIT_FAILURE;
        }

        const QString portnumberStr = *it;
        bool ok = false;
        const auto portnumber = portnumberStr.toInt( &ok );
        if ( !ok ) {
          std::cerr << "Error: Port number shoudl be integer" << std::endl;
          printProvidedArgs( std::cerr, args );
          printUsage( std::cerr );
          return EXIT_FAILURE;
        }
        kernel::server::KernelServer::setMongodbPortNumber( portnumber );
        continue;
      }

      if ( *it == "--workgrouptag" ) {
        ++ it;
        if ( it == args.end() ) {
          std::cerr << "Error: MongoDB workgroup tag was not specified" << std::endl;
          printProvidedArgs( std::cerr, args );
          printUsage( std::cerr );
          return EXIT_FAILURE;
        }
        const QString workGroupTag = *it;
        kernel::server::KernelServer::setWorkgroupTag( workGroupTag );
        continue;
      }

      if ( *it == "--logpath" ) {
        ++it;
        if ( it == args.end() ) {
          std::cerr << "Error: Log path was not specified" << std::endl;
          printProvidedArgs( std::cerr, args );
          printUsage( std::cerr );
          return EXIT_FAILURE;
        }
        const QString logpath = *it;
        if ( !QDir( logpath ).exists() ) {
          std::cerr << "Error: Log path directory does not exists" << std::endl;
          printProvidedArgs( std::cerr, args );
          printUsage( std::cerr );
          return EXIT_FAILURE;
        }
        kernel::utils::ThreadSaveLogger::setLogPath( logpath );
        continue;
      }

      if ( *it == "--logfilename" ) {
        ++it;
        if ( it == args.end() ) {
          std::cerr << "Error: Log file name was not specified" << std::endl;
          printProvidedArgs( std::cerr, args );
          printUsage( std::cerr );
          return EXIT_FAILURE;
        }
        const QString logfilename = *it;
        kernel::utils::ThreadSaveLogger::setLogFilename( logfilename );
        continue;
      }

      if ( (*it == "--help") || (*it == "-h") ) {
        printProvidedArgs( std::cerr, args );
        printUsage( std::cout );
        return EXIT_SUCCESS;
      }
      else {
        std::cerr << "Unknown opion: " << (*it).toStdString() << std::endl;
        printProvidedArgs( std::cerr, args );
        printUsage( std::cout );
        return EXIT_FAILURE;
      }

    }

    server_ptr = kernel::server::KernelServer::getInstance();
    if ( ! (*server_ptr).isListening() ) {
      qWarning() << "Server error: " << (*server_ptr).serverError();
      return EXIT_FAILURE;
    }

    return a.exec();
  }

  ~Application() {}

  bool handleSignal(int signal)
  {
    // std::cout << "Handling signal " << signal << std::endl;
    if ( server_ptr != nullptr ) {
      (*server_ptr).stopServer();
    }
    return true;
  }
private:
  kernel::server::KernelServer* server_ptr;
};

int main(int argc, char* argv[])
{
  Application app;
  return app.main(argc, argv);
}
