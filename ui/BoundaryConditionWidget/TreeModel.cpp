#include "TreeModel.hpp"

#include <RootTreeItem.hpp>

#include <QStringList>
#include <QDebug>
#include <QPixmap>

#include <DirihletTreeItem.hpp>
#include <NeumannTreeItem.hpp>

/*!
 * \brief TreeModel::TreeModel
 * \param parent
 */
TreeModel::TreeModel(QObject *parent)
  : QAbstractItemModel(parent)
{
  QStringList headers;
  headers.push_back( "Name" );
  headers.push_back( "Scalar value" );
  headers.push_back( "X value" );
  headers.push_back( "Y value" );
  headers.push_back( "Z value" );
  headers.push_back( "Color" );
  rootItem = new RootTreeItem( headers );
  rootItem->setParent( rootItem );
}

/*!
 * \brief TreeModel::~TreeModel
 */
TreeModel::~TreeModel()
{
  delete rootItem;
}

/*!
 * \brief TreeModel::flags
 * \param index
 * \return
 */
Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const
{
  if (!index.isValid())
    return Qt::ItemIsEnabled;

  return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

/*!
 * \brief TreeModel::columnCount
 * \param parent
 * \return
 */
int TreeModel::columnCount(const QModelIndex &parent) const
{
  if (parent.isValid())
    return static_cast<AbstractTreeItem*>(parent.internalPointer())->columnCount();
  else
    return rootItem->columnCount();
}

/*!
 * \brief TreeModel::appendItem
 * \param item
 */
void TreeModel::appendItem(AbstractTreeItem *item)
{
  beginInsertRows( QModelIndex(), rootItem->row() - 1, rootItem->row() - 1 );
  rootItem->appendChild( item );
  endInsertRows();
}

/*!
 * \brief TreeModel::removeItem
 * \param position
 */
void TreeModel::removeItem(int position)
{
  beginRemoveRows( QModelIndex(), position, position );
  rootItem->removeChild( position );
  endRemoveRows();
}

/*!
 * \brief TreeModel::getTreeItem
 * \param row
 * \return
 */
AbstractTreeItem *TreeModel::getTreeItem(int row) const
{
  return rootItem->child( row );
}

/*!
 * \brief TreeModel::getAllTreeItem
 * \return
 */
QList<AbstractTreeItem *> TreeModel::getAllTreeItem() const
{
  return rootItem->children();
}

/*!
 * \brief TreeModel::data
 * \param index
 * \param role
 * \return
 */
QVariant TreeModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid())
    return QVariant();

  AbstractTreeItem *item = static_cast< AbstractTreeItem *>( index.internalPointer() );

  if ( role == Qt::DisplayRole || role == Qt::EditRole ) {
    return item->data( index.column() );
  }

  if ( role == Qt::DecorationRole && index.column() == 0 ) {
    QPixmap pixmap( 16, 16 );
    pixmap.fill( item->getColor() );
    return qVariantFromValue<QPixmap>( pixmap );
  }

  return QVariant();
}

/*!
 * \brief TreeModel::setData
 * \param index
 * \param value
 * \param role
 * \return
 */
bool TreeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  if ( index.isValid() && role == Qt::EditRole ) {
    if ( rootItem->child( index.row() )->setData( index.column(), value ) ) {
      emit dataChanged( index, index );
    }
    return true;
  }

  return false;
}

/*!
 * \brief TreeModel::headerData
 * \param section
 * \param orientation
 * \param role
 * \return
 */
QVariant TreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    return rootItem->data( section );

  return QVariant();
}

/*!
 * \brief TreeModel::index
 * \param row
 * \param column
 * \param parent
 * \return
 */
QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent) const
{
  if ( ! hasIndex( row, column, parent ) ) {
    return QModelIndex();
  }

  AbstractTreeItem *childItem = rootItem->child( row );
  if ( childItem ) {
    return createIndex( row, column, childItem );
  }
  else {
    return QModelIndex();
  }
}

/*!
 * \brief TreeModel::parent
 * \param index
 * \return
 */
QModelIndex TreeModel::parent(const QModelIndex &index) const
{
  if ( !index.isValid() ) {
    return QModelIndex();
  }

  AbstractTreeItem *childItem = static_cast< AbstractTreeItem *>(index.internalPointer());
  RootTreeItem *parentItem = childItem->parent();

  if ( parentItem == rootItem ) {
    return QModelIndex();
  }
  else {
    return createIndex( parentItem->row(), 0, parentItem );
  }

}

/*!
 * \brief TreeModel::rowCount
 * \param parent
 * \return
 */
int TreeModel::rowCount(const QModelIndex &parent) const
{
  if ( parent == QModelIndex() ) {
    return rootItem->childCount();
  }
  else {
    return 0;
  }
}


#include <QApplication>
#include <QFile>
#include <QTreeView>

bool TreeModelTest(int argc, char *argv[])
{
  QApplication app( argc, argv );

  TreeModel model;

  QTreeView view;
  view.setModel( &model );
  view.setWindowTitle( QObject::tr( "Simple Tree Model" ) );
  view.show();
  view.resize( 640, 480 );
  return app.exec();
}
