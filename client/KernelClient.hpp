#ifndef TERMETCLIENT_HPP
#define TERMETCLIENT_HPP

#include <QObject>
#include <QFuture>
#include <QLinkedList>

#include "../server/ServerCommands.hpp"
#include "ServerDescription.hpp"
#include "command/ClientCommand.hpp"


namespace kernel {

  namespace server {
    namespace command {
      class ServerStatus;
    }
  }

  namespace client {

    namespace command {
      class PingPongResult;
    }


    /*!
     * \brief The KernelClient class
     */
    class KernelClient : public QObject
    {
      Q_OBJECT

    public:
      static KernelClient* getInstance();

      virtual ~KernelClient() {}

      // Sync and async interface
      bool executeAndWait(std::shared_ptr<command::ClientCommand> commandProcessor);
      QFuture<bool> execute(std::shared_ptr<command::ClientCommand> commandProcessor);

      // commands handlers
      QFuture< bool > pingHost(command::PingPongResult* pingPongResultPtr = nullptr, const quint32 numberOfPackets = 4, const quint32 packetSize = 256 /*bytes*/);

      QFuture< bool > serverStatus(server::command::ServerStatus* serverStatusPtr);

      // Setters and getters
      ServerDescription getServerDescription() const;
      void setServerDescription(const ServerDescription &value);

    private:
      explicit KernelClient(QObject *parent = nullptr) : QObject(parent) {}

      Q_DISABLE_COPY(KernelClient)

    private:
      static bool activated;
      ServerDescription serverDescription;
    };


    void concatBool(bool &rb, const bool &crb);

  }
}

#endif // TERMETCLIENT_HPP
