#include "ColorColumnDelegate.hpp"

#include <CreateBoundaryConditionDialog.hpp>

#include <QColorDialog>
#include <QLabel>
#include <QPushButton>
#include <QPalette>
#include <QPainter>

/*!
 * \brief ColorColumnDelegate::ColorColumnDelegate
 * \param parent
 */
ColorColumnDelegate::ColorColumnDelegate(QObject *parent) : QStyledItemDelegate( parent ) {}

/*!
 * \brief ColorColumnDelegate::paint
 * \param painter
 * \param option
 * \param index
 */
void ColorColumnDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
  if (index.data().canConvert<QColor>()) {
    QColor color = qvariant_cast<QColor>( index.data() );

    if ( option.state & QStyle::State_Selected ) {
      painter->fillRect( option.rect, option.palette.highlight() );
    }

    {
      painter->save();

      painter->setRenderHint( QPainter::Antialiasing, true );
      painter->setPen( Qt::NoPen );
      painter->setBrush( QBrush( color ) );
      painter->drawRect( option.rect );

      painter->restore();
    }

  } else {
    QStyledItemDelegate::paint(painter, option, index);
  }
}

/*!
 * \brief ColorColumnDelegate::createEditor
 * \param parent
 * \return
 */
QWidget *ColorColumnDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/*option*/, const QModelIndex &/*index*/) const
{
  QColorDialog* editor = new QColorDialog( parent );
  editor->setModal( true );
  return editor;
}

/*!
 * \brief ColorColumnDelegate::setEditorData
 * \param editor
 * \param index
 */
void ColorColumnDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QVariant qVariant = index.model()->data( index, Qt::EditRole );
  QColor color = qVariant.value<QColor>();

  QColorDialog* dialog = static_cast< QColorDialog* >( editor );
  dialog->setCurrentColor( color );
}

/*!
 * \brief ColorColumnDelegate::setModelData
 * \param editor
 * \param model
 * \param index
 */
void ColorColumnDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
  QColorDialog* dialog = static_cast< QColorDialog* >( editor );
  QColor color = dialog->currentColor();

  QVariant qVariant = qVariantFromValue( color );
  model->setData( index, qVariant, Qt::EditRole );
  emit changeAppearance();
}

/*!
 * \brief ColorColumnDelegate::updateEditorGeometry
 * \param editor
 * \param option
 * \param index
 */
void ColorColumnDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/*index*/) const
{
  editor->setGeometry(option.rect);
}
