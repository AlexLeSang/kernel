#include <iostream>

#include <vtk_utils/VTKPolyDataSlicer.hpp>
#include <vtk_utils/CGALInterfaceMesher.hpp>
#include <HeatFemSolver.hpp>
#include <mesh/ViennaMeshMesher.hpp>

#include <mongo/MongoConnectionTest.hpp>

#include <boundary_condition/AppliedBoundaryCondition.hpp>
#include <mesh/MeshAppender.hpp>

#include <viennacl/ViennaCLContextTools.hpp>

int main(int argc, char **argv)
{
  if ( ! test::ViennaCLContextTest() ) { return EXIT_FAILURE; }

  // VTK Slicer tests
  //  if ( ! test::VTKSlicerXTest() ) { return EXIT_FAILURE; }
  //  if ( ! test::VTKSlicerYTest() ) { return EXIT_FAILURE; }
  //  if ( ! test::VTKSlicerZTest() ) { return EXIT_FAILURE; }
  //  if ( ! test::VTKSlicerXYZTest() ) { return EXIT_FAILURE; }
  //  if ( ! test::VTKSlicerXYZTestGeometry() ) { return EXIT_FAILURE; }
  //  if ( ! test::VTKSlicerXYZTestLargeGeometry() ) { return EXIT_FAILURE; }
  //  if ( ! test::VTKSlicerExtractBoundaryEdges() ) { return EXIT_FAILURE; }
  //  if ( ! test::VTKComplexInterface() ) { return EXIT_FAILURE; }
  //  if ( !test::SliceAndSave( argc, argv ) ) { return EXIT_FAILURE; } // "simple_cube.vtp" Y "simple_cube_front.vtp" "simple_cube_back.vtp"

  // CGAL interface mesh test
  //  if ( ! test::CGALMeshInterfaceTest() ) { return EXIT_FAILURE; }

  //  if ( ! test::ApplicableNodesContainerTest() ) { return EXIT_FAILURE; }

  // Mesh tests
//   if ( ! test::ViennaMeshMesherTest( argc, argv ) ) { return EXIT_FAILURE; } // "simple_cube.vtp" 0.01 "simple_cube_mesh_test.vtu"// "simple_cube_right.vtp" 0.0005 "simple_cube_right.vtu"
  // if ( ! test::AppendMeshesTest( argc, argv ) ) { return EXIT_FAILURE; } // "simple_cube_left.vtu" "simple_cube_right.vtu" "simple_cube_assembed.vtu"

  // Mongo tests
  //  if ( ! test::namedFileGridFSTest() ) { return EXIT_FAILURE; }
  //  if ( ! test::bufferGridFSTest( argc, argv ) ) { return EXIT_FAILURE; } // simple_cube.vtu simple_cube_copied.vtu
  //  if ( ! test::classTest( argc, argv ) ) { return EXIT_FAILURE; } // simple_cube.vtu simple_cube_copied.vtu

  //  if ( ! test::DirihletBoundaryConditionSerializationTest( argc, argv ) ) { return EXIT_FAILURE; }
  //  if ( ! test::NeumannBoundaryConditionSerializationTest( argc, argv ) ) { return EXIT_FAILURE; }

  // ApplyedBoundaryConditionTest
  //  if ( ! test::ApplyedBoundaryConditionTest( argc, argv ) ) { return EXIT_FAILURE; }
  //  if ( ! test::ApplyedBoundaryConditionReadTest( argc, argv ) ) { return EXIT_FAILURE; }
  //  if ( ! test::ApplyedBoundaryConditionWriteTest( argc, argv ) ) { return EXIT_FAILURE; }

  // Fem solver tests
  // if ( ! test::HeatFemSolverTest( argc, argv ) ) { return EXIT_FAILURE; } // Test options 0.05 0.001
  //  if ( ! test::HeatFemSolverTestBC( argc, argv ) ) { return EXIT_FAILURE; } // "simple_cube.vtu" "Dirihlet 0.kkbc" "Neumann 1.kkbc" 0.05 0.0005
  return EXIT_SUCCESS;
}
