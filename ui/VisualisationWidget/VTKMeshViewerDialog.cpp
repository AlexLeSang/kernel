#include "VTKMeshViewerDialog.hpp"
#include "ui_VTKMeshViewerDialog.h"

#include <vtkAxesActor.h>
#include <vtkDoubleClickPickerTrackballCameraInterractor.hpp>
#include <vtkMapper.h>
#include <vtkPolyDataMapper.h>
#include <vtkPlane.h>
#include <vtkClipPolyData.h>
#include <vtkClipClosedSurface.h>
#include <vtkPlaneCollection.h>
#include <vtkExtractUnstructuredGrid.h>
#include <vtkGeometryFilter.h>
#include <vtkCubeSource.h>
#include <vtkProperty.h>
#include <vtkAlgorithmOutput.h>
#include <vtkXMLUnstructuredGridReader.h>

/*!
 * \brief VTKMeshViewerDialog::VTKMeshViewerDialog
 * \param parent
 * \param actor
 */
VTKMeshViewerDialog::VTKMeshViewerDialog(QWidget *parent, vtkActor *actor) :
  QDialog(parent),
  ui(new Ui::VTKMeshViewerDialog),
  actorPtr( actor ),
  meshedActorPtr( nullptr ),
  xMin( -1.0 ),
  xMax( 1.0 ),
  yMin( -1.0 ),
  yMax( 1.0 ),
  zMin( -1.0 ),
  zMax( 1.0 )
{
  ui->setupUi(this);

  originalActorRenderer = vtkSmartPointer< vtkRenderer >::New();
  ui->originalActorQvtkWidget->GetRenderWindow()->AddRenderer( originalActorRenderer );

  vtkSmartPointer< vtkDoubleClickPickerTrackballCameraInterractor > styleOriginal = vtkSmartPointer< vtkDoubleClickPickerTrackballCameraInterractor >::New();
  styleOriginal->SetDefaultRenderer( originalActorRenderer );

  ui->originalActorQvtkWidget->GetInteractor()->SetInteractorStyle( styleOriginal );
  QObject::connect( styleOriginal.GetPointer(), SIGNAL( signal_actorDeselected(vtkActor*) ), styleOriginal.GetPointer(), SLOT( slot_removeFromSelected(vtkActor*) ) );

  auto axesActorOriginal = vtkSmartPointer< vtkAxesActor >::New();
  axesActorOriginal->AxisLabelsOff();
  originalActorRenderer->AddActor( axesActorOriginal );

  axesActorOriginal->SetConeRadius( 0.1 );
  double axesLenght[3] = { 0.5, 0.5, 0.5 };
  axesActorOriginal->SetTotalLength( axesLenght );

  originalActorRenderer->AddActor( actorPtr );

  double bounds[ 6 ];

  actorPtr->GetBounds( bounds );
  xMin = bounds[ 0 ];
  xMax = bounds[ 1 ];

  yMin = bounds[ 2 ];
  yMax = bounds[ 3 ];

  zMin = bounds[ 4 ];
  zMax = bounds[ 5 ];


  ui->xMinDoubleSpinBox->setValue( xMin );
  ui->xMaxDoubleSpinBox->setValue( xMax );

  ui->yMinDoubleSpinBox->setValue( yMin );
  ui->yMaxDoubleSpinBox->setValue( yMax );

  ui->zMinDoubleSpinBox->setValue( zMin );
  ui->zMaxDoubleSpinBox->setValue( zMax );
  on_clearPushButton_clicked();
}

/*!
 * \brief VTKMeshViewerDialog::~VTKMeshViewerDialog
 */
VTKMeshViewerDialog::~VTKMeshViewerDialog()
{
  delete ui;
}

/*!
 * \brief VTKMeshViewerDialog::changeEvent
 * \param e
 */
void VTKMeshViewerDialog::changeEvent(QEvent *e)
{
  QDialog::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}

/*!
 * \brief VTKMeshViewerDialog::on_pushButton_clicked
 */
void VTKMeshViewerDialog::on_pushButton_clicked()
{
  originalActorRenderer->RemoveAllViewProps();

  vtkSmartPointer< vtkExtractUnstructuredGrid > clipper = vtkSmartPointer< vtkExtractUnstructuredGrid >::New();
  clipper->SetInputConnection( actorPtr->GetMapper()->GetInputConnection(0, 0) );

  clipper->SetCellClipping( true );
  clipper->SetExtent( xMin, xMax, yMin, yMax, zMin, zMax );

  vtkSmartPointer< vtkGeometryFilter > geometryFilter = vtkSmartPointer< vtkGeometryFilter >::New();
  geometryFilter->SetInputConnection( clipper->GetOutputPort() );
  geometryFilter->Update();

  vtkSmartPointer< vtkPolyDataMapper > mapper = vtkSmartPointer< vtkPolyDataMapper >::New();
  mapper->SetInputConnection( geometryFilter->GetOutputPort() );
  mapper->Update();

  vtkSmartPointer< vtkActor > actor = vtkSmartPointer< vtkActor >::New();
  actor->SetMapper( mapper );
  meshedActorPtr = actor.GetPointer();

  originalActorRenderer->AddActor( meshedActorPtr );

  {
    vtkSmartPointer< vtkPlaneSource > xminPlane = vtkSmartPointer< vtkPlaneSource >::New();
    xminPlane->SetOrigin( xMin, yMin - 0.5, zMin - 0.5 );
    xminPlane->SetPoint1( xMin, yMin - 0.5, zMax + 0.5 );
    xminPlane->SetPoint2( xMin, yMax + 0.5, zMin - 0.5 );
    xminPlane->SetNormal( 1.0, 0.0, 0.0 );

    vtkSmartPointer< vtkPolyDataMapper > xminPlaneMapper = vtkSmartPointer< vtkPolyDataMapper >::New();
    xminPlaneMapper->SetInput( xminPlane->GetOutput() );

    vtkSmartPointer< vtkActor > xminPlaneActor = vtkSmartPointer< vtkActor >::New();
    xminPlaneActor->SetMapper( xminPlaneMapper );
    xminPlaneActor->GetProperty()->SetOpacity( 0.15 );
    xminPlaneActor->GetProperty()->EdgeVisibilityOn();
    xminPlaneActor->GetProperty()->SetEdgeColor( 1.0, 0.0, 0.0 );
    xminPlaneActor->GetProperty()->SetLineWidth( 5.0 );

    originalActorRenderer->AddActor( xminPlaneActor );
  }

  {
    vtkSmartPointer< vtkPlaneSource > xmaxPlane = vtkSmartPointer< vtkPlaneSource >::New();
    xmaxPlane->SetOrigin( xMax, yMin - 0.5, zMin - 0.5 );
    xmaxPlane->SetPoint1( xMax, yMin - 0.5, zMax + 0.5 );
    xmaxPlane->SetPoint2( xMax, yMax + 0.5, zMin - 0.5 );
    xmaxPlane->SetNormal( -1.0, 0.0, 0.0 );

    vtkSmartPointer< vtkPolyDataMapper > xmaxPlaneMapper = vtkSmartPointer< vtkPolyDataMapper >::New();
    xmaxPlaneMapper->SetInput( xmaxPlane->GetOutput() );

    vtkSmartPointer< vtkActor > xmaxPlaneActor = vtkSmartPointer< vtkActor >::New();
    xmaxPlaneActor->SetMapper( xmaxPlaneMapper );
    xmaxPlaneActor->GetProperty()->SetOpacity( 0.15 );
    xmaxPlaneActor->GetProperty()->EdgeVisibilityOn();
    xmaxPlaneActor->GetProperty()->SetEdgeColor( 1.0, 0.0, 0.0 );
    xmaxPlaneActor->GetProperty()->SetLineWidth( 5.0 );

    originalActorRenderer->AddActor( xmaxPlaneActor );
  }

  {
    vtkSmartPointer< vtkPlaneSource > yminPlane = vtkSmartPointer< vtkPlaneSource >::New();
    yminPlane->SetOrigin( xMin - 0.5, yMin, zMin - 0.5 );
    yminPlane->SetPoint1( xMin - 0.5, yMin, zMax + 0.5 );
    yminPlane->SetPoint2( xMax + 0.5, yMin, zMin - 0.5 );
    yminPlane->SetNormal( 0.0, 1.0, 0.0 );

    vtkSmartPointer< vtkPolyDataMapper > yminPlaneMapper = vtkSmartPointer< vtkPolyDataMapper >::New();
    yminPlaneMapper->SetInput( yminPlane->GetOutput() );

    vtkSmartPointer< vtkActor > yminPlaneActor = vtkSmartPointer< vtkActor >::New();
    yminPlaneActor->SetMapper( yminPlaneMapper );
    yminPlaneActor->GetProperty()->SetOpacity( 0.15 );
    yminPlaneActor->GetProperty()->EdgeVisibilityOn();
    yminPlaneActor->GetProperty()->SetEdgeColor( 0.0, 1.0, 0.0 );
    yminPlaneActor->GetProperty()->SetLineWidth( 5.0 );

    originalActorRenderer->AddActor( yminPlaneActor );
  }

  {
    vtkSmartPointer< vtkPlaneSource > ymaxPlane = vtkSmartPointer< vtkPlaneSource >::New();
    ymaxPlane->SetOrigin( xMin - 0.5, yMax, zMin - 0.5 );
    ymaxPlane->SetPoint1( xMin - 0.5, yMax, zMax + 0.5 );
    ymaxPlane->SetPoint2( xMax + 0.5, yMax, zMin - 0.5 );
    ymaxPlane->SetNormal( 0.0, -1.0, 0.0 );

    vtkSmartPointer< vtkPolyDataMapper > ymaxPlaneMapper = vtkSmartPointer< vtkPolyDataMapper >::New();
    ymaxPlaneMapper->SetInput( ymaxPlane->GetOutput() );

    vtkSmartPointer< vtkActor > ymaxPlaneActor = vtkSmartPointer< vtkActor >::New();
    ymaxPlaneActor->SetMapper( ymaxPlaneMapper );
    ymaxPlaneActor->GetProperty()->SetOpacity( 0.15 );
    ymaxPlaneActor->GetProperty()->EdgeVisibilityOn();
    ymaxPlaneActor->GetProperty()->SetEdgeColor( 0.0, 1.0, 0.0 );
    ymaxPlaneActor->GetProperty()->SetLineWidth( 5.0 );

    originalActorRenderer->AddActor( ymaxPlaneActor );
  }


  {
    vtkSmartPointer< vtkPlaneSource > zminPlane = vtkSmartPointer< vtkPlaneSource >::New();
    zminPlane->SetOrigin( xMin - 0.5, yMin - 0.5, zMin );
    zminPlane->SetPoint1( xMin - 0.5, yMax + 0.5, zMin );
    zminPlane->SetPoint2( xMax + 0.5, yMin - 0.5, zMin );
    zminPlane->SetNormal( 0.0, 0.0, 1.0 );

    vtkSmartPointer< vtkPolyDataMapper > zminPlaneMapper = vtkSmartPointer< vtkPolyDataMapper >::New();
    zminPlaneMapper->SetInput( zminPlane->GetOutput() );

    vtkSmartPointer< vtkActor > zminPlaneActor = vtkSmartPointer< vtkActor >::New();
    zminPlaneActor->SetMapper( zminPlaneMapper );
    zminPlaneActor->GetProperty()->SetOpacity( 0.15 );
    zminPlaneActor->GetProperty()->EdgeVisibilityOn();
    zminPlaneActor->GetProperty()->SetEdgeColor( 0.0, 0.0, 1.0 );
    zminPlaneActor->GetProperty()->SetLineWidth( 5.0 );

    originalActorRenderer->AddActor( zminPlaneActor );
  }

  {
    vtkSmartPointer< vtkPlaneSource > zmaxPlane = vtkSmartPointer< vtkPlaneSource >::New();
    zmaxPlane->SetOrigin( xMin - 0.5, yMin - 0.5, zMax );
    zmaxPlane->SetPoint1( xMin - 0.5, yMax + 0.5, zMax );
    zmaxPlane->SetPoint2( xMax + 0.5, yMin - 0.5, zMax );
    zmaxPlane->SetNormal( 0.0, 0.0, 1.0 );

    vtkSmartPointer< vtkPolyDataMapper > zmaxPlaneMapper = vtkSmartPointer< vtkPolyDataMapper >::New();
    zmaxPlaneMapper->SetInput( zmaxPlane->GetOutput() );

    vtkSmartPointer< vtkActor > zmaxPlaneActor = vtkSmartPointer< vtkActor >::New();
    zmaxPlaneActor->SetMapper( zmaxPlaneMapper );
    zmaxPlaneActor->GetProperty()->SetOpacity( 0.15 );
    zmaxPlaneActor->GetProperty()->EdgeVisibilityOn();
    zmaxPlaneActor->GetProperty()->SetEdgeColor( 0.0, 0.0, 1.0 );
    zmaxPlaneActor->GetProperty()->SetLineWidth( 5.0 );

    originalActorRenderer->AddActor( zmaxPlaneActor );
  }

  ui->originalActorQvtkWidget->update();
}

/*!
 * \brief VTKMeshViewerDialog::on_clearPushButton_clicked
 */
void VTKMeshViewerDialog::on_clearPushButton_clicked()
{
  originalActorRenderer->RemoveAllViewProps();
  originalActorRenderer->AddActor( actorPtr );
  ui->originalActorQvtkWidget->update();
}


/*!
 * \brief VTKMeshViewerDialog::on_xMinDoubleSpinBox_valueChanged
 * \param arg1
 */
void VTKMeshViewerDialog::on_xMinDoubleSpinBox_valueChanged(double arg1)
{
    xMin = arg1;
    ui->xMaxDoubleSpinBox->setMinimum( arg1 );
    on_pushButton_clicked();
}

/*!
 * \brief VTKMeshViewerDialog::on_xMaxDoubleSpinBox_valueChanged
 * \param arg1
 */
void VTKMeshViewerDialog::on_xMaxDoubleSpinBox_valueChanged(double arg1)
{
   xMax = arg1;
   ui->xMinDoubleSpinBox->setMaximum( arg1 );
   on_pushButton_clicked();
}

/*!
 * \brief VTKMeshViewerDialog::on_yMinDoubleSpinBox_valueChanged
 * \param arg1
 */
void VTKMeshViewerDialog::on_yMinDoubleSpinBox_valueChanged(double arg1)
{
   yMin = arg1;
   ui->yMaxDoubleSpinBox->setMinimum( arg1 );
   on_pushButton_clicked();
}

/*!
 * \brief VTKMeshViewerDialog::on_yMaxDoubleSpinBox_valueChanged
 * \param arg1
 */
void VTKMeshViewerDialog::on_yMaxDoubleSpinBox_valueChanged(double arg1)
{
  yMax = arg1;
  ui->yMinDoubleSpinBox->setMaximum( arg1 );
  on_pushButton_clicked();
}

/*!
 * \brief VTKMeshViewerDialog::on_zMinDoubleSpinBox_valueChanged
 * \param arg1
 */
void VTKMeshViewerDialog::on_zMinDoubleSpinBox_valueChanged(double arg1)
{
  zMin = arg1;
  ui->zMaxDoubleSpinBox->setMinimum( arg1 );
  on_pushButton_clicked();
}


/*!
 * \brief VTKMeshViewerDialog::on_zMaxDoubleSpinBox_valueChanged
 * \param arg1
 */
void VTKMeshViewerDialog::on_zMaxDoubleSpinBox_valueChanged(double arg1)
{
  zMax = arg1;
  ui->zMinDoubleSpinBox->setMaximum( arg1 );
  on_pushButton_clicked();
}


bool test::VTKMeshViewerDialogTest(int argc, char *argv[])
{
  QApplication a( argc, argv );

  vtkSmartPointer< vtkActor > actorPtr = vtkSmartPointer< vtkActor >::New();
  {
    vtkSmartPointer< vtkXMLUnstructuredGridReader > reader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
    reader->SetFileName( "simple_cube.vtu" );
    reader->Update();

    vtkSmartPointer< vtkDataSetMapper > mapper = vtkSmartPointer< vtkDataSetMapper >::New();
    mapper->SetInputConnection( reader->GetOutputPort() );

    vtkSmartPointer< vtkProperty > backFaces = vtkSmartPointer< vtkProperty >::New();
    backFaces->SetSpecular( 0.0 );
    backFaces->SetDiffuse( 0.0 );
    backFaces->SetAmbient( 1.0 );
    backFaces->SetAmbientColor( 1.0000, 0.3882, 0.2784 );

    actorPtr->SetBackfaceProperty( backFaces );

    actorPtr->SetMapper( mapper );
  }

  VTKMeshViewerDialog dialog( nullptr, actorPtr );
  dialog.exec();
  a.exit();

  return true;
}

