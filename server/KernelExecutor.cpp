#include "KernelExecutor.hpp"

#include <QDataStream>
#include <QFuture>
#include <QtConcurrentRun>
#include <QHostAddress>

#include "ServerCommands.hpp"
#include "command/Command.hpp"

#include "ThreadSaveLogger.hpp"

namespace kernel {
  namespace server {

    /*!
     * \brief KernelExecutor::KernelExecutor
     * \param socketDescriptor
     * \param parent
     */
    KernelExecutor::KernelExecutor(int socketDescriptor, QObject *parent) : QObject(parent), socketDescriptor_(socketDescriptor), interruptFlag_(false) {}

    void KernelExecutor::run()
    {
      QTcpSocket socket;

      if ( !socket.setSocketDescriptor( socketDescriptor_ ) ) {
        utils::ThreadSaveLogger::addLineStatic( "Socket error: " + socket.errorString() );
        return;
      }

      if ( ! socket.waitForReadyRead( Timeout ) ) {
        utils::ThreadSaveLogger::addLineStatic( "Socket timeout: " + socket.errorString() );
        return;
      }

      utils::ThreadSaveLogger::addLineStatic( "Receiving connection from " + socket.peerAddress().toString() + ":" + QString::number( socket.peerPort() ) +
                                              ". Local socket addresss: " + socket.localAddress().toString() + ":" + QString::number( socket.localPort() ) );

      // Read client command
      ServerCommands command;
      {
        quint32 blockSize;
        QDataStream in( &socket );
        in.setVersion( QDataStream::Qt_4_8 );

        in >> blockSize;

        while ( socket.bytesAvailable() < blockSize ) {
          if ( ! socket.waitForReadyRead( Timeout ) ) {
            utils::ThreadSaveLogger::addLineStatic( socket.errorString() );
            return;
          }
        }

        quint32 rawCommand;
        in >> rawCommand;
        command = static_cast< ServerCommands >( rawCommand );
      }

      // utils::ThreadSaveLogger::addLineStatic( "TermetExecutor::run() command " + QString::number( (quint32)command ) );

      auto commandProcessor = command::CommandProcessor( command );
      /*const bool result = */commandProcessor( socket, interruptFlag_ ); // TODO process result

      socket.disconnectFromHost();
      if ( QTcpSocket::SocketState::UnconnectedState != socket.state() ) {
        socket.waitForDisconnected( Timeout );
      }

      socket.close();
    }

    void KernelExecutor::interrupt()
    {
      interruptFlag_.store( true );
    }
  }
}

