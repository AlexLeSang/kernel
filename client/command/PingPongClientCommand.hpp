#ifndef PINGPONGCLIENTCOMMAND_HPP
#define PINGPONGCLIENTCOMMAND_HPP

#include "ClientCommand.hpp"
#include "ServerDescription.hpp"

#include <tuple>

#include <QMutex>
#include <QMutexLocker>
#include <QWaitCondition>
#include <QThread>
#include <QThreadPool>

namespace kernel {
  namespace client {
    namespace command {

      /*!
       * \brief The PingPongResult class
       */
      class PingPongResult
      {
      public:
        PingPongResult() : min( 0 ), max( 0 ), avg( 0 ), minResultSet( false ), maxResultSet( false ), avgResultSet( false ) {}

        quint32 getMin() const
        {
          QMutexLocker l( &mutex );
          if ( !minResultSet ) {
            minCond.wait( &mutex );
          }
          return min;
        }

        void setMin(const quint32 &value)
        {
          QMutexLocker l( &mutex );
          min = value;
          minResultSet = true;
          minCond.wakeAll();
        }

        quint32 getMax() const
        {
          QMutexLocker l( &mutex );
          if ( !maxResultSet ) {
            maxCond.wait( &mutex );
          }
          return max;
        }

        void setMax(const quint32 &value)
        {
          QMutexLocker l( &mutex );
          max = value;
          maxResultSet = true;
          maxCond.wakeAll();
        }

        quint32 getAvg() const
        {
          QMutexLocker l( &mutex );
          if ( !avgResultSet ) {
            avgCond.wait( &mutex );
          }
          return avg;
        }

        void setAvg(const quint32 &value)
        {
          QMutexLocker l( &mutex );
          avg = value;
          avgResultSet = true;
          avgCond.wakeAll();
        }

      private:
        quint32 min;
        quint32 max;
        quint32 avg;
        bool minResultSet;
        bool maxResultSet;
        bool avgResultSet;

        mutable QMutex mutex;
        mutable QWaitCondition minCond;
        mutable QWaitCondition maxCond;
        mutable QWaitCondition avgCond;
      };

      /*!
       * \brief The PingPongClientCommand class
       */
      class PingPongClientCommand : public ClientCommand
      {
      public:
        PingPongClientCommand() : numberOfPackets( 10 ), packetSize( 100 /*bytes*/ ), timeout( 3 /*ms*/ ), pingPongResultPtr( nullptr ) {}
        virtual ~PingPongClientCommand() {}

        virtual bool operator ()();
        virtual server::ServerCommands commandName() const;

        quint32 getNumberOfPackets() const;
        void setNumberOfPackets(const quint32 &value);

        quint32 getPacketSize() const;
        void setPacketSize(const quint32 &value);

        quint32 getTimeout() const;
        void setTimeout(const quint32 &value);

        PingPongResult *getPingPongResultPtr() const;
        void setPingPongResultPtr(PingPongResult *value);

      private:
        quint32 numberOfPackets;
        quint32 packetSize; // bytes
        quint32 timeout; // ms
        PingPongResult* pingPongResultPtr;
      };

    }
  }
}


#include <iostream>
#include <KernelClient.hpp>
#include <ServerDescription.hpp>

#include <QHostInfo>

#include "../utils/ThreadSleep.hpp"

namespace test {

  inline bool PingPongClientCommandTest()
  {
    std::cout << "PingPongClientCommandTest()" << std::endl;

    kernel::client::KernelClient* client = kernel::client::KernelClient::getInstance();
    kernel::client::ServerDescription sd;
    sd.address = QHostAddress( QHostAddress::LocalHost );
    // sd.address = QHostAddress( "192.168.0.9" );
    // sd.address = QHostAddress( "192.168.0.10" );
    sd.port = 31017;
    client->setServerDescription( sd );

    kernel::client::command::PingPongResult pingPongResult;

    auto result = client->pingHost( &pingPongResult );

    qWarning() << QThread::currentThread() << " is going to wait for result";
    result.waitForFinished();
    const bool commandSuccessful = result.result();
    if ( commandSuccessful ) {
      qWarning() << "pingPongResult.getAvg() =" << pingPongResult.getAvg();
    }

    return true;
  }

  inline bool ParallelPingPongCommandTest()
  {
    std::cout << "PingPongClientCommandTest()" << std::endl;

    QThreadPool::globalInstance()->setMaxThreadCount( QThread::idealThreadCount() * 10 );

    kernel::client::KernelClient* client = kernel::client::KernelClient::getInstance();
    kernel::client::ServerDescription sd;
    sd.address = QHostAddress( QHostAddress::LocalHost );

    QVector< kernel::client::ServerDescription > sdVector;

    const auto repeatNumber = 400;

    sd.port = 3101;
    for ( auto i = 0; i < repeatNumber; ++ i ) {
      sdVector.push_back( sd );
    }

    /*
    sd.port = 31011;
    for ( auto i = 0; i < repeatNumber; ++ i ) {
      sdVector.push_back( sd );
    }

    sd.port = 31012;
    for ( auto i = 0; i < repeatNumber; ++ i ) {
      sdVector.push_back( sd );
    }

    sd.port = 31013;
    for ( auto i = 0; i < repeatNumber; ++ i ) {
      sdVector.push_back( sd );
    }
    */

    QVector< QFuture< bool > > futures;
    futures.reserve( sdVector.size() );
    QVector< std::shared_ptr< kernel::client::command::PingPongResult > > pingPongResultVector;
    pingPongResultVector.reserve( sdVector.size() );

    for ( auto i = 0; i < sdVector.size(); ++ i )  {
      kernel::client::ServerDescription sd = sdVector[ i ];
      client->setServerDescription( sd );

      std::shared_ptr< kernel::client::command::PingPongResult > pingPongResult( new kernel::client::command::PingPongResult() );
      QFuture< bool > f = client->pingHost( pingPongResult.get(), 20, 1024 );
      pingPongResultVector.push_back( pingPongResult );
      futures.push_back( f );
    }

    for ( auto i = 0; i < sdVector.size(); ++ i )  {
      QFuture< bool > f = futures.at( i );
      if ( f.result() ) {
        kernel::client::ServerDescription sd = sdVector[ i ];
        std::shared_ptr< kernel::client::command::PingPongResult > pingPongResult = pingPongResultVector.at( i );
        qDebug() << "Avg ping to" << sd.address.toString() << ":" << QString::number( sd.port ) << " = " << pingPongResult->getAvg();
      }
      else {
        qDebug() << "Node " << sd.address.toString() << ":" << QString::number( sd.port ) << " is unaccessible";
      }

    }


    return true;
  }

  /*
   *
   */
  class AvgWriter : public QThread
  {
    Q_OBJECT
  public:
    AvgWriter(kernel::client::command::PingPongResult* ptr, QObject *parent = NULL) : QThread(parent), resPtr( ptr ) {}

    virtual void run()
    {
      kernel::utils::ThreadSleep::msleep( 30 );
      resPtr->setAvg( 30 );
      return;
    }

  private:
    kernel::client::command::PingPongResult* resPtr;
  };

  class AvgReader : public QThread
  {
    Q_OBJECT
  public:
    AvgReader(kernel::client::command::PingPongResult* ptr, QObject *parent = NULL) : QThread(parent), resPtr( ptr ) {}

    virtual void run()
    {
      const auto avg = resPtr->getAvg();
      qDebug() << "AvgReader::run(): avg = " << avg;
      return;
    }

  private:
    kernel::client::command::PingPongResult* resPtr;
  };


  class MinWriter : public QThread
  {
    Q_OBJECT
  public:
    MinWriter(kernel::client::command::PingPongResult* ptr, QObject *parent = NULL) : QThread(parent), resPtr( ptr ) {}

    virtual void run()
    {
      kernel::utils::ThreadSleep::msleep( 10 );
      resPtr->setMin( 10 );
      return;
    }

  private:
    kernel::client::command::PingPongResult* resPtr;
  };

  class MinReader : public QThread
  {
    Q_OBJECT
  public:
    MinReader(kernel::client::command::PingPongResult* ptr, QObject *parent = NULL) : QThread(parent), resPtr( ptr ) {}

    virtual void run()
    {
      const auto min = resPtr->getMin();
      qDebug() << "MinReader::run(): min = " << min;
      return;
    }

  private:
    kernel::client::command::PingPongResult* resPtr;
  };


  class MaxWriter : public QThread
  {
    Q_OBJECT
  public:
    MaxWriter(kernel::client::command::PingPongResult* ptr, QObject *parent = NULL) : QThread(parent), resPtr( ptr ) {}

    virtual void run()
    {
      kernel::utils::ThreadSleep::msleep( 20 );
      resPtr->setMax( 20 );
      return;
    }

  private:
    kernel::client::command::PingPongResult* resPtr;
  };

  class MaxReader : public QThread
  {
    Q_OBJECT
  public:
    MaxReader(kernel::client::command::PingPongResult* ptr, QObject *parent = NULL) : QThread(parent), resPtr( ptr ) {}

    virtual void run()
    {
      const auto max = resPtr->getMax();
      qDebug() << "MaxReader::run(): max = " << max;
      return;
    }

  private:
    kernel::client::command::PingPongResult* resPtr;
  };


  inline bool PingPongPingPongResult()
  {
    std::cout << "PingPongPingPongResult()" << std::endl;
    kernel::client::command::PingPongResult pingPongResult;

    MinWriter minWriter( &pingPongResult );
    MinReader minReader( &pingPongResult );

    MaxWriter maxWriter( &pingPongResult );
    MaxReader maxReader( &pingPongResult );

    AvgWriter avgWriter( &pingPongResult );
    AvgReader avgReader( &pingPongResult );


    minWriter.start();
    minReader.start();

    maxWriter.start();
    maxReader.start();

    avgWriter.start();
    avgReader.start();


    minWriter.wait();
    minReader.wait();

    maxWriter.wait();
    maxReader.wait();

    avgWriter.wait();
    avgReader.wait();

    return true;
  }

}

#endif // PINGPONGCLIENTCOMMAND_HPP
