#include "StopServerClientCommand.hpp"

#include "ServerCommands.hpp"
#include "KernelClient.hpp"

namespace kernel {
  namespace client {
    namespace command {

      bool StopServerClientCommand::operator ()()
      {
        // Connect to the server
        QTcpSocket socket;
        socket.connectToHost( description.address, description.port );

        if ( !socket.waitForConnected( server::Timeout ) ) {
          qWarning() << "StopServerClientCommand: " << socket.errorString();
          // TODO log it
          return false;
        }

        // Write to server
        // Send command
        QByteArray block;
        QDataStream out( &block, QIODevice::WriteOnly );
        out.setVersion( QDataStream::Qt_4_8 );
        out << (quint32)0;
        out << (quint32)server::ServerCommands::STOP_SERVER;
        out.device()->seek(0);
        out << (quint32)( block.size() - sizeof(quint32) );

        socket.write( block );
        socket.disconnectFromHost();

        if ( ! socket.waitForDisconnected( server::Timeout ) ) {
          // TODO log it
          return false;
        }

        return true;
      }

      server::ServerCommands StopServerClientCommand::commandName() const
      {
        return server::ServerCommands::STOP_SERVER;
      }

    }
  }
}
