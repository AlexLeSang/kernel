#ifndef VTKPROPERYLISTMODEL_HPP
#define VTKPROPERYLISTMODEL_HPP

#include <QAbstractListModel>

#include <QList>
#include <QDebug>

#include "vtkActor.h"
#include "vtkSmartPointer.h"

enum ActorType
{
  MESH_ACTOR,
  GEOMETRY_ACTOR,
  RESULT_ACTOR
};

typedef std::tuple< vtkSmartPointer< vtkActor >, const ActorType, const QString > ActorDescriptionType;

Q_DECLARE_METATYPE( ActorDescriptionType );

/*!
 * \brief The VTKActorListModel class
 */\
class VTKActorListModel : public QAbstractListModel
{
  Q_OBJECT

public:
  explicit VTKActorListModel(QObject *parent, vtkRenderer* _rendererPtr);

  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
  bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::DisplayRole);
  Qt::ItemFlags flags(const QModelIndex &index) const;
  int rowCount(const QModelIndex &parent) const;

  void addActor(vtkSmartPointer< vtkActor > actor , const ActorType actorType, const QString name);
  bool removeRows(int row, int count, const QModelIndex &parent);

  QList< ActorDescriptionType >::iterator begin() { return actorList.begin(); }
  QList< ActorDescriptionType >::iterator end() { return actorList.end(); }

signals:
  void selectActor(vtkActor* actor) const;
  void deselectActor(vtkActor* actor) const;

private:
  QList< ActorDescriptionType > actorList;
  vtkRenderer* rendererPtr;
};


inline QDebug operator <<(QDebug qdebug, const ActorDescriptionType& actorDescriptionType)
{
  qdebug << "actor: " << std::get<0>( actorDescriptionType );
  const auto& actorType = std::get<1>( actorDescriptionType );
  qdebug << "type: " << (actorType == MESH_ACTOR ? "MESH_ACTOR" : actorType == GEOMETRY_ACTOR ? "GEOMETRY_ACTOR" : "RESULT_ACTOR");
  return qdebug.maybeSpace();
}

#endif // VTKPROPERYLISTMODEL_HPP
