#include "vtkDoubleClickPickerTrackballCameraInterractor.hpp"

#include <vtkCellPicker.h>
#include <vtkSelectionNode.h>
#include <vtkSelection.h>
#include <vtkExtractSelection.h>
#include <vtkUnstructuredGrid.h>
#include <vtkProperty.h>
#include <vtkSmartPointer.h>
#include <vtkCubeSource.h>

#include <algorithm>
#include <cassert>

/*!
 * \brief vtkDoubleClickPickerTrackballCameraInterractor::vtkDoubleClickPickerTrackballCameraInterractor
 */
vtkDoubleClickPickerTrackballCameraInterractor::vtkDoubleClickPickerTrackballCameraInterractor() : NumberOfClicks(0), ResetPixelDistance(5)
{
  this->PreviousPosition[0] = 0;
  this->PreviousPosition[1] = 0;
}

/*!
 * \brief vtkDoubleClickPickerTrackballCameraInterractor::OnLeftButtonDown
 */
void vtkDoubleClickPickerTrackballCameraInterractor::OnLeftButtonDown()
{
  this->NumberOfClicks++;
  int pickPosition[2];
  this->GetInteractor()->GetEventPosition(pickPosition);

  int xdist = pickPosition[0] - this->PreviousPosition[0];
  int ydist = pickPosition[1] - this->PreviousPosition[1];

  this->PreviousPosition[0] = pickPosition[0];
  this->PreviousPosition[1] = pickPosition[1];

  int moveDistance = (int)sqrt((double)(xdist*xdist + ydist*ydist));

  if ( moveDistance > this->ResetPixelDistance ) {
    this->NumberOfClicks = 1;
  }

  if ( this->NumberOfClicks == 2 ) {
    this->NumberOfClicks = 0;

    vtkSmartPointer< vtkCellPicker > picker = vtkSmartPointer< vtkCellPicker >::New();
    picker->SetTolerance( 0.0005 );

    // Pick from this location.
    picker->Pick( pickPosition[0], pickPosition[1], 0, this->GetDefaultRenderer() );
    auto pickedActor = picker->GetActor();
    if ( ! isBoundingBox( pickedActor ) ) {
      if ( pickedActor != nullptr ) {
        addOrRemoveFromSelected( pickedActor );
      }
      else {
        cleanSelection();
      }
    }
    else {
      // Just do not select it
    }
  }

  vtkInteractorStyleTrackballCamera::OnLeftButtonDown();
}

/*!
 * \brief vtkDoubleClickPickerTrackballCameraInterractor::OnRightButtonDown
 */
void vtkDoubleClickPickerTrackballCameraInterractor::OnRightButtonDown()
{
  int* pos = this->GetInteractor()->GetEventPosition();

  vtkSmartPointer< vtkCellPicker > picker = vtkSmartPointer< vtkCellPicker >::New();
  picker->SetTolerance( 0.0005 );

  // Pick from this location.
  picker->Pick( pos[0], pos[1], 0, this->GetDefaultRenderer() );
  auto pickedActor = picker->GetActor();
  setOrRemoveBoundingBox( pickedActor ); // nullptr as an actor cleans the selection

  vtkInteractorStyleTrackballCamera::OnRightButtonDown();
}

/*!
 * \brief vtkDoubleClickPickerTrackballCameraInterractor::slot_addToSelected
 * \param pickedActor
 */
void vtkDoubleClickPickerTrackballCameraInterractor::slot_addToSelected(vtkActor *pickedActor)
{
  //  std::cerr << "vtkDoubleClickPickerTrackballCameraInterractor::slot_addToSelected(): " << pickedActor << std::endl;

  bool found = false;
  for ( auto it = selectedActorList.begin(); it != selectedActorList.end();  ++it ) {
    if ( ( it->first.GetPointer() ) == pickedActor ) {
      found = true;
      break;
    }
  }

  if ( ! found ) {
    addSelected( pickedActor );
  }
}

/*!
 * \brief vtkDoubleClickPickerTrackballCameraInterractor::slot_removeFromSelected
 * \param pickedActor
 */
void vtkDoubleClickPickerTrackballCameraInterractor::slot_removeFromSelected(vtkActor *pickedActor)
{
  //  std::cerr << "vtkDoubleClickPickerTrackballCameraInterractor::slot_removeFromSelected(): " << pickedActor << std::endl;

  bool found = false;
  for ( auto it = selectedActorList.begin(); it != selectedActorList.end();  ++it ) {
    if ( ( it->first.GetPointer() ) == pickedActor ) {
      found = true;
      break;
    }
  }

  if ( found ) {
    removeSelected( pickedActor );
  }
}

/*!
 * \brief vtkDoubleClickPickerTrackballCameraInterractor::slot_removeBoundingBoxForActor
 * \param actorPtr
 */
void vtkDoubleClickPickerTrackballCameraInterractor::slot_addOrRemoveBoundingBoxForActor(vtkActor *actorPtr)
{
  assert( actorPtr != nullptr );

  const auto hasBoubdingBox = ( boundingtBoxes.find( actorPtr ) != boundingtBoxes.end() );
  if ( ! hasBoubdingBox ) {
    addBoundingBox( actorPtr );
  }
  else {
    removeBoundingBox( actorPtr );
  }

}

/*!
 * \brief vtkDoubleClickPickerTrackballCameraInterractor::slot_cleanSelection
 */
void vtkDoubleClickPickerTrackballCameraInterractor::slot_cleanSelection()
{
  cleanSelection();
}

/*!
 * \brief vtkDoubleClickPickerTrackballCameraInterractor::addOrRemoveFromSelected
 * \param pickedActor
 */
void vtkDoubleClickPickerTrackballCameraInterractor::addOrRemoveFromSelected(vtkActor* pickedActor)
{
  bool found = false;
  for ( auto it = selectedActorList.begin(); it != selectedActorList.end();  ++it ) {
    if ( ( it->first.GetPointer() ) == pickedActor ) {
      found = true;
      break;
    }
  }

  if ( found ) {
    removeSelected( pickedActor );
    emit signal_actorDeselected( pickedActor );
  }
  else {
    addSelected( pickedActor );
    emit signal_actorSelected( pickedActor );
  }
}

/*!
 * \brief vtkDoubleClickPickerTrackballCameraInterractor::cleanSelection
 */
void vtkDoubleClickPickerTrackballCameraInterractor::cleanSelection()
{
  while ( ! selectedActorList.empty() ) {
    vtkActor* actorPtr = selectedActorList.front().first;
    emit signal_actorDeselected( actorPtr );
  }
}

/*!
 * \brief vtkDoubleClickPickerTrackballCameraInterractor::addSelected
 * \param pickedActor
 */
void vtkDoubleClickPickerTrackballCameraInterractor::addSelected(vtkActor *pickedActor)
{
  vtkSmartPointer< vtkActor > selectedActor( pickedActor );

  vtkSmartPointer< vtkProperty > oldProperty = vtkSmartPointer< vtkProperty >::New();
  oldProperty->DeepCopy( pickedActor->GetProperty() );
  selectedActorList.push_back( std::make_pair( selectedActor, oldProperty ) );

  pickedActor->GetProperty()->EdgeVisibilityOn();
  pickedActor->GetProperty()->SetEdgeColor( 0, 0.75, 0 );
  pickedActor->GetProperty()->SetLineWidth( 1.15 );
}

/*!
 * \brief vtkDoubleClickPickerTrackballCameraInterractor::removeSelected
 * \param pickedActor
 */
void vtkDoubleClickPickerTrackballCameraInterractor::removeSelected(vtkActor *pickedActor)
{
  for ( auto it = selectedActorList.begin(); it != selectedActorList.end();  ++it ) {
    if ( ( it->first.GetPointer() ) == pickedActor ) {
      it->first->GetProperty()->DeepCopy( it->second );
      selectedActorList.erase( it );
      break;
    }
  }
}

/*!
 * \brief vtkDoubleClickPickerTrackballCameraInterractor::addBoundingBox
 * \param actorPtr
 */
void vtkDoubleClickPickerTrackballCameraInterractor::addBoundingBox(vtkActor *actorPtr)
{
  double bounds[ 6 ];
  actorPtr->GetBounds( bounds );

  const auto xMin = bounds [ 0 ];
  const auto xMax = bounds [ 1 ];

  const auto yMin = bounds [ 2 ];
  const auto yMax = bounds [ 3 ];

  const auto zMin = bounds [ 4 ];
  const auto zMax = bounds [ 5 ];

  vtkSmartPointer< vtkCubeSource > cubeSource = vtkSmartPointer< vtkCubeSource >::New();
  cubeSource->Update();
  cubeSource->SetCenter( (xMax + xMin)/2.0, (yMax + yMin)/2.0, (zMax + zMin)/2.0 );

  cubeSource->SetXLength( (xMax) - (xMin) );
  cubeSource->SetYLength( (yMax) - (yMin) );
  cubeSource->SetZLength( (zMax) - (zMin) );

  vtkSmartPointer< vtkPolyDataMapper > cubeMapper = vtkSmartPointer< vtkPolyDataMapper >::New();
  cubeMapper->SetInputConnection( cubeSource->GetOutputPort() );

  vtkSmartPointer< vtkActor > cubeActor = vtkSmartPointer< vtkActor >::New();
  cubeActor->SetMapper( cubeMapper );
  cubeActor->GetProperty()->SetOpacity( 0.45 );
  cubeActor->GetProperty()->SetColor( 0.4, 0.8, 1. );

  GetDefaultRenderer()->AddActor( cubeActor );

  boundingtBoxes.insert( actorPtr, cubeActor );
}

/*!
 * \brief vtkDoubleClickPickerTrackballCameraInterractor::removeBoundingBox
 * \param actorPtr
 */
void vtkDoubleClickPickerTrackballCameraInterractor::removeBoundingBox(vtkActor *actorPtr)
{
  auto it = boundingtBoxes.find( actorPtr );
  GetDefaultRenderer()->RemoveActor( it.value() );
  boundingtBoxes.remove( actorPtr );
}

/*!
 * \brief vtkDoubleClickPickerTrackballCameraInterractor::setOrRemoveBoundingBox
 * \param actorPtr
 */
void vtkDoubleClickPickerTrackballCameraInterractor::setOrRemoveBoundingBox(vtkActor *actorPtr)
{
  if ( actorPtr == nullptr ) {
    for ( auto it = boundingtBoxes.begin(); it != boundingtBoxes.end(); ++ it ) {
      GetDefaultRenderer()->RemoveActor( it.value() );
    }
    boundingtBoxes.clear();
    return;
  }

  auto boundingBoxIter = boundingtBoxes.begin();

  for ( ;boundingBoxIter != boundingtBoxes.end(); ++ boundingBoxIter ) {
    if ( boundingBoxIter.value() == actorPtr ) {
      break;
    }
  }

  const bool isBoundingBox = ( boundingtBoxes.end() != boundingBoxIter );
  if ( isBoundingBox )
  {
    // std::cout << "Remove box. actorPtr: " << actorPtr << std::endl;
    GetDefaultRenderer()->RemoveActor( actorPtr );
    boundingtBoxes.remove( boundingBoxIter.key() );
    return;
  }

  const auto hasBoubdingBox = ( boundingtBoxes.find( actorPtr ) != boundingtBoxes.end() );
  if ( ! hasBoubdingBox ) {
    addBoundingBox(actorPtr);
  }

}

/*!
 * \brief vtkDoubleClickPickerTrackballCameraInterractor::isBoundingBox
 * \return
 */
bool vtkDoubleClickPickerTrackballCameraInterractor::isBoundingBox(vtkActor *actorPtr)
{
  auto boundingBoxIter = boundingtBoxes.begin();
  for ( ; boundingBoxIter != boundingtBoxes.end(); ++ boundingBoxIter ) {
    if ( boundingBoxIter.value() == actorPtr ) {
      break;
    }
  }
  return ( boundingtBoxes.end() != boundingBoxIter );
}

vtkStandardNewMacro( vtkDoubleClickPickerTrackballCameraInterractor )
