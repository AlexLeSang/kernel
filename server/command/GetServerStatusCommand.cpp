#include "GetServerStatusCommand.hpp"

#include "SystemLoad.hpp"
#include "ThreadSaveLogger.hpp"

#include <memory>

#include <functions/node/MongoDBMaintenancer.hpp>

namespace kernel {
  namespace server {
    namespace command {

      /*!
       * \brief GetServerStatusCommand
       * \param socket
       * \return
       */
      bool GetServerStatusCommand(QTcpSocket &socket, std::atomic<bool> &)
      {
        utils::ThreadSaveLogger::addLineStatic( QString("GetServerStatusCommand") );

        // KernelServer* server = KernelServer::getInstance();

        ServerStatus serverStatus;

        serverStatus.setMongodbAddress( client::ServerDescription( KernelServer::mongodbServerAddress(), KernelServer::mongodbPortNumber() ) );
        serverStatus.setWorkgroupTag( KernelServer::getWorkgroupTag() );

        //        QVector< client::ServerDescription > workgroup = KernelServer::workgroup();
        //        serverStatus.setWorkgroup( workgroup );

        {
          // Create connection string
          std::ostringstream os;
          os << server::functions::node::replica_set_name;
          os << "/";
          os << KernelServer::mongodbServerAddress().toString().toStdString();
          os << ":";
          os << KernelServer::mongodbPortNumber();

          const std::string replSet = os.str();
          std::cout << "replSet: " << replSet << std::endl;

          // Create connection
          mongo::ScopedDbConnection scopedConnection ( replSet );

          if ( scopedConnection.ok() ) {
            const auto queyObj = BSON( "workgroupTag" << KernelServer::getWorkgroupTag().toStdString() );
            const auto maskField = mongo::fromjson( "{_id:0, posted:0, workgroupTag:0}" );

            QList< server::command::ServerStatus::WorkgroupGraphEdge > edgeList;

            auto cursor = scopedConnection->query( server::functions::node::kernel_workgroup_graph_ns,
                                                   mongo::Query( queyObj ),
                                                   0, 0,
                                                   &maskField,
                                                   mongo::QueryOption_SlaveOk | mongo::QueryOption_Exhaust  );
            while ( cursor->more() ) {
              const auto obj = cursor->next();
              const QString edgeFrom = QString( obj.getStringField( "edgeFrom" ) );
              const QString edgeTo = obj.getStringField( "edgeTo" );
              const quint32 avgPing = obj.getIntField( "avgPing" );
              edgeList.append( server::command::ServerStatus::WorkgroupGraphEdge( edgeFrom, edgeTo, avgPing ) );
            }

            serverStatus.setWorkgroupGraph( edgeList );
          }
          else {
            utils::ThreadSaveLogger::addLineStatic( QString("Access to MongoDB replica set ") + replSet.c_str() + "  was unsuccessful" );
          }

          scopedConnection.done();
        }


        QByteArray block;
        QDataStream out( &block, QIODevice::WriteOnly );
        out.setVersion( QDataStream::Qt_4_8 );
        out << (quint32)0;
        out << serverStatus;
        out.device()->seek( 0 );
        out << (quint32)( block.size() - sizeof(quint32) );

        socket.write( block );

        if ( ! socket.waitForBytesWritten() ) {
          utils::ThreadSaveLogger::addLineStatic( QString("PingPongCommand: ") + socket.errorString() );
          return false;
        }
        return true;
      }

      
    }
  }
}
