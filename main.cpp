#include <iostream>

int main(int argc, char *argv[])
{
  std::cout << "kernel" << std::endl;
  return 0;
}

/*
 *project(kernel)

cmake_minimum_required(VERSION 2.8)

find_package(VTK REQUIRED)
include(${VTK_USE_FILE})

if(${VTK_VERSION} VERSION_GREATER "6" AND VTK_QT_VERSION VERSION_GREATER "4")
  # Instruct CMake to run moc automatically when needed.
  set(CMAKE_AUTOMOC ON)
  find_package(Qt5Widgets REQUIRED QUIET)
else()
  find_package(Qt4 REQUIRED)
  include(${QT_USE_FILE})
endif()


include_directories(${CMAKE_CURRENT_SOURCE_DIR})

file(GLOB UI_FILES *.ui)
file(GLOB QT_WRAP *.hpp)
file(GLOB CXX_FILES *.cpp)

if(${VTK_VERSION} VERSION_GREATER "6" AND VTK_QT_VERSION VERSION_GREATER "4")
  qt5_wrap_ui(UISrcs ${UI_FILES} )
  # CMAKE_AUTOMOC in ON so the MocHdrs will be automatically wrapped.
  add_executable(SideBySideRenderWindowsQt MACOSX_BUNDLE
    ${CXX_FILES} ${UISrcs} ${QT_WRAP})
  qt5_use_modules(kernel Core Gui)
  target_link_libraries(kernel ${VTK_LIBRARIES})
else()
  QT4_WRAP_UI(UISrcs ${UI_FILES})
  QT4_WRAP_CPP(MOCSrcs ${QT_WRAP})
  add_executable(kernel MACOSX_BUNDLE ${CXX_FILES} ${UISrcs} ${MOCSrcs})

  if(VTK_LIBRARIES)
    if(${VTK_VERSION} VERSION_LESS "6")
      target_link_libraries(kernel ${VTK_LIBRARIES} QVTK)
    else()
      target_link_libraries(kernel ${VTK_LIBRARIES})
    endif()
  else()
    target_link_libraries(kernel vtkHybrid QVTK vtkViews ${QT_LIBRARIES})
  endif()
endif()

add_definitions(-std=c++0x)

 */

//#include <QCoreApplication>
//#include <QThreadPool>
//#include "TermetServer.hpp"
//#include "TermetClient/TermetClient.hpp"
//#include "ThreadSleep.hpp"

//class ClientStarter : public QRunnable
//{
//  void run()
//  {
//    termet::client::TermetClient client;
//    client.executeAndWait( termet::server::ServerCommands::SERVER_APPEARS, true );
//    ThreadSleep::msleep( 5 );
//    client.executeAndWait( termet::server::ServerCommands::TEST_TIME );
//    ThreadSleep::msleep( 5 );
//    client.executeAndWait( termet::server::ServerCommands::STOP_SERVER );
//  }
//};


//int main(int argc, char *argv[])
//{
//  QCoreApplication a( argc, argv );

//  auto & server = termet::server::TermetServer::getInstance();
//  if ( ! server.listen( QHostAddress::Any, termet::server::port ) ) {
//    qWarning() << "Server error: " << server.serverError();
//    return EXIT_FAILURE;
//  }
//  else {
//    qDebug() << "Server address: " << server.serverAddress().toString() << " Port: " << server.serverPort();
//  }

////  QThreadPool::globalInstance()->start( new ClientStarter() );
//  return a.exec();
//}


/*
#include <QtCore>
#include <QDebug>

enum PrinterTypes
{
  BASE,
  A,
  B
};

struct Printer
{
  virtual void operator()() = 0;
};


struct APrinter : public Printer
{
  virtual void operator ()()
  {
    qDebug() << "A";
  }
};

struct BPrinter : public Printer
{
  virtual void operator ()()
  {
    qDebug() << "B";
  }
};


template< PrinterTypes >
struct get_printer
{
  typedef Printer type;
};


template<>
struct get_printer< A >
{
  typedef APrinter type;
};

template<>
struct get_printer< B >
{
  typedef BPrinter type;
};

int main(int argc, char *argv[])
{
  Q_UNUSED(argc);
  Q_UNUSED(argv);

  get_printer<A>::type printer;
  printer();

  return 0;
}
*/
