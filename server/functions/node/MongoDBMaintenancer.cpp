#include "MongoDBMaintenancer.hpp"

#include <vector>

#include <../utils/ThreadSaveLogger.hpp>

namespace kernel {
  namespace server {
    namespace functions {
      namespace node {

        /*!
         * \brief MongoDBMaintenancer::MongoDBMaintenancer
         * \param parent
         * \param address
         * \param port
         */
        MongoDBMaintenancer::MongoDBMaintenancer(QObject *parent, const QHostAddress &address, const quint32 port) :
          QObject( parent ),
          mongodbAddress_( address ),
          port_( port ),
          parametersChanged( true )
        {
        }

        /*!
         * \brief MongoDBMaintenancer::~MongoDBMaintenancer
         */
        MongoDBMaintenancer::~MongoDBMaintenancer()
        {
        }

        /*!
         * \brief MongoDBMaintenancer::defineRS
         * \return
         */
        bool MongoDBMaintenancer::defineRS()
        {
          utils::ThreadSaveLogger::addLineStatic( QString( "MongoDBMaintenancer::defineRS" ) );

          const QHostInfo mongodbAddressHostInfo = QHostInfo::fromName( mongodbAddress_.toString() );
          if ( mongodbAddressHostInfo.error() != QHostInfo::NoError ) {
            if ( mongodbAddressHostInfo.error() == QHostInfo::HostNotFound ) {
              const auto qErrorString = QString( "MongoDBMaintenancer::defineRS: Host not found" );
              utils::ThreadSaveLogger::addLineStatic( qErrorString );
              return false;
            }
            if ( mongodbAddressHostInfo.error() == QHostInfo::UnknownError ) {
              const auto qErrorString = QString( "MongoDBMaintenancer::defineRS: Unknown host address error" );
              utils::ThreadSaveLogger::addLineStatic( qErrorString );
              return false;
            }
            return false;
          }

          hostPortVector.clear();
          hostPortVector.push_back( mongo::HostAndPort( mongodbAddress_.toString().toStdString(), port_ ) );
          return true;
        }

        /*!
         * \brief MongoDBMaintenancer::mongodbAddress
         * \return
         */
        QHostAddress MongoDBMaintenancer::mongodbAddress() const
        {
          return mongodbAddress_;
        }

        /*!
         * \brief MongoDBMaintenancer::setMongodbAddress
         * \param mongodbAddress
         */
        void MongoDBMaintenancer::setMongodbAddress(const QHostAddress &mongodbAddress)
        {
          mongodbAddress_ = mongodbAddress;
          parametersChanged = true;
          defineRS();
        }

        /*!
         * \brief MongoDBMaintenancer::port
         * \return
         */
        quint32 MongoDBMaintenancer::port() const
        {
          return port_;
        }

        /*!
         * \brief MongoDBMaintenancer::setPort
         * \param port
         */
        void MongoDBMaintenancer::setPort(const quint32 &port)
        {
          port_ = port;
          parametersChanged = true;
          defineRS();
        }

        /*!
         * \brief MongoDBMaintenancer::run
         */
        void MongoDBMaintenancer::run()
        {
          mongodbMaintenance();
        }

        /*!
         * \brief MongoDBMaintenancer::createConnectionToRS
         * \return
         */
        std::shared_ptr< mongo::DBClientReplicaSet > MongoDBMaintenancer::createConnectionToRS()
        {
          static std::shared_ptr< mongo::DBClientReplicaSet > connectionPtr_;

          if ( parametersChanged ) {
            utils::ThreadSaveLogger::addLineStatic( QString( "MongoDBMaintenancer::createConnectionToRS: creating connection..." ) );

            connectionPtr_ = decltype(connectionPtr_)( new mongo::DBClientReplicaSet( replica_set_name, hostPortVector ) );

            try {
              const bool connResult = connectionPtr_->connect();
              if ( !connResult ) {
                const auto qErrorString = "MongoDBMaintenancer::createConnectionToRS: MongoDB connection error string: " + QString( connectionPtr_->getLastError().c_str() );
                utils::ThreadSaveLogger::addLineStatic( qErrorString );
                return std::shared_ptr< mongo::DBClientReplicaSet >();
              }
            }
            catch( const mongo::DBException &e ) {
              const auto qErrorString = QString( "MongoDBMaintenancer::createConnectionToRS: MongoDB connection error string: " ) + "MongoDB caught " + QString( e.what() );
              utils::ThreadSaveLogger::addLineStatic( qErrorString );
              return std::shared_ptr< mongo::DBClientReplicaSet >();
            }

            parametersChanged = false;
            utils::ThreadSaveLogger::addLineStatic( QString( "MongoDBMaintenancer::createConnectionToRS: connection created" ) );
          }

          return connectionPtr_;
        }

        /*!
         * \brief MongoDBMaintenancer::enshureIndexMaster
         * \param connection
         */
        void MongoDBMaintenancer::enshureIndexMaster(mongo::DBClientBase* connection)
        {
          utils::ThreadSaveLogger::addLineStatic( QString( "MongoDBMaintenancer::enshureIndexMaster;: enshing indexes..." ) );

          // kernel_nodes_ns
          connection->ensureIndex( kernel_nodes_ns,
                                   mongo::fromjson( "{address:1, port:1, workgroupTag:1}" ),
                                   true );

          connection->ensureIndex( kernel_nodes_ns,
                                   mongo::fromjson( "{lastHeartbeat:1}" ),
                                   false, "", true, false, -1, kernel_nodes_ttl );

          // kernel_workgroup_graph_ns
          connection->ensureIndex( kernel_workgroup_graph_ns,
                                   mongo::fromjson( "{workgroupTag:1,edgeFrom:1,edgeTo:1}" ),
                                   true );

          connection->ensureIndex( kernel_workgroup_graph_ns,
                                   mongo::fromjson( "{posted:1}" ),
                                   false, "", true, false, -1, kernel_nodes_ttl );

          utils::ThreadSaveLogger::addLineStatic( QString( "MongoDBMaintenancer::enshureIndexMaster: indexes on master enshured" ) );
        }

        /*!
         * \brief MongoDBMaintenancer::enshureIndexes
         * \param connectionPtr_
         * \return
         */
        bool MongoDBMaintenancer::enshureIndexes(std::shared_ptr< mongo::DBClientReplicaSet > connectionPtr_)
        {
          utils::ThreadSaveLogger::addLineStatic( QString( "MongoDBMaintenancer::enshureIndexes: getting connection..." ) );

          if ( ! connectionPtr_->isStillConnected() ) {
            const auto qErrorString = "MongoDBMaintenancer::enshureIndexes: MongoDB connection is disconnected: " + QString( connectionPtr_->getLastError().c_str() );
            utils::ThreadSaveLogger::addLineStatic( qErrorString );
            return false;
          }

          bool isMaster = false;
          {
            const bool commRes = connectionPtr_->isMaster( isMaster );
            if ( ! commRes ) {
              const auto qErrorString = "MongoDBMaintenancer::enshureIndexes: MongoDB connection is disconnected: " + QString( connectionPtr_->getLastError().c_str() );
              utils::ThreadSaveLogger::addLineStatic( qErrorString );
              return false;
            }
          }

          if ( !isMaster ) {
            const auto qErrorString = QString( "MongoDBMaintenancer::enshureIndexes: MongoDB connection is not master connection, trying obtain the master connection..." );
            utils::ThreadSaveLogger::addLineStatic( qErrorString );
            mongo::DBClientConnection& connection = connectionPtr_->masterConn();
            enshureIndexMaster( &connection );
          }
          else {
            utils::ThreadSaveLogger::addLineStatic( QString( "MongoDBMaintenancer::enshureIndexes: MongoDB connection is master" ) );
            enshureIndexMaster( connectionPtr_.get() );
          }

          utils::ThreadSaveLogger::addLineStatic( QString( "MongoDBMaintenancer::enshureIndexes: indexes enshured" ) );
          return true;
        }

        /*!
         * \brief MongoDBMaintenancer::mongodbMaintenance
         * \return
         */
        bool MongoDBMaintenancer::mongodbMaintenance()
        {
          const auto r1 = defineRS();
          auto conn = createConnectionToRS();
          const auto r2 = enshureIndexes( conn );
          return ( r1 && r2 );
        }

      }
    }
  }
}
