#ifndef THREADSAVELOGGER_HPP
#define THREADSAVELOGGER_HPP

#include <QFile>
#include <QMutex>
#include <QObject>
#include <QTextStream>

namespace kernel {
  namespace utils {

    /*!
     * \brief The ThreadSaveLogger class
     */
    class ThreadSaveLogger : public QObject
    {
      Q_OBJECT

    public:
      static ThreadSaveLogger &getInstance();
      static void addLineStatic(const QString line);

      static QString logPath();
      static void setLogPath(const QString &logPath);

      static QString logFilename();
      static void setLogFilename(const QString &logFilename);

    private:
      Q_DISABLE_COPY(ThreadSaveLogger)

      ThreadSaveLogger(QObject *parent = nullptr);
      void init();

    private:
      QMutex m;
      QFile f;
      QTextStream stream;

      static bool activated;
      static QString logPath_;
      static QString logFilename_;
    };
  }
}

#endif // THREADSAVELOGGER_HPP
