#include "ResultsViewWidgetDesignerPlugin.hpp"

#include <ResultsViewWidget.hpp>

#include <QtPlugin>

ResultsViewWidgetDesignerPlugin::ResultsViewWidgetDesignerPlugin(QObject *parent) : QObject( parent ), initialized( false )
{}

QString ResultsViewWidgetDesignerPlugin::name() const
{
  return "ResultsViewWidget";
}

QString ResultsViewWidgetDesignerPlugin::group() const
{
  return "QVTK";
}

QString ResultsViewWidgetDesignerPlugin::toolTip() const
{
  return "Results view widget";
}

QString ResultsViewWidgetDesignerPlugin::whatsThis() const
{
  return "Results view widget";
}

QString ResultsViewWidgetDesignerPlugin::includeFile() const
{
  return "ResultsViewWidget.hpp";
}

QIcon ResultsViewWidgetDesignerPlugin::icon() const
{
  return QIcon();
}

bool ResultsViewWidgetDesignerPlugin::isContainer() const
{
  return false;
}

QWidget *ResultsViewWidgetDesignerPlugin::createWidget(QWidget *parent)
{
  return new ResultsViewWidget( parent );
}

bool ResultsViewWidgetDesignerPlugin::isInitialized() const
{
  return initialized;
}

void ResultsViewWidgetDesignerPlugin::initialize(QDesignerFormEditorInterface *)
{
  if ( initialized ) {
    return;
  }

  initialized = true;
}

QString ResultsViewWidgetDesignerPlugin::domXml() const
{
  /*
  return
  "<ui language=\"c++\>\n"
   "<widget class=\"QWidget\" name=\"visualisationWidget\">\n"
    "<property name=\"geometry\">\n"
     "<rect>\n"
      "<x>0</x>\n"
      "<y>0</y>\n"
      "<width>1024</width>\n"
      "<height>768</height>\n"
     "</rect>\n"
    "</property>\n"
    "<property name=\"sizePolicy\">\n"
     "<sizepolicy hsizetype=\"MinimumExpanding\" vsizetype=\"MinimumExpanding\">\n"
      "<horstretch>0</horstretch>\n"
      "<verstretch>0</verstretch>\n"
     "</sizepolicy>\n"
    "</property>\n"
    "<property name=\"minimumSize\">\n"
     "<size>\n"
      "<width>1024</width>\n"
      "<height>768</height>\n"
     "</size>\n"
    "</property>\n"
   "</widget>\n"
   "</ui>\n";
   */

  return "<ui language=\"c++\">\n"
              " <widget class=\"ResultsViewWidget\" name=\"resultsViewWidget\">\n"
              "  <property name=\"geometry\">\n"
              "   <rect>\n"
              "    <x>0</x>\n"
              "    <y>0</y>\n"
              "    <width>1024</width>\n"
              "    <height>768</height>\n"
              "   </rect>\n"
              "  </property>\n"
              "  <property name=\"toolTip\" >\n"
              "   <string>Results view widget</string>\n"
              "  </property>\n"
              "  <property name=\"whatsThis\" >\n"
              "   <string>Results view widget</string>\n"
              "  </property>\n"
              " </widget>\n"
              "<resources/>\n"
              "<connections/>\n"
              "<slots>\n"
              " <slot>addSource()</slot>\n"
              " <slot>selectActor(QModelIndex)</slot>\n"
              "</slots>\n"
              "</ui>\n";
}

QString ResultsViewWidgetDesignerPlugin::codeTemplate() const
{
  return "";
}

Q_EXPORT_PLUGIN2( resultsviewwidget, ResultsViewWidgetDesignerPlugin )
