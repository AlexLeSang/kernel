#include "ClientCommand.hpp"

#include "StopServerClientCommand.hpp"
#include "ServerStatusClientCommand.hpp"
#include "PingPongClientCommand.hpp"

namespace kernel {
  namespace client {
    namespace command {

      ClientCommand *CommandProcessor(const server::ServerCommands command)
      {
        switch ( command ) {
          case server::ServerCommands::STOP_SERVER:
            return new StopServerClientCommand();
            break;

          case server::ServerCommands::GET_SERVER_STATUS:
            return new ServerStatusClientCommand();
            break;

          case server::ServerCommands::PING_PONG:
            return new PingPongClientCommand();
            break;

          case server::ServerCommands::FRAMEWORK_TEST_COMMAND:
            return new FrameworkTestClientCommand();
            break;

          default:
            return nullptr;
            break;
        }
      }

      bool ClientCommandRunner(std::shared_ptr< kernel::client::command::ClientCommand > pClientCommand, ServerDescription sd)
      {
        // qWarning() << "ClientCommandRunner";
        Q_ASSERT( pClientCommand );

        pClientCommand->setServerDescription( sd );
        bool result = (*pClientCommand)();
        // const auto commandName = (*pClientCommand).commandName();
        // qWarning() << "commandName: " << quint32(commandName); // TODO remove debug output
        // qWarning() << "result: " << result; // TODO remove debug output
        return result;
      }

    }
  }
}



#include "KernelClient.hpp"
#include <iostream>

namespace test {

  bool FrameworkTestCommandTest()
  {
    std::cout << "FrameworkTestCommandTest()" << std::endl;

    kernel::client::KernelClient* client = kernel::client::KernelClient::getInstance();

    kernel::client::ServerDescription sd;
    sd.address = QHostAddress( QHostAddress::LocalHost );
    // sd.address = QHostAddress( "192.168.0.9" );
    // sd.address = QHostAddress( "192.168.0.10" );
    sd.port = 31017;
    client->setServerDescription( sd );

    std::shared_ptr< kernel::client::command::ClientCommand > commandProcessor( kernel::client::command::CommandProcessor( kernel::server::ServerCommands::FRAMEWORK_TEST_COMMAND ) );

    kernel::client::command::FrameworkTestClientCommand* frameworkTestProcessor = static_cast< kernel::client::command::FrameworkTestClientCommand* > ( commandProcessor.get() );

    frameworkTestProcessor->setCommandResult( false );
    frameworkTestProcessor->setTestDelay( 30 );

    auto future = (*client).execute( commandProcessor );

    future.waitForFinished();
    const auto futureResult = future.result();
    std::cout << "futureResult: " << futureResult << std::endl;

    return true;
  }
}
