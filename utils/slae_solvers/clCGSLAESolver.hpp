#ifndef CLCGSLAESOLVER_HPP
#define CLCGSLAESOLVER_HPP

#include <slae_solvers/AbstractSLAESolver.hpp>

class clCGSLAESolver : public AbstractSLAESolver
{
  // AbstractSLAESolver interface
public:
  virtual bool operator ()(getfem::model &model, gmm::iteration &iter);
};

#endif // CLCGSLAESOLVER_HPP
