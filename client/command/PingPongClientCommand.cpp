#include "PingPongClientCommand.hpp"

#include <boost/timer.hpp>

#include <limits>

#include <QTime>
#include <QVector>

namespace kernel {
  namespace client {
    namespace command {

      qint8 randomByte()
      {
        qsrand( QTime::currentTime().msec() );
        const auto min = std::numeric_limits< quint8 >::min();
        const auto max = std::numeric_limits< quint8 >::max();
        return ( ( qrand() % ( max - min + 1 ) ) + min );
      }

      /*!
       * \brief PingPongClientCommand::operator ()
       * \return
       */
      bool PingPongClientCommand::operator ()()
      {
        // qWarning() << "PingPongClientCommand::operator ()()";

        if ( description.address == QHostAddress::Null ) {
          // qWarning() << "PingPongClientCommand: uninitialized server";
          // TODO log it
          return false;
        }

        // qWarning() << "Pinging host: " << description; // TODO remove debug output

        QTcpSocket socket;
        socket.connectToHost( description.address, description.port );

        if ( ! socket.waitForConnected( timeout ) ) {
          // qWarning() << "PingPongClientCommand: " << socket.errorString();
          // TODO log it
          return false;
        }


        // qWarning() << "Connected to host: " << description; // TODO remove debug output
        // Write to server
        {
          QByteArray block;
          QDataStream out( &block, QIODevice::WriteOnly );
          out.setVersion( QDataStream::Qt_4_8 );
          out << (quint32)0;
          out << (quint32)server::ServerCommands::PING_PONG;
          out << numberOfPackets;
          out << packetSize;
          out.device()->seek(0);
          out << (quint32)( block.size() - sizeof(quint32) );

          socket.write( block );

          if ( ! socket.waitForBytesWritten() ) {
            // qWarning() << "PingPongClientCommand: " << socket.errorString();
            // TODO log it
            return false;
          }
        }

        // Generate block of desired size
        static QByteArray randomArr( packetSize, '\0' );
        {
          static bool generated = false;
          if ( !generated ) {
            for ( quint32 i = 0; i < packetSize; ++ i ) {
              randomArr[ i ] = randomByte();
            }
            generated = true;
          }
        }

        QVector< int > elapsedTime;
        elapsedTime.reserve( numberOfPackets );
        QTime packedSendTimer;

        for ( quint32 packetCounter = 0; packetCounter < numberOfPackets; ++ packetCounter ) {

          packedSendTimer.restart();
          {
            // Write to server
            {
              QByteArray block;
              QDataStream out( &block, QIODevice::WriteOnly );
              out.setVersion( QDataStream::Qt_4_8 );
              out << (quint32)0;
              out << packetCounter;
              out << randomArr;
              out.device()->seek(0);
              out << (quint32)( block.size() - sizeof(quint32) );

              socket.write( block );

              if ( ! socket.waitForBytesWritten() ) {
                // qWarning() << "PingPongClientCommand: " << socket.errorString();
                // TODO log it
                return false;
              }
            }

            // Receive sent packet back
            {
              // Receive from server
              while ( socket.bytesAvailable() < (qint64)sizeof(quint32) ) {
                if ( (! socket.waitForReadyRead( timeout ) ) && ( socket.state() != QAbstractSocket::SocketState::ConnectedState ) ) {
                  // qWarning() << "PingPongClientCommand: " << socket.errorString();
                  // TODO log it
                  return false;
                }
              }

              quint32 blockSize;
              quint32 receivedPacketCounter;

              QDataStream in( &socket );
              in.setVersion( QDataStream::Qt_4_8 );
              in >> blockSize;

              while ( socket.bytesAvailable() < blockSize ) {
                if ( ! socket.waitForReadyRead( timeout ) ) {
                  // qWarning() << "PingPongClientCommand: " << socket.errorString();
                  // TODO log it
                  return false;
                }
              }
              in >> receivedPacketCounter;
              // qWarning() << "receivedPacketCounter: " << receivedPacketCounter;

              QByteArray receivedRandomArr;
              in >> receivedRandomArr;
              // qWarning() << "receivedRandomArr.size(): " << receivedRandomArr.size();
            }

            elapsedTime.push_back( packedSendTimer.elapsed() );

          }
        }

        const auto max = *std::max_element( elapsedTime.begin(), elapsedTime.end() );
        const auto min = *std::min_element( elapsedTime.begin(), elapsedTime.end() );
        const auto avg = std::accumulate( elapsedTime.begin(), elapsedTime.end(), 0.0 ) / elapsedTime.size();

        if ( pingPongResultPtr != nullptr ) {
          pingPongResultPtr->setMin( min );
          pingPongResultPtr->setMax( max );
          pingPongResultPtr->setAvg( avg );
        }
        // qWarning() << "Max elapsedTime: " << max;
        // qWarning() << "Min elapsedTime: " << min;
        // qWarning() << "Avg elapsedTime: " << avg;

        return true;
      }

      /*!
       * \brief PingPongClientCommand::commandName
       * \return
       */
      server::ServerCommands PingPongClientCommand::commandName() const
      {
        return server::ServerCommands::PING_PONG;
      }

      /*!
       * \brief PingPongClientCommand::getNumberOfPackets
       * \return
       */
      quint32 PingPongClientCommand::getNumberOfPackets() const
      {
        return numberOfPackets;
      }

      /*!
       * \brief PingPongClientCommand::setNumberOfPackets
       * \param value
       */
      void PingPongClientCommand::setNumberOfPackets(const quint32 &value)
      {
        numberOfPackets = value;
      }

      /*!
       * \brief PingPongClientCommand::getPacketSize
       * \return
       */
      quint32 PingPongClientCommand::getPacketSize() const
      {
        return packetSize;
      }

      /*!
       * \brief PingPongClientCommand::setPacketSize
       * \param value
       */
      void PingPongClientCommand::setPacketSize(const quint32 &value)
      {
        packetSize = value;
      }

      /*!
       * \brief PingPongClientCommand::getTimeout
       * \return
       */
      quint32 PingPongClientCommand::getTimeout() const
      {
        return timeout;
      }

      /*!
       * \brief PingPongClientCommand::setTimeout
       * \param value
       */
      void PingPongClientCommand::setTimeout(const quint32 &value)
      {
        timeout = value;
      }

      /*!
       * \brief PingPongClientCommand::getPingPongResultPtr
       * \return
       */
      PingPongResult *PingPongClientCommand::getPingPongResultPtr() const
      {
        return pingPongResultPtr;
      }
      
      /*!
       * \brief PingPongClientCommand::setPingPongResultPtr
       * \param value
       */
      void PingPongClientCommand::setPingPongResultPtr(PingPongResult *value)
      {
        pingPongResultPtr = value;
      }

    }
  }
}
