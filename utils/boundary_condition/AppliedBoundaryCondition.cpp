#include "AppliedBoundaryCondition.hpp"

#include <QDebug>
#include <QFile>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXMLUnstructuredGridWriter.h>


constexpr auto eps = std::numeric_limits<double>::epsilon();

/*!
 * \brief AppliedBoundaryCondition::AppliedBoundaryCondition
 */
AppliedBoundaryCondition::AppliedBoundaryCondition()
{
  selectedGrid = vtkSmartPointer< vtkUnstructuredGrid >::New();
}

/*!
 * \brief AppliedBoundaryCondition::getConditionType
 * \return
 */
BOUNDARY_CONDITION_TYPE AppliedBoundaryCondition::getConditionType() const
{
  return boundaryCondition->getBoundaryConditionType();
}

/*!
 * \brief AppliedBoundaryCondition::getName
 * \return
 */
QString AppliedBoundaryCondition::getName() const
{
  return QString( boundaryCondition->getName().c_str() );
}

/*!
 * \brief AppliedBoundaryCondition::setName
 * \param value
 */
void AppliedBoundaryCondition::setName(const QString &value)
{
  boundaryCondition->setName( value.toStdString() );
}

/*!
 * \brief AppliedBoundaryCondition::getSelectedGrid
 * \return
 */
vtkSmartPointer<vtkUnstructuredGrid> AppliedBoundaryCondition::getSelectedGrid() const
{
  return selectedGrid;
}

/*!
 * \brief AppliedBoundaryCondition::setSelectedGrid
 * \param value
 */
void AppliedBoundaryCondition::setSelectedGrid(const vtkSmartPointer<vtkUnstructuredGrid> &value)
{
  selectedGrid = value;
}

/*!
 * \brief AppliedBoundaryCondition::getColor
 * \return
 */
QColor AppliedBoundaryCondition::getColor() const
{
  return color;
}

/*!
 * \brief AppliedBoundaryCondition::setColor
 * \param value
 */
void AppliedBoundaryCondition::setColor(const QColor &value)
{
  color = value;
}


/*!
 * \brief AppliedBoundaryCondition::getBoundaryCondition
 * \return
 */
std::shared_ptr<AbstractBoundaryCondition> AppliedBoundaryCondition::getBoundaryCondition() const
{
  return std::shared_ptr<AbstractBoundaryCondition>( boundaryCondition );
}

/*!
 * \brief AppliedBoundaryCondition::setBoundaryCondition
 * \param value
 */
void AppliedBoundaryCondition::setBoundaryCondition(std::shared_ptr<AbstractBoundaryCondition>& value)
{
  boundaryCondition = value;
}

/*!
 * \brief AppliedBoundaryCondition::checkCompatibility
 * \param polyData
 * \return
 */
bool AppliedBoundaryCondition::checkCompatibility(vtkPolyData *polyData)
{
  auto same = [&](const double &p1, const double &p2) {
    return (fabs(p1 - p2) <= eps);
  };

  bool isInPolyDataPoints = true;

  if ( selectedGrid->GetNumberOfCells() && selectedGrid->GetNumberOfPoints() ) {

    const auto selectedGridNumberOfPoints = selectedGrid->GetNumberOfPoints();
    vtkPoints* points = polyData->GetPoints();

    auto isInPoints = [&](double p[]) -> bool {
      bool found = false;
      for ( auto j = 0; j < points->GetNumberOfPoints(); ++ j ) {
        double lp[ 3 ];
        points->GetPoint( j, lp );

        if ( same( p[ 0 ], lp[ 0 ] ) && same( p[ 1 ], lp[ 1 ] ) && same( p[ 2 ], lp[ 2 ] ) ) {
          found = true;
          break;
        }
      }
      return found;
    };

    for ( auto i = 0; i < selectedGridNumberOfPoints; ++ i ) {
      double p[ 3 ];
      selectedGrid->GetPoint( i, p );
      if ( ! isInPoints( p ) ) {
        isInPolyDataPoints = false;
        break;
      }
    }

  }

  return isInPolyDataPoints;
}


/*!
 * \brief AppliedBoundaryCondition::readFromFile
 * \param fileName
 * \return
 */
AppliedBoundaryCondition AppliedBoundaryCondition::readFromFile(const QString &fileName)
{
  assert( !fileName.isEmpty() );
  AppliedBoundaryCondition applyedBoundaryCondition;
  QFile file( fileName );
  if( file.open( QIODevice::ReadOnly ) ) {
    QDataStream stream( &file );
    stream.setVersion( QDataStream::Qt_4_8 );
    stream >> applyedBoundaryCondition;
    file.close();
  }
  return applyedBoundaryCondition;
}

/*!
 * \brief AppliedBoundaryCondition::writeToFile
 * \param condition
 * \param fileName
 * \return
 */
bool AppliedBoundaryCondition::writeToFile(const AppliedBoundaryCondition &condition, const QString &fileName)
{
  assert( ! fileName.isEmpty() );
  QFile file( fileName );
  if( file.open( QIODevice::WriteOnly ) ) {
    QDataStream stream( &file );
    stream.setVersion( QDataStream::Qt_4_8 );
    stream << condition;
    file.close();
    return true;
  }
  else {
    return false;
  }
}


/*!
 * \brief AppliedBoundaryCondition::getApplicableNodes
 * \return
 */
ApplicableNodes AppliedBoundaryCondition::getApplicableNodes() const
{
  return ApplicableNodes( selectedGrid );
}


/*!
 * \brief operator <<
 * \param ds
 * \param grid
 * \return
 */
QDataStream &operator<<(QDataStream &ds, const vtkSmartPointer<vtkUnstructuredGrid> &grid)
{
  vtkSmartPointer< vtkUnstructuredGridWriter > stringWriter = vtkSmartPointer< vtkUnstructuredGridWriter >::New();
  stringWriter->SetInput( grid );
  stringWriter->SetFileTypeToBinary();
  stringWriter->WriteToOutputStringOn();
  stringWriter->Write();

  const char* gridString = stringWriter->GetOutputString();
  const unsigned int gridStringSize = stringWriter->GetOutputStringLength();

  assert( gridStringSize );

  ds << gridStringSize;
  ds.writeBytes( gridString, gridStringSize );
  return ds;
}


/*!
 * \brief operator >>
 * \param ds
 * \param grid
 * \return
 */
QDataStream &operator>>(QDataStream &ds, vtkSmartPointer<vtkUnstructuredGrid> &grid)
{
  unsigned int gridStringSize = 0;
  ds >> gridStringSize;
  assert( gridStringSize );

  char* gridString = new char[ gridStringSize ];
  ds.readBytes( gridString, gridStringSize );

  vtkSmartPointer< vtkUnstructuredGridReader > stringReader = vtkSmartPointer< vtkUnstructuredGridReader >::New();
  stringReader->ReadFromInputStringOn();
  stringReader->SetBinaryInputString( gridString, gridStringSize );
  stringReader->Update();

  grid = stringReader->GetOutput();

  delete [] gridString;

  return ds;
}


/*!
 * \brief operator <<
 * \param ds
 * \param condition
 * \return
 */
QDataStream &operator<<(QDataStream &ds, const AppliedBoundaryCondition &condition)
{
  auto shared = condition.getBoundaryCondition();
  AbstractBoundaryCondition* aCond = shared.get();
  BOUNDARY_CONDITION_TYPE condType = aCond->getBoundaryConditionType();
  ds << (quint32)condType;
  switch ( condType ) {
    case BOUNDARY_CONDITION_TYPE::DIRICHLET_BOUNDARY_CONDITION:
      {
        DirihletBoundaryCondition* dirihletBC = static_cast< DirihletBoundaryCondition* >( aCond );
        ds << *dirihletBC;
      }
      break;
    case BOUNDARY_CONDITION_TYPE::NEUMANN_BOUNDARY_CONDITION:
      {
        NeumannBoundaryCondition* neumannBC = static_cast< NeumannBoundaryCondition* >( aCond );
        ds << *neumannBC;
      }
      break;
    default:
      break;
  }

  ds << condition.getColor();
  ds << condition.getSelectedGrid();
  return ds;
}


/*!
 * \brief operator >>
 * \param ds
 * \param condition
 * \return
 */
QDataStream &operator>>(QDataStream &ds, AppliedBoundaryCondition &condition)
{
  BOUNDARY_CONDITION_TYPE condType;
  quint32 condType1;
  ds >> condType1;
  condType = BOUNDARY_CONDITION_TYPE( condType1 );
  switch ( condType ) {
    case BOUNDARY_CONDITION_TYPE::DIRICHLET_BOUNDARY_CONDITION:
      {
        DirihletBoundaryCondition* dirihletBC = new DirihletBoundaryCondition;
        ds >> *dirihletBC;
        std::shared_ptr< AbstractBoundaryCondition > aCond( dirihletBC );
        condition.setBoundaryCondition( aCond );
      }
      break;
    case BOUNDARY_CONDITION_TYPE::NEUMANN_BOUNDARY_CONDITION:
      {
        NeumannBoundaryCondition* neumannBC = new NeumannBoundaryCondition;
        ds >> *neumannBC;
        std::shared_ptr< AbstractBoundaryCondition > aCond( neumannBC );
        condition.setBoundaryCondition( aCond );
      }
      break;
    default:
      break;
  }

  QColor color;
  ds >> color;
  condition.setColor( color );

  vtkSmartPointer< vtkUnstructuredGrid > grid = vtkSmartPointer< vtkUnstructuredGrid >::New();
  ds >> grid;
  condition.setSelectedGrid( grid );
  return ds;
}


bool test::ApplyedBoundaryConditionTest(int argc, char *argv[])
{
  std::cout << "ApplicableNodesContainerTest()" << std::endl;
  {
    AppliedBoundaryCondition applyedCondition;
    assert( argc == 2 );
    std::string inputFilename = argv[ 1 ];

    {
      vtkSmartPointer< vtkXMLUnstructuredGridReader > xmlReader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
      xmlReader->SetFileName( inputFilename.c_str() );
      xmlReader->Update();
      vtkSmartPointer< vtkUnstructuredGrid > grid = xmlReader->GetOutput();
      assert( grid->GetNumberOfPoints() );
      assert( grid->GetNumberOfCells() );
      applyedCondition.setSelectedGrid( grid );
    }

    {
      NeumannBoundaryCondition* neumannBC = new NeumannBoundaryCondition;
      neumannBC->setCondition( 42, 0 );
      std::shared_ptr< AbstractBoundaryCondition > aCond( neumannBC );
      applyedCondition.setBoundaryCondition( aCond );
    }

    {
      QColor color = Qt::red;
      applyedCondition.setColor( color );
    }

    QByteArray buffer;
    { // Serialize
      QDataStream ostream( &buffer, QIODevice::WriteOnly );
      ostream << applyedCondition;
    }

    qDebug() << "buffer.size(): " << buffer.size();

    AppliedBoundaryCondition newApplyedCondition;
    {
      QDataStream istream( &buffer, QIODevice::ReadOnly );
      istream >> newApplyedCondition;
    }

    assert( applyedCondition.getColor() == newApplyedCondition.getColor() );
    assert( applyedCondition.getSelectedGrid()->GetNumberOfCells() == newApplyedCondition.getSelectedGrid()->GetNumberOfCells() );
    assert( applyedCondition.getSelectedGrid()->GetNumberOfPoints() == newApplyedCondition.getSelectedGrid()->GetNumberOfPoints() );

    assert( *applyedCondition.getBoundaryCondition().get() == *newApplyedCondition.getBoundaryCondition().get() );
  }


  /*
  {
    assert( argc == 3 );
    std::string inputFilename = argv[ 1 ];

    vtkSmartPointer< vtkXMLUnstructuredGridReader > xmlReader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
    xmlReader->SetFileName( inputFilename.c_str() );
    xmlReader->Update();
    vtkSmartPointer< vtkUnstructuredGrid > grid = xmlReader->GetOutput();
    assert( grid->GetNumberOfPoints() );
    assert( grid->GetNumberOfCells() );

    QByteArray buffer;
    { // Serialize
      QDataStream ostream( &buffer, QIODevice::WriteOnly );
      ostream << grid;
    }


    vtkSmartPointer< vtkUnstructuredGrid > newGrid = vtkSmartPointer< vtkUnstructuredGrid >::New();
    {
      QDataStream istream( &buffer, QIODevice::ReadOnly );
      istream >> newGrid;
    }

    assert( newGrid->GetNumberOfCells() == grid->GetNumberOfCells() );
    assert( newGrid->GetNumberOfPoints() == grid->GetNumberOfPoints() );

    vtkSmartPointer< vtkXMLUnstructuredGridWriter > writer = vtkSmartPointer< vtkXMLUnstructuredGridWriter >::New();
    writer->SetFileName( argv[ 2 ] );
    writer->SetInput( newGrid );
    writer->Write();
  }
  */

  /*
  {
    assert( argc == 2 );
    std::string inputFilename = argv[ 1 ];

    vtkSmartPointer< vtkXMLUnstructuredGridReader > xmlReader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
    xmlReader->SetFileName( inputFilename.c_str() );
    xmlReader->Update();
    vtkSmartPointer< vtkUnstructuredGrid > grid = xmlReader->GetOutput();
    assert( grid->GetNumberOfPoints() );
    assert( grid->GetNumberOfCells() );

    vtkSmartPointer< vtkUnstructuredGridWriter > stringWriter = vtkSmartPointer< vtkUnstructuredGridWriter >::New();
    stringWriter->SetInput( grid );
    stringWriter->WriteToOutputStringOn();
    stringWriter->Write();
    const char* gridString = stringWriter->GetOutputString();
    const std::size_t gridStringSize = stringWriter->GetOutputStringLength();
    assert( gridStringSize );

    vtkSmartPointer< vtkUnstructuredGridReader > stringReader = vtkSmartPointer< vtkUnstructuredGridReader >::New();
    stringReader->ReadFromInputStringOn();
    stringReader->SetInputString( gridString, gridStringSize );
    stringReader->Update();

    vtkSmartPointer< vtkUnstructuredGrid > readGrid = stringReader->GetOutput();
    assert( readGrid->GetNumberOfCells() == readGrid->GetNumberOfCells() );
    assert( readGrid->GetNumberOfPoints() == readGrid->GetNumberOfPoints() );
  }
  */

  /*
  {
    QColor color = Qt::red;
    const std::string fileName = "color";

    std::ofstream ofs( fileName );
    {
      boost::archive::text_oarchive oa( ofs );
      oa << color;
    }

    QColor newColor;
    {
      std::ifstream ifs( fileName );
      boost::archive::text_iarchive ia( ifs );
      ia >> newColor;
    }

    assert( newColor == color );
  }
  */

  /*
  {
    QColor color = Qt::red;

    QByteArray buffer;
    QDataStream stream( &buffer, QIODevice::WriteOnly );
    stream << color;

    QColor newColor;
    QDataStream newStream( &buffer, QIODevice::ReadOnly );
    newStream >> newColor;
    assert( color == newColor );
  }
  */


  std::cout << "ApplicableNodesContainerTest() end" << std::endl;
  return true;
}


bool test::ApplyedBoundaryConditionReadTest(int argc, char *argv[])
{
  std::cout << "ApplyedBoundaryConditionReadTest" << std::endl;
  assert( argc == 2 );

  const QString fileName = argv[ 1 ];
  AppliedBoundaryCondition applyedBoundaryCondition = AppliedBoundaryCondition::readFromFile( fileName );
  assert( applyedBoundaryCondition.getSelectedGrid()->GetNumberOfCells() );
  assert( ! applyedBoundaryCondition.getName().isEmpty() );
  std::cout << "ApplyedBoundaryConditionReadTest end" << std::endl;
  return true;
}


bool test::ApplyedBoundaryConditionWriteTest(int argc, char *argv[])
{
  std::cout << "ApplyedBoundaryConditionReadTest" << std::endl;
  assert( argc == 3 );

  const QString readFromfileName = argv[ 1 ];
  AppliedBoundaryCondition applyedBoundaryCondition = AppliedBoundaryCondition::readFromFile( readFromfileName );
  assert( applyedBoundaryCondition.getSelectedGrid()->GetNumberOfCells() );
  assert( ! applyedBoundaryCondition.getName().isEmpty() );

  const QString saveToFileName = argv[ 2 ];
  AppliedBoundaryCondition::writeToFile( applyedBoundaryCondition, saveToFileName );

  AppliedBoundaryCondition newCondition = AppliedBoundaryCondition::readFromFile( saveToFileName );
  assert( newCondition.getName() == applyedBoundaryCondition.getName() );
  assert( newCondition.getColor() == applyedBoundaryCondition.getColor() );
  assert( newCondition.getConditionType() == applyedBoundaryCondition.getConditionType() );
  std::cout << "ApplyedBoundaryConditionReadTest end" << std::endl;
  return true;
}
