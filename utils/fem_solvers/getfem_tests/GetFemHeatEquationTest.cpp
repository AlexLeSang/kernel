#include "GetFemHeatEquationTest.hpp"

#include <getfem/getfem_model_solvers.h>
#include <getfem/getfem_export.h>
#include <getfem/getfem_regular_meshes.h>
#include <gmm/gmm.h>

#include <boost/timer/timer.hpp>

#include <vtkUnstructuredGrid.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkSmartPointer.h>
#include <vtkIdList.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkPointData.h>
#include <vtkDataSetMapper.h>
#include <vtkScalarBarActor.h>
#include <vtkLookupTable.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkProperty.h>

#include <vector>

namespace heat_equation {

  /* some Getfem++ types that we will be using */
  using bgeot::base_small_vector; /* special class for small (dim<16) vectors */
  using bgeot::base_node; /* geometrical nodes (derived from base_small_vector)*/
  using bgeot::scalar_type; /* = double */
  using bgeot::size_type;   /* = unsigned long */

  /* definition of some matrix/vector types. These ones are built
 * using the predefined types in Gmm++
 */
  typedef gmm::rsvector<scalar_type> sparse_vector_type;
  typedef gmm::row_matrix<sparse_vector_type> sparse_matrix_type;
  typedef gmm::col_matrix<sparse_vector_type> col_sparse_matrix_type;
  typedef std::vector<scalar_type> plain_vector;

  /* Definitions for the exact solution of the Heat_Equation problem,
 *  i.e. Delta(sol_u) + sol_f = 0
 */

  base_small_vector sol_K; /* a coefficient */
  scalar_type diffusion_coefficient; // Diffusion coefficient
  scalar_type solution_time;

  /* Custom BC */
  scalar_type thermal_permittivity(const base_node &x)
  {
    scalar_type t_p = 10;
    return t_p;
  }

  /* Neumann condition */
  base_small_vector neumann(const base_node &x)
  {
    base_small_vector b( -8.0, 3.0, -7.0 );
    return b;
  }

  /* Dirihlet condition */
  scalar_type dirihlet(const base_node &x)
  {
    scalar_type t( 40.0 );
    return t;
  }

  /*
  structure for the Heat_Equation problem
  (not mandatory, just to gather the variables)
*/
  struct heat_equation_problem {

    enum { DIRICHLET_BOUNDARY_NUM = 0, NEUMANN_BOUNDARY_NUM = 1};
    enum { DIRICHLET_WITH_MULTIPLIERS = 0, DIRICHLET_WITH_PENALIZATION = 1};
    getfem::mesh mesh;        /* the mesh */
    getfem::mesh_im mim;      /* the integration methods. */
    getfem::mesh_fem mf_u;    /* the main mesh_fem, for the Heat_Equation solution */
    getfem::mesh_fem mf_rhs;  /* the mesh_fem for the right hand side(f(x),..) */

    scalar_type residual;        /* max residual for the iterative solvers     */
    size_type N;
    size_type dirichlet_version;
    scalar_type dirichlet_coefficient; /* Penalization parameter.              */
    plain_vector U;

    scalar_type dt;
    scalar_type T;
    scalar_type theta;

    // bgeot::md_param PARAM;

    vtkSmartPointer< vtkUnstructuredGrid > grid;

    bool solve(void);
    void init(void);
    heat_equation_problem(void) : mim(mesh), mf_u(mesh), mf_rhs(mesh) {}
  };

  /* Read parameters from the .param file, build the mesh, set finite element
 * and integration methods and selects the boundaries.
 */
  void heat_equation_problem::init(void) {

    /* First step : build the mesh */
    bgeot::pgeometric_trans pgt = bgeot::geometric_trans_descriptor( "GT_PK(3,1)" );
    N = pgt->dim();

    {
      assert( grid );

      const auto numberOfPoints = grid->GetNumberOfPoints();
      const auto numberOfCells = grid->GetNumberOfCells();

      assert( numberOfPoints );
      assert( numberOfCells );

      std::vector< size_type > indecies;
      indecies.reserve( numberOfPoints );

      for ( auto p = 0; p < numberOfPoints; ++p ) {
        double point[3];
        grid->GetPoint( p, point );
        indecies.push_back( mesh.add_point( bgeot::base_node( point[ 0 ], point[ 1 ], point[ 2 ] ) ) );
      }

      for ( auto c = 0; c < numberOfCells; ++c ) {
        vtkSmartPointer< vtkIdList > cell_points = vtkSmartPointer< vtkIdList >::New();
        grid->GetCellPoints( c, cell_points );
        size_t num_cell_points = cell_points->GetNumberOfIds();
        assert( num_cell_points == 4 );

        std::vector< size_type > convex( 4 );
        convex[ 0 ] = indecies[ cell_points->GetId( 0 ) ];
        convex[ 1 ] = indecies[ cell_points->GetId( 1 ) ];
        convex[ 2 ] = indecies[ cell_points->GetId( 2 ) ];
        convex[ 3 ] = indecies[ cell_points->GetId( 3 ) ];

        mesh.add_convex( pgt, convex.begin() );
      }

    }

    T = 3;
    dt = T/50;
    theta = 0.5;
    diffusion_coefficient = 0.5;

    residual = 1e-10;

    if (residual == 0.) {
      residual = 1e-10;
    }

    sol_K.resize( N );
    solution_time = 0.;

    /* set the finite element on the mf_u */
    getfem::pfem pf_u = getfem::fem_descriptor( "FEM_PK(3,2)" );
    getfem::pintegration_method ppi = getfem::int_method_descriptor( "IM_TETRAHEDRON(2)" );

    mim.set_integration_method(mesh.convex_index(), ppi);
    mf_u.set_finite_element(mesh.convex_index(), pf_u);

    mf_rhs.set_finite_element( mesh.convex_index(), pf_u );

    /* set boundary conditions
     * (Neuman on the upper face, Dirichlet elsewhere) */
    std::cout << "Selecting Neumann and Dirichlet boundaries\n";

    getfem::mesh_region border_faces;
    getfem::outer_faces_of_mesh(mesh, border_faces);

    for (getfem::mr_visitor i(border_faces); !i.finished(); ++i) {
      assert(i.is_face());
      base_node un = mesh.normal_of_face_of_convex(i.cv(), i.f());
      un /= gmm::vect_norm2(un);

      /*
      if(gmm::abs( un[N - 3] - 1.0 ) < 1.0E-7) { // new Neumann face X
        mesh.region(NEUMANN_BOUNDARY_NUM).add(i.cv(), i.f());
      }
    else
      if(gmm::abs( un[N - 2] - 1.0 ) < 1.0E-7) { // new Neumann face Y
        mesh.region(NEUMANN_BOUNDARY_NUM).add(i.cv(), i.f());
      }
      else
    */
      if(gmm::abs( un[N - 1] - 1.0 ) < 1.0E-7 ) { // new Neumann face Z
        mesh.region( NEUMANN_BOUNDARY_NUM ).add( i.cv(), i.f() );
      }
      else {
        mesh.region( DIRICHLET_BOUNDARY_NUM ).add( i.cv(), i.f() );
      }
    }
  }

  bool heat_equation_problem::solve(void) {

    dal::bit_vector transient_bricks;

    getfem::model model;

    // Main unknown of the problem
    model.add_fem_variable( "u", mf_u, 2 );

    // Laplacian term on u.
    model.add_initialized_scalar_data( "c", diffusion_coefficient );
    transient_bricks.add( getfem::add_generic_elliptic_brick( model, mim, "u", "c" ) );

    std::vector<scalar_type> F( mf_rhs.nb_dof() );


    // Volumic source term.
    ///*
    getfem::interpolation_function( mf_rhs, F, thermal_permittivity );
    model.add_initialized_fem_data( "VolumicData", mf_rhs, F );
    getfem::add_source_term_brick( model, mim, "u", "VolumicData" );
    //*/

    // Neumann condition.
    gmm::resize( F, mf_rhs.nb_dof() * N );
    getfem::interpolation_function( mf_rhs, F, neumann, NEUMANN_BOUNDARY_NUM );

    // The two version of the data make only a difference for midpoint scheme
    model.add_fem_data( "NeumannData", mf_rhs, bgeot::dim_type( N ), 2 );
    gmm::copy( F, model.set_real_variable( "NeumannData", 0 ) );
    gmm::copy( F, model.set_real_variable( "NeumannData", 1 ) );
    transient_bricks.add( getfem::add_normal_source_term_brick( model, mim, "u", "NeumannData", NEUMANN_BOUNDARY_NUM ) );

    // Dirichlet condition.
    gmm::resize( F, mf_rhs.nb_dof() );
    getfem::interpolation_function( mf_rhs, F, dirihlet );
    model.add_initialized_fem_data( "DirichletData", mf_rhs, F );

    getfem::add_Dirichlet_condition_with_multipliers( model, mim, "u", mf_u, DIRICHLET_BOUNDARY_NUM, "DirichletData" );

    // transient part.
    model.add_initialized_scalar_data( "dt", dt );
    getfem::add_basic_d_on_dt_brick( model, mim, "u", "dt" );

    model.add_initialized_scalar_data( "theta", theta );
    getfem::add_theta_method_dispatcher( model, transient_bricks, "theta" );

    gmm::iteration iter( residual, 0, 40000 );

    model.first_iter();

    // Null initial value for the temperature. Can be modified.
    gmm::resize( U, mf_u.nb_dof() );
    gmm::clear( U );
    gmm::copy( U, model.set_real_variable( "u", 1 ) );


    unsigned int parts_counter = 0;
    const std::string solution_name = "getfem_heat_equation_test_output.";
    const std::string extention = ".vtk";

    // Save initial temp
    {
      std::ostringstream ss;
      ss << parts_counter;
      const std::string part_number( ss.str() );
      ++ parts_counter;

      getfem::vtk_export exp(  solution_name + part_number + extention );
      exp.exporting( mf_u );
      exp.write_point_data( mf_u, U, "Temperature" ); // write a vector field
    }

    for ( scalar_type t = 0; t < T; t += dt) {
      solution_time = t + dt;

      gmm::resize (F, mf_rhs.nb_dof() * N );
      getfem::interpolation_function( mf_rhs, F, neumann, NEUMANN_BOUNDARY_NUM );
      gmm::copy( F, model.set_real_variable("NeumannData"));

      gmm::resize( F, mf_rhs.nb_dof() );
      getfem::interpolation_function( mf_rhs, F, dirihlet );
      gmm::copy( F, model.set_real_variable("DirichletData") );

      cout << "solving for t = " << solution_time << endl;
      iter.init();
      getfem::standard_solve( model, iter );
      gmm::copy( model.real_variable( "u" ), U );

      // Provide sequence of vtk files
      {
        std::ostringstream ss;
        ss << parts_counter;
        const std::string part_number( ss.str() );
        ++ parts_counter;

        getfem::vtk_export exp(  solution_name + part_number + extention );
        exp.exporting( mf_u );
        exp.write_point_data( mf_u, U, "Temperature"); // write a vector field
      }

      model.next_iter();
    }

    return ( iter.converged() );
  }

}



bool test::GetFemHeatEquationTest(int argc, char **argv)
{
  GMM_SET_EXCEPTION_DEBUG; // Exceptions make a memory fault, to debug.
  FE_ENABLE_EXCEPT;        // Enable floating point exception for Nan.
  try {
    boost::timer::auto_cpu_timer t;

    heat_equation::heat_equation_problem p;
    // p.PARAM.read_command_line( argc, argv );
    {
      vtkSmartPointer< vtkXMLUnstructuredGridReader > reader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
      reader->SetFileName( "simple_cube.vtu" );
      reader->Update();

      vtkSmartPointer< vtkUnstructuredGrid > grid( reader->GetOutput() );
      p.grid = grid;
    }

    p.init();
    if ( ! p.solve() ) GMM_ASSERT1( false, "Solve procedure has failed" );


  } GMM_STANDARD_CATCH_ERROR;

  // Visualize
  {
    vtkSmartPointer<vtkUnstructuredGridReader> reader = vtkSmartPointer<vtkUnstructuredGridReader>::New();
    reader->SetFileName( "getfem_heat_equation_test_output.50.vtk" );
    reader->Update();

    std::string results_name;
    double range[ 2 ];

    vtkSmartPointer< vtkUnstructuredGrid > grid( reader->GetOutput() );
    {

      vtkSmartPointer< vtkPointData > pd( grid->GetPointData() );
      if ( pd )
      {
        std::cout << "Contains point data with " << pd->GetNumberOfArrays() << " arrays." << std::endl;
        for (int i = 0; i < pd->GetNumberOfArrays(); i++)
        {
          std::cout << "\tArray " << i << " is named " << (pd->GetArrayName(i) ? pd->GetArrayName(i) : "NULL") << std::endl;
          results_name = pd->GetArrayName(i);
          //*
          vtkSmartPointer< vtkDataArray > arr( pd->GetArray( i ) );
          assert( arr );
          const auto arrNumOfTuples = arr->GetNumberOfTuples();
          std::cout << "arrNumOfTuples: " << arrNumOfTuples << std::endl;
          arr->GetRange( range );
          std::cout << "[" << range[ 0 ] << ", " << range[ 1 ] << "]" << std::endl;
          /*
          for ( auto j = 0; j < arrNumOfTuples; ++ j ) {
            std::cout << arr->GetComponent( VTK_DOUBLE, j ) << "\n";
          }
          */
          std::cout << std::endl;
          //*/
        }
      }

    }

    vtkSmartPointer<vtkDataSetMapper> mapper = vtkSmartPointer<vtkDataSetMapper>::New();
    mapper->SetInputConnection( reader->GetOutputPort() );
    mapper->ScalarVisibilityOn();
    mapper->SetScalarModeToUsePointData();

    vtkSmartPointer<vtkScalarBarActor> scalarBar = vtkSmartPointer<vtkScalarBarActor>::New();
    scalarBar->SetLookupTable(mapper->GetLookupTable());
    scalarBar->SetTitle( results_name.c_str() );
    scalarBar->SetNumberOfLabels( 5 );

    // Create a lookup table to share between the mapper and the scalarbar
    vtkSmartPointer<vtkLookupTable> hueLut = vtkSmartPointer<vtkLookupTable>::New();
    hueLut->SetTableRange( range );
    hueLut->SetNumberOfColors( 1024 );
    hueLut->Build();

    mapper->SetLookupTable( hueLut );
    mapper->SetScalarRange( range );
    scalarBar->SetLookupTable( hueLut );

    vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
    actor->SetMapper(mapper);
    actor->GetProperty()->EdgeVisibilityOn();

    vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
    vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
    renderWindow->AddRenderer(renderer);
    renderWindow->SetSize( 1024, 768 );
    vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
    renderWindowInteractor->SetRenderWindow(renderWindow);

    vtkSmartPointer<vtkInteractorStyleTrackballCamera> style = vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New(); //like paraview

    renderWindowInteractor->SetInteractorStyle( style );

    renderer->AddActor(actor);
    renderer->AddActor2D(scalarBar);
    renderer->SetBackground(.0, .0, .0);

    renderWindow->Render();
    renderWindowInteractor->Start();
  }

  return true;
}
