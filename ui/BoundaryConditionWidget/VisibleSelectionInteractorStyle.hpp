#ifndef VISIBLESELECTIONINTERACTORSTYLE_HPP
#define VISIBLESELECTIONINTERACTORSTYLE_HPP

#include <vtkSphereSource.h>
#include <vtkCubeSource.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkHardwareSelector.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkInteractorStyleRubberBandPick.h>
#include <vtkObjectFactory.h>
#include <vtkRendererCollection.h>
#include <vtkDataSetMapper.h>
#include <vtkExtractSelection.h>
#include <vtkSelection.h>
#include <vtkProperty.h>
#include <vtkAreaPicker.h>
#include <vtkGlyph3D.h>
#include <vtkArrowSource.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkCell.h>
#include <vtkCellCenters.h>
#include <vtkFloatArray.h>
#include <vtkDoubleArray.h>
#include <vtkPointData.h>
#include <vtkPolyDataNormals.h>
#include <vtkCellData.h>
#include <vtkTriangle.h>
#include <vtkUnstructuredGrid.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkAppendFilter.h>
#include <../utils/vtk_utils/vtk_includes/vtkBooleanOperationPolyDataFilter.hpp>

#include <TreeItems/AbstractTreeItem.hpp>

#include <cassert>
#include <tuple>

#include <QObject>

#define VTKISRBP_ORIENT 0
#define VTKISRBP_SELECT 1

/*!
 * \brief The GLYPH_TYPE enum
 */
enum GLYPH_TYPE
{
  SPHERE,
  CUBE,
  ARROW
};

/*!
 * \brief The VisibleSelectionInteractorStyle class
 */
class VisibleSelectionInteractorStyle : public QObject, public vtkInteractorStyleRubberBandPick
{
  Q_OBJECT

public:
  static VisibleSelectionInteractorStyle* New();
  vtkTypeMacro(VisibleSelectionInteractorStyle, vtkInteractorStyleTrackballCamera);

  VisibleSelectionInteractorStyle();

  virtual void OnLeftButtonUp();
  virtual void OnKeyPress();

  void SetPolyData(vtkSmartPointer<vtkPolyData> polyData);

  double GetGlyphScaleCoefficient() const;
  void SetGlyphScaleCoefficient(double value);
  AbstractTreeItem *GetCurrentTreeItem() const;
  void SetCurrentTreeItem(AbstractTreeItem *value);

  void SetPrevSelected(QList<AbstractTreeItem*> itemsList);

signals:
  void switchState();

private:
  void updateSelection();
  void appendSelectedGrid(vtkSmartPointer< vtkUnstructuredGrid > selected);
  void replaceSelectedGrid(vtkSmartPointer< vtkUnstructuredGrid > selected);
  vtkSmartPointer< vtkPolyData > getGlyphFigure(BOUNDARY_CONDITION_TYPE boundaryConditionType);
  vtkSmartPointer<vtkPolyData> getGlyph3D(vtkSmartPointer<vtkPolyData> cellCentersPolyData, vtkSmartPointer<vtkPolyData> glyphFigure);
  vtkSmartPointer<vtkUnstructuredGrid> extractSelected();
  vtkSmartPointer<vtkUnsignedCharArray> getGlyphColors(vtkUnstructuredGrid *selectedGrid, QColor glyphColor);
  vtkSmartPointer<vtkDoubleArray> getCellNormals(vtkUnstructuredGrid *selectedGrid);
  vtkSmartPointer<vtkPolyData> getSelectedGridWithGlyphs();
  std::tuple<float, float, float> reduceColor(const QColor &color);
  void removeActors();
  void addPrevSelectedActor();
  void removePrevSelectedActor();
  void addActors();
  void highlightSelectedGrid();
  void highlightPrevSelectedGrid();
  void processSelection();
  void displaySelectedGrid();
  bool selectionMakesSence();

private:
  vtkSmartPointer< vtkPolyData > PolyData;
  vtkSmartPointer< vtkDataSetMapper > SelectedMapper;
  vtkSmartPointer< vtkActor > SelectedActor;

  vtkSmartPointer< vtkDataSetMapper > SelectedGridMapper;
  vtkSmartPointer< vtkActor > SelectedGridActor;

  vtkSmartPointer< vtkDataSetMapper > PrevSelectedMapper;
  vtkSmartPointer< vtkActor > PrevSelectedActor;

  vtkSmartPointer< vtkDataSetMapper > PrevSelectedGridMapper;
  vtkSmartPointer< vtkActor > PrevSelectedGridActor;

  vtkSmartPointer< vtkSelection > selection;

  double GlyphScaleCoefficient;
  QColor prevSelectionColor;

  vtkSmartPointer< vtkUnstructuredGrid > SelectedGrid;

  AbstractTreeItem* CurrentTreeItem;

};

bool VisibleSelectionInteractorStyleTest(int argc, char *argv[]);

#endif // VISIBLESELECTIONINTERACTORSTYLE_HPP
