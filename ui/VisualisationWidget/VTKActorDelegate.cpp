#include "VTKActorDelegate.hpp"
#include "VTKActorEditorDialog.hpp"

#include "vtkActor.h"
#include "vtkProperty.h"

#include "VisualisationWidget.hpp"

/*!
 * \brief VTKActorDelegate::createEditor
 * \param parent
 * \param option
 * \param index
 * \return
 */
QWidget *VTKActorDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &) const
{
  VTKActorEditorDialog* editor = new VTKActorEditorDialog( parent );
  connect( editor, SIGNAL( editingFinished() ), this, SLOT( commitAndCloseEditor() ) );
  return editor;
}

/*!
 * \brief VTKActorDelegate::setEditorData
 * \param editor
 * \param index
 */
void VTKActorDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  const auto actorType = index.data( Qt::EditRole ).value< ActorDescriptionType >();
  vtkActor* actorPtr = std::get< 0 > ( actorType );
  VTKActorEditorDialog* myEditor = nullptr;
  myEditor = qobject_cast< VTKActorEditorDialog* > ( editor );
  myEditor->setActor( actorPtr );
}

/*!
 * \brief VTKActorDelegate::setModelData
 * \param editor
 * \param model
 * \param index
 */
void VTKActorDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
  VTKActorEditorDialog* myEditor = nullptr;
  myEditor = qobject_cast< VTKActorEditorDialog* > ( editor );
  vtkActor* editorActorPtr = myEditor->actor();
  const auto actorType = index.data( Qt::EditRole ).value< ActorDescriptionType >();
  vtkActor* actorPtr = std::get<0>( actorType );
  if ( actorPtr == editorActorPtr ) {
    model->setData( index, qVariantFromValue( (void *) actorPtr ), Qt::EditRole );
  }
}

/*!
 * \brief VTKActorDelegate::commitAndCloseEditor
 */
void VTKActorDelegate::commitAndCloseEditor()
{
  VTKActorEditorDialog * editor = qobject_cast< VTKActorEditorDialog *>( sender() );
  emit commitData( editor );
  emit closeEditor( editor );
}
