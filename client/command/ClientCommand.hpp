#ifndef CLIENTCOMMAND_HPP
#define CLIENTCOMMAND_HPP

#include <atomic>
#include <memory>

#include "../server/ServerCommands.hpp"
#include "ServerDescription.hpp"

#include "../utils/ThreadSleep.hpp"

namespace kernel {
  namespace client {
    namespace command {

      const quint32 Timeout = 1 * 1000; // 1 sec

      /*!
       * \brief The ClientCommand class describes the general interface for the client command signature
       */
      class ClientCommand
      {
      public:
        ClientCommand() : description( ServerDescription() ), interruptFlag( false ) {}

        virtual bool operator ()() = 0;
        virtual server::ServerCommands commandName() const = 0;

        virtual ~ClientCommand() {}

        void interrupt() { interruptFlag.store( true ); }
        void setServerDescription(const ServerDescription sd) { description = sd; }
        ServerDescription serverDescription() const { return description; }

      protected:
        ServerDescription description;
        std::atomic< bool > interruptFlag;
      };


      /*!
       * \brief The FrameworkTestClientCommand class
       */
      class FrameworkTestClientCommand : public ClientCommand
      {
      public:
        FrameworkTestClientCommand(const quint32 testDelay = 300 /*ms*/, const bool commandResult = false) : testDelay_( testDelay ), commandResult_( commandResult ) {}

        // ClientCommand interface
        virtual bool operator ()()
        {
          qWarning() << "FrameworkTestClientCommand::operator ()(): going to sleem for " + QString::number( testDelay_ ) + " ms...";
          utils::ThreadSleep::msleep( testDelay_ );
          qWarning() << "FrameworkTestClientCommand::operator ()(): commandResult_: " << commandResult_;
          return commandResult_;
        }


        virtual server::ServerCommands commandName() const
        {
          return server::ServerCommands::FRAMEWORK_TEST_COMMAND;
        }


        quint32 testDelay() const
        {
          return testDelay_;
        }


        void setTestDelay(const quint32 &testDelay)
        {
          testDelay_ = testDelay;
        }


        bool commandResult() const
        {
          return commandResult_;
        }


        void setCommandResult(const bool commandResult)
        {
          commandResult_ = commandResult;
        }

      private:
        quint32 testDelay_;
        bool commandResult_;
      };

      /*!
       * \brief ClientCommandRunner function that runs generic client command
       * \param pClientCommand
       * \param sd
       * \return
       */
      bool ClientCommandRunner(std::shared_ptr< kernel::client::command::ClientCommand > pClientCommand, ServerDescription sd);

      /*!
       * \brief CommandProcessor functions that returns actual command by its name
       * \param command
       * \return
       */
      ClientCommand *CommandProcessor(const server::ServerCommands command);
    }
  }
}


namespace test {
  bool FrameworkTestCommandTest();

}


#endif // CLIENTCOMMAND_HPP
