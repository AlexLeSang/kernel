#ifndef VIENNAMESHMESHER_HPP
#define VIENNAMESHMESHER_HPP

#include <vtkSmartPointer.h>
#include <vtkPolyData.h>

#include <viennamesh/viennamesh.hpp>

namespace kernel {
  namespace utils {
    namespace mesh {

      /*!
       * \brief The ViennaMeshMesher class
       */
      class ViennaMeshMesher
      {
      private:
        typedef viennagrid::plc_3d_mesh GeometryMeshType;

        typedef viennagrid::result_of::point<GeometryMeshType>::type PointType;
        typedef viennamesh::result_of::parameter_handle< GeometryMeshType >::type GeometryHandle;
        typedef viennagrid::result_of::vertex_handle<GeometryMeshType>::type GeometryVertexHandle;
        typedef viennagrid::result_of::line_handle<GeometryMeshType>::type GeometryLineHandle;
        typedef viennagrid::result_of::facet_handle<GeometryMeshType>::type GeometryFacetHandle;

      public:
        ViennaMeshMesher();

        vtkSmartPointer<vtkPolyData> getGeometry() const;
        void setGeometry(const vtkSmartPointer<vtkPolyData> &value);

        void init();
        void createMesh();
        void writeMesh();

        double getCellSize() const;
        void setCellSize(double value);

        double getMaxRadiusEdgeRatio() const;
        void setMaxRadiusEdgeRatio(double value);

        double getMinDihedralAngle() const;
        void setMinDihedralAngle(double value);

        std::string getOutputFileName() const;
        void setOutputFileName(const std::string &value);

      private:
        void fillGeometry();
        void setMeshParameters();
        void setOutputParameters();

      private:
        vtkSmartPointer< vtkPolyData > geometry;
        std::string outputFileName;

        double cellSize;
        double maxRadiusEdgeRatio;
        double minDihedralAngle;

        GeometryHandle geometryHandle;
        viennamesh::algorithm_handle mesher;
        viennamesh::algorithm_handle writer;
      };

    }
  }
}


namespace test {
  bool ViennaMeshMesherTest(int argc, char **argv);
}


#endif // VIENNAMESHMESHER_HPP
