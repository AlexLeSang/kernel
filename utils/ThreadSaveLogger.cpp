#include "ThreadSaveLogger.hpp"

#include <cassert>
#include <iostream>

#include <QDateTime>
#include <QThread>

#include <QDebug>

namespace kernel {
  namespace utils {

    bool ThreadSaveLogger::activated = false;
    QString ThreadSaveLogger::logPath_ = ".";
    QString ThreadSaveLogger::logFilename_ = "kernel.log";

    /*!
     * \brief ThreadSaveLogger::getInstance
     * \return
     */
    ThreadSaveLogger &ThreadSaveLogger::getInstance()
    {
      static ThreadSaveLogger instance;
      return instance;
    }

    /*!
     * \brief ThreadSaveLogger::addLineStatic
     * \param line
     */
    void ThreadSaveLogger::addLineStatic(const QString line)
    {
      QMutexLocker l( &getInstance().m );
      const QString logString( QDateTime::currentDateTime().toString() + ": " + line );
      std::cout << logString.toStdString() << std::endl;
      getInstance().stream << logString << endl;
    }

    /*!
     * \brief ThreadSaveLogger::ThreadSaveLogger
     * \param parent
     */
    ThreadSaveLogger::ThreadSaveLogger(QObject *parent) : QObject(parent)
    {
      init();
    }

    /*!
     * \brief ThreadSaveLogger::init
     */
    void ThreadSaveLogger::init()
    {
      f.setFileName( logFilename_ );

      if ( f.exists() ) {
        QString newLogFilename( logFilename_  );
        newLogFilename.append( "_" + QDateTime::currentDateTime().toString( Qt::ISODate ) );
        if ( !QFile::rename( logFilename_, newLogFilename ) ) {
          qFatal( QString( "Cannot rename log file to: " + newLogFilename ).toStdString().c_str() );
        }
      }

      f.setFileName( logFilename_ );
      if ( ! f.open( QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate ) ) {
        qFatal( QString( "Cannot create log file: " + logFilename_ ).toStdString().c_str() );
      }
      stream.setDevice( &f );
      activated = true;
    }

    /*!
     * \brief ThreadSaveLogger::logFilename
     * \return
     */
    QString ThreadSaveLogger::logFilename()
    {
      return logFilename_;
    }

    /*!
     * \brief ThreadSaveLogger::setLogFilename
     * \param logFilename
     */
    void ThreadSaveLogger::setLogFilename(const QString &logFilename)
    {
      assert( !activated );
      logFilename_ = logFilename;
    }

    /*!
     * \brief ThreadSaveLogger::logPath
     * \return
     */
    QString ThreadSaveLogger::logPath()
    {
      return logPath_;
    }

    /*!
     * \brief ThreadSaveLogger::setLogPath
     * \param logPath
     */
    void ThreadSaveLogger::setLogPath(const QString &logPath)
    {
      assert( !activated );
      logPath_ = logPath;
    }

  }
}
