#include "StandardSLAESolver.hpp"

bool StandardSLAESolver::operator ()(getfem::model &model, gmm::iteration &iter)
{
  std::cout << "StandardSLAESolver::operator ()" << std::endl;

  getfem::standard_solve( model, iter );
  return iter.converged();
}
