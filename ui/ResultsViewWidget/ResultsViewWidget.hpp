#ifndef RESULTSVIEWWIDGET_HPP
#define RESULTSVIEWWIDGET_HPP

#include <QWidget>

#include <unordered_map>
#include <map>
#include <vtkUnstructuredGrid.h>
#include <vtkSmartPointer.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkScalarBarActor.h>

#include <QTimer>
#include <SettingsDialog.hpp>

#include <memory>

namespace Ui {
  class ResultsViewWidget;
}

typedef std::unordered_map< double, vtkSmartPointer< vtkUnstructuredGrid > > TimeGrid;

typedef std::map< double, std::pair< vtkSmartPointer< vtkActor >, vtkSmartPointer< vtkScalarBarActor > > > TimeActors;

/*!
 * \brief The ResultsViewWidget class
 */
class ResultsViewWidget : public QWidget
{
  Q_OBJECT

public:
  explicit ResultsViewWidget(QWidget *parent = 0);
  ~ResultsViewWidget();
  void setTimeGrids(const TimeGrid &value);

public slots:
  void setResults(std::shared_ptr< TimeGrid> timeGrids);

protected:
  void changeEvent(QEvent *e);

private slots:
  void on_resultSlider_valueChanged(int value);
  void on_prevToolButton_clicked();
  void on_playToolButton_clicked();
  void on_pauseToolButton_clicked();
  void on_stopToolButton_clicked();
  void on_nextToolButton_clicked();
  void on_configToolButton_clicked();
  void timerTimeout();
  void keypressSpaceSlot();

private:
  void initGrids();

private:
  Ui::ResultsViewWidget *ui;

private:
  int currentResult;
  vtkSmartPointer< vtkRenderer > renderer;

  TimeActors timeActors;

  Settings settings;
  QTimer* timer;
};

#endif // RESULTSVIEWWIDGET_HPP
