#ifndef SERVERSTATUSWIDGET_HPP
#define SERVERSTATUSWIDGET_HPP

#include <QWidget>

#include "../client/KernelClient.hpp"

#include <WorkgroupGraphVisualizer.hpp>


/*!
 * \brief The WorkgroupGraphProducer class
 */
class WorkgroupGraphProducer : public GraphProducerBase
{
public:
  WorkgroupGraphProducer() : done_( false ) {}

  // GraphProducerBase interface
  virtual bool done();
  virtual QList<kernel::server::command::ServerStatus::WorkgroupGraphEdge> produce();

  QList<kernel::server::command::ServerStatus::WorkgroupGraphEdge> getGraph() const;
  void setGraph(const QList<kernel::server::command::ServerStatus::WorkgroupGraphEdge> &value);

  bool getDone() const;
  void setDone(const bool val);

private:
  QList<kernel::server::command::ServerStatus::WorkgroupGraphEdge> graph;
  mutable QMutex graphMutex;
  bool done_;
};

namespace Ui {
  class ServerStatusWidget;
}

/*!
 * \brief The ServerStatusWidget class
 */
class ServerStatusWidget : public QWidget
{
  Q_OBJECT

public:
  explicit ServerStatusWidget(QWidget *parent = 0);
  ~ServerStatusWidget();

protected:
  void changeEvent(QEvent *e);

private slots:
  void fillUpdateIntervalList();

  void on_testPushButton_clicked();
  void on_connectPushButton_clicked();
  void on_disconnectPushButton_clicked();
  void on_updateTimer_timeout();
  void on_updateIntervalComboBox_activated(int index);
  void on_visualizePushButton_clicked();


private:
  Ui::ServerStatusWidget *ui;

  kernel::client::KernelClient* kernelClient;

  QTimer* updateTimerPtr;

  std::shared_ptr< WorkgroupGraphProducer > producer;
};

#endif // SERVERSTATUSWIDGET_HPP
