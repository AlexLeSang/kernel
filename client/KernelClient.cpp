#include "KernelClient.hpp"

#include <functional>
#include <cassert>
#include <memory>

#include <QtConcurrentRun>
#include <QtConcurrentMap>

#include "KernelServer.hpp"

#include "ServerCommands.hpp"
#include "ClientCommand.hpp"
#include "PingPongClientCommand.hpp"
#include "ServerStatusClientCommand.hpp"

namespace kernel {
  namespace client {

    bool KernelClient::activated = false;

    /*!
     * \brief KernelClient::getInstance
     * \return
     */
    KernelClient *KernelClient::getInstance()
    {
      static KernelClient instance;
      return &instance;
    }

    /*!
     * \brief KernelClient::pingHost
     * \param pingPongResultPtr
     * \param numberOfPackets
     * \param packetSize
     * \return
     */
    QFuture< bool > KernelClient::pingHost(command::PingPongResult* pingPongResultPtr, const quint32 numberOfPackets, const quint32 packetSize)
    {
      KernelClient* client = KernelClient::getInstance();
      std::shared_ptr< kernel::client::command::ClientCommand > commandProcessor( kernel::client::command::CommandProcessor( kernel::server::ServerCommands::PING_PONG ) );
      kernel::client::command::PingPongClientCommand* pingPongProcessor = static_cast< kernel::client::command::PingPongClientCommand* > ( commandProcessor.get() );

      pingPongProcessor->setNumberOfPackets( numberOfPackets );
      pingPongProcessor->setPacketSize( packetSize );
      pingPongProcessor->setPingPongResultPtr( pingPongResultPtr );

      return (*client).execute( commandProcessor );
    }

    /*!
     * \brief KernelClient::serverStatus
     * \param serverStatusPtr
     * \return
     */
    QFuture<bool> KernelClient::serverStatus(server::command::ServerStatus *serverStatusPtr)
    {
      KernelClient* client = KernelClient::getInstance();
      std::shared_ptr< kernel::client::command::ClientCommand > commandProcessor( kernel::client::command::CommandProcessor( kernel::server::ServerCommands::GET_SERVER_STATUS ) );
      kernel::client::command::ServerStatusClientCommand* serverStatusProcessor = static_cast< kernel::client::command::ServerStatusClientCommand* > ( commandProcessor.get() );

      serverStatusProcessor->setServerStatusPtr( serverStatusPtr );

      return (*client).execute( commandProcessor );
    }

    template < typename T >
    /*!
     * \brief FutureWaiter
     * \param future
     * \return
     */
    T FutureWaiter(const QFuture<T> &future)
    {
      return future.result();
    }

    /*!
     * \brief KernelClient::execute
     * \param commandProcessor
     * \return
     */
    QFuture<bool> KernelClient::execute(std::shared_ptr< kernel::client::command::ClientCommand > commandProcessor)
    {
      assert( activated );

      /*
      if ( serverDescription.address.isNull() ) {
        qWarning() << "KernelClient::executeAndWait(): unspecified host address";
        qWarning() << "serverDescription.address: " << serverDescription.address;
        // TODO log it
        return QFuture<bool>();
      }
      */

      QFuture<bool> f = QtConcurrent::run( command::ClientCommandRunner, commandProcessor, serverDescription );
      return f;
    }

    /*!
     * \brief KernelClient::executeAndWait
     * \param commandProcessor
     * \return
     */
    bool KernelClient::executeAndWait(std::shared_ptr< kernel::client::command::ClientCommand > commandProcessor)
    {
      assert( activated );

      if ( serverDescription.address.isNull() ) {
        qWarning() << "KernelClient::executeAndWait(): unspecified host address";
        qWarning() << "serverDescription.address: " << serverDescription.address;
        // TODO log it
        return false;
      }

      bool result = command::ClientCommandRunner( commandProcessor, serverDescription );
      qWarning() << "KernelClient::executeAndWait(): result: " << result;
      return result;
    }

    /*!
     * \brief KernelClient::getServerDescription
     * \return
     */
    ServerDescription KernelClient::getServerDescription() const
    {
      return serverDescription;
    }

    /*!
     * \brief KernelClient::setServerDescription
     * \param value
     */
    void KernelClient::setServerDescription(const ServerDescription &value)
    {
      activated = true;
      serverDescription = value;
    }

    /*!
     * \brief concatBool
     * \param rb
     * \param crb
     */
    void concatBool(bool &rb, const bool &crb)
    {
      rb |= crb;
    }

  }
}
