#include "CgILDTSLAESolver.hpp"

bool CgILDTSLAESolver::operator ()(getfem::model &model, gmm::iteration &iter)
{
  using VECTOR = std::remove_const< std::remove_reference< decltype( model.real_rhs() )>::type >::type;
  using MATRIX = std::remove_const< std::remove_reference< decltype( model.real_tangent_matrix() )>::type >::type;

  std::cout << "CgILDTSLAESolver::operator ()" << std::endl;
  dal::shared_ptr< getfem::abstract_linear_solver< MATRIX, VECTOR > > p( new getfem::linear_solver_gmres_preconditioned_ilu<MATRIX, VECTOR> );
  VECTOR state( model.nb_dof() );
  model.assembly( getfem::model::BUILD_ALL );
  model.from_variables( state );
  (*p)( model.real_tangent_matrix(), state, model.real_rhs(), iter );
  model.to_variables( state );
  return iter.converged();
}
