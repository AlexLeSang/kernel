#include <QApplication>
#include <QDesktopWidget>

#include <ResultsViewWidget.hpp>

#include <vtkXMLUnstructuredGridReader.h>

int main(int argc, char *argv[])
{
  QApplication a( argc, argv );

  TimeGrid timeGrids;
  {
    // const std::string prefix = "getfem_heat_equation_test_output_";
    // const std::string suffix = ".vtu";


    for ( auto i = 1; i < argc; ++ i ) {
      const std::string str( argv[ i ] );
      const auto u_last = str.find_last_of( '_' );
      const auto dot_first = str.find_last_of( '.' );

      const auto time_str = str.substr( u_last + 1, dot_first - u_last - 1 );

      std::cout << "arg [" << i << "]: " << str << " time_str: " << time_str << std::endl;

      const double time = std::stod( time_str );

      {
        vtkSmartPointer< vtkXMLUnstructuredGridReader > reader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
        reader->SetFileName( str.c_str() );
        reader->Update();

        vtkSmartPointer< vtkUnstructuredGrid > grid = reader->GetOutput();
        timeGrids.insert( std::make_pair( time, grid ) );
      }

    }

  }


  ResultsViewWidget widget;
  widget.setTimeGrids( timeGrids );
  widget.show();
  widget.move( QApplication::desktop()->screen()->rect().center() - widget.rect().center() );

  return a.exec();
}


/*
#include <SettingsDialog.hpp>
#include <QDebug>

int main(int argc, char *argv[])
{
  QApplication a( argc, argv );

  const double dataTimeStep = 0.75;

  SettingsDialog dialog;
  dialog.setDataTimeStep( dataTimeStep );

  if ( dialog.exec() == QDialog::Accepted ) {
    const auto timeStep = dialog.getTimeStep();
    const auto looped = dialog.getLooped();
    qDebug() << "Accepted";
    qDebug() << "timeStep: " << timeStep;
    qDebug() << "looped: " << looped;
  }
  else {
    qDebug() << "Rejected";
  }

  return EXIT_SUCCESS;
}
*/
