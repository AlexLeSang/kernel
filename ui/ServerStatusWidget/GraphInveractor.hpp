#ifndef GRAPHINVERACTOR_HPP
#define GRAPHINVERACTOR_HPP

#include <vtkObjectFactory.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkSmartPointer.h>
#include <vtkGraphLayoutView.h>
#include <vtkProperty.h>
#include <vtkActor.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkCommand.h>

#include <algorithm>
#include <list>

class WorkgroupGraphVisualizer;

/*!
 * \brief The GraphInveractor class
 */
class GraphInveractor : public vtkInteractorStyleTrackballCamera
{

public:
  static GraphInveractor* New();
  vtkTypeMacro(GraphInveractor, vtkInteractorStyleTrackballCamera);

  virtual void OnLeftButtonDown();
  virtual void OnKeyPress();

  virtual void Execute(vtkObject *vtkNotUsed(caller), unsigned long eventId, void *vtkNotUsed(callData));

  void SetWorkgroupGraphVisualizer(WorkgroupGraphVisualizer* visualizer);

  void addSelected(vtkActor *pickedActor);
  void removeSelected(vtkActor *pickedActor);
  void cleanSelection();
  void addOrRemoveFromSelected(vtkActor* pickedActor);

private:
  std::list< std::pair< vtkSmartPointer< vtkActor >, vtkSmartPointer< vtkProperty > > > selectedActorList;
  WorkgroupGraphVisualizer* workgroupGraphVisualizer;
};

#endif // GRAPHINVERACTOR_HPP
