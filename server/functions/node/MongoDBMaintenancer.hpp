#ifndef MONGODBMAINTENANCER_HPP
#define MONGODBMAINTENANCER_HPP

#include <QRunnable>
#include <QObject>
#include <QHostInfo>
#include <QHostAddress>

#include <memory>

#include <mongo/client/dbclient.h>
#include <mongo/client/dbclient_rs.h>

namespace kernel {
  namespace server {
    namespace functions {
      namespace node {

        /*!
         * \brief replica_set_name
         */
        static const std::string replica_set_name = "NULP";

        /*!
         * \brief kernel_nodes_ns
         */
        static const std::string kernel_nodes_ns = "kernel.nodes";

        /*!
         * \brief kernel_workgroup_graph_ns
         */
        static const std::string kernel_workgroup_graph_ns = "kernel.workgopup_graph";

        /*!
         * \brief kernel_nodes_ttl
         */
        static const quint32 kernel_nodes_ttl = 60; // sec

        /*!
         * \brief The MongoDBMaintenancer class
         */
        class MongoDBMaintenancer : public QObject, QRunnable
        {
          Q_OBJECT
        public:
          MongoDBMaintenancer(QObject* parent = nullptr, const QHostAddress& address = QHostAddress::LocalHost, const quint32 port = 27017);

          virtual ~MongoDBMaintenancer();

          bool defineRS();

          QHostAddress mongodbAddress() const;
          void setMongodbAddress(const QHostAddress &mongodbAddress);

          quint32 port() const;
          void setPort(const quint32 &port);

          std::shared_ptr<mongo::DBClientReplicaSet> createConnectionToRS();
          bool mongodbMaintenance();

          // QRunnable interface
        public:
          virtual void run();

          void enshureIndexMaster(mongo::DBClientBase *connection);
        private:
          bool enshureIndexes(std::shared_ptr<mongo::DBClientReplicaSet> connectionPtr_);

        private:
          QHostAddress mongodbAddress_;
          quint32 port_;
          std::vector< mongo::HostAndPort > hostPortVector;
          bool parametersChanged;
        };

      }
    }
  }
}

#endif // MONGODBMAINTENANCER_HPP
