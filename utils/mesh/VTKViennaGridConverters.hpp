#ifndef VTKVIENNAGRIDCONVERTERS_HPP
#define VTKVIENNAGRIDCONVERTERS_HPP

#include <vtkSmartPointer.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#include <vtkIndent.h>
#include <vtkIdTypeArray.h>
#include <vtkDoubleArray.h>
#include <vtkCellArray.h>
#include <vtkUnsignedCharArray.h>
#include <vtkUnstructuredGrid.h>

#include <viennagrid/io/netgen_reader.hpp>
#include <viennagrid/io/vtk_common.hpp>
#include <viennagrid/io/vtk_reader.hpp>
#include <viennagrid/io/vtk_writer.hpp>
#include <viennagrid/algorithm/volume.hpp>
#include <viennagrid/forwards.hpp>
#include <viennagrid/accessor.hpp>

#include <viennadata/api.hpp>

#include <viennafem/forwards.h>

#include <vtkXMLUnstructuredGridWriter.h>


namespace kernel {
  namespace math {
    namespace mesh {


      template<typename map_type>
      /*!
       * \brief clear_map
       * \param map
       */
      void clear_map( map_type & map )
      {
        for (typename map_type::iterator it = map.begin(); it != map.end(); ++it)
          delete it->second;

        map.clear();
      }

      template< typename VectorType, typename MeshType, typename SegmentationType, typename StorageType >
      /*!
       * \brief ViennaGridToVTKUnstructuredGrid
       * \param result
       * \param mesh_obj
       * \param segmentation
       * \param storage
       * \param grid
       * \param id
       */
      void ViennaGridToVTKUnstructuredGrid(const VectorType& result, const MeshType& mesh_obj, const SegmentationType & segmentation, const StorageType & storage, vtkUnstructuredGrid* grid, const long id)
      {
        // Typedefs
        typedef typename SegmentationType::segment_id_type segment_id_type;

        typedef typename viennagrid::result_of::point<MeshType>::type   PointType;
        typedef typename viennagrid::result_of::coord<PointType>::type  CoordType;

        typedef typename viennagrid::result_of::cell_tag<MeshType>::type                   CellTag;
        typedef typename viennagrid::result_of::element<MeshType, CellTag>::type           CellType;
        typedef typename viennagrid::result_of::const_handle<MeshType, CellTag>::type      ConstCellHandleType;
        typedef typename viennagrid::result_of::id<CellType>::type                         CellIDType;

        typedef typename viennagrid::result_of::element<MeshType, viennagrid::vertex_tag>::type            VertexType;
        typedef typename viennagrid::result_of::handle<MeshType, viennagrid::vertex_tag>::type             VertexHandleType;
        typedef typename viennagrid::result_of::const_handle<MeshType, viennagrid::vertex_tag>::type       ConstVertexHandleType;
        typedef typename viennagrid::result_of::id<VertexType>::type                           VertexIDType;

        typedef typename viennagrid::result_of::const_element_range<MeshType, viennagrid::vertex_tag>::type   VertexContainer;
        typedef typename viennagrid::result_of::iterator<VertexContainer>::type                                 VertexIterator;

        typedef typename SegmentationType::segment_handle_type SegmentHandleType;


        typedef viennafem::mapping_key          MappingKeyType;
        typedef viennafem::boundary_key         BoundaryKeyType;



        typedef std::vector<double> vector_data_type;

        typedef viennagrid::base_dynamic_field<const double, VertexType> VertexScalarBaseAccesor;
        typedef std::map< std::string, VertexScalarBaseAccesor * > VertexScalarOutputAccessorContainer;

        typedef viennagrid::base_dynamic_field<const vector_data_type, VertexType> VertexVectorBaseAccesor;
        typedef std::map< std::string, VertexVectorBaseAccesor * > VertexVectorOutputAccessorContainer;

        typedef viennagrid::base_dynamic_field<const double, CellType> CellScalarBaseAccesor;
        typedef std::map< std::string, CellScalarBaseAccesor * > CellScalarOutputAccessorContainer;

        typedef viennagrid::base_dynamic_field<const vector_data_type, CellType> CellVectorBaseAccesor;
        typedef std::map< std::string, CellVectorBaseAccesor * > CellVectorOutputAccessorContainer;


        // End typedefs

        // Data structures definition

        std::map< segment_id_type, std::map< ConstVertexHandleType, VertexIDType> >             vertex_to_index_map;
        std::map< segment_id_type, std::map< VertexIDType, ConstVertexHandleType> >             used_vertex_map;
        std::map< segment_id_type, std::map< CellIDType, ConstCellHandleType> >                 used_cell_map;


        VertexScalarOutputAccessorContainer          vertex_scalar_data;
        VertexVectorOutputAccessorContainer          vertex_vector_data;

        CellScalarOutputAccessorContainer          cell_scalar_data;
        CellVectorOutputAccessorContainer          cell_vector_data;

        std::map< segment_id_type, VertexScalarOutputAccessorContainer > segment_vertex_scalar_data;
        std::map< segment_id_type, VertexVectorOutputAccessorContainer > segment_vertex_vector_data;

        std::map< segment_id_type, CellScalarOutputAccessorContainer >   segment_cell_scalar_data;
        std::map< segment_id_type, CellVectorOutputAccessorContainer >   segment_cell_vector_data;

        // End data structures definition


        // Lambdas
        auto preparePoints = [&](const MeshType& segment, segment_id_type seg_id) {
          typedef typename viennagrid::result_of::const_element_range<MeshType, CellTag>::type     CellRange;
          typedef typename viennagrid::result_of::iterator<CellRange>::type                                         CellIterator;

          typedef typename viennagrid::result_of::const_element_range<CellType, viennagrid::vertex_tag>::type      VertexOnCellRange;
          typedef typename viennagrid::result_of::iterator<VertexOnCellRange>::type         VertexOnCellIterator;

          std::map< VertexIDType, ConstVertexHandleType > & current_used_vertex_map = used_vertex_map[seg_id];
          std::map< ConstVertexHandleType, VertexIDType > & current_vertex_to_index_map = vertex_to_index_map[seg_id];

          CellRange cells(segment);

          for (CellIterator it = cells.begin(); it != cells.end(); ++it) {
            VertexOnCellRange vertices_on_cell(*it);
            for (VertexOnCellIterator jt = vertices_on_cell.begin(); jt != vertices_on_cell.end(); ++jt) {
              typename std::map< VertexIDType, ConstVertexHandleType >::iterator kt = current_used_vertex_map.find( jt->id() );
              if (kt == current_used_vertex_map.end()) {
                current_used_vertex_map.insert( std::make_pair(jt->id(), jt.handle()) );
              }
            }
          }

          typename VertexIDType::base_id_type index = 0;
          for (typename std::map< VertexIDType, ConstVertexHandleType >::iterator it = current_used_vertex_map.begin(); it != current_used_vertex_map.end(); ++it) {
            current_vertex_to_index_map.insert( std::make_pair( it->second, VertexIDType(index++) ) );
          }

          return current_vertex_to_index_map.size();
        };

        auto prepareCells = [&](const MeshType& domseg, segment_id_type seg_id) {
          typedef typename viennagrid::result_of::const_element_range<MeshType, CellTag>::type     CellRange;
          typedef typename viennagrid::result_of::iterator<CellRange>::type                                         CellIterator;

          std::map< CellIDType, ConstCellHandleType > & current_used_cells_map = used_cell_map[seg_id];

          int index = 0;
          CellRange cells(domseg);
          for (CellIterator cit  = cells.begin(); cit != cells.end(); ++cit, ++index) {
            current_used_cells_map[ cit->id() ] = cit.handle();
          }

          return current_used_cells_map.size();
        };

        auto writePoints = [&](segment_id_type seg_id){
          vtkPoints* const vtk_points = vtkPoints::New();
          std::map< VertexIDType, ConstVertexHandleType > & current_used_vertex_map = used_vertex_map[seg_id];
          for (typename std::map< VertexIDType, ConstVertexHandleType >::iterator it = current_used_vertex_map.begin(); it != current_used_vertex_map.end(); ++it) {
            auto point = viennagrid::point(mesh_obj, it->second);
            vtk_points->InsertNextPoint( point[ 0 ], point[ 1 ], point[ 2 ] );
          }
          grid->SetPoints( vtk_points );
        };

        auto writePointDataS = [&](std::string const & name, const VertexScalarBaseAccesor & accessor, segment_id_type seg_id) {
          typedef typename VertexScalarBaseAccesor::value_type ValueType;
          vtkSmartPointer< vtkDoubleArray > scalars = vtkSmartPointer< vtkDoubleArray >::New();
          scalars->SetNumberOfComponents( 1 );
          scalars->SetName( name.c_str() );
          std::map< VertexIDType, ConstVertexHandleType > & current_used_vertex_map = used_vertex_map[seg_id];
          for (typename std::map< VertexIDType, ConstVertexHandleType >::iterator it = current_used_vertex_map.begin(); it != current_used_vertex_map.end(); ++it) {
            const double scalar = accessor( viennagrid::dereference_handle(mesh_obj, it->second) );
            scalars->InsertNextValue( scalar );
          }
          grid->GetPointData()->SetScalars( scalars );
        };

        auto writePointDataV = [&](std::string const & name, const VertexVectorBaseAccesor & accessor, segment_id_type seg_id) {
          typedef typename VertexVectorBaseAccesor::value_type ValueType;

          vtkSmartPointer< vtkDoubleArray > vectors = vtkSmartPointer< vtkDoubleArray >::New();
          vectors->SetNumberOfComponents( 3 );
          vectors->SetName( name.c_str() );

          std::map< VertexIDType, ConstVertexHandleType > & current_used_vertex_map = used_vertex_map[seg_id];
          for (typename std::map< VertexIDType, ConstVertexHandleType >::iterator it = current_used_vertex_map.begin(); it != current_used_vertex_map.end(); ++it) {
            const std::vector< double > vector = accessor( viennagrid::dereference_handle(mesh_obj, it->second) );
            double p[ 3 ];
            p[ 0 ] = vector[ 0 ];
            p[ 1 ] = vector[ 1 ];
            p[ 2 ] = vector[ 2 ];
            vectors->InsertNextTupleValue( p );
          }
          grid->GetPointData()->SetVectors( vectors );
        };

        auto writeCellDataS = [&](std::string const & name, const CellScalarBaseAccesor & accessor, segment_id_type seg_id) {
          typedef typename CellScalarBaseAccesor::value_type ValueType;
          vtkSmartPointer< vtkDoubleArray > scalars = vtkSmartPointer< vtkDoubleArray >::New();
          scalars->SetNumberOfComponents( 1 );
          scalars->SetName( name.c_str() );
          std::map< CellIDType, ConstCellHandleType > & current_used_cells_map = used_cell_map[seg_id];
          for (typename std::map< CellIDType, ConstCellHandleType >::iterator it = current_used_cells_map.begin(); it != current_used_cells_map.end(); ++it) {
            CellType const & cell = viennagrid::dereference_handle(mesh_obj, it->second);
            const double scalar = accessor( cell );
            scalars->InsertNextValue( scalar );
          }
          grid->GetCellData()->SetScalars( scalars );
        };

        auto writeCellDataV = [&](std::string const & name, const CellVectorBaseAccesor & accessor, segment_id_type seg_id) {
          typedef typename CellVectorBaseAccesor::value_type ValueType;
          vtkSmartPointer< vtkDoubleArray > vectors = vtkSmartPointer< vtkDoubleArray >::New();
          vectors->SetNumberOfComponents( 3 );
          vectors->SetName( name.c_str() );
          std::map< CellIDType, ConstCellHandleType > & current_used_cells_map = used_cell_map[seg_id];
          for (typename std::map< CellIDType, ConstCellHandleType >::iterator it = current_used_cells_map.begin(); it != current_used_cells_map.end(); ++it) {
            CellType const & cell = viennagrid::dereference_handle(mesh_obj, it->second);
            const std::vector< double > vector = accessor( cell );
            double p[ 3 ];
            p[ 0 ] = vector[ 0 ];
            p[ 1 ] = vector[ 1 ];
            p[ 2 ] = vector[ 2 ];
            vectors->InsertNextTupleValue( p );
          }
          grid->GetCellData()->SetVectors( vectors );
        };

        auto writeCells = [&](segment_id_type seg_id){
          typedef typename viennagrid::result_of::const_element_range<CellType, viennagrid::vertex_tag>::type      VertexOnCellRange;
          typedef typename viennagrid::result_of::iterator<VertexOnCellRange>::type         VertexOnCellIterator;

          vtkCellArray* const vtk_cells = vtkCellArray::New();
          vtkSmartPointer< vtkIdTypeArray > CellPoints = vtkSmartPointer< vtkIdTypeArray >::New();
          vtkSmartPointer< vtkIdTypeArray > CellOffsets = vtkSmartPointer< vtkIdTypeArray >::New();

          std::map< ConstVertexHandleType, VertexIDType > & current_vertex_to_index_map = vertex_to_index_map[seg_id];

          std::map< CellIDType, ConstCellHandleType > & current_used_cells_map = used_cell_map[seg_id];
          for (typename std::map< CellIDType, ConstCellHandleType >::iterator it = current_used_cells_map.begin(); it != current_used_cells_map.end(); ++it) {
            //Step 1: Write vertex indices in ViennaGrid orientation to array:
            CellType const & cell = viennagrid::dereference_handle( mesh_obj, it->second );

            std::vector<VertexIDType> viennagrid_vertices(viennagrid::boundary_elements<CellTag, viennagrid::vertex_tag>::num);
            VertexOnCellRange vertices_on_cell = viennagrid::elements<viennagrid::vertex_tag>(cell);
            std::size_t j = 0;
            for (VertexOnCellIterator vocit = vertices_on_cell.begin(); vocit != vertices_on_cell.end(); ++vocit, ++j) {
              viennagrid_vertices[j] = current_vertex_to_index_map[vocit.handle()];
            }

            //Step 2: Write the transformed connectivities:
            viennagrid::io::detail::viennagrid_to_vtk_orientations<CellTag> reorderer;
            for (std::size_t i=0; i<viennagrid_vertices.size(); ++i) {
              const vtkIdType id = viennagrid_vertices[reorderer(i)].get();
              CellPoints->InsertNextValue( id );
            }

          }

          // Offsets
          for (std::size_t offsets = 1; offsets <= viennagrid::elements<CellTag>( mesh_obj ).size(); ++offsets) {
            CellOffsets->InsertNextValue( offsets * viennagrid::boundary_elements<CellTag, viennagrid::vertex_tag>::num );
          }

          for ( auto i = 0; i < CellOffsets->GetNumberOfTuples(); ++i ) {
            vtkIdType offset = CellOffsets->GetValue( i ); // Offset into the connectivity array for the end of each cell
            vtkIdType cell[ 4 ];
            cell[ 0 ] = CellPoints->GetValue( offset - 4 );
            cell[ 1 ] = CellPoints->GetValue( offset - 3 );
            cell[ 2 ] = CellPoints->GetValue( offset - 2 );
            cell[ 3 ] = CellPoints->GetValue( offset - 1 );
            vtk_cells->InsertNextCell( 4, cell );
          }
          grid->SetCells( VTK_TETRA, vtk_cells );

        };

        auto clear = [&]() {
          clear_map(vertex_scalar_data);
          clear_map(vertex_vector_data);

          clear_map(cell_scalar_data);
          clear_map(cell_vector_data);


          for (auto it = segment_vertex_scalar_data.begin(); it != segment_vertex_scalar_data.end(); ++it)
            clear_map(it->second);

          for (auto it = segment_vertex_vector_data.begin(); it != segment_vertex_vector_data.end(); ++it)
            clear_map(it->second);

          for (auto it = segment_cell_scalar_data.begin(); it != segment_cell_scalar_data.end(); ++it)
            clear_map(it->second);

          for (auto it = segment_cell_vector_data.begin(); it != segment_cell_vector_data.end(); ++it)
            clear_map(it->second);

          segment_vertex_scalar_data.clear();
          segment_vertex_vector_data.clear();

          segment_cell_scalar_data.clear();
          segment_cell_vector_data.clear();

          used_vertex_map.clear();
          vertex_to_index_map.clear();
          used_cell_map.clear();
        };
        // End lambdas

        // Code
        std::vector<long> id_vector(1);
        id_vector[0] = id;

        std::map< std::string, std::deque<double> > output_values;
        for (std::size_t i=0; i<id_vector.size(); ++i) {
          auto id = id_vector[i];

          MappingKeyType  map_key(id);
          BoundaryKeyType bnd_key(id);

          typename viennadata::result_of::accessor<const StorageType, viennafem::mapping_key, long, VertexType>::type   mapping_accessor = viennadata::make_accessor<viennafem::mapping_key, long, VertexType>(storage, map_key);

          typename viennadata::result_of::accessor<const StorageType, BoundaryKeyType, double, VertexType>::type        boundary_accessor = viennadata::make_accessor<BoundaryKeyType, double, VertexType>(storage, bnd_key);

          std::stringstream ss;
          ss << "fem_result" << id;
          std::string result_string = ss.str(); // also used for passing staff over to VTK

          typename viennagrid::result_of::accessor< std::deque<double>, VertexType >::type output_value_accessor( output_values[result_string] );

          VertexContainer vertices = viennagrid::elements<VertexType>( mesh_obj );
          for (VertexIterator vit = vertices.begin(); vit != vertices.end(); ++vit) {
            long cur_index = mapping_accessor(*vit);
            if (cur_index > -1)
              output_value_accessor(*vit) = result[cur_index];
            else //use Dirichlet boundary data:
            {
              // TODO if Accessor concept takes care of that -> change!
              if (boundary_accessor.find(*vit))
                output_value_accessor(*vit) = boundary_accessor(*vit);
              else
                output_value_accessor(*vit) = false;
            }
            //output_value_accessor(*vit) = boundary_accessor(*vit);
          }

          // my_vtk_writer.add_scalar_data_on_vertices( output_value_accessor, result_string );
          {
            // add_to_vertex_container(vertex_scalar_data, accessor_or_field, quantity_name); }
            // Map: vertex_scalar_data
            // AccessorOrFieldType: output_value_accessor
            auto it = vertex_scalar_data.find( result_string );
            if ( it != vertex_scalar_data.end() )
            {
              delete it->second;
              it->second = new viennagrid::dynamic_field_wrapper<const decltype( output_value_accessor ), VertexType>( output_value_accessor );
            }
            else
              vertex_scalar_data[ result_string ] = new viennagrid::dynamic_field_wrapper<const decltype( output_value_accessor ), VertexType>( output_value_accessor );
          }
        }


        // operator()(MeshType const & mesh_obj, SegmentationType const & segmentation, std::string const & filename)
        assert( segmentation.size() <= 1 );

        // operator()(MeshType const & mesh_obj, std::string const & filename)
        {
          segment_id_type tmp_id = segment_id_type();

          preparePoints(mesh_obj, tmp_id);
          prepareCells(mesh_obj, tmp_id);
          writePoints( tmp_id );

          if ( vertex_scalar_data.size() > 0 || vertex_vector_data.size() > 0 ) {
            for (typename VertexScalarOutputAccessorContainer::const_iterator it = vertex_scalar_data.begin(); it != vertex_scalar_data.end(); ++it) {
              writePointDataS( it->first, *(it->second), tmp_id );
            }
            for (typename VertexVectorOutputAccessorContainer::const_iterator it = vertex_vector_data.begin(); it != vertex_vector_data.end(); ++it) {
              writePointDataV( it->first, *(it->second), tmp_id );
            }
          }
          writeCells( tmp_id );
          if (cell_scalar_data.size() > 0 || cell_vector_data.size() > 0) {
            for (typename CellScalarOutputAccessorContainer::const_iterator it = cell_scalar_data.begin(); it != cell_scalar_data.end(); ++it) {
              writeCellDataS( it->first, *(it->second), tmp_id );
            }
            for (typename CellVectorOutputAccessorContainer::const_iterator it = cell_vector_data.begin(); it != cell_vector_data.end(); ++it) {
              writeCellDataV( it->first, *(it->second), tmp_id );
            }
          }
          clear();
        }

      }


      template< typename MeshType, typename SegmentationType = typename viennagrid::result_of::segmentation<MeshType>::type >
      /*!
       * \brief vtkUnstructuredGridToViennaGridDomain
       * \param mesh_obj
       * \param segmentation
       * \param grid
       */
      void vtkUnstructuredGridToViennaGridDomain(MeshType & mesh_obj, SegmentationType & segmentation, vtkUnstructuredGrid* const grid)
      {
        // Typedefs
        typedef MeshType mesh_type;
        typedef SegmentationType segmentation_type;

        typedef typename SegmentationType::segment_handle_type SegmentHandleType;
        typedef typename SegmentationType::segment_id_type segment_id_type;


        typedef typename viennagrid::result_of::point<MeshType>::type PointType;
        typedef typename viennagrid::result_of::coord<PointType>::type CoordType;
        static const int geometric_dim = viennagrid::result_of::static_size<PointType>::value;

        typedef typename viennagrid::result_of::cell_tag<MeshType>::type CellTag;


        typedef typename viennagrid::result_of::vertex<MeshType>::type                          VertexType;
        typedef typename viennagrid::result_of::vertex_handle<MeshType>::type                           VertexHandleType;
        typedef typename viennagrid::result_of::vertex_id<MeshType>::type                           VertexIDType;
        typedef typename viennagrid::result_of::element<MeshType, CellTag>::type     CellType;
        typedef typename viennagrid::result_of::handle<MeshType, CellTag>::type     CellHandleType;

        typedef typename viennagrid::result_of::vertex_range<MeshType>::type   VertexRange;
        typedef typename viennagrid::result_of::iterator<VertexRange>::type                             VertexIterator;

        typedef typename viennagrid::result_of::line_range<MeshType>::type     EdgeRange;
        typedef typename viennagrid::result_of::iterator<EdgeRange>::type                               EdgeIterator;

        typedef typename viennagrid::result_of::facet_range<MeshType>::type   FacetRange;
        typedef typename viennagrid::result_of::iterator<FacetRange>::type                                   FacetIterator;

        typedef typename viennagrid::result_of::cell_range<MeshType>::type     CellRange;
        typedef typename viennagrid::result_of::iterator<CellRange>::type                  CellIterator;


        typedef std::vector<double> vector_data_type;

        typedef std::map< std::string, viennagrid::base_dynamic_field<double, VertexType> * >             VertexScalarOutputFieldContainer;
        typedef std::map< std::string, viennagrid::base_dynamic_field<vector_data_type, VertexType> * >   VertexVectorOutputFieldContainer;

        typedef std::map< std::string, viennagrid::base_dynamic_field<double, CellType> * >               CellScalarOutputFieldContainer;
        typedef std::map< std::string, viennagrid::base_dynamic_field<vector_data_type, CellType> * >     CellVectorOutputFieldContainer;
        typedef viennagrid::element_key<CellType> CellElementKeyType;



        // Data structures declaration
        std::map<PointType, std::size_t, viennagrid::point_less>         global_points;
        std::map<std::size_t, PointType>                     global_points_2;
        std::map<int, std::deque<std::size_t> >              local_to_global_map;
        std::map<int, std::deque<std::size_t> >              local_cell_vertices;
        std::map<int, std::deque<std::size_t> >              local_cell_offsets;
        std::map<int, std::size_t>                           local_cell_num;
        std::map<int, std::deque<CellHandleType> >           local_cell_handle;

        std::map<CellElementKeyType, CellHandleType>         global_cells;

        std::map<int, std::deque<std::pair<std::string, std::deque<double> > > >  local_scalar_vertex_data;
        std::map<int, std::deque<std::pair<std::string, std::deque<double> > > >  local_vector_vertex_data;
        std::map<int, std::deque<std::pair<std::string, std::deque<double> > > >  local_scalar_cell_data;
        std::map<int, std::deque<std::pair<std::string, std::deque<double> > > >  local_vector_cell_data;


        // Quantities read:
        std::vector<std::pair<std::size_t, std::string> >         vertex_data_scalar_read;
        std::vector<std::pair<std::size_t, std::string> >         vertex_data_vector_read;

        std::vector<std::pair<std::size_t, std::string> >         cell_data_scalar_read;
        std::vector<std::pair<std::size_t, std::string> >         cell_data_vector_read;


        std::map< std::string, std::map<segment_id_type, std::deque<double> > >           vertex_scalar_data;
        std::map< std::string, std::map<segment_id_type, std::deque<vector_data_type> > > vertex_vector_data;

        std::map< std::string, std::map<segment_id_type, std::deque<double> > >           cell_scalar_data;
        std::map< std::string, std::map<segment_id_type, std::deque<vector_data_type> > > cell_vector_data;


        VertexScalarOutputFieldContainer          registered_vertex_scalar_data;
        VertexVectorOutputFieldContainer          registered_vertex_vector_data;

        CellScalarOutputFieldContainer          registered_cell_scalar_data;
        CellVectorOutputFieldContainer          registered_cell_vector_data;

        std::map< segment_id_type, VertexScalarOutputFieldContainer > registered_segment_vertex_scalar_data;
        std::map< segment_id_type, VertexVectorOutputFieldContainer > registered_segment_vertex_vector_data;

        std::map< segment_id_type, CellScalarOutputFieldContainer >   registered_segment_cell_scalar_data;
        std::map< segment_id_type, CellVectorOutputFieldContainer >   registered_segment_cell_vector_data;

        // End data structured declaration


        // Lambdas declaration

        auto pre_clear = [&]() {
          vertex_data_scalar_read.clear();
          vertex_data_vector_read.clear();

          cell_data_scalar_read.clear();
          cell_data_vector_read.clear();

          vertex_scalar_data.clear();
          vertex_vector_data.clear();

          cell_scalar_data.clear();
          cell_vector_data.clear();

          global_points.clear();
          global_points_2.clear();
          local_to_global_map.clear();
          local_cell_vertices.clear();
          local_cell_offsets.clear();
          local_cell_num.clear();
          local_cell_handle.clear();

          global_cells.clear();

          local_scalar_vertex_data.clear();
          local_vector_vertex_data.clear();
          local_scalar_cell_data.clear();
          local_vector_cell_data.clear();
        };

        auto readNodeCoordinates = [&](const std::size_t nodeNum, const std::size_t numberOfComponents, segment_id_type seg_id) {
          local_to_global_map[ seg_id ].resize( nodeNum );
          vtkPoints* pointsPtr = grid->GetPoints();
          for ( std::size_t i = 0; i < nodeNum; ++ i ) {
            PointType p;
            double* pPtr = pointsPtr->GetPoint( i );
            for ( std::size_t j = 0; j < numberOfComponents; ++ j ) {
              if ( j < static_cast<std::size_t>(geometric_dim) )
                p[ j ] = pPtr[ j ];
            }
            if ( global_points.find( p ) == global_points.end() ) {
              const std::size_t new_global_id = global_points.size();
              global_points.insert( std::make_pair( p, new_global_id ) );
              global_points_2.insert( std::make_pair( new_global_id, p ) );
              local_to_global_map[ seg_id ][ i ] = new_global_id;
            }
            else {
              local_to_global_map[ seg_id ][ i ] = global_points[ p ];
            }
          }
        };

        auto readData = [&](std::deque< double >& container, vtkDataArray* const arr) {
          const auto arrNumOfTuples = arr->GetNumberOfTuples();
          for ( auto i = 0; i < arrNumOfTuples; ++ i ) {
            container.push_back( arr->GetComponent( VTK_DOUBLE, i ) );
          }
        };

        auto readPointData = [&](segment_id_type seg_id,
            decltype( local_scalar_vertex_data )& scalar_data,
            decltype( local_vector_vertex_data )& vector_data,
            decltype( vertex_data_scalar_read )& data_names_scalar,
            decltype( vertex_data_vector_read )&  data_names_vector) {
          vtkSmartPointer< vtkPointData > pd( grid->GetPointData() );
          if ( pd ) {
            for ( int i = 0; i < pd->GetNumberOfArrays(); ++ i ) {
              const std::string name = ( pd->GetArrayName( i ) ? pd->GetArrayName( i ) : "" );
              vtkSmartPointer< vtkDataArray > arr( pd->GetArray( i ) );
              const std::size_t components = arr->GetNumberOfTuples();
              if ( components == 1 ) {
                data_names_scalar.push_back( std::make_pair( seg_id, name ) );
                scalar_data[ seg_id ].push_back( std::make_pair( name, std::deque<double>() ) );
                readData( scalar_data[ seg_id ].back().second, arr );
              }
              else if ( components == 3 )
              {
                data_names_vector.push_back( std::make_pair( seg_id, name ) );
                vector_data[ seg_id ].push_back( std::make_pair( name, std::deque<double>() ) );
                readData( vector_data[ seg_id ].back().second, arr );
              }
            }

          }// pd

        };


        auto readCellData = [&](segment_id_type seg_id,
            decltype( local_scalar_cell_data )& scalar_data,
            decltype( local_vector_cell_data )& vector_data,
            decltype( cell_data_scalar_read )& data_names_scalar,
            decltype( cell_data_vector_read )&  data_names_vector) {

          vtkSmartPointer< vtkCellData > cd( grid->GetCellData() );
          if ( cd ) {
            for ( int i = 0; i < cd->GetNumberOfArrays(); ++ i ) {
              const std::string name = ( cd->GetArrayName( i ) ? cd->GetArrayName( i ) : "" );
              vtkSmartPointer< vtkDataArray > arr( cd->GetArray( i ) );
              const std::size_t components = arr->GetNumberOfTuples();
              if ( components == 1 ) {
                data_names_scalar.push_back( std::make_pair( seg_id, name ) );
                scalar_data[ seg_id ].push_back( std::make_pair( name, std::deque<double>() ) );
                readData( scalar_data[ seg_id ].back().second, arr );
              }
              else if ( components == 3 )
              {
                data_names_vector.push_back( std::make_pair( seg_id, name ) );
                vector_data[ seg_id ].push_back( std::make_pair( name, std::deque<double>() ) );
                readData( vector_data[ seg_id ].back().second, arr );
              }
            }

          }// cd

        };


        auto readCells = [&](segment_id_type seg_id) {
          vtkSmartPointer< vtkCellArray > cellArr( grid->GetCells() );
          // vtkSmartPointer< vtkUnsignedCharArray > cellTypesArray( grid->GetCellTypesArray() );

          // Connectivity
          vtkSmartPointer< vtkIdTypeArray > CellPoints = vtkSmartPointer< vtkIdTypeArray >::New();
          vtkSmartPointer< vtkIdTypeArray > CellOffsets = vtkSmartPointer< vtkIdTypeArray >::New();

          [&](vtkCellArray* cells) {
            vtkIdTypeArray* connectivity = cells->GetData();
            vtkIdType numberOfCells = cells->GetNumberOfCells();
            vtkIdType numberOfTuples = connectivity->GetNumberOfTuples();

            CellPoints->SetNumberOfTuples(numberOfTuples - numberOfCells);
            CellOffsets->SetNumberOfTuples(numberOfCells);

            vtkIdType* inCell = connectivity->GetPointer(0);
            vtkIdType* outCellPointsBase = CellPoints->GetPointer(0);
            vtkIdType* outCellPoints = outCellPointsBase;
            vtkIdType* outCellOffset = CellOffsets->GetPointer(0);

            vtkIdType i;
            for( i = 0; i < numberOfCells; ++i ) {
              vtkIdType numberOfPoints = *inCell++;
              memcpy(outCellPoints, inCell, sizeof(vtkIdType)*numberOfPoints);
              outCellPoints += numberOfPoints;
              inCell += numberOfPoints;
              *outCellOffset++ = outCellPoints - outCellPointsBase;
            }
          }( cellArr );
          // Process CellPoints
          for ( auto i = 0; i < CellPoints->GetNumberOfTuples(); ++ i ) {
            local_cell_vertices[ seg_id ].push_back( CellPoints->GetValue( i ) );
          }

          // Offsets
          // Process CellOffsets
          for ( auto i = 0; i < CellOffsets->GetNumberOfTuples(); ++ i ) {
            local_cell_offsets[ seg_id ].push_back( CellOffsets->GetValue( i ) );
          }

          // Types
        };

        auto process_vtu = [&](vtkUnstructuredGrid* const grid, segment_id_type seg_id = 0) {
          const std::size_t nodeNum = grid->GetNumberOfPoints();
          local_cell_num[ seg_id ] = grid->GetNumberOfCells();
          const std::size_t numberOfComponents = 3; // For coordinates
          readNodeCoordinates( nodeNum, numberOfComponents, seg_id );
          readPointData( seg_id, local_scalar_vertex_data, local_vector_vertex_data, vertex_data_scalar_read, vertex_data_vector_read );
          readCells( seg_id );
          readCellData( seg_id, local_scalar_cell_data, local_vector_cell_data, cell_data_scalar_read, cell_data_vector_read );
        };

        auto setupVertices = [&]() {
          for (std::size_t i=0; i<global_points_2.size(); ++i) {
            viennagrid::make_vertex_with_id( mesh_obj,
                                             typename VertexType::id_type(
                                               typename VertexType::id_type::base_id_type(i)),
                                             global_points_2[i] );
          }
        };

        auto setupCells = [&](segment_id_type seg_id) {
          std::size_t numVertices = 0;
          std::size_t offsetIdx = 0;
          std::deque< std::size_t > const & offsets = local_cell_offsets[ seg_id ];
          for ( std::size_t i = 0; i < local_cell_num[ seg_id ]; ++i ) {
            if( i == 0 ) {
              offsetIdx = 0;
              numVertices = offsets[ i ];
            }
            else {
              offsetIdx = offsets[ i - 1 ];
              numVertices = offsets[ i ] - offsets[ i - 1 ];
            }
            viennagrid::static_array<VertexHandleType, viennagrid::boundary_elements<CellTag, viennagrid::vertex_tag>::num> cell_vertex_handles;
            std::vector<VertexIDType> cell_vertex_ids(numVertices);

            viennagrid::io::detail::vtk_to_viennagrid_orientations<CellTag> reorderer;
            for (std::size_t j = 0; j < numVertices; j++) {
              const std::size_t reordered_j = reorderer(j);
              const std::size_t local_index = local_cell_vertices[seg_id][reordered_j + offsetIdx];
              const std::size_t global_vertex_index = local_to_global_map[seg_id][local_index];

              cell_vertex_handles[j] = viennagrid::elements<viennagrid::vertex_tag>(mesh_obj).handle_at(global_vertex_index);
              viennagrid::add( segmentation[seg_id], viennagrid::dereference_handle(segmentation, cell_vertex_handles[j]) );

              cell_vertex_ids[j] = viennagrid::dereference_handle(mesh_obj, cell_vertex_handles[j]).id();
            }

            CellElementKeyType cell_key(cell_vertex_ids);
            auto chit = global_cells.find( cell_key );
            if (chit != global_cells.end()) {
              local_cell_handle[seg_id].push_back( chit->second );
              viennagrid::add( segmentation[seg_id], viennagrid::dereference_handle(mesh_obj, chit->second) );
            }
            else {
              CellHandleType cell_handle = viennagrid::make_element<CellType>(segmentation[seg_id], cell_vertex_handles.begin(), cell_vertex_handles.end());
              global_cells[cell_key] = cell_handle;

              local_cell_handle[seg_id].push_back(cell_handle);
            }
          }
        };

        auto setupDataVertex = [&](segment_id_type seg_id, SegmentHandleType &
    #ifndef NDEBUG
            segment
    #endif
            ,const std::pair<std::string, std::deque<double> > & container, std::size_t num_components) {
          std::string const & name = container.first;

          if (num_components == 1) {
            VertexScalarOutputFieldContainer & current_registered_segment_vertex_scalar_data = registered_segment_vertex_scalar_data[seg_id];
            if (registered_vertex_scalar_data.find(name) != registered_vertex_scalar_data.end()) {
              for (std::size_t i=0; i<container.second.size(); ++i) {
                std::size_t global_vertex_id = local_to_global_map[seg_id][i];
                VertexType const & vertex = viennagrid::elements<viennagrid::vertex_tag>(mesh_obj)[global_vertex_id];

                (*registered_vertex_scalar_data[name])(vertex) = (container.second)[i];
              }
            }
            else if (current_registered_segment_vertex_scalar_data.find(name) != current_registered_segment_vertex_scalar_data.end()) {
              for (std::size_t i=0; i<container.second.size(); ++i) {
                std::size_t global_vertex_id = local_to_global_map[seg_id][i];
                VertexType const & vertex = viennagrid::elements<viennagrid::vertex_tag>(mesh_obj)[global_vertex_id];

                (*current_registered_segment_vertex_scalar_data[name])(vertex) = (container.second)[i];
              }
            }
            else {
              for (std::size_t i=0; i<container.second.size(); ++i) {
                std::size_t global_vertex_id = local_to_global_map[seg_id][i];
                VertexType const & vertex = viennagrid::elements<viennagrid::vertex_tag>(mesh_obj)[global_vertex_id];

                if ( static_cast<typename VertexType::id_type::base_id_type>(vertex_scalar_data[container.first][seg_id].size()) <= vertex.id().get())
                  vertex_scalar_data[container.first][seg_id].resize(static_cast<std::size_t>(vertex.id().get()+1));
                vertex_scalar_data[container.first][seg_id][static_cast<std::size_t>(vertex.id().get())] = (container.second)[i];
              }
            }
          }
          else {
            VertexVectorOutputFieldContainer & current_registered_segment_vertex_vector_data = registered_segment_vertex_vector_data[seg_id];
            if (registered_vertex_vector_data.find(name) != registered_vertex_vector_data.end()) {
              for (std::size_t i=0; i<container.second.size()/3; ++i) {
                std::size_t global_vertex_id = local_to_global_map[seg_id][i];
                VertexType const & vertex = viennagrid::elements<viennagrid::vertex_tag>(mesh_obj)[global_vertex_id];

                (*registered_vertex_vector_data[name])(vertex).resize(3);
                (*registered_vertex_vector_data[name])(vertex)[0] = (container.second)[3*i+0];
                (*registered_vertex_vector_data[name])(vertex)[1] = (container.second)[3*i+1];
                (*registered_vertex_vector_data[name])(vertex)[2] = (container.second)[3*i+2];
              }
            }
            else if (current_registered_segment_vertex_vector_data.find(name) != current_registered_segment_vertex_vector_data.end()) {
              for (std::size_t i=0; i<container.second.size(); ++i) {
                std::size_t global_vertex_id = local_to_global_map[seg_id][i];
                VertexType const & vertex = viennagrid::elements<viennagrid::vertex_tag>(mesh_obj)[global_vertex_id];

                (*current_registered_segment_vertex_vector_data[name])(vertex).resize(3);
                (*current_registered_segment_vertex_vector_data[name])(vertex)[0] = (container.second)[3*i+0];
                (*current_registered_segment_vertex_vector_data[name])(vertex)[1] = (container.second)[3*i+1];
                (*current_registered_segment_vertex_vector_data[name])(vertex)[2] = (container.second)[3*i+2];
              }
            }
            else {
              assert( 3 * viennagrid::elements<viennagrid::vertex_tag>(segment).size() == container.second.size());
              for (std::size_t i=0; i<container.second.size() / 3; ++i) {
                std::size_t global_vertex_id = local_to_global_map[seg_id][i];
                VertexType const & vertex = viennagrid::elements<viennagrid::vertex_tag>(mesh_obj)[global_vertex_id];

                if ( static_cast<typename VertexType::id_type::base_id_type>(vertex_vector_data[container.first][seg_id].size()) <= vertex.id().get())
                  vertex_vector_data[container.first][seg_id].resize(static_cast<std::size_t>(vertex.id().get()+1));
                vertex_vector_data[container.first][seg_id][static_cast<std::size_t>(vertex.id().get())].resize(3);
                vertex_vector_data[container.first][seg_id][static_cast<std::size_t>(vertex.id().get())][0] = (container.second)[3*i+0];
                vertex_vector_data[container.first][seg_id][static_cast<std::size_t>(vertex.id().get())][1] = (container.second)[3*i+1];
                vertex_vector_data[container.first][seg_id][static_cast<std::size_t>(vertex.id().get())][2] = (container.second)[3*i+2];
              }
            }
          }
        };

        auto setupDataCell = [&](segment_id_type seg_id, SegmentHandleType &
    #ifndef NDEBUG
            segment
    #endif
            ,const std::pair<std::string, std::deque<double> > & container, std::size_t num_components ) {
          std::string const & name = container.first;

          if (num_components == 1) {
            CellScalarOutputFieldContainer & current_registered_segment_cell_scalar_data = registered_segment_cell_scalar_data[seg_id];
            if (registered_cell_scalar_data.find(name) != registered_cell_scalar_data.end()) {
              for (std::size_t i=0; i<container.second.size(); ++i) {
                CellType const & cell = viennagrid::dereference_handle( segment, local_cell_handle[seg_id][i] );

                (*registered_cell_scalar_data[name])(cell) = (container.second)[i];
              }
            }
            else if (current_registered_segment_cell_scalar_data.find(name) != current_registered_segment_cell_scalar_data.end()) {
              for (std::size_t i=0; i<container.second.size(); ++i) {
                CellType const & cell = viennagrid::dereference_handle( segment, local_cell_handle[seg_id][i] );

                (*current_registered_segment_cell_scalar_data[name])(cell) = (container.second)[i];
              }
            }
            else {
              for (std::size_t i=0; i<container.second.size(); ++i) {
                CellType const & cell = viennagrid::dereference_handle( segment, local_cell_handle[seg_id][i] );

                if ( static_cast<typename CellType::id_type::base_id_type>(cell_scalar_data[container.first][seg_id].size()) <= cell.id().get())
                  cell_scalar_data[container.first][seg_id].resize(static_cast<std::size_t>(cell.id().get()+1));
                cell_scalar_data[container.first][seg_id][static_cast<std::size_t>(cell.id().get())] = (container.second)[i];
              }
            }
          }
          else {
            CellVectorOutputFieldContainer & current_registered_segment_cell_vector_data = registered_segment_cell_vector_data[seg_id];
            if (registered_cell_vector_data.find(name) != registered_cell_vector_data.end()) {
              for (std::size_t i=0; i<container.second.size()/3; ++i) {
                CellType const & cell = viennagrid::dereference_handle( segment, local_cell_handle[seg_id][i] );

                (*registered_cell_vector_data[name])(cell).resize(3);
                (*registered_cell_vector_data[name])(cell)[0] = (container.second)[3*i+0];
                (*registered_cell_vector_data[name])(cell)[1] = (container.second)[3*i+1];
                (*registered_cell_vector_data[name])(cell)[2] = (container.second)[3*i+2];
              }
            }
            else if (current_registered_segment_cell_vector_data.find(name) != current_registered_segment_cell_vector_data.end()) {
              for (std::size_t i=0; i<container.second.size(); ++i) {
                CellType const & cell = viennagrid::dereference_handle( segment, local_cell_handle[seg_id][i] );

                (*current_registered_segment_cell_vector_data[name])(cell).resize(3);
                (*current_registered_segment_cell_vector_data[name])(cell)[0] = (container.second)[3*i+0];
                (*current_registered_segment_cell_vector_data[name])(cell)[1] = (container.second)[3*i+1];
                (*current_registered_segment_cell_vector_data[name])(cell)[2] = (container.second)[3*i+2];
              }
            }
            else {
              assert( 3 * viennagrid::elements<CellTag>(segment).size() == container.second.size());
              for (std::size_t i=0; i<container.second.size() / 3; ++i) {
                CellType const & cell = viennagrid::dereference_handle( segment, local_cell_handle[seg_id][i] );

                if ( static_cast<typename CellType::id_type::base_id_type>(cell_vector_data[container.first][seg_id].size()) <= cell.id().get())
                  cell_vector_data[container.first][seg_id].resize(static_cast<std::size_t>(cell.id().get()+1));
                cell_vector_data[container.first][seg_id][static_cast<std::size_t>(cell.id().get())].resize(3);
                cell_vector_data[container.first][seg_id][static_cast<std::size_t>(cell.id().get())][0] = (container.second)[3*i+0];
                cell_vector_data[container.first][seg_id][static_cast<std::size_t>(cell.id().get())][1] = (container.second)[3*i+1];
                cell_vector_data[container.first][seg_id][static_cast<std::size_t>(cell.id().get())][2] = (container.second)[3*i+2];
              }
            }
          }
        };

        auto setupData = [&](segment_id_type seg_id) {
          for ( size_t i = 0; i < local_scalar_vertex_data[ seg_id ].size(); ++i ) {
            setupDataVertex( seg_id, segmentation[seg_id], local_scalar_vertex_data[ seg_id ][ i ], 1 );
          }
          for ( size_t i = 0; i < local_vector_vertex_data[ seg_id ].size(); ++i ) {
            setupDataVertex( seg_id, segmentation[seg_id], local_vector_vertex_data[ seg_id ][ i ], 3 );
          }
          for ( size_t i = 0; i < local_scalar_cell_data[ seg_id ].size(); ++i ) {
            setupDataCell( seg_id, segmentation[seg_id], local_scalar_cell_data[ seg_id ][ i ], 1 );
          }
          for (size_t i = 0; i < local_vector_cell_data[ seg_id ].size(); ++i ) {
            setupDataCell( seg_id, segmentation[seg_id], local_vector_cell_data[ seg_id ][ i ], 3 );
          }
        };

        auto post_clear = [&]() {
          clear_map( registered_vertex_scalar_data );
          clear_map( registered_vertex_vector_data );

          clear_map( registered_cell_scalar_data );
          clear_map( registered_cell_vector_data );

          for (auto it = registered_segment_vertex_scalar_data.begin(); it != registered_segment_vertex_scalar_data.end(); ++it)
            clear_map(it->second);

          for (auto it = registered_segment_vertex_vector_data.begin(); it != registered_segment_vertex_vector_data.end(); ++it)
            clear_map(it->second);


          for (auto it = registered_segment_cell_scalar_data.begin(); it != registered_segment_cell_scalar_data.end(); ++it)
            clear_map(it->second);

          for (auto it = registered_segment_cell_vector_data.begin(); it != registered_segment_cell_vector_data.end(); ++it)
            clear_map(it->second);


          registered_segment_vertex_scalar_data.clear();
          registered_segment_vertex_vector_data.clear();

          registered_segment_cell_scalar_data.clear();
          registered_segment_cell_vector_data.clear();
        };

        // End lambdas declaration

        // Convertion code
        pre_clear();
        process_vtu( grid );

        setupVertices();
        for (auto it = local_cell_num.begin(); it != local_cell_num.end(); ++it) {
          segmentation.get_make_segment( it->first );
        }

        for (auto it = local_cell_num.begin(); it != local_cell_num.end(); ++it) {
          setupCells( it->first );
          setupData( it->first );
        }

        post_clear();
      }

    }
  }
}

#endif // VTKVIENNAGRIDCONVERTERS_HPP
