#include "DirihletBoundaryCondition.hpp"

#include <QDebug>

/*!
 * \brief DirihletBoundaryCondition::getCondition
 * \return
 */
double DirihletBoundaryCondition::getCondition() const
{
  return condition;
}

/*!
 * \brief DirihletBoundaryCondition::setCondition
 * \param value
 */
void DirihletBoundaryCondition::setCondition(double value)
{
  condition = value;
}

/*!
 * \brief DirihletBoundaryCondition::appyBoundaryCondition
 * \return
 */
double DirihletBoundaryCondition::appyBoundaryCondition(const bgeot::base_node &/*node*/) const
{
  return condition;
}


/*!
 * \brief DirihletBoundaryCondition::operator ==
 * \param rhs
 * \return
 */
bool DirihletBoundaryCondition::operator==(const AbstractBoundaryCondition &rhs) const
{
  // assert( conditionType == rhs.getBoundaryConditionType() );
  return AbstractBoundaryCondition::operator ==( rhs ) && ( condition == static_cast< const DirihletBoundaryCondition* >( &rhs )->getCondition() );
}

/*!
 * \brief DirihletBoundaryCondition::operator !=
 * \param rhs
 * \return
 */
bool DirihletBoundaryCondition::operator!=(const AbstractBoundaryCondition &rhs) const
{
  return ! operator ==( rhs );
}


/*!
 * \brief operator <<
 * \param ds
 * \param bc
 * \return
 */
QDataStream &operator<<(QDataStream &ds, const DirihletBoundaryCondition &bc)
{
  ds << QString( bc.getName().c_str() );
  ds << bc.getCondition();
  return ds;
}

/*!
 * \brief operator >>
 * \param ds
 * \param bc
 * \return
 */
QDataStream &operator>>(QDataStream &ds, DirihletBoundaryCondition &bc)
{
  QString name;
  ds >> name;
  bc.setName( name.toStdString() );
  double condition;
  ds >> condition;
  bc.setCondition( condition );
  return ds;
}








bool test::DirihletBoundaryConditionSerializationTest(int argc, char *argv[])
{
  std::cout << "test::DirihletBoundaryConditionSerializationTest" << std::endl;

  DirihletBoundaryCondition dirihlet;
  dirihlet.setName( "Dirihlet" );
  dirihlet.setCondition( 10 );

  QByteArray buffer;
  { // Serialize
    QDataStream ostream( &buffer, QIODevice::WriteOnly );
    ostream << dirihlet;
  }


  DirihletBoundaryCondition newDirihlet;
  { // Deserialize
    QDataStream istream( &buffer, QIODevice::ReadOnly );
    istream >> newDirihlet;
  }

  qDebug() << QString( newDirihlet.getName().c_str() );
  qDebug() << newDirihlet.getCondition();

  assert( dirihlet == newDirihlet );
  assert( dirihlet.getBoundaryConditionType() == newDirihlet.getBoundaryConditionType() );
  assert( dirihlet.getCondition() == newDirihlet.getCondition() );
  assert( dirihlet.getName() == newDirihlet.getName() );


  std::cout << "test::DirihletBoundaryConditionSerializationTest end" << std::endl;
  return true;
}
