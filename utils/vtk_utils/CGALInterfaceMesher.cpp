#include "CGALInterfaceMesher.hpp"

#include <limits>


#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Delaunay_mesher_2.h>
#include <CGAL/Delaunay_mesh_face_base_2.h>
#include <CGAL/Delaunay_mesh_size_criteria_2.h>

#include <CGAL/Delaunay_mesher_no_edge_refinement_2.h>

#include <CGAL/squared_distance_2.h>


#include <vtkFeatureEdges.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkCellArray.h>
#include <vtkExtractEdges.h>
#include <vtkLine.h>
#include <vtkStripper.h>
#include <vtkProperty.h>
#include <vtkPolyLine.h>
#include <vtkCubeSource.h>
#include <vtkTriangleFilter.h>

namespace kernel {
  namespace utils {
    namespace vtk_utils {

      vtkSmartPointer<vtkPolyData> triangulate_vtk_polydata_interfaseX(vtkPolyData *interface, const double B, const double S)
      {
        typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
        typedef CGAL::Triangulation_vertex_base_2<K> Vb;
        typedef CGAL::Delaunay_mesh_face_base_2<K> Fb;
        typedef CGAL::Triangulation_data_structure_2<Vb, Fb> Tds;
        typedef CGAL::Constrained_Delaunay_triangulation_2<K, Tds, CGAL::Exact_predicates_tag > CDT;
        typedef CGAL::Delaunay_mesh_size_criteria_2<CDT> Criteria;
        typedef CGAL::Delaunay_mesher_2<CDT, Criteria> Mesher;
        typedef CDT::Vertex_handle Vertex_handle;
        typedef CDT::Point Point;

        assert( interface != nullptr );
        assert( interface->GetNumberOfPoints() > 0 );

        CDT cdt;

        const double reference_x = interface->GetPoint( 0 )[ 0 ];

        for ( auto i = 0; i < interface->GetNumberOfPoints(); ++ i ) {
          auto point = interface->GetPoint( i );
          assert( fabs( point[ 0 ] - reference_x ) <= std::numeric_limits< double >::epsilon()  );
          // std::cout << i << ": (" << point[ 0 ] << ", " << point[ 1 ] << ", " << point[ 2 ] << ")" << std::endl;
        }

        std::vector< Point > seeds;
        {
          // Get boundary edges and set them as constrains

          vtkSmartPointer<vtkExtractEdges> featureEdges =  vtkSmartPointer<vtkExtractEdges>::New();
          featureEdges->SetInput( interface );
          featureEdges->Update();

          /*
          vtkSmartPointer<vtkFeatureEdges> featureEdges = vtkSmartPointer<vtkFeatureEdges>::New();
          {
            featureEdges->SetInput( interface );
            featureEdges->BoundaryEdgesOn();
            featureEdges->FeatureEdgesOff();
            featureEdges->ManifoldEdgesOff();
            featureEdges->NonManifoldEdgesOff();
            featureEdges->Update();
          }
          */

          // vtkSmartPointer< vtkPolyData > boundaryEdges = featureEdges->GetOutput();
          vtkSmartPointer< vtkPolyData > boundaryEdges = interface;

          vtkPoints* points = boundaryEdges->GetPoints();

          std::cout << "Boundary Edges" << std::endl;
          std::cout << "Cells: " << boundaryEdges->GetLines()->GetNumberOfCells() << std::endl;
          std::cout << "Points: " << points->GetNumberOfPoints() << std::endl;

          // Traverse all of the edges
          for(vtkIdType i = 0; i < boundaryEdges->GetNumberOfCells(); i++) {
            vtkSmartPointer<vtkLine> line = vtkLine::SafeDownCast( boundaryEdges->GetCell(i) );

            auto point0 = points->GetPoint( line->GetPointId( 0 ) );
            Point p0( point0[ 1 ], point0[ 2 ] );

            auto point1 = points->GetPoint( line->GetPointId( 1 ) );
            Point p1( point1[ 1 ], point1[ 2 ] );

            cdt.insert_constraint( p0, p1 );
          }

          auto seedsx = kernel::utils::vtk_utils::find_seeds_X( boundaryEdges );
          seeds.reserve( seedsx.size() );
          std::for_each( seedsx.begin(), seedsx.end(), [&](const decltype(seedsx)::value_type& v) {
            seeds.push_back( Point( std::get<0>( v ), std::get<1>( v ) ) );
          } );

        }

        assert( cdt.is_valid() );
        // std::cout << "Number of vertices: " << cdt.number_of_vertices() << std::endl;
        // std::cout << "Number of faces: " << cdt.number_of_faces() << std::endl;

        std::cout << "Meshing ..." << std::endl;

        Mesher mesher( cdt );
        mesher.set_seeds( seeds.begin(), seeds.end() );
        mesher.set_criteria( Criteria( B, S ) );
        mesher.refine_mesh();

        std::cout << "Number of vertices: " << cdt.number_of_vertices() << std::endl;
        std::cout << "Number of faces: " << cdt.number_of_faces() << std::endl << std::endl;

        vtkSmartPointer<vtkPolyData> triangulatedInterfase = CGAL::output_interface_X_to_vtk_polydata( cdt, reference_x );
        return triangulatedInterfase;
      }


      vtkSmartPointer<vtkPolyData> triangulate_vtk_polydata_interfaseY(vtkPolyData *interface, const double B, const double S)
      {
        typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
        typedef CGAL::Triangulation_vertex_base_2<K> Vb;
        typedef CGAL::Delaunay_mesh_face_base_2<K> Fb;
        typedef CGAL::Triangulation_data_structure_2<Vb, Fb> Tds;
        typedef CGAL::Constrained_Delaunay_triangulation_2<K, Tds, CGAL::Exact_predicates_tag > CDT;
        typedef CGAL::Delaunay_mesh_size_criteria_2<CDT> Criteria;
        typedef CGAL::Delaunay_mesher_2<CDT, Criteria> Mesher;
        typedef CDT::Vertex_handle Vertex_handle;
        typedef CDT::Point Point;

        assert( interface != nullptr );
        assert( interface->GetNumberOfPoints() > 0 );

        CDT cdt;

        const double reference_y = interface->GetPoint( 0 )[ 1 ];

        for ( auto i = 0; i < interface->GetNumberOfPoints(); ++ i ) {
          auto point = interface->GetPoint( i );
          assert( fabs( point[ 1 ] - reference_y ) <= std::numeric_limits< double >::epsilon()  );
          // std::cout << i << ": (" << point[ 0 ] << ", " << point[ 1 ] << ", " << point[ 2 ] << ")" << std::endl;
        }

        std::vector< Point > seeds;
        {
          // Get boundary edges and set them as constrains

          vtkSmartPointer<vtkFeatureEdges> featureEdges = vtkSmartPointer<vtkFeatureEdges>::New();
          featureEdges->SetInput( interface );
          featureEdges->BoundaryEdgesOn();
          featureEdges->FeatureEdgesOff();
          featureEdges->ManifoldEdgesOff();
          featureEdges->NonManifoldEdgesOff();
          featureEdges->Update();

          vtkSmartPointer< vtkPolyData > boundaryEdges = featureEdges->GetOutput();

          vtkPoints* points = boundaryEdges->GetPoints();

          std::cout << "Boundary Edges" << std::endl;
          std::cout << "Cells: " << boundaryEdges->GetLines()->GetNumberOfCells() << std::endl;
          std::cout << "Points: " << points->GetNumberOfPoints() << std::endl;

          // Traverse all of the edges
          for(vtkIdType i = 0; i < boundaryEdges->GetNumberOfCells(); i++) {
            vtkSmartPointer<vtkLine> line = vtkLine::SafeDownCast( boundaryEdges->GetCell(i) );

            auto point0 = points->GetPoint( line->GetPointId( 0 ) );
            Point p0( point0[ 0 ], point0[ 2 ] );

            auto point1 = points->GetPoint( line->GetPointId( 1 ) );
            Point p1( point1[ 0 ], point1[ 2 ] );

            cdt.insert_constraint( p0, p1 );
          }

          auto seedsy = kernel::utils::vtk_utils::find_seeds_Y( boundaryEdges );
          seeds.reserve( seedsy.size() );
          std::for_each( seedsy.begin(), seedsy.end(), [&](const decltype(seedsy)::value_type& v) {
            seeds.push_back( Point( std::get<0>( v ), std::get<1>( v ) ) );
          } );

        }

        assert( cdt.is_valid() );
        // std::cout << "Number of vertices: " << cdt.number_of_vertices() << std::endl;
        // std::cout << "Number of faces: " << cdt.number_of_faces() << std::endl;

        std::cout << "Meshing ..." << std::endl;

        Mesher mesher( cdt );
        mesher.set_seeds( seeds.begin(), seeds.end() );
        mesher.set_criteria( Criteria( B, S ) );
        mesher.refine_mesh();

        std::cout << "Number of vertices: " << cdt.number_of_vertices() << std::endl;
        std::cout << "Number of faces: " << cdt.number_of_faces() << std::endl << std::endl;

        vtkSmartPointer<vtkPolyData> triangulatedInterfase = CGAL::output_interface_Y_to_vtk_polydata( cdt, reference_y );
        return triangulatedInterfase;
      }


      vtkSmartPointer<vtkPolyData> triangulate_vtk_polydata_interfaseZ(vtkPolyData *interface, const double B, const double S)
      {
        typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
        typedef CGAL::Triangulation_vertex_base_2<K> Vb;
        typedef CGAL::Delaunay_mesh_face_base_2<K> Fb;
        typedef CGAL::Triangulation_data_structure_2<Vb, Fb> Tds;
        typedef CGAL::Constrained_Delaunay_triangulation_2<K, Tds, CGAL::Exact_predicates_tag > CDT;
        typedef CGAL::Delaunay_mesh_size_criteria_2<CDT> Criteria;
        typedef CGAL::Delaunay_mesher_2<CDT, Criteria> Mesher;
        typedef CDT::Vertex_handle Vertex_handle;
        typedef CDT::Point Point;

        assert( interface != nullptr );
        assert( interface->GetNumberOfPoints() > 0 );

        CDT cdt;

        const double reference_z = interface->GetPoint( 0 )[ 2 ];

        for ( auto i = 0; i < interface->GetNumberOfPoints(); ++ i ) {
          auto point = interface->GetPoint( i );
          assert( fabs( point[ 2 ] - reference_z ) <= std::numeric_limits< double >::epsilon()  );
          // std::cout << i << ": (" << point[ 0 ] << ", " << point[ 1 ] << ", " << point[ 2 ] << ")" << std::endl;
        }

        std::vector< Point > seeds;
        {
          // Get boundary edges and set them as constrains

          vtkSmartPointer<vtkFeatureEdges> featureEdges = vtkSmartPointer<vtkFeatureEdges>::New();
          featureEdges->SetInput( interface );
          featureEdges->BoundaryEdgesOn();
          featureEdges->FeatureEdgesOff();
          featureEdges->ManifoldEdgesOff();
          featureEdges->NonManifoldEdgesOff();
          featureEdges->Update();

          vtkSmartPointer< vtkPolyData > boundaryEdges = featureEdges->GetOutput();

          vtkPoints* points = boundaryEdges->GetPoints();

          std::cout << "Boundary Edges" << std::endl;
          std::cout << "Cells: " << boundaryEdges->GetLines()->GetNumberOfCells() << std::endl;
          std::cout << "Points: " << points->GetNumberOfPoints() << std::endl;

          // Traverse all of the edges
          for(vtkIdType i = 0; i < boundaryEdges->GetNumberOfCells(); i++) {
            vtkSmartPointer<vtkLine> line = vtkLine::SafeDownCast( boundaryEdges->GetCell(i) );

            auto point0 = points->GetPoint( line->GetPointId( 0 ) );
            Point p0( point0[ 0 ], point0[ 1 ] );

            auto point1 = points->GetPoint( line->GetPointId( 1 ) );
            Point p1( point1[ 0 ], point1[ 1 ] );

            cdt.insert_constraint( p0, p1 );
          }

          auto seedsz = kernel::utils::vtk_utils::find_seeds_Z( boundaryEdges );
          seeds.reserve( seedsz.size() );
          std::for_each( seedsz.begin(), seedsz.end(), [&](const decltype(seedsz)::value_type& v) {
            seeds.push_back( Point( std::get<0>( v ), std::get<1>( v ) ) );
          } );

        }

        assert( cdt.is_valid() );
        // std::cout << "Number of vertices: " << cdt.number_of_vertices() << std::endl;
        // std::cout << "Number of faces: " << cdt.number_of_faces() << std::endl;

        std::cout << "Meshing ..." << std::endl;

        Mesher mesher( cdt );
        mesher.set_seeds( seeds.begin(), seeds.end() );
        mesher.set_criteria( Criteria( B, S ) );
        mesher.refine_mesh();

        std::cout << "Number of vertices: " << cdt.number_of_vertices() << std::endl;
        std::cout << "Number of faces: " << cdt.number_of_faces() << std::endl << std::endl;

        vtkSmartPointer<vtkPolyData> triangulatedInterfase = CGAL::output_interface_Z_to_vtk_polydata( cdt, reference_z );
        return triangulatedInterfase;
      }


      struct Bounds
      {
        double minX;
        double maxX;

        double minY;
        double maxY;

        double minZ;
        double maxZ;
      };

      std::vector< std::tuple< double, double > > find_seeds_X(vtkSmartPointer< vtkPolyData > boundaryEdges)
      {
        std::vector< std::tuple< double, double > > seeds;
        vtkSmartPointer<vtkStripper> stripper = vtkSmartPointer<vtkStripper>::New();
        stripper->SetInput( boundaryEdges );
        stripper->Update();

        vtkSmartPointer< vtkPolyData > strippedEdges = stripper->GetOutput();

        std::cout << "stripped numberOfCells: " << strippedEdges->GetNumberOfCells() << std::endl << std::endl;

        typedef std::tuple< double, double > Center;

        std::vector< std::tuple< int, Center, double, bool, Bounds > > contours; // true means outher contour
        contours.reserve( strippedEdges->GetNumberOfCells() );

        for ( auto i = 0; i < strippedEdges->GetNumberOfCells(); ++ i ) {
          vtkSmartPointer< vtkPolyLine > polyline = vtkPolyLine::SafeDownCast( strippedEdges->GetCell( i ) );

          double bounds[ 6 ];
          double center[ 3 ];
          polyline->GetBounds( bounds );

          center[ 0 ] = ( bounds[ 1 ] + bounds[ 0 ] ) / 2.0;
          center[ 1 ] = ( bounds[ 3 ] + bounds[ 2 ] ) / 2.0;
          center[ 2 ] = ( bounds[ 5 ] + bounds[ 4 ] ) / 2.0;

          //const double xlen = bounds[ 1 ] - bounds[ 0 ];
          const double ylen = bounds[ 3 ] - bounds[ 2 ];
          const double zlen = bounds[ 5 ] - bounds[ 4 ];
          const double area = ylen * zlen;


          /*
          double centroid[ 3 ];
          {
            vtkSmartPointer< vtkPolyData > pd = vtkSmartPointer< vtkPolyData >::New();
            pd->SetPoints( polyline->GetPoints() );

            vtkSmartPointer<vtkCenterOfMass> centerOfMassFilter = vtkSmartPointer<vtkCenterOfMass>::New();
            centerOfMassFilter->SetInput( pd );
            centerOfMassFilter->SetUseScalarsAsWeights(false);
            centerOfMassFilter->Update();

            double center[3];
            centerOfMassFilter->GetCenter( centroid );

            std::cout << "Centroid: (" << centroid[ 0 ] << ", " << centroid[ 1 ] << " ," << center[ 2 ] << ")" << std::endl;
          }
          */

          // std::cout << "Center: (" << center[ 0 ] << ", " << center[ 1 ] << ", " << center[ 2 ] << ")" << std::endl;
          // std::cout << "Area: " << area << std::endl;
          // std::cout << std::endl;

          Center c = std::make_tuple( center[ 1 ], center[ 2 ] );

          Bounds b;
          b.minX = bounds[ 0 ];
          b.maxX = bounds[ 1 ];
          b.minY = bounds[ 2 ];
          b.maxY = bounds[ 3 ];
          b.minZ = bounds[ 4 ];
          b.maxZ = bounds[ 5 ];

          contours.push_back( std::make_tuple( i, c, area, false, b ) );
        }

        auto comp = [](const std::tuple< int, Center, double, bool, Bounds >& a, const std::tuple< int, Center, double, bool, Bounds >&b  ) {
          return (std::get<2>( a ) > std::get<2>( b ) );
        };

        auto print = [&](const std::tuple< int, Center, double, bool, Bounds >& a) {
          Center center = std::get<1>( a );
          std::cout << "Center: (" << std::get<0>(center) << ", " << std::get<1>(center) << ")" << std::endl;
          std::cout << "Area: " << std::get<2>( a ) << std::endl;
          std::cout << "Outher: " << std::boolalpha << std::get<3>( a ) << std::endl;
          Bounds b = std::get<4>( a );
          std::cout << "Bound: "
                    << " x: [ " << b.minX << ", " << b.maxX
                    << " ] y: [ " << b.minY << ", " << b.maxY
                    << " ] z: [ " << b.minZ << ", " << b.maxZ << " ]" << std::endl;
          std::cout << std::endl;
        };

        auto assign = [&](const std::tuple< int, Center, double, bool, Bounds >& a) {
          if ( !std::get<3>( a ) ) {
            const Center& center = std::get<1>( a );
            seeds.push_back( std::make_tuple( std::get<0>(center), std::get<1>(center) ) );
          }
        };

        auto inside = [](const double& min, const double& max, const double& p) {
          assert( max > min );
          return ( (p >= min) && (p <= max) );
        };

        /*
        std::cout << "Contours (" << contours.size() << "): \n" << std::endl;
        std::for_each( contours.begin(), contours.end(), print );
        */

        std::sort( contours.begin(), contours.end(), comp );

        for ( auto it = contours.begin(); it != contours.end(); ++ it ) {
          const Center& center = std::get<1>( *it );
          bool belongs = false;
          for ( auto it1 = contours.begin(); it1 != it; ++ it1 ) {
            const Bounds& b = std::get<4>( *it1 );
            if ( inside( b.minY, b.maxY, std::get<0>( center ) ) && inside( b.minZ, b.maxZ, std::get<1>( center ) ) ) {
              belongs = true;
              break;
            }
          }
          if ( !belongs ) {
            std::get<3>( *it ) = true;
          }
        }

        std::cout << "Contours (" << contours.size() << "): \n" << std::endl;
        std::for_each( contours.begin(), contours.end(), print );

        std::for_each( contours.begin(), contours.end(), assign );

        return seeds;
      }


      std::vector< std::tuple< double, double > > find_seeds_Y(vtkSmartPointer< vtkPolyData > boundaryEdges)
      {
        std::vector< std::tuple< double, double > > seeds;
        vtkSmartPointer<vtkStripper> stripper = vtkSmartPointer<vtkStripper>::New();
        stripper->SetInput( boundaryEdges );
        stripper->Update();

        vtkSmartPointer< vtkPolyData > strippedEdges = stripper->GetOutput();

        std::cout << "stripped numberOfCells: " << strippedEdges->GetNumberOfCells() << std::endl << std::endl;

        typedef std::tuple< double, double > Center;

        std::vector< std::tuple< int, Center, double, bool, Bounds > > contours; // true means outher contour
        contours.reserve( strippedEdges->GetNumberOfCells() );

        for ( auto i = 0; i < strippedEdges->GetNumberOfCells(); ++ i ) {
          vtkSmartPointer< vtkPolyLine > polyline = vtkPolyLine::SafeDownCast( strippedEdges->GetCell( i ) );

          double bounds[ 6 ];
          double center[ 3 ];
          polyline->GetBounds( bounds );

          center[ 0 ] = ( bounds[ 1 ] + bounds[ 0 ] ) / 2.0;
          center[ 1 ] = ( bounds[ 3 ] + bounds[ 2 ] ) / 2.0;
          center[ 2 ] = ( bounds[ 5 ] + bounds[ 4 ] ) / 2.0;

          const double xlen = bounds[ 1 ] - bounds[ 0 ];
          // const double ylen = bounds[ 3 ] - bounds[ 2 ];
          const double zlen = bounds[ 5 ] - bounds[ 4 ];
          const double area = xlen * zlen;

          /*
          double centroid[ 3 ];
          {
            vtkSmartPointer< vtkPolyData > pd = vtkSmartPointer< vtkPolyData >::New();
            pd->SetPoints( polyline->GetPoints() );

            vtkSmartPointer<vtkCenterOfMass> centerOfMassFilter = vtkSmartPointer<vtkCenterOfMass>::New();
            centerOfMassFilter->SetInput( pd );
            centerOfMassFilter->SetUseScalarsAsWeights(false);
            centerOfMassFilter->Update();

            double center[3];
            centerOfMassFilter->GetCenter( centroid );

            std::cout << "Centroid: (" << centroid[ 0 ] << ", " << centroid[ 1 ] << " ," << center[ 2 ] << ")" << std::endl;
          }
          */

          // std::cout << "Center: (" << center[ 0 ] << ", " << center[ 1 ] << ", " << center[ 2 ] << ")" << std::endl;
          // std::cout << "Area: " << area << std::endl;
          // std::cout << std::endl;

          Center c = std::make_tuple( center[ 0 ], center[ 2 ] );

          Bounds b;
          b.minX = bounds[ 0 ];
          b.maxX = bounds[ 1 ];
          b.minY = bounds[ 2 ];
          b.maxY = bounds[ 3 ];
          b.minZ = bounds[ 4 ];
          b.maxZ = bounds[ 5 ];

          contours.push_back( std::make_tuple( i, c, area, false, b ) );
        }

        auto comp = [](const std::tuple< int, Center, double, bool, Bounds >& a, const std::tuple< int, Center, double, bool, Bounds >&b  ) {
          return (std::get<2>( a ) > std::get<2>( b ) );
        };

        auto print = [&](const std::tuple< int, Center, double, bool, Bounds >& a) {
          Center center = std::get<1>( a );
          std::cout << "Center: (" << std::get<0>(center) << ", " << std::get<1>(center) << ")" << std::endl;
          std::cout << "Area: " << std::get<2>( a ) << std::endl;
          std::cout << "Outher: " << std::boolalpha << std::get<3>( a ) << std::endl;
          Bounds b = std::get<4>( a );
          std::cout << "Bound: "
                    << " x: [ " << b.minX << ", " << b.maxX
                    << " ] y: [ " << b.minY << ", " << b.maxY
                    << " ] z: [ " << b.minZ << ", " << b.maxZ << " ]" << std::endl;
          std::cout << std::endl;
        };

        auto assign = [&](const std::tuple< int, Center, double, bool, Bounds >& a) {
          if ( !std::get<3>( a ) ) {
            const Center& center = std::get<1>( a );
            seeds.push_back( std::make_tuple( std::get<0>(center), std::get<1>(center) ) );
          }
        };

        auto inside = [](const double& min, const double& max, const double& p) {
          assert( max > min );
          return ( (p >= min) && (p <= max) );
        };

        /*
        std::cout << "Contours (" << contours.size() << "): \n" << std::endl;
        std::for_each( contours.begin(), contours.end(), print );
        */

        std::sort( contours.begin(), contours.end(), comp );

        for ( auto it = contours.begin(); it != contours.end(); ++ it ) {
          const Center& center = std::get<1>( *it );
          bool belongs = false;
          for ( auto it1 = contours.begin(); it1 != it; ++ it1 ) {
            const Bounds& b = std::get<4>( *it1 );
            if ( inside( b.minX, b.maxX, std::get<0>( center ) ) && inside( b.minZ, b.maxZ, std::get<1>( center ) ) ) {
              belongs = true;
              break;
            }
          }
          if ( !belongs ) {
            std::get<3>( *it ) = true;
          }
        }

        std::cout << "Contours (" << contours.size() << "): \n" << std::endl;
        std::for_each( contours.begin(), contours.end(), print );

        std::for_each( contours.begin(), contours.end(), assign );

        return seeds;
      }


      std::vector< std::tuple< double, double > > find_seeds_Z(vtkSmartPointer< vtkPolyData > boundaryEdges)
      {
        std::vector< std::tuple< double, double > > seeds;
        vtkSmartPointer<vtkStripper> stripper = vtkSmartPointer<vtkStripper>::New();
        stripper->SetInput( boundaryEdges );
        stripper->Update();

        vtkSmartPointer< vtkPolyData > strippedEdges = stripper->GetOutput();

        std::cout << "stripped numberOfCells: " << strippedEdges->GetNumberOfCells() << std::endl << std::endl;

        typedef std::tuple< double, double > Center;

        std::vector< std::tuple< int, Center, double, bool, Bounds > > contours; // true means outher contour
        contours.reserve( strippedEdges->GetNumberOfCells() );

        for ( auto i = 0; i < strippedEdges->GetNumberOfCells(); ++ i ) {
          vtkSmartPointer< vtkPolyLine > polyline = vtkPolyLine::SafeDownCast( strippedEdges->GetCell( i ) );

          double bounds[ 6 ];
          double center[ 3 ];
          polyline->GetBounds( bounds );

          center[ 0 ] = ( bounds[ 1 ] + bounds[ 0 ] ) / 2.0;
          center[ 1 ] = ( bounds[ 3 ] + bounds[ 2 ] ) / 2.0;
          center[ 2 ] = ( bounds[ 5 ] + bounds[ 4 ] ) / 2.0;

          const double xlen = bounds[ 1 ] - bounds[ 0 ];
          const double ylen = bounds[ 3 ] - bounds[ 2 ];
          // const double zlen = bounds[ 5 ] - bounds[ 4 ];
          const double area = xlen * ylen;


          /*
          double centroid[ 3 ];
          {
            vtkSmartPointer< vtkPolyData > pd = vtkSmartPointer< vtkPolyData >::New();
            pd->SetPoints( polyline->GetPoints() );

            vtkSmartPointer<vtkCenterOfMass> centerOfMassFilter = vtkSmartPointer<vtkCenterOfMass>::New();
            centerOfMassFilter->SetInput( pd );
            centerOfMassFilter->SetUseScalarsAsWeights(false);
            centerOfMassFilter->Update();

            double center[3];
            centerOfMassFilter->GetCenter( centroid );

            std::cout << "Centroid: (" << centroid[ 0 ] << ", " << centroid[ 1 ] << " ," << center[ 2 ] << ")" << std::endl;
          }
          */

          // std::cout << "Center: (" << center[ 0 ] << ", " << center[ 1 ] << ", " << center[ 2 ] << ")" << std::endl;
          // std::cout << "Area: " << area << std::endl;
          // std::cout << std::endl;

          Center c = std::make_tuple( center[ 0 ], center[ 1 ] );

          Bounds b;
          b.minX = bounds[ 0 ];
          b.maxX = bounds[ 1 ];
          b.minY = bounds[ 2 ];
          b.maxY = bounds[ 3 ];
          b.minZ = bounds[ 4 ];
          b.maxZ = bounds[ 5 ];

          contours.push_back( std::make_tuple( i, c, area, false, b ) );
        }

        auto comp = [](const std::tuple< int, Center, double, bool, Bounds >& a, const std::tuple< int, Center, double, bool, Bounds >&b  ) {
          return (std::get<2>( a ) > std::get<2>( b ) );
        };

        auto print = [&](const std::tuple< int, Center, double, bool, Bounds >& a) {
          Center center = std::get<1>( a );
          std::cout << "Center: (" << std::get<0>(center) << ", " << std::get<1>(center) << ")" << std::endl;
          std::cout << "Area: " << std::get<2>( a ) << std::endl;
          std::cout << "Outher: " << std::boolalpha << std::get<3>( a ) << std::endl;
          Bounds b = std::get<4>( a );
          std::cout << "Bound: "
                    << " x: [ " << b.minX << ", " << b.maxX
                    << " ] y: [ " << b.minY << ", " << b.maxY
                    << " ] z: [ " << b.minZ << ", " << b.maxZ << " ]" << std::endl;
          std::cout << std::endl;
        };

        auto assign = [&](const std::tuple< int, Center, double, bool, Bounds >& a) {
          if ( !std::get<3>( a ) ) {
            const Center& center = std::get<1>( a );
            seeds.push_back( std::make_tuple( std::get<0>(center), std::get<1>(center) ) );
          }
        };

        auto inside = [](const double& min, const double& max, const double& p) {
          assert( max > min );
          return ( (p >= min) && (p <= max) );
        };

        /*
        std::cout << "Contours (" << contours.size() << "): \n" << std::endl;
        std::for_each( contours.begin(), contours.end(), print );
        */

        std::sort( contours.begin(), contours.end(), comp );

        for ( auto it = contours.begin(); it != contours.end(); ++ it ) {
          const Center& center = std::get<1>( *it );
          bool belongs = false;
          for ( auto it1 = contours.begin(); it1 != it; ++ it1 ) {
            const Bounds& b = std::get<4>( *it1 );
            if ( inside( b.minX, b.maxX, std::get<0>( center ) ) && inside( b.minY, b.maxY, std::get<1>( center ) ) ) {
              belongs = true;
              break;
            }
          }
          if ( !belongs ) {
            std::get<3>( *it ) = true;
          }
        }

        std::cout << "Contours (" << contours.size() << "): \n" << std::endl;
        std::for_each( contours.begin(), contours.end(), print );

        std::for_each( contours.begin(), contours.end(), assign );

        return seeds;
      }


    }
  }
}



#include <vtkXMLPolyDataReader.h>
#include <vtkXMLPolyDataWriter.h>

bool test::CGALMeshInterfaceTest()
{
  std::cout << "CGALMeshInterfaceTest()" << std::endl;
  vtkSmartPointer<vtkPolyData> polyData;

  {
    vtkSmartPointer<vtkXMLPolyDataReader> reader = vtkSmartPointer<vtkXMLPolyDataReader>::New();
    reader->SetFileName( "complex_interface.vtp" );
    reader->Update();

    polyData = reader->GetOutput();
  }

  vtkSmartPointer<vtkPolyData> triangulatedPolyData = kernel::utils::vtk_utils::triangulate_vtk_polydata_interfaseX( polyData );

  {
    vtkSmartPointer<vtkXMLPolyDataWriter> writer =  vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName( "complex_interface_triangulated.vtp" );
    writer->SetInput( triangulatedPolyData );
    writer->Write();
  }

  return true;
}

