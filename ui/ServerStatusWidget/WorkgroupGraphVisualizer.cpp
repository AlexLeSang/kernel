#include "WorkgroupGraphVisualizer.hpp"

#include <GraphInveractor.hpp>

#include <../utils/ThreadSleep.hpp>

/*!
 * \brief WorkgroupGraphVisualizer::WorkgroupGraphVisualizer
 * \param parent
 */
WorkgroupGraphVisualizer::WorkgroupGraphVisualizer(std::shared_ptr<GraphProducerBase> proucer, const QString windowName, const quint32 updateInterval, QObject *parent) :
  QObject( parent ),
  graphProducer( proucer ),
  timerId( -1 ),
  updateInterval( updateInterval )
{
  graphLayoutView = vtkSmartPointer< vtkGraphLayoutView >::New();
  if ( !windowName.isEmpty() ) {
    graphLayoutView->GetRenderWindow()->SetWindowName( windowName.toStdString().c_str() );
  }
}

/*!
 * \brief WorkgroupGraphVisualizer::~WorkgroupGraphVisualizer
 */
WorkgroupGraphVisualizer::~WorkgroupGraphVisualizer()
{
  graphLayoutView->GetInteractor()->DestroyTimer( timerId );
}

/*!
 * \brief WorkgroupGraphVisualizer::update
 */
void WorkgroupGraphVisualizer::update()
{
  if ( graphProducer->done() ) {
    graphLayoutView->GetInteractor()->ExitCallback();
  }
  else {

  // Create or obtain a new graph
  auto interactor = graphLayoutView->GetInteractorStyle();

  // Clean up and old data
  graphLayoutView->RemoveRepresentation( graphLayoutView->GetRepresentation() );
  graphLayoutView->GetRenderer()->RemoveAllViewProps();

  // Display graph on layout
  displayGraphOnLayout( transformToVtkGraph( graphProducer->produce() ) );
  graphLayoutView->SetInteractorStyle( interactor );
  }
}

/*!
 * \brief WorkgroupGraphVisualizer::run
 */
void WorkgroupGraphVisualizer::run()
{
  if ( ! graphProducer->done() ) {
    visualizeVtkGraph( transformToVtkGraph( graphProducer->produce() ) );
  }
}


/*!
 * \brief WorkgroupGraphVisualizer::vtkUpdatePropertes
 * \param vertexLabel
 * \param vtkId
 * \param from
 * \param vertexLabels
 * \param scaledVerticies
 * \param vertexColors
 */
void WorkgroupGraphVisualizer::vtkUpdatePropertes(const QString &vertexLabel, const vtkIdType &vtkId, bool from, vtkStringArray *vertexLabels, vtkDoubleArray *scaledVerticies, vtkFloatArray *vertexColors)
{
  // Add vertex label
  vertexLabels->InsertNextValue( vertexLabel.toStdString() );

  // Add vertex scale
  scaledVerticies->InsertNextValue( kernel::utils::random::uniform_real_rand( 1.0, 2.0 ) );

  // Add vertex number for a lookup table
  // vertexColors->InsertNextValue( vtkId );
  if ( from ) {
    float c[ 3 ] = { 1.0f, 0.0f, 0.0f };
    vertexColors->InsertNextTupleValue( c );
  }
  else {
    float c[ 3 ] = { 0.0f, 1.0f, 0.0f };
    vertexColors->InsertNextTupleValue( c );
  }
}

/*!
 * \brief WorkgroupGraphVisualizer::transformToVtkGraph
 * \param graph
 * \return
 */
vtkSmartPointer<vtkMutableDirectedGraph> WorkgroupGraphVisualizer::transformToVtkGraph(const QList<kernel::server::command::ServerStatus::WorkgroupGraphEdge> &graph)
{
  vtkSmartPointer< vtkMutableDirectedGraph > vtkGraph = vtkSmartPointer< vtkMutableDirectedGraph >::New();

  QMap< QString, vtkIdType > stringToVtkId;

  vtkSmartPointer< vtkStringArray > vertexLabels = vtkSmartPointer< vtkStringArray >::New();
  vertexLabels->SetNumberOfComponents( 1 );
  vertexLabels->SetName( "vertexLabels" );

  vtkSmartPointer< vtkDoubleArray > scaledVerticies = vtkSmartPointer< vtkDoubleArray >::New();
  scaledVerticies->SetNumberOfComponents( 1 );
  scaledVerticies->SetName( "scaledVerticies" );

  vtkSmartPointer< vtkFloatArray > vertexColors = vtkSmartPointer< vtkFloatArray >::New();
  vertexColors->SetNumberOfComponents( 3 );
  vertexColors->SetName( "vertexColor" );

  vtkSmartPointer< vtkIntArray > weights = vtkSmartPointer< vtkIntArray >::New();
  weights->SetNumberOfComponents( 1 );
  weights->SetName( "weights" );

  vtkSmartPointer< vtkDoubleArray > scaledWeights = vtkSmartPointer< vtkDoubleArray >::New();
  scaledWeights->SetNumberOfComponents( 1 );
  scaledWeights->SetName( "scaledWeights" );

  for ( auto it = graph.begin(); it != graph.end(); ++ it ) {
    const auto from = (*it).edgeFrom;
    const auto to = (*it).edgeTo;
    const auto ping = (*it).avgPing;

    vtkIdType vFrom;
    auto itFrom = stringToVtkId.find( from );
    if ( itFrom == stringToVtkId.end() ) {
      vFrom = vtkGraph->AddVertex();
      vtkUpdatePropertes( from, vFrom, true, vertexLabels, scaledVerticies, vertexColors );
      stringToVtkId.insert( from, vFrom );
    }
    else {
      vFrom = itFrom.value();
    }

    vtkIdType vTo ;
    auto itTo = stringToVtkId.find( to );
    if ( itTo == stringToVtkId.end() ) {
      vTo = vtkGraph->AddVertex();
      vtkUpdatePropertes( to, vFrom, false, vertexLabels, scaledVerticies, vertexColors );
      stringToVtkId.insert( to, vTo );
    }
    else {
      vTo = itTo.value();
    }

    vtkGraph->AddEdge( vFrom, vTo );
    weights->InsertNextValue( ping );
  }


  // scaledWeights
  {
    const double maxPing = weights->GetMaxNorm();
    for ( auto i = 0; i < weights->GetNumberOfTuples(); ++i ) {
      const int w = weights->GetValue( i );
      scaledWeights->InsertNextValue( (w / maxPing) );
    }
  }

  vtkGraph->GetVertexData()->AddArray( vertexLabels );
  vtkGraph->GetVertexData()->AddArray( scaledVerticies );
  vtkGraph->GetVertexData()->AddArray( vertexColors );

  vtkGraph->GetEdgeData()->AddArray( weights );
  vtkGraph->GetEdgeData()->AddArray( scaledWeights );

  return vtkGraph;
}

/*!
 * \brief WorkgroupGraphVisualizer::displayGraphOnLayout
 * \param vtkGraph
 */
void WorkgroupGraphVisualizer::displayGraphOnLayout(vtkMutableDirectedGraph *vtkGraph)
{
  vtkSmartPointer< CustomSphereRepresentation > representation = vtkSmartPointer< CustomSphereRepresentation >::New();
  representation->SetInputConnection( vtkGraph->GetProducerPort() );
  representation->SetVertexSize( 20 );
  representation->SetGlyphType( vtkGraphToGlyphs::SPHERE );

  graphLayoutView->AddRepresentation( representation );

  vtkSmartPointer< vtkGraphLayout > layout =  vtkSmartPointer< vtkGraphLayout >::New();
  layout->SetInput( vtkGraph );

  vtkSmartPointer< vtkForceDirectedLayoutStrategy > strategy =  vtkSmartPointer< vtkForceDirectedLayoutStrategy >::New();
  strategy->ThreeDimensionalLayoutOn();
  strategy->SetEdgeWeightField( "weights" );
  // strategy->SetEdgeWeightField( "scaledWeights" );
  strategy->SetWeightEdges( true );

  layout->SetLayoutStrategy( strategy );

  graphLayoutView->SetLayoutStrategyToPassThrough();
  graphLayoutView->SetEdgeLayoutStrategyToPassThrough();
  graphLayoutView->AddRepresentationFromInputConnection( layout->GetOutputPort() );

  vtkSmartPointer< vtkGraphToPolyData > graphToPoly =  vtkSmartPointer< vtkGraphToPolyData >::New();
  graphToPoly->SetInputConnection( layout->GetOutputPort() );
  graphToPoly->EdgeGlyphOutputOn();
  graphToPoly->SetEdgeGlyphPosition( 0.55 );

  vtkSmartPointer< vtkGlyphSource2D > arrowSource = vtkSmartPointer< vtkGlyphSource2D >::New();
  arrowSource->SetGlyphTypeToEdgeArrow();
  arrowSource->SetScale( 0.02 );
  arrowSource->Update();

  vtkSmartPointer< vtkGlyph3D > arrowGlyph = vtkSmartPointer< vtkGlyph3D >::New();
  arrowGlyph->SetInputConnection( 0, graphToPoly->GetOutputPort( 1 ) );
  arrowGlyph->SetSourceConnection( arrowSource->GetOutputPort() );
  arrowGlyph->SetColorModeToColorByScalar();
  // arrowGlyph->SetInputArrayToProcess( 3, 0, 0, 0, "edgeColors" );

  vtkSmartPointer< vtkPolyDataMapper > arrowMapper = vtkSmartPointer< vtkPolyDataMapper >::New();
  arrowMapper->SetInputConnection( arrowGlyph->GetOutputPort() );
  vtkSmartPointer< vtkActor > arrowActor = vtkSmartPointer< vtkActor >::New();
  arrowActor->SetMapper( arrowMapper );
  graphLayoutView->GetRenderer()->AddActor( arrowActor );

  graphLayoutView->SetVertexLabelArrayName( "vertexLabels" );
  graphLayoutView->SetVertexLabelVisibility( true );

  graphLayoutView->SetVertexColorArrayName( "vertexColor" );
  graphLayoutView->SetColorVertices( true );

  graphLayoutView->SetScalingArrayName( "scaledVerticies" );
  graphLayoutView->SetScaledGlyphs( true );

  graphLayoutView->SetEdgeLabelArrayName( "weights" );
  graphLayoutView->SetEdgeLabelVisibility( true );

  graphLayoutView->SetEdgeColorArrayName( "scaledWeights" );
  graphLayoutView->SetColorEdges( true );

  graphLayoutView->GetRenderWindow()->SetSize( 1024, 768 );
  graphLayoutView->ResetCamera();
  graphLayoutView->GetRenderer()->SetBackground( 0.0, 0.0, 0.0 );
  graphLayoutView->GetRenderer()->SetBackground2( 1/255., 50/255., 25/255. );
  graphLayoutView->Render();
  graphLayoutView->SetInteractionModeTo3D();
  graphLayoutView->SetSelectionModeToSurface();

}


/*!
 * \brief WorkgroupGraphVisualizer::visualizeVtkGraph
 * \param vtkGraph
 */
void WorkgroupGraphVisualizer::visualizeVtkGraph(vtkSmartPointer<vtkMutableDirectedGraph> vtkGraph)
{
  displayGraphOnLayout( vtkGraph );

  /* Fun but not useful
  vtkSmartPointer< vtkInteractorStyleSwitch > style = vtkSmartPointer< vtkInteractorStyleSwitch >::New();
  graphLayoutView->GetRenderWindow()->GetInteractor()->SetInteractorStyle( style );
  */

  /* Selects nothing
  vtkSmartPointer< vtkInteractorStyleRubberBand3D > style = vtkSmartPointer< vtkInteractorStyleRubberBand3D >::New();
  graphLayoutView->GetRenderWindow()->GetInteractor()->SetInteractorStyle( style );
  */

  /* Dont pick
  vtkSmartPointer< vtkInteractorStyleRubberBandPick > style = vtkSmartPointer< vtkInteractorStyleRubberBandPick >::New();
  graphLayoutView->GetRenderWindow()->GetInteractor()->SetInteractorStyle( style );
  */


  // TODO create selecting interactor on the base of vtkInteractorStyleRubberBand
  /* Simple Trackball Camera
  vtkSmartPointer< vtkInteractorStyleTrackballCamera > style = vtkSmartPointer< vtkInteractorStyleTrackballCamera >::New();
  graphLayoutView->GetRenderWindow()->GetInteractor()->SetInteractorStyle( style );
  */


  graphLayoutView->GetInteractor()->Initialize();
  timerId = graphLayoutView->GetInteractor()->CreateRepeatingTimer( updateInterval );

  vtkSmartPointer< GraphInveractor > graphInteractor = vtkSmartPointer< GraphInveractor >::New();
  graphLayoutView->GetInteractor()->AddObserver( vtkCommand::TimerEvent, graphInteractor.GetPointer(), &GraphInveractor::Execute );

  graphInteractor->SetWorkgroupGraphVisualizer( this );
  graphLayoutView->GetInteractor()->SetInteractorStyle( graphInteractor );
  graphLayoutView->GetInteractor()->Start();
}



vtkStandardNewMacro(CustomSphereRepresentation);

/*!
 * \brief CustomSphereRepresentation::SetVertexSize
 * \param vertexSize
 */
void CustomSphereRepresentation::SetVertexSize(int vertexSize)
{
  this->VertexGlyph->SetScreenSize( vertexSize );
  this->VertexGlyph->Modified();
}



namespace test {


  bool RandomGraphProducer::done()
  {
    return ( graphCounter == 0 );
  }

  QList<kernel::server::command::ServerStatus::WorkgroupGraphEdge> RandomGraphProducer::produce()
  {
    -- graphCounter;
    return create_graph( vertexNumber, lowPing, highPing );
  }

  quint32 RandomGraphProducer::getVertexNumber() const
  {
    return vertexNumber;
  }

  void RandomGraphProducer::setVertexNumber(const quint32 &value)
  {
    vertexNumber = value;
  }

  quint32 RandomGraphProducer::getLowPing() const
  {
    return lowPing;
  }

  void RandomGraphProducer::setLowPing(const quint32 &value)
  {
    lowPing = value;
  }

  quint32 RandomGraphProducer::getHighPing() const
  {
    return highPing;
  }

  void RandomGraphProducer::setHighPing(const quint32 &value)
  {
    highPing = value;
  }
  quint32 RandomGraphProducer::getGraphCounter() const
  {
    return graphCounter;
  }

  void RandomGraphProducer::setGraphCounter(const quint32 &value)
  {
    graphCounter = value;
  }



  bool VisualizeWorkgroupGraphTest()
  {
    std::shared_ptr< GraphProducerBase > producer( new RandomGraphProducer() );
    WorkgroupGraphVisualizer* visualizer = new WorkgroupGraphVisualizer( std::move( producer ), "VisualizeWorkgroupGraphTest", 2000 );
    QThreadPool::globalInstance()->start( visualizer );

    kernel::utils::ThreadSleep::msleep( 5000 );
    QThreadPool::globalInstance()->waitForDone();
    return true;
  }

  QList<kernel::server::command::ServerStatus::WorkgroupGraphEdge> create_graph(const quint32 vertexNumber, const quint32 lowPing, const quint32 highPing)
  {
    using namespace kernel::server::command;

    QList< ServerStatus::WorkgroupGraphEdge > graph;
    for ( quint32 i = 0; i < vertexNumber;  ++ i ) {
      const auto from = "# " + QString::number( i );
      for ( quint32 j = 0; j < i; ++ j ) {
        const auto to = "# " + QString::number( j );
        const auto ping = kernel::utils::random::uniform_int_rand( lowPing, highPing );
        graph.append( ServerStatus::WorkgroupGraphEdge( from, to, ping ) );
      }
    }

    return graph;
  }

}
