#include "VTKPolyDataSlicer.hpp"

#include <algorithm>
#include <sstream>
#include <cassert>
#include <list>

#include <boost/timer/timer.hpp>

#include <vtkFeatureEdges.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkCellArray.h>
#include <vtkExtractEdges.h>
#include <vtkLine.h>
#include <vtkStripper.h>
#include <vtkProperty.h>
#include <vtkPolyLine.h>
#include <vtkCubeSource.h>
#include <vtkTriangleFilter.h>
#include <vtkCleanPolyData.h>

#include <vtkXMLPolyDataWriter.h>

#include <vtk_includes/vtkCenterOfMass.hpp>

#include <CGALInterfaceMesher.hpp>

namespace kernel {
  namespace utils {
    namespace vtk_utils {

      vtkSmartPointer<vtkPlane> SetClipPlaneOrigin(double *center, vtkSmartPointer<vtkPolyData> inputPolyData)
      {
        vtkSmartPointer<vtkPlane> clipPlane = vtkSmartPointer<vtkPlane>::New();
        if ( center == nullptr ) {
          clipPlane->SetOrigin( inputPolyData->GetCenter() );
        }
        else {
          clipPlane->SetOrigin( center );
        }

        return clipPlane;
      }

      void SetClipPlaneNormal(vtkSmartPointer<vtkPlane> clipPlane, CLIP_COORDINATE clipCoordinate)
      {
        switch ( clipCoordinate ) {
          case X_CLIP:
            clipPlane->SetNormal( -1.0, 0.0, 0.0 );
            break;

          case Y_CLIP:
            clipPlane->SetNormal( 0.0, -1.0, 0.0 );
            break;

          case Z_CLIP:
            clipPlane->SetNormal( 0.0, 0.0, -1.0 );
            break;
          default:
            break;
        }
      }

      vtkSmartPointer<vtkCutter> CreateCutter(vtkSmartPointer<vtkPlane> clipPlane, vtkSmartPointer<vtkPolyData> inputPolyData)
      {
        vtkSmartPointer<vtkCutter> cutter = vtkSmartPointer<vtkCutter>::New();
        cutter->SetCutFunction( clipPlane );
        cutter->SetInput( inputPolyData );
        cutter->Update();

        return cutter;
      }

      vtkSmartPointer<vtkContourTriangulator> CreateContourTriangulator(vtkSmartPointer<vtkCutter> cutter, vtkSmartPointer<vtkPlane> clipPlane)
      {
        vtkSmartPointer<vtkContourTriangulator> contourTriangulator = vtkSmartPointer<vtkContourTriangulator>::New();
        contourTriangulator->SetNormal( clipPlane->GetNormal() );
        contourTriangulator->SetInputConnection( cutter->GetOutputPort() );
        contourTriangulator->Update();

        return contourTriangulator;
      }

      vtkSmartPointer<vtkPlane> CreateClipPlane(vtkSmartPointer<vtkPolyData> inputPolyData, double *center, CLIP_COORDINATE clipCoordinate)
      {
        vtkSmartPointer<vtkPlane> clipPlane = SetClipPlaneOrigin( center, inputPolyData );
        SetClipPlaneNormal( clipPlane, clipCoordinate );

        return clipPlane;
      }

      vtkSmartPointer<vtkPolyData> CreateClippedInterface(vtkSmartPointer<vtkPlane> clipPlane, vtkSmartPointer<vtkPolyData> inputPolyData)
      {
        vtkSmartPointer<vtkCutter> cutter = CreateCutter( clipPlane, inputPolyData );
        vtkSmartPointer<vtkContourTriangulator> contourTriangulator = CreateContourTriangulator( cutter, clipPlane );

        return contourTriangulator->GetOutput();
      }

      vtkSmartPointer<vtkPolyData> CreateUnclippedInterface(vtkSmartPointer<vtkPolyData> clippedInterfacePolydata)
      {
        vtkSmartPointer<vtkReverseSense> reverseSense = vtkSmartPointer<vtkReverseSense>::New();
        reverseSense->SetInput( clippedInterfacePolydata );
        reverseSense->ReverseCellsOn();
        reverseSense->ReverseNormalsOn();

        return reverseSense->GetOutput();
      }

      vtkSmartPointer<vtkClipPolyData> CreateClipper(vtkSmartPointer<vtkPlane> clipPlane, vtkSmartPointer<vtkPolyData> inputPolyData)
      {
        vtkSmartPointer<vtkClipPolyData> clipper = vtkSmartPointer<vtkClipPolyData>::New();
        clipper->SetInput( inputPolyData );
        clipper->SetClipFunction(clipPlane);
        clipper->GenerateClippedOutputOn();

        return clipper;
      }

      void CreateUnclippedAndClippedGeometry(vtkSmartPointer<vtkPolyData> inputPolyData, vtkSmartPointer<vtkPlane> clipPlane, vtkSmartPointer<vtkPolyData>& unclippedPolydata, vtkSmartPointer<vtkPolyData>& clippedPolydata)
      {
        vtkSmartPointer<vtkClipPolyData> clipper = CreateClipper( clipPlane, inputPolyData );
        unclippedPolydata = clipper->GetOutput();
        clippedPolydata = clipper->GetClippedOutput();
      }

      vtkSmartPointer<vtkCleanPolyData> CreateCleaner(vtkSmartPointer<vtkAppendPolyData> appender)
      {
        vtkSmartPointer<vtkCleanPolyData> clippedCleaner = vtkSmartPointer<vtkCleanPolyData>::New();
        clippedCleaner->SetInputConnection( appender->GetOutputPort() );
        clippedCleaner->ConvertLinesToPointsOn();
        clippedCleaner->ConvertPolysToLinesOn();
        clippedCleaner->SetAbsoluteTolerance( 1e-9 );
        clippedCleaner->Update();

        return clippedCleaner;
      }

      vtkSmartPointer<vtkAppendPolyData> CreateAppender(vtkSmartPointer<vtkPolyData> geometry, vtkSmartPointer<vtkPolyData> interface)
      {
        vtkSmartPointer<vtkAppendPolyData> appender = vtkSmartPointer<vtkAppendPolyData>::New();
        appender->AddInput( geometry );
        appender->AddInput( interface );
        appender->Update();

        return appender;
      }

      vtkSmartPointer<vtkPolyData> AssemblePart(vtkSmartPointer<vtkPolyData> geometry, vtkSmartPointer<vtkPolyData> interface)
      {
        vtkSmartPointer<vtkAppendPolyData> appender = CreateAppender( geometry, interface );
        vtkSmartPointer<vtkCleanPolyData> cleaner = CreateCleaner( appender );

        return cleaner->GetOutput();
      }

      std::tuple<vtkSmartPointer<vtkPolyData>, vtkSmartPointer<vtkPolyData>, vtkSmartPointer<vtkPolyData> >
      SliceVTKPolyData(vtkSmartPointer<vtkPolyData> inputPolyData, CLIP_COORDINATE clipCoordinate, double *center)
      {
        boost::timer::auto_cpu_timer t;

        vtkSmartPointer<vtkPlane> clipPlane = CreateClipPlane( inputPolyData, center, clipCoordinate );
        vtkSmartPointer<vtkPolyData> clippedInterface = CreateClippedInterface( clipPlane, inputPolyData );
        vtkSmartPointer<vtkPolyData> unclippedInterface = CreateUnclippedInterface( clippedInterface );

        vtkSmartPointer<vtkPolyData> clippedGeometry;
        vtkSmartPointer<vtkPolyData> unclippedGeometry;

        CreateUnclippedAndClippedGeometry( inputPolyData, clipPlane, unclippedGeometry, clippedGeometry );

        vtkSmartPointer<vtkPolyData> clippedPart = AssemblePart( clippedGeometry, clippedInterface );
        vtkSmartPointer<vtkPolyData> unnclippedPart = AssemblePart( unclippedGeometry, unclippedInterface );

        return std::make_tuple( clippedInterface, clippedPart, unnclippedPart );
      }

    }
  }
}



#include <vtkSphereSource.h>

#include <vtkXMLPolyDataWriter.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkInteractorStyleTrackballCamera.h>

#include <vtkXMLUnstructuredGridReader.h>
#include <vtkGeometryFilter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkSTLReader.h>

#include <vtk_includes/vtkBooleanOperationPolyDataFilter.hpp>


#include <algorithm>
#include <sstream>

void visualizeTreeParts(vtkSmartPointer<vtkPolyData> interface, vtkSmartPointer<vtkPolyData> above, vtkSmartPointer<vtkPolyData> below)
{
  {
    vtkSmartPointer<vtkRenderWindow> renderWindow =  vtkSmartPointer<vtkRenderWindow>::New();
    renderWindow->SetSize(1024, 768);

    // And one interactor
    vtkSmartPointer<vtkRenderWindowInteractor> interactor =  vtkSmartPointer<vtkRenderWindowInteractor>::New();
    interactor->SetRenderWindow( renderWindow );

    //    vtkSmartPointer<vtkInteractorStyleTrackballCamera> style = vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New(); //like paraview
    //    interactor->SetInteractorStyle( style );

    // (xmin, ymin, xmax, ymax)
    double aboveViewport[4] = {0.0, 0.0, 1.0, 0.33};
    double middleViewport[4] = {0.0, 0.33, 1.0, 0.66};
    double belowViewport[4] = {0.0, 0.66, 1.0, 1.0};


    // Setup both renderers
    vtkSmartPointer<vtkRenderer> aboveRenderer = vtkSmartPointer<vtkRenderer>::New();
    renderWindow->AddRenderer(aboveRenderer);
    aboveRenderer->SetViewport(aboveViewport);
    aboveRenderer->SetBackground(.5, .0, .0);


    vtkSmartPointer<vtkRenderer> middleRenderer = vtkSmartPointer<vtkRenderer>::New();
    renderWindow->AddRenderer(middleRenderer);
    middleRenderer->SetViewport(middleViewport);
    middleRenderer->SetBackground(.0, .5, .0);


    vtkSmartPointer<vtkRenderer> belowRenderer = vtkSmartPointer<vtkRenderer>::New();
    renderWindow->AddRenderer(belowRenderer);
    belowRenderer->SetViewport(belowViewport);
    belowRenderer->SetBackground(.0, .0, .5);


    // Add actors
    {
      // Above
      vtkSmartPointer<vtkPolyDataMapper> aboveMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
      aboveMapper->SetInput( above );

      vtkSmartPointer<vtkActor> aboveActor = vtkSmartPointer<vtkActor>::New();
      aboveActor->SetMapper( aboveMapper );
      aboveActor->GetProperty()->EdgeVisibilityOn();

      {
        vtkSmartPointer< vtkProperty > backFaces = vtkSmartPointer< vtkProperty >::New();
        backFaces->SetSpecular( 0.0 );
        backFaces->SetDiffuse( 0.0 );
        backFaces->SetAmbient( 1.0 );
        backFaces->SetAmbientColor( 1.0000, 0.3882, 0.2784 );

        aboveActor->SetBackfaceProperty( backFaces );
      }

      aboveRenderer->AddActor( aboveActor );

      // Interface
      vtkSmartPointer<vtkPolyDataMapper> interfaceMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
      interfaceMapper->SetInput( interface );

      vtkSmartPointer<vtkActor> interfaceActor = vtkSmartPointer<vtkActor>::New();
      interfaceActor->SetMapper(interfaceMapper);
      interfaceActor->GetProperty()->EdgeVisibilityOn();

      {
        vtkSmartPointer< vtkProperty > backFaces = vtkSmartPointer< vtkProperty >::New();
        backFaces->SetSpecular( 0.0 );
        backFaces->SetDiffuse( 0.0 );
        backFaces->SetAmbient( 1.0 );
        backFaces->SetAmbientColor( 1.0000, 0.3882, 0.2784 );

        interfaceActor->SetBackfaceProperty( backFaces );
      }

      middleRenderer->AddActor( interfaceActor );


      // Below
      vtkSmartPointer<vtkPolyDataMapper> belowMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
      belowMapper->SetInput( below );

      vtkSmartPointer<vtkActor> belowActor = vtkSmartPointer<vtkActor>::New();
      belowActor->SetMapper( belowMapper );
      belowActor->GetProperty()->EdgeVisibilityOn();

      {
        vtkSmartPointer< vtkProperty > backFaces = vtkSmartPointer< vtkProperty >::New();
        backFaces->SetSpecular( 0.0 );
        backFaces->SetDiffuse( 0.0 );
        backFaces->SetAmbient( 1.0 );
        backFaces->SetAmbientColor( 1.0000, 0.3882, 0.2784 );

        belowActor->SetBackfaceProperty( backFaces );
      }


      belowRenderer->AddActor( belowActor );

    }

    aboveRenderer->ResetCamera();
    middleRenderer->ResetCamera();
    belowRenderer->ResetCamera();

    renderWindow->Render();
    interactor->Start();
  }
}


bool test::VTKSlicerXTest()
{
  std::cout << "\n\nVTKSlicerXTest()" << std::endl;

  vtkSmartPointer< vtkPolyData > input;
  //  /*
  {
    vtkSmartPointer<vtkPolyData> base;

    {
      vtkSmartPointer<vtkSphereSource> source = vtkSmartPointer<vtkSphereSource>::New();
      source->SetCenter(0, 0, 0);
      source->SetRadius( 10.0 );
      source->SetPhiResolution( 30 );
      source->SetThetaResolution( 30 );
      source->Update();
      base = source->GetOutput();
    }


    std::list< vtkSmartPointer<vtkPolyData> > objects;
    // Generate spheres
    {
      //Create a renderer, render window, and interactor
      vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
      vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
      renderWindow->SetSize(1024, 768);
      renderWindow->AddRenderer(renderer);

      vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
      renderWindowInteractor->SetRenderWindow(renderWindow);

      srand( 2 );

      const auto sphereCount = 6;
      for ( auto i = 0; i < sphereCount; ++ i ) {
        vtkSmartPointer<vtkSphereSource> sphereSource = vtkSmartPointer<vtkSphereSource>::New();
        // const float x = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
        const float x = 0.0f;

        const float ylo = -8.0f;
        const float yhi = 8.0f;
        const float y = ylo + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(yhi - ylo)));


        const float zlo = -8.0f;
        const float zhi = 8.0f;
        const float z = zlo + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(zhi - zlo)));

        const float lo = 0.5f;
        const float hi = 1.0f;
        const float r = lo + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(hi - lo)));

        sphereSource->SetCenter( x, y, z );
        sphereSource->SetRadius( r );
        sphereSource->SetPhiResolution( 7 );
        sphereSource->SetThetaResolution( 7 );
        sphereSource->Update();
        objects.push_back( sphereSource->GetOutput() );
        std::cout << "Sphere : ( " << x << ", " << y << ", " << z << " ), r: " << r << std::endl;

        vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        mapper->SetInput( sphereSource->GetOutput()  );

        vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
        actor->SetMapper( mapper );

        {
          vtkSmartPointer< vtkProperty > backFaces = vtkSmartPointer< vtkProperty >::New();
          backFaces->SetSpecular( 0.0 );
          backFaces->SetDiffuse( 0.0 );
          backFaces->SetAmbient( 1.0 );
          backFaces->SetAmbientColor( 1.0000, 0.3882, 0.2784 );

          actor->SetBackfaceProperty( backFaces );
        }

        renderer->AddActor( actor );
      }

      renderWindow->Render();
      renderWindowInteractor->Start();
    }

    while ( !objects.empty() ) {
      vtkSmartPointer<vtkBooleanOperationPolyDataFilter> booleanOperation = vtkSmartPointer<vtkBooleanOperationPolyDataFilter>::New();
      booleanOperation->SetOperationToDifference();

      booleanOperation->SetInputConnection( 0, base->GetProducerPort() );
      booleanOperation->SetInputConnection( 1, objects.front()->GetProducerPort() );

      base = booleanOperation->GetOutput();
      objects.pop_front();
    }

    input = base;
  }
  //  */

  auto interface_left_right = kernel::utils::vtk_utils::SliceVTKPolyData( input, kernel::utils::vtk_utils::CLIP_COORDINATE::X_CLIP );

  std::cout << "Slice X completed" << std::endl;

  auto& interface = std::get<0>( interface_left_right );
  auto& left = std::get<1>( interface_left_right );
  auto& right = std::get<2>( interface_left_right );

  // Visualization
  visualizeTreeParts( interface, left, right );

  // Write
  {
    {
      vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
      writer->SetFileName( "interfaceX.vtp" );
      writer->SetInput( interface );
      writer->Write();
      std::cout << "Interface X wroted" << std::endl;
    }

    {
      vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
      writer->SetFileName( "left.vtp" );
      writer->SetInput( left );
      writer->Write();
      std::cout << "Left part wroted" << std::endl;
    }

    {
      vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
      writer->SetFileName( "right.vtp" );
      writer->SetInput( right );
      writer->Write();
      std::cout << "Right part wroted" << std::endl;
    }
  }

  return true;
}


bool test::VTKSlicerYTest()
{
  std::cout << "\n\nVTKSlicerYTest()" << std::endl;

  vtkSmartPointer< vtkPolyData > input;
  /*
  {
    vtkSmartPointer<vtkPolyData> input1;
    vtkSmartPointer<vtkPolyData> input2;

    vtkSmartPointer<vtkSphereSource> source1 = vtkSmartPointer<vtkSphereSource>::New();
    source1->SetCenter( 0.0, 0.0, 0.0 );
    source1->SetRadius( 2.0 );
    source1->Update();
    input1 = source1->GetOutput();

    vtkSmartPointer<vtkSphereSource> source2 = vtkSmartPointer<vtkSphereSource>::New();
    source2->SetCenter( 0.0, 0.0, 0.0 );
    source2->SetRadius( 1.0 );
    source2->Update();
    input2 = source2->GetOutput();

    vtkSmartPointer<vtkBooleanOperationPolyDataFilter> booleanOperation = vtkSmartPointer<vtkBooleanOperationPolyDataFilter>::New();
    booleanOperation->SetOperationToDifference();

    booleanOperation->SetInputConnection( 0, input1->GetProducerPort() );
    booleanOperation->SetInputConnection( 1, input2->GetProducerPort() );

    input = booleanOperation->GetOutput();
  }
  */

  {
    vtkSmartPointer<vtkPolyData> base;

    {
      vtkSmartPointer<vtkSphereSource> source = vtkSmartPointer<vtkSphereSource>::New();
      source->SetCenter(0, 0, 0);
      source->SetRadius( 1.0 );
      source->SetPhiResolution( 50 );
      source->SetThetaResolution( 50 );
      source->Update();
      base = source->GetOutput();
    }

    std::list< vtkSmartPointer<vtkPolyData> > objects;
    // Generate spheres Artifact edges cause wrong triangulation

    {
      //Create a renderer, render window, and interactor
      vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
      vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
      renderWindow->SetSize(1024, 768);
      renderWindow->AddRenderer(renderer);

      vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
      renderWindowInteractor->SetRenderWindow(renderWindow);

      srand( 3 );
      const auto sphereCount = 12;
      for ( auto i = 0; i < sphereCount; ++ i ) {
        vtkSmartPointer<vtkSphereSource> sphereSource = vtkSmartPointer<vtkSphereSource>::New();
        const float x = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - 0.5f;
        // const float y = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - 0.5f;
        const float y = 0.0f;
        const float z = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - 0.5f;

        const float lo = 0.1;
        const float hi = 0.2;
        const float r = lo + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(hi - lo)));

        sphereSource->SetCenter( x, y, z );
        sphereSource->SetRadius( r );
        sphereSource->SetPhiResolution( 10 );
        sphereSource->SetThetaResolution( 10 );
        sphereSource->Update();
        objects.push_back( sphereSource->GetOutput() );

        std::cout << "Sphere : ( " << x << ", " << y << ", " << z << " ), r: " << r << std::endl;

        vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        mapper->SetInput( sphereSource->GetOutput()  );

        vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
        actor->SetMapper( mapper );

        {
          vtkSmartPointer< vtkProperty > backFaces = vtkSmartPointer< vtkProperty >::New();
          backFaces->SetSpecular( 0.0 );
          backFaces->SetDiffuse( 0.0 );
          backFaces->SetAmbient( 1.0 );
          backFaces->SetAmbientColor( 1.0000, 0.3882, 0.2784 );

          actor->SetBackfaceProperty( backFaces );
        }

        renderer->AddActor( actor );
      }

      renderWindow->Render();
      renderWindowInteractor->Start();

    }

    /*
    {
      //Create a renderer, render window, and interactor
      vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
      vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
      renderWindow->SetSize(1024, 768);
      renderWindow->AddRenderer(renderer);

      vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
      renderWindowInteractor->SetRenderWindow(renderWindow);

      srand( 1 ); // Artifact edges cause wrong triangulation

      const auto sphereCount = 6;
      for ( auto i = 0; i < sphereCount; ++ i ) {
        vtkSmartPointer<vtkSphereSource> sphereSource = vtkSmartPointer<vtkSphereSource>::New();
        const float x = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - 0.5f;
        // const float y = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - 0.5f;
        const float y = 0.0f;
        const float z = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - 0.5f;

        const float lo = 0.1;
        const float hi = 0.3;
        const float r = lo + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(hi - lo)));

        sphereSource->SetCenter( x, y, z );
        sphereSource->SetRadius( r );
        sphereSource->Update();
        objects.push_back( sphereSource->GetOutput() );

        std::cout << "Sphere : ( " << x << ", " << y << ", " << z << " ), r: " << r << std::endl;

        vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        mapper->SetInput( sphereSource->GetOutput()  );

        vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
        actor->SetMapper( mapper );

        {
          vtkSmartPointer< vtkProperty > backFaces = vtkSmartPointer< vtkProperty >::New();
          backFaces->SetSpecular( 0.0 );
          backFaces->SetDiffuse( 0.0 );
          backFaces->SetAmbient( 1.0 );
          backFaces->SetAmbientColor( 1.0000, 0.3882, 0.2784 );

          actor->SetBackfaceProperty( backFaces );
        }

        renderer->AddActor( actor );
      }

      renderWindow->Render();
      renderWindowInteractor->Start();

    }
    */

    while ( !objects.empty() ) {
      vtkSmartPointer<vtkBooleanOperationPolyDataFilter> booleanOperation = vtkSmartPointer<vtkBooleanOperationPolyDataFilter>::New();
      booleanOperation->SetOperationToDifference();

      booleanOperation->SetInputConnection( 0, base->GetProducerPort() );
      booleanOperation->SetInputConnection( 1, objects.front()->GetProducerPort() );

      base = booleanOperation->GetOutput();
      objects.pop_front();
    }

    input = base;
  }


  auto interface_below_above = kernel::utils::vtk_utils::SliceVTKPolyData( input, kernel::utils::vtk_utils::CLIP_COORDINATE::Y_CLIP );
  std::cout << "Slice Y completed" << std::endl;

  auto& interface = std::get<0>( interface_below_above );
  auto& below = std::get<1>( interface_below_above );
  auto& above = std::get<2>( interface_below_above );

  // Visualize
  visualizeTreeParts( interface, above, below );

  // Write
  {
    {
      vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
      writer->SetFileName( "interfaceY.vtp" );
      writer->SetInput( interface );
      writer->Write();
      std::cout << "Interface Y wroted" << std::endl;
    }

    {
      vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
      writer->SetFileName( "below.vtp" );
      writer->SetInput( below );
      writer->Write();
      std::cout << "Below part wroted" << std::endl;
    }

    {
      vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
      writer->SetFileName( "above.vtp" );
      writer->SetInput( above );
      writer->Write();
      std::cout << "Above part wroted" << std::endl;
    }

  }

  return true;
}


bool test::VTKSlicerZTest()
{
  std::cout << "\n\nVTKSlicerZTest()" << std::endl;

  vtkSmartPointer< vtkPolyData > input;
  /*
  {

    vtkSmartPointer<vtkPolyData> input1;
    vtkSmartPointer<vtkPolyData> input2;

    vtkSmartPointer<vtkSphereSource> source1 = vtkSmartPointer<vtkSphereSource>::New();
    source1->SetCenter( 0.0, 0.0, 0.0 );
    source1->SetRadius( 2.0 );
    source1->Update();
    input1 = source1->GetOutput();

    vtkSmartPointer<vtkSphereSource> source2 = vtkSmartPointer<vtkSphereSource>::New();
    source2->SetCenter( 0.0, 0.0, 0.0 );
    source2->SetRadius( 1.0 );
    source2->Update();
    input2 = source2->GetOutput();

    vtkSmartPointer<vtkBooleanOperationPolyDataFilter> booleanOperation = vtkSmartPointer<vtkBooleanOperationPolyDataFilter>::New();
    booleanOperation->SetOperationToDifference();

    booleanOperation->SetInputConnection( 0, input1->GetProducerPort() );
    booleanOperation->SetInputConnection( 1, input2->GetProducerPort() );

    input = booleanOperation->GetOutput();
  }
  */

  {
    vtkSmartPointer<vtkPolyData> base;

    {
      vtkSmartPointer<vtkSphereSource> source = vtkSmartPointer<vtkSphereSource>::New();
      source->SetCenter(0, 0, 0);
      source->SetRadius( 1.0 );
      source->SetPhiResolution( 50 );
      source->SetThetaResolution( 50 );
      source->Update();
      base = source->GetOutput();
    }

    std::list< vtkSmartPointer<vtkPolyData> > objects;
    // Generate spheres
    {
      //Create a renderer, render window, and interactor
      vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
      vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
      renderWindow->SetSize(1024, 768);
      renderWindow->AddRenderer(renderer);

      vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
      renderWindowInteractor->SetRenderWindow(renderWindow);

      srand( 1 );

      const auto sphereCount = 6;
      for ( auto i = 0; i < sphereCount; ++ i ) {
        vtkSmartPointer<vtkSphereSource> sphereSource = vtkSmartPointer<vtkSphereSource>::New();
        const float x = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - 0.5f;
        const float y = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - 0.5f;
        // const float z = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - 0.5f;
        const float z = 0.0f;

        const float lo = 0.1;
        const float hi = 0.3;
        const float r = lo + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(hi - lo)));

        sphereSource->SetCenter( x, y, z );
        sphereSource->SetRadius( r );
        sphereSource->SetPhiResolution( 10 );
        sphereSource->SetThetaResolution( 10 );
        sphereSource->Update();
        objects.push_back( sphereSource->GetOutput() );

        std::cout << "Sphere : ( " << x << ", " << y << ", " << z << " ), r: " << r << std::endl;

        vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        mapper->SetInput( sphereSource->GetOutput()  );

        vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
        actor->SetMapper( mapper );

        {
          vtkSmartPointer< vtkProperty > backFaces = vtkSmartPointer< vtkProperty >::New();
          backFaces->SetSpecular( 0.0 );
          backFaces->SetDiffuse( 0.0 );
          backFaces->SetAmbient( 1.0 );
          backFaces->SetAmbientColor( 1.0000, 0.3882, 0.2784 );

          actor->SetBackfaceProperty( backFaces );
        }

        renderer->AddActor( actor );
      }

      renderWindow->Render();
      renderWindowInteractor->Start();

    }

    while ( !objects.empty() ) {
      vtkSmartPointer<vtkBooleanOperationPolyDataFilter> booleanOperation = vtkSmartPointer<vtkBooleanOperationPolyDataFilter>::New();
      booleanOperation->SetOperationToDifference();

      booleanOperation->SetInputConnection( 0, base->GetProducerPort() );
      booleanOperation->SetInputConnection( 1, objects.front()->GetProducerPort() );

      base = booleanOperation->GetOutput();
      objects.pop_front();
    }

    input = base;
  }


  auto interface_back_front = kernel::utils::vtk_utils::SliceVTKPolyData( input, kernel::utils::vtk_utils::CLIP_COORDINATE::Z_CLIP );
  std::cout << "Slice Z completed" << std::endl;

  auto& interface = std::get<0>( interface_back_front );
  auto& back = std::get<1>( interface_back_front );
  auto& front = std::get<2>( interface_back_front );


  // Visualize
  visualizeTreeParts( interface, back, front );


  // Write
  {
    {
      vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
      writer->SetFileName( "interfaceZ.vtp" );
      writer->SetInput( interface );
      writer->Write();
      std::cout << "Interface Z wroted" << std::endl;
    }

    {
      vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
      writer->SetFileName( "back.vtp" );
      writer->SetInput( back );
      writer->Write();
      std::cout << "Back part wroted" << std::endl;
    }

    {
      vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
      writer->SetFileName( "front.vtp" );
      writer->SetInput( front );
      writer->Write();
      std::cout << "Front part wroted" << std::endl;
    }
  }

  return true;
}


bool test::VTKSlicerXYZTest()
{
  std::cout << "VTKSlicerXYZTest()" << std::endl;

  vtkSmartPointer<vtkSphereSource> sphereSource = vtkSmartPointer<vtkSphereSource>::New();
  sphereSource->SetThetaResolution( 100 );
  sphereSource->SetPhiResolution( 100 );
  sphereSource->Update();

  vtkSmartPointer< vtkPolyData > initialPolydata = sphereSource->GetOutput();
  std::vector< vtkSmartPointer< vtkPolyData > > xVector;
  {
    auto i_l_r = kernel::utils::vtk_utils::SliceVTKPolyData( initialPolydata, kernel::utils::vtk_utils::CLIP_COORDINATE::X_CLIP );
    xVector.push_back( std::get<1>( i_l_r ) );
    xVector.push_back( std::get<2>( i_l_r ) );
  }

  std::for_each( xVector.begin(), xVector.end(), [&](vtkSmartPointer< vtkPolyData >& pd) {

    static std::size_t counter = 1;

    std::stringstream ss;
    ss << "x_fragment_";
    ss << counter++;
    ss << ".vtp";

    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName( ss.str().c_str() );
    writer->SetInput( pd );
    writer->Write();
    std::cout << ss.str() << " processed" << std::endl;
  } );
  std::cout << "xVector filled\n" << std::endl;

  std::vector< vtkSmartPointer< vtkPolyData > > yVector;
  std::for_each( xVector.begin(), xVector.end(), [&](vtkSmartPointer< vtkPolyData >& pd) {
    auto i_b_a = kernel::utils::vtk_utils::SliceVTKPolyData( vtkSmartPointer< vtkPolyData >(pd), kernel::utils::vtk_utils::CLIP_COORDINATE::Y_CLIP );
    yVector.push_back( std::get<1>( i_b_a ) );
    yVector.push_back( std::get<2>( i_b_a ) );
  } );

  std::for_each( yVector.begin(), yVector.end(), [&](vtkSmartPointer< vtkPolyData >& pd) {

    static std::size_t counter = 1;

    std::stringstream ss;
    ss << "y_fragment_";
    ss << counter++;
    ss << ".vtp";

    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName( ss.str().c_str() );
    writer->SetInput( pd );
    writer->Write();
    std::cout << ss.str() << " processed" << std::endl;
  } );

  std::cout << "yVector filled\n" << std::endl;

  std::vector< vtkSmartPointer< vtkPolyData > > zVector;
  std::for_each( yVector.begin(), yVector.end(), [&](vtkSmartPointer< vtkPolyData >& pd) {
    auto i_b_f = kernel::utils::vtk_utils::SliceVTKPolyData( vtkSmartPointer< vtkPolyData >(pd), kernel::utils::vtk_utils::CLIP_COORDINATE::Z_CLIP );
    zVector.push_back( std::get<1>( i_b_f ) );
    zVector.push_back( std::get<2>( i_b_f ) );
  } );
  std::cout << "zVector filled\n" << std::endl;

  std::for_each( zVector.begin(), zVector.end(), [&](vtkSmartPointer< vtkPolyData >& pd) {

    static std::size_t counter = 1;

    std::stringstream ss;
    ss << "z_fragment_";
    ss << counter++;
    ss << ".vtp";

    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName( ss.str().c_str() );
    writer->SetInput( pd );
    writer->Write();
    std::cout << ss.str() << " processed" << std::endl;
  } );

  return true;
}


bool test::VTKSlicerXYZTestGeometry()
{
  std::cout << "VTKSlicerXYZTestGeometry()" << std::endl;

  vtkSmartPointer< vtkPolyData > initialPolydata;
  {
    vtkSmartPointer< vtkXMLUnstructuredGridReader > reader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
    reader->SetFileName( "ktoolcav.vtu" );
    reader->Update();

    vtkSmartPointer<vtkGeometryFilter> geometryFilter = vtkSmartPointer<vtkGeometryFilter>::New();
    geometryFilter->SetInput( reader->GetOutput() );
    geometryFilter->Update();

    initialPolydata = geometryFilter->GetOutput();
  }
  double* center = initialPolydata->GetCenter();

  std::vector< vtkSmartPointer< vtkPolyData > > xVector;
  std::vector< vtkSmartPointer< vtkPolyData > > yVector;
  std::vector< vtkSmartPointer< vtkPolyData > > zVector;
  {
    boost::timer::auto_cpu_timer t;
    {
      auto i_r_l = kernel::utils::vtk_utils::SliceVTKPolyData( initialPolydata, kernel::utils::vtk_utils::CLIP_COORDINATE::X_CLIP, center );
      xVector.push_back( std::get<1>( i_r_l ) );
      xVector.push_back( std::get<2>( i_r_l ) );
    }

    std::for_each( xVector.begin(), xVector.end(), [&](vtkSmartPointer< vtkPolyData >& pd) {
      auto i_a_b = kernel::utils::vtk_utils::SliceVTKPolyData( vtkSmartPointer< vtkPolyData >(pd), kernel::utils::vtk_utils::CLIP_COORDINATE::Y_CLIP, center );
      yVector.push_back( std::get<1>( i_a_b ) );
      yVector.push_back( std::get<2>( i_a_b ) );
    } );

    std::for_each( yVector.begin(), yVector.end(), [&](vtkSmartPointer< vtkPolyData >& pd) {
      auto i_f_b = kernel::utils::vtk_utils::SliceVTKPolyData( vtkSmartPointer< vtkPolyData >(pd), kernel::utils::vtk_utils::CLIP_COORDINATE::Z_CLIP, center );
      zVector.push_back( std::get<1>( i_f_b ) );
      zVector.push_back( std::get<2>( i_f_b ) );
    } );

  }


  std::for_each( xVector.begin(), xVector.end(), [&](vtkSmartPointer< vtkPolyData >& pd) {

    static std::size_t counter = 1;

    std::stringstream ss;
    ss << "ktoolcav_x_fragment_";
    ss << counter++;
    ss << ".vtp";

    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName( ss.str().c_str() );
    writer->SetInput( pd );
    writer->Write();
    std::cout << ss.str() << " processed" << std::endl;
  } );

  std::for_each( yVector.begin(), yVector.end(), [&](vtkSmartPointer< vtkPolyData >& pd) {

    static std::size_t counter = 1;

    std::stringstream ss;
    ss << "ktoolcav_y_fragment_";
    ss << counter++;
    ss << ".vtp";

    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName( ss.str().c_str() );
    writer->SetInput( pd );
    writer->Write();
    std::cout << ss.str() << " processed" << std::endl;
  } );

  std::for_each( zVector.begin(), zVector.end(), [&](vtkSmartPointer< vtkPolyData >& pd) {

    static std::size_t counter = 1;

    std::stringstream ss;
    ss << "ktoolcav_z_fragment_";
    ss << counter++;
    ss << ".vtp";

    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName( ss.str().c_str() );
    writer->SetInput( pd );
    writer->Write();
    std::cout << ss.str() << " processed" << std::endl;
  } );

  return true;
}


bool test::VTKSlicerXYZTestLargeGeometry()
{
  std::cout << "VTKSlicerXYZTestLargeGeometry()" << std::endl;

  vtkSmartPointer< vtkPolyData > initialPolydata;
  {
    vtkSmartPointer< vtkSTLReader > reader = vtkSmartPointer< vtkSTLReader >::New();
    reader->SetFileName( "angry.stl" );
    reader->Update();

    initialPolydata = reader->GetOutput();
  }
  double* center = initialPolydata->GetCenter();

  std::vector< vtkSmartPointer< vtkPolyData > > xVector;
  std::vector< vtkSmartPointer< vtkPolyData > > yVector;
  std::vector< vtkSmartPointer< vtkPolyData > > zVector;
  {
    boost::timer::auto_cpu_timer t;
    {
      auto i_l_r = kernel::utils::vtk_utils::SliceVTKPolyData( initialPolydata, kernel::utils::vtk_utils::CLIP_COORDINATE::X_CLIP, center );
      xVector.push_back( std::get<1>( i_l_r ) );
      xVector.push_back( std::get<2>( i_l_r ) );
    }

    std::for_each( xVector.begin(), xVector.end(), [&](vtkSmartPointer< vtkPolyData >& pd) {
      auto i_b_a = kernel::utils::vtk_utils::SliceVTKPolyData( vtkSmartPointer< vtkPolyData >(pd), kernel::utils::vtk_utils::CLIP_COORDINATE::Y_CLIP, center );
      yVector.push_back( std::get<1>( i_b_a ) );
      yVector.push_back( std::get<2>( i_b_a ) );
    } );

    std::for_each( yVector.begin(), yVector.end(), [&](vtkSmartPointer< vtkPolyData >& pd) {
      auto i_b_f = kernel::utils::vtk_utils::SliceVTKPolyData( vtkSmartPointer< vtkPolyData >(pd), kernel::utils::vtk_utils::CLIP_COORDINATE::Z_CLIP, center );
      zVector.push_back( std::get<1>( i_b_f ) );
      zVector.push_back( std::get<2>( i_b_f ) );
    } );

  }


  std::for_each( xVector.begin(), xVector.end(), [&](vtkSmartPointer< vtkPolyData >& pd) {

    static std::size_t counter = 1;

    std::stringstream ss;
    ss << "angry_x_fragment_";
    ss << counter++;
    ss << ".vtp";

    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName( ss.str().c_str() );
    writer->SetInput( pd );
    writer->Write();
    std::cout << ss.str() << " processed" << std::endl;
  } );

  std::for_each( yVector.begin(), yVector.end(), [&](vtkSmartPointer< vtkPolyData >& pd) {

    static std::size_t counter = 1;

    std::stringstream ss;
    ss << "angry_y_fragment_";
    ss << counter++;
    ss << ".vtp";

    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName( ss.str().c_str() );
    writer->SetInput( pd );
    writer->Write();
    std::cout << ss.str() << " processed" << std::endl;
  } );

  std::for_each( zVector.begin(), zVector.end(), [&](vtkSmartPointer< vtkPolyData >& pd) {

    static std::size_t counter = 1;

    std::stringstream ss;
    ss << "angry_z_fragment_";
    ss << counter++;
    ss << ".vtp";

    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName( ss.str().c_str() );
    writer->SetInput( pd );
    writer->Write();
    std::cout << ss.str() << " processed" << std::endl;
  } );
  return true;
}


#include <vtkFeatureEdges.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkCellArray.h>
#include <vtkExtractEdges.h>
#include <vtkLine.h>
#include <vtkStripper.h>
#include <vtkProperty.h>
#include <vtkPolyLine.h>


bool test::VTKSlicerExtractBoundaryEdges()
{
  vtkSmartPointer< vtkPolyData > input;
  {
    vtkSmartPointer<vtkPolyData> input1;
    vtkSmartPointer<vtkPolyData> input2;
    vtkSmartPointer<vtkPolyData> input3;
    vtkSmartPointer<vtkPolyData> input4;

    vtkSmartPointer<vtkSphereSource> source1 = vtkSmartPointer<vtkSphereSource>::New();
    source1->SetCenter(0, 0, 0);
    source1->SetRadius( 2.0 );
    source1->Update();
    input1 = source1->GetOutput();

    vtkSmartPointer<vtkSphereSource> source2 = vtkSmartPointer<vtkSphereSource>::New();
    source2->SetCenter( 0, 1.0, 0 );
    source2->SetRadius( 0.5 );
    source2->Update();
    input2 = source2->GetOutput();

    vtkSmartPointer<vtkSphereSource> source3 = vtkSmartPointer<vtkSphereSource>::New();
    source3->SetCenter( 0, -2.0, 0 );
    source3->SetRadius( 0.5 );
    source3->Update();
    input3 = source3->GetOutput();

    vtkSmartPointer<vtkSphereSource> source4 = vtkSmartPointer<vtkSphereSource>::New();
    source4->SetCenter( 0, -1.0, 0 );
    source4->SetRadius( 0.125 );
    source4->Update();
    input4 = source4->GetOutput();

    vtkSmartPointer<vtkBooleanOperationPolyDataFilter> booleanOperation1 = vtkSmartPointer<vtkBooleanOperationPolyDataFilter>::New();
    booleanOperation1->SetOperationToDifference();
    booleanOperation1->SetInputConnection( 0, input1->GetProducerPort() );
    booleanOperation1->SetInputConnection( 1, input2->GetProducerPort() );

    vtkSmartPointer<vtkBooleanOperationPolyDataFilter> booleanOperation2 = vtkSmartPointer<vtkBooleanOperationPolyDataFilter>::New();
    booleanOperation2->SetOperationToDifference();
    booleanOperation2->SetInputConnection( 0, booleanOperation1->GetOutputPort() );
    booleanOperation2->SetInputConnection( 1, input3->GetProducerPort() );

    vtkSmartPointer<vtkBooleanOperationPolyDataFilter> booleanOperation3 = vtkSmartPointer<vtkBooleanOperationPolyDataFilter>::New();
    booleanOperation3->SetOperationToDifference();
    booleanOperation3->SetInputConnection( 0, booleanOperation2->GetOutputPort() );
    booleanOperation3->SetInputConnection( 1, input4->GetProducerPort() );

    input = booleanOperation3->GetOutput();
  }

  auto interface_left_right = kernel::utils::vtk_utils::SliceVTKPolyData( input, kernel::utils::vtk_utils::CLIP_COORDINATE::X_CLIP );

  std::cout << "Slice X completed" << std::endl;

  auto& interface = std::get<0>( interface_left_right );
  {
    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName( "interfaceX.vtp" );
    writer->SetInput( interface );
    writer->Write();
    std::cout << "Interface X wroted" << std::endl;
  }

  auto& left = std::get<1>( interface_left_right );
  {
    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName( "left.vtp" );
    writer->SetInput( left );
    writer->Write();
    std::cout << "Left part wroted" << std::endl;
  }

  auto& right = std::get<2>( interface_left_right );
  {
    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName( "right.vtp" );
    writer->SetInput( right );
    writer->Write();
    std::cout << "Right part wroted" << std::endl;
  }

  // vtkSmartPointer< vtkPolyData > interface = std::get< 0 >( interface_left_right );

  // Create a renderer, render window, and interactor
  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();

  vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();

  renderWindow->AddRenderer(renderer);

  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);


  vtkSmartPointer<vtkFeatureEdges> featureEdges = vtkSmartPointer<vtkFeatureEdges>::New();
  featureEdges->SetInput( interface );
  featureEdges->BoundaryEdgesOn();
  featureEdges->FeatureEdgesOff();
  featureEdges->ManifoldEdgesOff();
  featureEdges->NonManifoldEdgesOff();
  featureEdges->Update();

  vtkSmartPointer< vtkPolyData > boundaryEdges = featureEdges->GetOutput();
  kernel::utils::vtk_utils::find_seeds_X( boundaryEdges );

  return true;

}


bool test::VTKComplexInterface()
{
  vtkSmartPointer< vtkPolyData > input1;
  {
    vtkSmartPointer<vtkCubeSource> cubeSource = vtkSmartPointer<vtkCubeSource>::New();
    cubeSource->SetCenter( 0, 0, 0 );
    cubeSource->SetXLength( 2.0 );
    cubeSource->SetYLength( 2.0 );
    cubeSource->SetZLength( 2.0 );

    vtkSmartPointer<vtkTriangleFilter> boxTri = vtkSmartPointer<vtkTriangleFilter>::New();
    boxTri->SetInput(cubeSource->GetOutput());
    input1 = boxTri->GetOutput();
  }
  vtkSmartPointer< vtkPolyData > input2;
  {
    vtkSmartPointer<vtkCubeSource> cubeSource = vtkSmartPointer<vtkCubeSource>::New();
    cubeSource->SetCenter( 0.55, 0.0, 0.0 );
    cubeSource->SetZLength( 3.0 );
    cubeSource->SetYLength( 0.45 );

    vtkSmartPointer<vtkTriangleFilter> boxTri = vtkSmartPointer<vtkTriangleFilter>::New();
    boxTri->SetInput(cubeSource->GetOutput());
    input2 = boxTri->GetOutput();
  }
  vtkSmartPointer< vtkPolyData > input3;
  {
    vtkSmartPointer<vtkSphereSource> source = vtkSmartPointer<vtkSphereSource>::New();
    source->SetCenter( 1.0, 0.5, 0.5 );
    source->SetRadius( 0.2 );
    source->Update();
    input3 = source->GetOutput();
  }
  vtkSmartPointer< vtkPolyData > input4;
  {
    vtkSmartPointer<vtkSphereSource> source = vtkSmartPointer<vtkSphereSource>::New();
    source->SetCenter( 1.0, -0.5, -0.5 );
    source->SetRadius( 0.2 );
    source->Update();
    input4 = source->GetOutput();
  }

  vtkSmartPointer< vtkPolyData > difference;
  {
    vtkSmartPointer<vtkBooleanOperationPolyDataFilter> booleanOperation1 = vtkSmartPointer<vtkBooleanOperationPolyDataFilter>::New();
    booleanOperation1->SetOperationToDifference();
    booleanOperation1->SetInputConnection( 0, input1->GetProducerPort() );
    booleanOperation1->SetInputConnection( 1, input2->GetProducerPort() );

    vtkSmartPointer<vtkBooleanOperationPolyDataFilter> booleanOperation2 = vtkSmartPointer<vtkBooleanOperationPolyDataFilter>::New();
    booleanOperation2->SetOperationToDifference();
    booleanOperation2->SetInputConnection( 0, booleanOperation1->GetOutputPort() );
    booleanOperation2->SetInputConnection( 1, input3->GetProducerPort() );

    vtkSmartPointer<vtkBooleanOperationPolyDataFilter> booleanOperation3 = vtkSmartPointer<vtkBooleanOperationPolyDataFilter>::New();
    booleanOperation3->SetOperationToDifference();
    booleanOperation3->SetInputConnection( 0, booleanOperation2->GetOutputPort() );
    booleanOperation3->SetInputConnection( 1, input4->GetProducerPort() );

    difference = booleanOperation3->GetOutput();
  }


  {
    vtkSmartPointer< vtkXMLPolyDataWriter > writer = vtkSmartPointer< vtkXMLPolyDataWriter >::New();
    writer->SetFileName( "complex_composition.vtp" );
    writer->SetInput( difference );
    writer->Write();
  }

  vtkSmartPointer<vtkPolyData> interface;
  {
    vtkSmartPointer<vtkPlane> clipPlane = vtkSmartPointer<vtkPlane>::New();
    clipPlane->SetOrigin( 1.0, 0.0, 0.0 );
    clipPlane->SetNormal( 1.0, 0.0, 0.0 );

    // Create cutter
    vtkSmartPointer<vtkCutter> cutter = vtkSmartPointer<vtkCutter>::New();
    cutter->SetCutFunction( clipPlane );
    cutter->SetInput( difference );

    vtkSmartPointer<vtkContourTriangulator> contourTriangulator = vtkSmartPointer<vtkContourTriangulator>::New();
    contourTriangulator->SetNormal( clipPlane->GetNormal() );
    contourTriangulator->SetInputConnection( cutter->GetOutputPort() );

    interface = contourTriangulator->GetOutput();
  }

  {
    vtkSmartPointer< vtkXMLPolyDataWriter > writer = vtkSmartPointer< vtkXMLPolyDataWriter >::New();
    writer->SetFileName( "complex_interface.vtp" );
    writer->SetInput( interface );
    writer->Write();
  }

  vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  // mapper->SetInput( difference );
  mapper->SetInput( interface );

  vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(mapper);

  // Create a renderer, render window, and interactor
  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();

  vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);
  renderWindow->SetSize( 800, 600 );
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);

  // Add the actors to the scene
  renderer->AddActor(actor);
  renderer->SetBackground( 0.0, 0.0, 0.0 );

  // Render and interact
  renderWindow->Render();
  renderWindowInteractor->Start();

  return true;
}

#include <vtkXMLPolyDataWriter.h>

bool test::SliceAndSave(int argc, char *argv[])
{
  using namespace kernel::utils::vtk_utils;

  assert( argc == 5 );
  const std::string inputFileName = argv[ 1 ];
  const std::string planeDescriptor = argv[ 2 ]; // "X", "Y", "Z"
  const std::string firstOutputFilename = argv[ 3 ];
  const std::string secondOutputFilename = argv[ 4 ];

  vtkSmartPointer< vtkXMLPolyDataReader > reader = vtkSmartPointer< vtkXMLPolyDataReader >::New();
  reader->SetFileName( inputFileName.c_str() );
  reader->Update();
  vtkSmartPointer< vtkPolyData > polydata = reader->GetOutput();
  assert( polydata->GetNumberOfCells() );

  CLIP_COORDINATE clipCoordinate;

  if ( planeDescriptor == "X" ) {
    clipCoordinate = CLIP_COORDINATE::X_CLIP;
  }
  else if ( planeDescriptor == "Y" ) {
    clipCoordinate = CLIP_COORDINATE::Y_CLIP;
  }
  else if ( planeDescriptor == "Z" ) {
    clipCoordinate = CLIP_COORDINATE::Z_CLIP;
  }
  else {
    assert( false ); // Cause failure
  }

  auto interfaceFirstSecond = SliceVTKPolyData( polydata, clipCoordinate, polydata->GetCenter() );

  vtkSmartPointer< vtkPolyData > interface = std::get< 0 >( interfaceFirstSecond );
  vtkSmartPointer< vtkPolyData > first = std::get< 1 >( interfaceFirstSecond );
  vtkSmartPointer< vtkPolyData > second = std::get< 2 >( interfaceFirstSecond );
  visualizeTreeParts( interface, first, second );

  {
    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName( firstOutputFilename.c_str() );
    writer->SetInput( first );
    writer->Write();
  }

  {
    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName( secondOutputFilename.c_str() );
    writer->SetInput( second );
    writer->Write();
  }


  return true;
}
