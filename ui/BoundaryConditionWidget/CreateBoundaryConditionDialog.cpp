#include "CreateBoundaryConditionDialog.hpp"
#include "ui_CreateBoundaryConditionDialog.h"

#include <QColorDialog>

#include <DirihletTreeItem.hpp>
#include <NeumannTreeItem.hpp>

/*!
 * \brief CreateBoundaryConditionDialog::CreateBoundaryConditionDialog
 * \param parent
 * \param item
 */
CreateBoundaryConditionDialog::CreateBoundaryConditionDialog(QWidget *parent, AbstractTreeItem *item) :
  QDialog(parent),
  ui(new Ui::CreateBoundaryConditionDialog)
{
  ui->setupUi(this);

  if ( item != nullptr ) {
    setTreeItem( item );
  }
  else {
    hideNeumannUI();
    applyPickedColor();
  }

}

/*!
 * \brief CreateBoundaryConditionDialog::~CreateBoundaryConditionDialog
 */
CreateBoundaryConditionDialog::~CreateBoundaryConditionDialog()
{
  delete ui;
}

/*!
 * \brief CreateBoundaryConditionDialog::treeItem
 * \return
 */
AbstractTreeItem *CreateBoundaryConditionDialog::treeItem()
{
  AbstractTreeItem* item;
  if ( ui->typeComboBox->currentIndex() == 0 ) { // Dirihlet
    item = new DirihletTreeItem( ui->nameLineEdit->text(), ui->scalarSpinBox->value(), color );
  }
  else { // Neumann
    item = new NeumannTreeItem( ui->nameLineEdit->text(), ui->xSpinBox->value(), ui->ySpinBox->value(), ui->zSpinBox->value(), color );
  }

  return item;
}

/*!
 * \brief CreateBoundaryConditionDialog::setTreeItem
 * \param item
 */
void CreateBoundaryConditionDialog::setTreeItem(AbstractTreeItem *item)
{
  color = item->getColor();
  ui->nameLineEdit->setText( item->getName() );

  if ( item->getIntemType() == BOUNDARY_CONDITION_TYPE::DIRICHLET_BOUNDARY_CONDITION ) {
    DirihletTreeItem* dirihletItem = static_cast< DirihletTreeItem* >( item );
    ui->scalarSpinBox->setValue( dirihletItem->getBcValue() );
    hideNeumannUI();
  }
  else { // NEUMANN
    NeumannTreeItem* neumannItem = static_cast< NeumannTreeItem* >( item );
    ui->xSpinBox->setValue( neumannItem->getBcXvalue() );
    ui->ySpinBox->setValue( neumannItem->getBcYvalue() );
    ui->zSpinBox->setValue( neumannItem->getBcZvalue() );
    ui->scalarSpinBox->setValue( 0.0 );
    showNeumannUI();
  }
  applyPickedColor();
}

/*!
 * \brief CreateBoundaryConditionDialog::changeEvent
 * \param e
 */
void CreateBoundaryConditionDialog::changeEvent(QEvent *e)
{
  QDialog::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}

/*!
 * \brief CreateBoundaryConditionDialog::showNeumannUI
 */
void CreateBoundaryConditionDialog::showNeumannUI()
{
  ui->xLabel->show();
  ui->yLabel->show();
  ui->zLabel->show();

  ui->xSpinBox->show();
  ui->ySpinBox->show();
  ui->zSpinBox->show();
}

/*!
 * \brief CreateBoundaryConditionDialog::hideNeumannUI
 */
void CreateBoundaryConditionDialog::hideNeumannUI()
{
  ui->xLabel->hide();
  ui->yLabel->hide();
  ui->zLabel->hide();

  ui->xSpinBox->hide();
  ui->ySpinBox->hide();
  ui->zSpinBox->hide();
}

/*!
 * \brief CreateBoundaryConditionDialog::on_typeComboBox_activated
 * \param index
 */
void CreateBoundaryConditionDialog::on_typeComboBox_activated(int index)
{
  if ( index == 0 ) { // Dirihlet
    ui->nameLineEdit->setText( "Dirihlet" );

    hideNeumannUI();
  }
  else { // Neumann
    ui->nameLineEdit->setText( "Neumann" );

    showNeumannUI();
  }
}

/*!
 * \brief CreateBoundaryConditionDialog::on_invertPushButton_clicked
 */
void CreateBoundaryConditionDialog::on_invertPushButton_clicked()
{
  ui->xSpinBox->setValue( -ui->xSpinBox->value() );
  ui->ySpinBox->setValue( -ui->ySpinBox->value() );
  ui->zSpinBox->setValue( -ui->zSpinBox->value() );
  ui->scalarSpinBox->setValue( -ui->scalarSpinBox->value() );
}

/*!
 * \brief CreateBoundaryConditionDialog::applyPickedColor
 */
void CreateBoundaryConditionDialog::applyPickedColor()
{
  QPalette palette = ui->colorPushButton->palette();
  palette.setColor( ui->colorPushButton->backgroundRole(), color );
  ui->colorPushButton->setPalette( palette );
}

/*!
 * \brief CreateBoundaryConditionDialog::on_colorPushButton_clicked
 */
void CreateBoundaryConditionDialog::on_colorPushButton_clicked()
{
  color = QColorDialog::getColor( color, this );
  applyPickedColor();
}

/*!
 * \brief CreateBoundaryConditionDialog::getColor
 * \return
 */
QColor CreateBoundaryConditionDialog::getColor() const
{
  return color;
}

/*!
 * \brief CreateBoundaryConditionDialog::setColor
 * \param value
 */
void CreateBoundaryConditionDialog::setColor(const QColor &value)
{
  color = value;
}



#include <QApplication>
#include <QDebug>

bool CreateBoundaryConditionDialogTest(int argc, char *argv[])
{
  QApplication a( argc, argv );

  CreateBoundaryConditionDialog dialog;
  if ( dialog.exec() == QDialog::Accepted ) {
    AbstractTreeItem* treeItem = dialog.treeItem();
    qDebug() << "getName: " << treeItem->getName();
  }

  return true;
}
