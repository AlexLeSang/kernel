#include "NeumannBoundaryCondition.hpp"

#include <QDebug>

#include <ApplicableNodes.hpp>

/*!
 * \brief NeumannBoundaryCondition::NeumannBoundaryCondition
 */
NeumannBoundaryCondition::NeumannBoundaryCondition() : AbstractBoundaryCondition( BOUNDARY_CONDITION_TYPE::NEUMANN_BOUNDARY_CONDITION )
{
  condition = bgeot::small_vector< double >( 0.0, 0.0, 0.0 );
}

/*!
 * \brief NeumannBoundaryCondition::operator ==
 * \param rhs
 * \return
 */
bool NeumannBoundaryCondition::operator==(const AbstractBoundaryCondition &rhs) const
{
  // assert( conditionType == rhs.getBoundaryConditionType() );
  return AbstractBoundaryCondition::operator ==( rhs ) && ( condition == static_cast< const NeumannBoundaryCondition* >( &rhs )->getCondition() );
}

/*!
 * \brief NeumannBoundaryCondition::operator !=
 * \param rhs
 * \return
 */
bool NeumannBoundaryCondition::operator!=(const AbstractBoundaryCondition &rhs) const
{
  return ! operator ==( rhs );
}

/*!
 * \brief NeumannBoundaryCondition::appyBoundaryCondition
 * \return
 */
bgeot::small_vector<double> NeumannBoundaryCondition::appyBoundaryCondition(const bgeot::base_node &) const
{
  return condition;
}

/*!
 * \brief NeumannBoundaryCondition::getCondition
 * \return
 */
bgeot::small_vector<double> NeumannBoundaryCondition::getCondition() const
{
  return condition;
}

/*!
 * \brief NeumannBoundaryCondition::getCondition
 * \param pos
 * \return
 */
double NeumannBoundaryCondition::getCondition(int pos) const
{
  assert( pos >= 0 && pos < condition.size() );
  return condition[ pos ];
}

/*!
 * \brief NeumannBoundaryCondition::setCondition
 * \param value
 */
void NeumannBoundaryCondition::setCondition(const bgeot::small_vector<double> &value)
{
  condition = value;
}

/*!
 * \brief NeumannBoundaryCondition::setCondition
 * \param val
 * \param pos
 */
void NeumannBoundaryCondition::setCondition(double val, int pos)
{
  assert( pos >= 0 && pos < condition.size() );
  condition[ pos ] = val;
}

/*!
 * \brief operator <<
 * \param ds
 * \param bc
 * \return
 */
QDataStream &operator<<(QDataStream &ds, const NeumannBoundaryCondition &bc)
{
  ds << QString( bc.getName().c_str() );
  auto condition = bc.getCondition();
  assert( condition.size() == 3 );
  ds << condition[ 0 ];
  ds << condition[ 1 ];
  ds << condition[ 2 ];
  return ds;
}

/*!
 * \brief operator >>
 * \param ds
 * \param bc
 * \return
 */
QDataStream &operator>>(QDataStream &ds, NeumannBoundaryCondition &bc)
{
  QString name;
  ds >> name;
  bc.setName( name.toStdString() );
  bgeot::small_vector<double> condition( 0.0, 0.0, 0.0 );
  ds >> condition[ 0 ];
  ds >> condition[ 1 ];
  ds >> condition[ 2 ];
  bc.setCondition( condition );
  return ds;
}






bool test::NeumannBoundaryConditionSerializationTest(int argc, char *argv[])
{
  std::cout << "NeumannBoundaryConditionSerializationTest" << std::endl;

  NeumannBoundaryCondition neumann;
  neumann.setName( "Neumann" );
  neumann.setCondition( 10, 0 );
  neumann.setCondition( 20, 1 );
  neumann.setCondition( 34, 2 );

  QByteArray buffer;
  { // Serialize
    QDataStream ostream( &buffer, QIODevice::WriteOnly );
    ostream << neumann;
  }

  NeumannBoundaryCondition newNeumann;
  { // Deserialize
    QDataStream istream( &buffer, QIODevice::ReadOnly );
    istream >> newNeumann;
  }

  assert( neumann == newNeumann );
  assert( neumann.getBoundaryConditionType() == newNeumann.getBoundaryConditionType() );
  assert( neumann.getName() == newNeumann.getName() );
  assert( neumann.getCondition( 0 ) == newNeumann.getCondition( 0 ) );
  assert( neumann.getCondition( 1 ) == newNeumann.getCondition( 1 ) );
  assert( neumann.getCondition( 2 ) == newNeumann.getCondition( 2 ) );


  std::cout << "NeumannBoundaryConditionSerializationTest end" << std::endl;
  return true;
}

