#ifndef BOUNDARYCONDITIONTREEMODEL_HPP
#define BOUNDARYCONDITIONTREEMODEL_HPP

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>

#include <RootTreeItem.hpp>
#include <AbstractTreeItem.hpp>

/*!
 * \brief The TreeModel class
 */
class TreeModel : public QAbstractItemModel
{
  Q_OBJECT

public:
  TreeModel(QObject *parent = 0);
  ~TreeModel();

  Qt::ItemFlags flags(const QModelIndex &index) const;
  bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

  QVariant data(const QModelIndex &index, int role) const;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
  QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
  QModelIndex parent(const QModelIndex &index) const;
  int rowCount(const QModelIndex &parent = QModelIndex()) const;
  int columnCount(const QModelIndex &parent = QModelIndex()) const;

  void appendItem(AbstractTreeItem* item);
  void removeItem(int row);

  AbstractTreeItem* getTreeItem(int row) const;
  QList< AbstractTreeItem* > getAllTreeItem() const;

private:
  RootTreeItem *rootItem;
};


bool TreeModelTest(int argc, char* argv[]);

#endif // BOUNDARYCONDITIONTREEMODEL_HPP
