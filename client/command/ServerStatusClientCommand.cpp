#include "ServerStatusClientCommand.hpp"

#include <QtNetwork/QTcpSocket>

#include "KernelClient.hpp"

#include <../server/command/GetServerStatusCommand.hpp>

#include <cassert>

namespace kernel {
  namespace client {
    namespace command {

      bool ServerStatusClientCommand::operator ()()
      {
        assert( serverStatusPtr != nullptr );

        QTcpSocket socket;
        socket.connectToHost( description.address, description.port );

        if ( ! socket.waitForConnected( Timeout ) ) {
          qWarning() << "ServerStatusClientCommand: " << socket.errorString();
          // TODO log it
          return false;
        }

        // Write to server
        {
          QByteArray block;
          QDataStream out( &block, QIODevice::WriteOnly );
          out.setVersion( QDataStream::Qt_4_8 );
          out << (quint32)0;
          out << (quint32)server::ServerCommands::GET_SERVER_STATUS;
          out.device()->seek(0);
          out << (quint32)( block.size() - sizeof(quint32) );

          socket.write( block );
          if ( ! socket.waitForBytesWritten( Timeout ) ) {
            qWarning() << "ServerStatusClientCommand: " << socket.errorString();
            // TODO log it
            return false;
          }
        }


        while ( socket.bytesAvailable() < (qint64)sizeof(quint32) ) {
          if ( ! socket.waitForReadyRead( Timeout ) ) {
            qWarning() << "ServerStatusClientCommand: " << socket.errorString();
            // TODO log it
            return false;
          }
        }

        // Read server state
        {
          quint32 blockSize;
          QDataStream in( &socket );
          in.setVersion( QDataStream::Qt_4_8 );
          in >> blockSize;

          while ( socket.bytesAvailable() < blockSize ) {
            if ( ! socket.waitForReadyRead( Timeout ) ) {
              qWarning() << "ServerStatusClientCommand: " << socket.errorString();
              // TODO log it
              return false;
            }
          }

          in >> *serverStatusPtr;
        }

        return true;
      }

      /*!
       * \brief ServerStatusClientCommand::commandName
       * \return
       */
      server::ServerCommands ServerStatusClientCommand::commandName() const
      {
        return server::ServerCommands::GET_SERVER_STATUS;
      }

      /*!
       * \brief ServerStatusClientCommand::setServerStatusPtr
       * \param value
       */
      void ServerStatusClientCommand::setServerStatusPtr(server::command::ServerStatus *value)
      {
        serverStatusPtr = value;
      }


    }
  }
}
