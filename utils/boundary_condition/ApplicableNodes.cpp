#include "ApplicableNodes.hpp"

/*!
 * \brief ApplicableNodes::ApplicableNodes
 * \param grid
 */
ApplicableNodes::ApplicableNodes(vtkSmartPointer<vtkUnstructuredGrid> grid)
{
  if ( grid && grid->GetNumberOfPoints() ) {
    const auto numberOfPoints = grid->GetNumberOfPoints();
    for ( auto i = 0; i < numberOfPoints; ++ i ) {
      double p[ 3 ];
      grid->GetPoint( i, p );
      bgeot::base_node node( p[ 0 ], p[ 1 ], p[ 2 ] );
      applicableNodes.insert( node );
    }
  }
}

/*!
 * \brief ApplicableNodes::applicableToFace
 * \param facePoints
 * \return
 */
bool ApplicableNodes::applicableToFace(const std::vector<bgeot::base_node> &facePoints) const
{
  bool applicable = true;
  for( auto it = facePoints.begin(); it != facePoints.end(); ++ it ) {
    const bgeot::base_node& node = *it;
    assert( node.size() == 3 );
    if ( applicableNodes.find( node ) == applicableNodes.end() ) {
      applicable = false;
      break;
    }
  }
  return applicable;
}

/*!
 * \brief test::ApplicableNodesContainerTest
 * \return
 */
bool test::ApplicableNodesContainerTest()
{
  // TODO
  std::unordered_set< bgeot::base_node, BaseNodeHash > applicableNodes;

  const int MAX = 1000000;

  for ( auto i = 0; i < MAX; i ++ ) {
    bgeot::base_node node( i, i * M_PI_2, i * M_PI_4 );
    auto res = applicableNodes.insert( node );
    assert( res.second );
  }
  std::cout << "Insertion successfull..." << std::endl;

  for ( auto i = 0; i < MAX; i ++ ) {
    bgeot::base_node node( i, i * M_PI_2, i * M_PI_4 );
    auto res = applicableNodes.insert( node );
    assert( !res.second );
  }
  std::cout << "Test insertion successfull..." << std::endl;

  return true;
}
