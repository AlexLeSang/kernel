#include "KernelWorkgroupMaintenancer.hpp"

#include <QDebug>

#include <QTime>

#include <mongo/client/dbclient.h>

#include <../utils/ThreadSaveLogger.hpp>

#include <../client/ServerDescription.hpp>
#include <../client/KernelClient.hpp>
#include <../client/command/PingPongClientCommand.hpp>

//#include <../utils/viennacl/ViennaCLContextTools.hpp>

#include <KernelServer.hpp>



namespace kernel {
  namespace server {

    /*!
     * \brief KernelWorkgroupMaintenancer::KernelWorkgroupMaintenancer
     * \param parent
     * \param serverAddress
     * \param serverPort
     * \param mongoAddress
     * \param mongoPort
     * \param workgroupTag
     */
    KernelWorkgroupMaintenancer::KernelWorkgroupMaintenancer(QObject *parent, const QHostAddress& serverAddress, const quint32 serverPort, const QHostAddress& mongoAddress, const quint32 mongoPort, QString workgroupTag) : QObject( parent ), serverAddress_( serverAddress ), serverPort_( serverPort ), mongodbServerAddress_( mongoAddress ), mongodbPortNumber_( mongoPort ), workgroupTag_( workgroupTag )
    {

      // Init maintenance
      // Init mongodb connection
      mongodbMaintenancerPtr_ = decltype(mongodbMaintenancerPtr_)( new functions::node::MongoDBMaintenancer( this, mongodbServerAddress_, mongodbPortNumber_ ) );
      const bool maintenanceResult = mongodbMaintenancerPtr_->mongodbMaintenance();
      if ( !maintenanceResult ) {
        const auto qErrorString = QString( "Server MongoDB maintenance result failed" );
        utils::ThreadSaveLogger::addLineStatic( qErrorString );
      }

      interruptFlag.store( false );
      mongodbParametersChanged = false;
    }

    /*!
     * \brief KernelWorkgroupMaintenancer::getAndSetWorkgroupInfo
     * \return
     */
    bool KernelWorkgroupMaintenancer::getAndSetWorkgroupInfo()
    {
      utils::ThreadSaveLogger::addLineStatic( QString( "KernelWorkgroupMaintenancer::getAndSetWorkgroupInfo" ) );

      if ( interruptFlag.load() ) {
        utils::ThreadSaveLogger::addLineStatic( QString( "KernelWorkgroupMaintenancer interrupted" ) );
        return false;
      }

      if ( mongodbParametersChanged ) {
        // Some parameters were changed, perhaps we need to make maintenance
        const bool maintenanceResult = mongodbMaintenancerPtr_->mongodbMaintenance();
        if ( !maintenanceResult ) {
          const auto qErrorString = QString( "KernelWorkgroupMaintenancer::getAndSetWorkgroupInfo MongoDB maintenance result failed" );
          utils::ThreadSaveLogger::addLineStatic( qErrorString );
          return false;
        }
        mongodbParametersChanged = false;
      }

      if ( interruptFlag.load() ) {
        utils::ThreadSaveLogger::addLineStatic( QString( "KernelWorkgroupMaintenancer interrupted" ) );
        return false;
      }

      std::shared_ptr< mongo::DBClientReplicaSet > mongoConnection = mongodbMaintenancerPtr_->createConnectionToRS();
      if ( !mongoConnection ) {
        const auto qErrorString = QString( "KernelWorkgroupMaintenancer::getAndSetWorkgroupInfo MongoDB connection not established" );
        utils::ThreadSaveLogger::addLineStatic( qErrorString );
        return false;
      }

      if ( interruptFlag.load() ) {
        utils::ThreadSaveLogger::addLineStatic( QString( "KernelWorkgroupMaintenancer interrupted" ) );
        return false;
      }

      // Query for object
      {

        static const auto queryObj = BSON( "address" << serverAddress_.toString().toStdString() <<
                                           "port" << serverPort_ << "workgroupTag" <<
                                           workgroupTag_.toStdString() );
        static const auto query = mongo::Query( queryObj );

        const auto count = mongoConnection->count( database_collection, queryObj );
        {
          const std::string errStr = mongoConnection->getLastError();
          if ( ! errStr.empty() ) {
            const auto qErrorString = "MongoDB node description insert error: " + QString( errStr.c_str() );
            qWarning() << qErrorString;
            utils::ThreadSaveLogger::addLineStatic( qErrorString );
            return false;
          }
        }

        if ( count == 1 ) {
          mongoConnection->update( database_collection, query, BSON( "$set" << BSON( "lastHeartbeat" << mongo::DATENOW ) ) );
          {
            const std::string errStr = mongoConnection->getLastError();
            if ( ! errStr.empty() ) {
              const auto qErrorString = "MongoDB node description insert error: " + QString( errStr.c_str() );
              qWarning() << qErrorString;
              utils::ThreadSaveLogger::addLineStatic( qErrorString );
              return false;
            }
          }
        }
        else {
          const mongo::BSONObj nodeDescription = createNodeDescription();

          mongoConnection->insert( database_collection, nodeDescription);
          {
            const std::string errStr = mongoConnection->getLastError();
            if ( ! errStr.empty() ) {
              const auto qErrorString = "MongoDB node description insert error: " + QString( errStr.c_str() );
              qWarning() << qErrorString;
              utils::ThreadSaveLogger::addLineStatic( qErrorString );
              return false;
            }
          }
        }

      }

      if ( interruptFlag.load() ) {
        utils::ThreadSaveLogger::addLineStatic( QString( "KernelWorkgroupMaintenancer interrupted" ) );
        return false;
      }

      {
        const unsigned int workgroupSize = mongoConnection->count( database_collection, BSON( "workgroupTag" << workgroupTag_.toStdString() ) );
        // qDebug() << "workgroupSize: " << workgroupSize;

        if ( interruptFlag.load() ) {
          utils::ThreadSaveLogger::addLineStatic( QString( "KernelWorkgroupMaintenancer interrupted" ) );
          return false;
        }

        QVector< client::ServerDescription > workgroup;
        workgroup.reserve( workgroupSize - 1 );
        //std::auto_ptr< mongo::DBClientCursor > cursor = mongoConnection->query( database_collection, BSON( "workgroutTag" << workgroupTag_.toStdString() ) );
        QTime mongoQueryTimer;
        {
          mongoQueryTimer.start();
          std::auto_ptr< mongo::DBClientCursor > cursor = mongoConnection->query( database_collection,
                                                                                  BSON( "workgroupTag" << workgroupTag_.toStdString() ),
                                                                                  0, 0, 0, mongo::QueryOption_SlaveOk | mongo::QueryOption_Exhaust );
          while ( cursor->more() ) {
            if ( interruptFlag.load() ) {
              utils::ThreadSaveLogger::addLineStatic( QString( "KernelWorkgroupMaintenancer interrupted" ) );
              return false;
            }

            const mongo::BSONObj obj = cursor->next();
            client::ServerDescription newSd;
            newSd.address =  QHostAddress( QString( obj.getStringField( "address" ) ) );
            newSd.port = obj.getIntField( "port" );
            if ( (newSd.address != serverAddress_) || (newSd.port != serverPort_) ) {
              workgroup.push_back( newSd );
            }
          }
        }
        const quint32 mongoQueryTime = mongoQueryTimer.elapsed();

        if ( interruptFlag.load() ) {
          utils::ThreadSaveLogger::addLineStatic( QString( "KernelWorkgroupMaintenancer interrupted" ) );
          return false;
        }

        // Ping the workgroup
        //*
        {
          QVector< QFuture< bool > > futuresVector;
          futuresVector.reserve( workgroup.size() );
          QVector< std::shared_ptr< client::command::PingPongResult > > pingPongResultVector;
          pingPongResultVector.reserve( workgroup.size() );

          for( auto i = 0; i < workgroup.size(); ++ i ) {

            if ( interruptFlag.load() ) {
              utils::ThreadSaveLogger::addLineStatic( QString( "KernelWorkgroupMaintenancer interrupted" ) );
              return false;
            }

            const client::ServerDescription& sd = workgroup.at( i );

            client::KernelClient* kernelClient = client::KernelClient::getInstance();
            kernelClient->setServerDescription( sd );

            std::shared_ptr< client::command::PingPongResult > pingPongResult( new client::command::PingPongResult);
            QFuture< bool > f = kernelClient->pingHost( pingPongResult.get(), 10, 1024 ); // TODO adjust it

            pingPongResultVector.push_back( pingPongResult );
            futuresVector.push_back( f );
          }

          QList< std::pair< client::ServerDescription, const quint32 > > accessibleHosts;

          for ( auto i = 0; i < workgroup.size(); ++ i ) {

            if ( interruptFlag.load() ) {
              utils::ThreadSaveLogger::addLineStatic( QString( "KernelWorkgroupMaintenancer interrupted" ) );
              return false;
            }

            client::ServerDescription currentSd = workgroup[ i ];
            QFuture< bool > f = futuresVector[ i ];
            const bool result = f.result();
            if ( !result ) {
              const auto qErrorString = "Ping host " + currentSd.address.toString() + ":" + QString::number( currentSd.port ) + " was unsuccessful";
              utils::ThreadSaveLogger::addLineStatic( qErrorString );
            }
            else {
              // const auto qErrorString = "Ping host " + currentSd.address.toString() + ":" + QString::number( currentSd.port ) + " was successful";
              // utils::ThreadSaveLogger::addLineStatic( qErrorString );

              std::shared_ptr< client::command::PingPongResult > pingPongResul( pingPongResultVector[ i ] );
              const quint32 avg = pingPongResul->getAvg();
              accessibleHosts.push_back( std::make_pair( currentSd, avg ) );
            }
          }

          if ( interruptFlag.load() ) {
            utils::ThreadSaveLogger::addLineStatic( QString( "KernelWorkgroupMaintenancer interrupted" ) );
            return false;
          }

          // Debug output
          {
            auto workgroupString = "Accessible workgroup nodes from " + serverAddress_.toString() + ":" + QString::number( serverPort_ ) + " (" + QString::number( accessibleHosts.size() ) + "): ";
            std::for_each( accessibleHosts.begin(), accessibleHosts.end(), [&](const std::pair< client::ServerDescription, const quint32 >& v ) {
              workgroupString += '(' + std::get< 0 >( v ).address.toString() + ":" + QString::number( std::get< 0 >( v ).port ) + ") ";
            } );
            utils::ThreadSaveLogger::addLineStatic( workgroupString );
          }


          if ( interruptFlag.load() ) {
            utils::ThreadSaveLogger::addLineStatic( QString( "KernelWorkgroupMaintenancer interrupted" ) );
            return false;
          }

          // Create graph
          {
            // TODO add MongoDB edge to the graph
            const QString selfAddressStr = serverAddress_.toString() + ":" + QString::number( serverPort_ );
            {
              mongo::BSONObjBuilder objBuilder;
              objBuilder.append( "workgroupTag", workgroupTag_.toStdString() );
              objBuilder.append( "avgPing", mongoQueryTime );

              mongo::BSONObj objFrom;
              mongo::BSONObj objTo;

              objFrom = BSON( "edgeFrom" << selfAddressStr.toStdString() );
              objTo = BSON( "edgeTo" << ( "DB:" + mongodbServerAddress_.toString() ).toStdString() );

              objBuilder.appendElements( objFrom );
              objBuilder.appendElements( objTo );
              objBuilder.appendElements( BSON("posted" << mongo::DATENOW) );

              const mongo::BSONObj edgeObj = objBuilder.obj();

              mongo::BSONObjBuilder queryBuilder;
              queryBuilder.append( "workgroupTag", workgroupTag_.toStdString() );
              queryBuilder.append( "avgPing", mongoQueryTime );
              queryBuilder.appendElements( objFrom );
              queryBuilder.appendElements( objTo );

              mongoConnection->update( workgroup_edges_collection,
                                       mongo::Query( queryBuilder.obj() ),
                                       edgeObj, 1 );
            }

            std::for_each( accessibleHosts.begin(), accessibleHosts.end(), [&](const std::pair< client::ServerDescription, const quint32 >& v ) {

              const auto avgPing = std::get< 1 >( v ) != 0 ? std::get< 1 >( v ) : 1;

              const QString addressStr = std::get< 0 >( v ).address.toString() + ":" + QString::number( std::get< 0 >( v ).port );

              mongo::BSONObjBuilder objBuilder;
              objBuilder.append( "workgroupTag", workgroupTag_.toStdString() );
              objBuilder.append( "avgPing", avgPing );

              mongo::BSONObj objFrom;
              mongo::BSONObj objTo;

              if ( selfAddressStr < addressStr ) {
                objFrom = BSON( "edgeFrom" << selfAddressStr.toStdString() );
                objTo = BSON( "edgeTo" << addressStr.toStdString() );
              }
              else {
                objFrom = BSON( "edgeFrom" << addressStr.toStdString() );
                objTo = BSON( "edgeTo" << selfAddressStr.toStdString() );
              }

              objBuilder.appendElements( objFrom );
              objBuilder.appendElements( objTo );
              objBuilder.appendElements( BSON("posted" << mongo::DATENOW) );

              const mongo::BSONObj edgeObj = objBuilder.obj();

              // std::cout << "edgeObj: " << edgeObj << std::endl;

              {
                mongo::BSONObjBuilder queryBuilder;
                queryBuilder.append( "workgroupTag", workgroupTag_.toStdString() );
                queryBuilder.append( "avgPing", avgPing );
                queryBuilder.appendElements( objFrom );
                queryBuilder.appendElements( objTo );

                mongoConnection->update( workgroup_edges_collection,
                                         mongo::Query( queryBuilder.obj() ),
                                         edgeObj, 1 );

              }
            } );

          }



        }
        //*/

      }

      return true;
    }

    /*!
     * \brief KernelWorkgroupMaintenancer::createNodeDescription
     * \return
     */
    mongo::BSONObj KernelWorkgroupMaintenancer::createNodeDescription()
    {
      mongo::BSONObjBuilder nodeDescriptionBuilder;

      // Create static part
      static mongo::BSONObj staticPart;
      static bool initialized = false;
      if ( !initialized ) {
        mongo::BSONObjBuilder staticBuilder;

        staticBuilder.append( "address", serverAddress_.toString().toStdString() );
        staticBuilder.append( "port", serverPort_ );
        staticBuilder.append( "workgroupTag", workgroupTag_.toStdString() );

        // Append node description array
        /* BUG discover why it takes 50% of core; Answer: probably it's connected to OpenCL driver implementation, which are waited for new tasks.
        mongo::BSONArray arr;
        mongo::BSONArrayBuilder arrBuilder;

        using namespace functions::node;

        if ( opencl::create_all_cpu_devices_context() ) {
          viennacl::ocl::switch_context( opencl::ALL_CPU_DEVICES_CONTEXT );

          mongo::BSONObjBuilder cpuBuilder;
          const auto documents = opencl::print_devices_for_current_context_to_bson();
          {
            mongo::BSONArrayBuilder cpuArrBuilder;
            for ( auto it = documents.begin(); it != documents.end(); ++ it ) {
              cpuArrBuilder.append( *it );
            }
            cpuBuilder.appendArray( "acllCPUContext", cpuArrBuilder.arr() );
          }
          arrBuilder.append( cpuBuilder.obj() );
        }

        if ( opencl::create_all_gpu_devices_context() ) {
          viennacl::ocl::switch_context( opencl::ALL_GPU_DEVICES_CONTEXT );

          mongo::BSONObjBuilder gpuBuilder;
          const auto documents = opencl::print_devices_for_current_context_to_bson();
          {
            mongo::BSONArrayBuilder gpuArrBuilder;
            for ( auto it = documents.begin(); it != documents.end(); ++ it ) {
              gpuArrBuilder.append( *it );
            }
            gpuBuilder.appendArray( "allGPUContext", gpuArrBuilder.arr() );
          }
          arrBuilder.append( gpuBuilder.obj() );
        }

        if ( opencl::create_all_devices_context() ) {
          viennacl::ocl::switch_context( opencl::ALL_DEVICES_CONTEXT );

          mongo::BSONObjBuilder allDevicesBuilder;
          const auto documents = opencl::print_devices_for_current_context_to_bson();
          {
            mongo::BSONArrayBuilder allDevicesArrBuilder;
            for ( auto it = documents.begin(); it != documents.end(); ++ it ) {
              allDevicesArrBuilder.append( *it );
            }
            allDevicesBuilder.appendArray( "allDevicesContext", allDevicesArrBuilder.arr() );
          }
          arrBuilder.append( allDevicesBuilder.obj() );
        }

        arr = arrBuilder.arr();
        staticBuilder.appendArray( "openCL", arr );
        */

        staticPart = staticBuilder.obj();
        initialized = true;
      }

      // Append static part
      nodeDescriptionBuilder.appendElements( staticPart );
      nodeDescriptionBuilder.appendElements( BSON( "lastHeartbeat" << mongo::DATENOW ) );

      mongo::BSONObj nodeDescription = nodeDescriptionBuilder.obj();
      return nodeDescription;
    }

    /*!
     * \brief KernelWorkgroupMaintenancer::mongodbServerAddress
     * \return
     */
    QHostAddress KernelWorkgroupMaintenancer::mongodbServerAddress() const
    {
      return mongodbServerAddress_;
    }

    /*!
     * \brief KernelWorkgroupMaintenancer::setMongodbServerAddress
     * \param mongodbServerAddress
     */
    void KernelWorkgroupMaintenancer::setMongodbServerAddress(const QHostAddress &mongodbServerAddress)
    {
      mongodbParametersChanged = true;
      mongodbServerAddress_ = mongodbServerAddress;
    }

    /*!
     * \brief KernelWorkgroupMaintenancer::mongodbPortNumber
     * \return
     */
    quint32 KernelWorkgroupMaintenancer::mongodbPortNumber() const
    {
      return mongodbPortNumber_;
    }

    /*!
     * \brief KernelWorkgroupMaintenancer::setMongodbPortNumber
     * \param mongodbPortNumber
     */
    void KernelWorkgroupMaintenancer::setMongodbPortNumber(const quint32 &mongodbPortNumber)
    {
      mongodbParametersChanged = true;
      mongodbPortNumber_ = mongodbPortNumber;
    }

    /*!
     * \brief KernelWorkgroupMaintenancer::workgroupTag
     * \return
     */
    QString KernelWorkgroupMaintenancer::workgroupTag() const
    {
      return workgroupTag_;
    }

    /*!
     * \brief KernelWorkgroupMaintenancer::setWorkgroupTag
     * \param workgroupTag
     */
    void KernelWorkgroupMaintenancer::setWorkgroupTag(const QString &workgroupTag)
    {
      mongodbParametersChanged = true;
      workgroupTag_ = workgroupTag;
    }

    /*!
     * \brief KernelWorkgroupMaintenancer::run
     */
    void KernelWorkgroupMaintenancer::run()
    {
      while ( true ) {
        if ( interruptFlag.load() ) {
          utils::ThreadSaveLogger::addLineStatic( QString( "KernelWorkgroupMaintenancer::run interrupted" ) );
          return;
        }

        utils::ThreadSaveLogger::addLineStatic( QString( "KernelWorkgroupMaintenancer::run: timeout: " ) + QString::number( update_interval ) + " sec" );
        /*const bool res = */getAndSetWorkgroupInfo(); // WARNING unprocessed result

        utils::ThreadSleep::msleep( update_interval * 1000 );

      }
    }

    /*!
     * \brief KernelWorkgroupMaintenancer::interrupt
     */
    void KernelWorkgroupMaintenancer::interrupt()
    {
      interruptFlag.store( true );
    }

  }
}
