#ifndef HEATFEMSOLVER_HPP
#define HEATFEMSOLVER_HPP

#include <getfem/getfem_model_solvers.h>
#include <getfem/getfem_export.h>
#include <gmm/gmm.h>

#include <boost/timer/timer.hpp>

#include <vtkUnstructuredGrid.h>
#include <vtkSmartPointer.h>

#include <unordered_map>

#include <boundary_condition/DirihletBoundaryCondition.hpp>
#include <boundary_condition/NeumannBoundaryCondition.hpp>
#include <boundary_condition/ApplicableNodes.hpp>
#include <boundary_condition/AppliedBoundaryCondition.hpp>

#include <vtkUnstructuredGridReader.h>
#include <vtkPointData.h>
#include <vtkDataSetMapper.h>
#include <vtkScalarBarActor.h>
#include <vtkLookupTable.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkProperty.h>
#include <vtkIdList.h>
#include <vtkDoubleArray.h>
#include <vtkCellData.h>

#include <limits>
#include <unordered_map>

#include <functional>
#include <atomic>

#include <QObject>

#include <slae_solvers/AbstractSLAESolver.hpp>
#include <slae_solvers/StandardSLAESolver.hpp>

typedef std::size_t size_type;

namespace kernel {
  namespace utils {
    namespace fem_solvers {

      /*!
       * \brief The HeatFemSolver class
       */
      class HeatFemSolver : public QObject
      {
        Q_OBJECT

      public slots:
        void stop();

      signals:
        void timeStepProcessed(double);
        void interrupted();
        void finished();


      public:
        HeatFemSolver() :
          integrationMethod( mesh ),
          meshFemU( mesh ),
          meshFemRhs( mesh ),
          pf_u(  getfem::fem_descriptor( "FEM_PK(3,1)" ) ),
          ppi( getfem::int_method_descriptor( "IM_TETRAHEDRON(2)" ) ),
          slaeSolver( new StandardSLAESolver() ),
          dimension( 3 ),
          residual( 1e-10 ),
          totalSolutionTime( 1 ),
          timeStep( totalSolutionTime / 50 ),
          theta( 0.5 ),
          thermalDiffusivity( 1.0 ),
          solutionTime( 0.0 ),
          initialTemperature( 0.0 )
        {}

        void setInputGrid(const vtkSmartPointer< vtkUnstructuredGrid >& value);
        vtkSmartPointer< vtkUnstructuredGrid > getInputGrid() const;

        void setResidual(const double& value);
        double getResidual() const;

        std::shared_ptr<AbstractSLAESolver> getSlaeSolver() const;

        void setSlaeSolver(const std::shared_ptr<AbstractSLAESolver> &value);

        void setTotalSolutionTime(const double& value);
        double getTotalSolutionTime() const;

        void setTimeStep(const double& value);
        double getTimeStep() const;

        void setTheta(const double& value);
        double getTheta() const;

        void setThermalDiffusivity(const double& value);
        double getThermalDiffusivity() const;

        void setSolutionTime(const double& value);
        double getSolutionTime() const;

        void setInitialTemperature(const double& value);
        double getInitialTemperature() const;

        void addBoundaryCondition(const std::list<AppliedBoundaryCondition>& applyedConditions);
        void addBoundaryCondition(const AppliedBoundaryCondition& applyedCondition);
        std::unordered_map< double, vtkSmartPointer< vtkUnstructuredGrid > > getOutputGrids();

        void init();
        void clear();
        bool solve();

      private:
        void actualizeIndexMapping();
        const long findVtkPointIndex(const bgeot::base_node& node);

        void setResultIntoGrid(vtkSmartPointer< vtkUnstructuredGrid >& newGrid, const std::vector< double >& results);
        bool pointEqualsNode(const double p[], const bgeot::base_node& node);

        void convertGrid();

        void clearMeshDataStructures();

        std::vector< size_type > getNodeIndiciesInMesh();

        void addSimplexesToMesh(const std::vector< size_type >& baseNodeIndiciesInMesh);

        void setFEM();

        void discoverOutherFacesOfMesh();

        void assignBCToOutherFace(getfem::mr_visitor& face);

        bgeot::base_node getFaceNormal(getfem::mr_visitor& face);
        std::vector< bgeot::base_node > getFacePoints(getfem::mr_visitor face);
        void fillNodeConditionMaps(AbstractBoundaryCondition *condition, const std::vector< bgeot::base_node >& facePoints, const bgeot::base_node& normal);
        bgeot::small_vector< double > directConditionTowardNormal(const bgeot::small_vector< double >& condition, const bgeot::base_node& normal);

        void addMainUnknown();
        void appLaplacianTerm();

        void assignNeumannCondtion();
        void computeNeumannData();
        bgeot::base_small_vector applyNeumann(const bgeot::base_node& node);
        void applyMidpointScheme();

        void assignDirihletCondition();
        void computeDirihletData();
        double applyDirihlet(const bgeot::base_node& node);

        void addTransientPart();
        void setInitialTemperature();
        void storeResult();
        void updateSolutionTime(double t);
        void updateNeumannData();
        void updateDirihletData();

      private:
        typedef gmm::rsvector< double > sparse_vector_type;
        typedef gmm::row_matrix< sparse_vector_type > sparse_matrix_type;
        typedef gmm::col_matrix< sparse_vector_type > col_sparse_matrix_type;

        std::atomic_bool stopFlag;

        getfem::model model;
        dal::bit_vector transientBricks;
        getfem::mesh mesh;
        getfem::mesh_im integrationMethod;
        getfem::mesh_fem meshFemU;
        getfem::mesh_fem meshFemRhs;
        getfem::pfem pf_u;
        getfem::pintegration_method ppi;

        std::shared_ptr<AbstractSLAESolver> slaeSolver;

        size_type dimension;
        std::vector< double > U;

        std::vector< double > dirihletData;
        std::vector< double > neumannData;

        double residual; // max residual for the iterative solvers

        double totalSolutionTime;
        double timeStep;
        double theta;
        double thermalDiffusivity;
        double solutionTime;
        double initialTemperature;

        vtkSmartPointer< vtkUnstructuredGrid > inputGrid;

        // Results
        std::list< std::pair< double, std::vector< double > > > solutionResultsForTime;
        std::unordered_map< std::size_t, double > vtkPointIndexToGetFemDofIndex;

        // BC
        std::list< std::pair< ApplicableNodes, std::shared_ptr< AbstractBoundaryCondition > > > applicableNodesAndCondition;
        std::unordered_map< bgeot::base_node, double, BaseNodeHash > nodeDirihletMap;
        std::unordered_map< bgeot::base_node, bgeot::small_vector< double >, BaseNodeHash > nodeNeumannMap;

      };

    }
  }
}


namespace test
{
  bool HeatFemSolverTest(int argc, char** argv);
  bool HeatFemSolverTestBC(int argc, char* argv[]);
}


#endif // HEATFEMSOLVER_HPP
