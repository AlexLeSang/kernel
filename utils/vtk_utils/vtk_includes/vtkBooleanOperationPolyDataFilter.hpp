/*=========================================================================

Program: Visualization Toolkit
Module: vtkBooleanOperationPolyDataFilter.h

Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
All rights reserved.
See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE. See the above copyright notice for more information.

=========================================================================*/
#ifndef __vtkBooleanOperationPolyDataFilter_h
#define __vtkBooleanOperationPolyDataFilter_h

//#include "vtkFiltersGeneralModule.h" // For export macro
#include "vtkPolyDataAlgorithm.h"

#include "vtkDataSetAttributes.h" // Needed for CopyCells() method

class vtkIdList;

class /*VTKFILTERSGENERAL_EXPORT*/ vtkBooleanOperationPolyDataFilter : public vtkPolyDataAlgorithm
{
public:
static vtkBooleanOperationPolyDataFilter *New();

vtkTypeMacro(vtkBooleanOperationPolyDataFilter,
vtkPolyDataAlgorithm);

void PrintSelf(ostream& os, vtkIndent indent);

enum OperationType
{
VTK_UNION=0,
VTK_INTERSECTION,
VTK_DIFFERENCE
};


vtkSetClampMacro( Operation, int, VTK_UNION, VTK_DIFFERENCE );
vtkGetMacro( Operation, int );
void SetOperationToUnion()
{ this->SetOperation( VTK_UNION ); }
void SetOperationToIntersection()
{ this->SetOperation( VTK_INTERSECTION ); }
void SetOperationToDifference()
{ this->SetOperation( VTK_DIFFERENCE ); }


vtkSetMacro( ReorientDifferenceCells, int );
vtkGetMacro( ReorientDifferenceCells, int );
vtkBooleanMacro( ReorientDifferenceCells, int );


vtkSetMacro(Tolerance, double);
vtkGetMacro(Tolerance, double);

protected:
vtkBooleanOperationPolyDataFilter();
~vtkBooleanOperationPolyDataFilter();


void SortPolyData(vtkPolyData* input, vtkIdList* intersectionList,
vtkIdList* unionList);

int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
int FillInputPortInformation(int, vtkInformation*);

private:
vtkBooleanOperationPolyDataFilter(const vtkBooleanOperationPolyDataFilter&); // Not implemented
void operator=(const vtkBooleanOperationPolyDataFilter&); // Not implemented


void CopyCells(vtkPolyData* in, vtkPolyData* out, int idx,
vtkDataSetAttributes::FieldList & pointFieldList,
vtkDataSetAttributes::FieldList & cellFieldList,
vtkIdList* cellIds, bool reverseCells);

double Tolerance;

int Operation;
int ReorientDifferenceCells;
};

#endif
