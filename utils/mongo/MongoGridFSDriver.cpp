#include "MongoGridFSDriver.hpp"

/*!
 * \brief utils::MongoGridFSDriver::files
 * \param databseName
 * \param ok
 * \return
 */
std::list<std::string> utils::MongoGridFSDriver::files(const std::string &databseName, bool *ok) const
{
  std::list< std::string > names;

  if ( replicaSet->isStillConnected() ) {

    mongo::GridFS gridFS( *replicaSet, databseName );

    std::auto_ptr< mongo::DBClientCursor > cursor = gridFS.list();
    while ( cursor->more() ) {
      mongo::BSONObj obj = cursor->next();
      std::string filename = obj.getStringField( "filename" );
      names.push_back( filename );
    }

    if ( ok != nullptr ) {
      *ok = true;
    }
  }
  else {
    if ( ok != nullptr ) {
      *ok = false;
    }
  }

  return names;
}

/*!
 * \brief utils::MongoGridFSDriver::readGrid
 * \param databseName
 * \param name
 * \param ok
 * \return
 */
vtkSmartPointer<vtkUnstructuredGrid> utils::MongoGridFSDriver::readGrid(const std::string &databseName, const std::string &name, bool *ok)
{
  vtkSmartPointer< vtkUnstructuredGrid > readGrid = vtkSmartPointer< vtkUnstructuredGrid >::New();

  if ( replicaSet->isStillConnected() ) {

    if ( fileExists( databseName, name ) ) {

      mongo::GridFS gridFS( *replicaSet, databseName );
      mongo::GridFile gridFile = gridFS.findFile( name );

      std::ostringstream buf;
      gridFile.write( buf );
      const auto str = buf.str();
      const auto gridStringSize = str.size();
      const char* gridString = str.c_str();

      vtkSmartPointer< vtkUnstructuredGridReader > stringReader = vtkSmartPointer< vtkUnstructuredGridReader >::New();
      stringReader->ReadFromInputStringOn();
      stringReader->SetBinaryInputString( gridString, gridStringSize );
      stringReader->Update();

      readGrid = stringReader->GetOutput();
    }

    if ( ok != nullptr ) {
      *ok = true;
    }
  }
  else {
    if ( ok != nullptr ) {
      *ok = false;
    }
  }

  return readGrid;
}

/*!
 * \brief utils::MongoGridFSDriver::saveGrid
 * \param grid
 * \param databseName
 * \param name
 * \param ok
 * \return
 */
bool utils::MongoGridFSDriver::saveGrid(vtkUnstructuredGrid *grid, const std::string &databseName, const std::string &name, bool *ok)
{
  bool success = true;
  if ( replicaSet->isStillConnected() ) {

    if ( fileExists( databseName, name ) ) {
      success = false;
    }
    else {

      vtkSmartPointer< vtkUnstructuredGridWriter > stringWriter = vtkSmartPointer< vtkUnstructuredGridWriter >::New();
      stringWriter->SetInput( grid );
      stringWriter->SetFileTypeToBinary();
      stringWriter->WriteToOutputStringOn();
      stringWriter->Write();
      const char* gridString = stringWriter->GetOutputString();
      const std::size_t gridStringSize = stringWriter->GetOutputStringLength();

      mongo::GridFS gridFS( *replicaSet, databseName );
      gridFS.storeFile( gridString, gridStringSize, name );
      success = true;
    }

    if ( ok != nullptr ) {
      *ok = true;
    }
  }
  else {
    if ( ok != nullptr ) {
      *ok = false;
    }
  }

  return success;
}

/*!
 * \brief utils::MongoGridFSDriver::saveGridToFile
 * \param databseName
 * \param name
 * \param fileName
 * \param ok
 * \return
 */
bool utils::MongoGridFSDriver::saveGridToFile(const std::string &databseName, const std::string &name, const std::string &fileName, bool *ok)
{
  bool success = true;
  if ( replicaSet->isStillConnected() ) {

    mongo::GridFS gridFS( *replicaSet, databseName );
    mongo::GridFile file = gridFS.findFile( name );
    if ( file.exists() ) {
      file.write( fileName );
      success = true;
    }
    else {
      success = false;
    }

    if ( ok != nullptr ) {
      *ok = true;
    }
  }
  else {
    if ( ok != nullptr ) {
      *ok = false;
    }
  }

  return success;
}

/*!
 * \brief utils::MongoGridFSDriver::saveGridFromFile
 * \param fileName
 * \param databseName
 * \param name
 * \param ok
 * \return
 */
bool utils::MongoGridFSDriver::saveGridFromFile(const std::string &fileName, const std::string &databseName, const std::string &name, bool *ok)
{
  bool success = true;

  if ( replicaSet->isStillConnected() ) {

    if ( fileExists( databseName, name ) ) {
      success = false;
    }
    else {

      vtkSmartPointer< vtkXMLUnstructuredGridReader > reader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
      reader->SetFileName( fileName.c_str() );
      reader->Update();
      vtkSmartPointer< vtkUnstructuredGrid > grid = reader->GetOutput();
      success = saveGrid( grid, databseName, name, ok );
    }

    if ( ok != nullptr ) {
      *ok = true;
    }
  }
  else {
    if ( ok != nullptr ) {
      *ok = false;
    }
  }

  return success;
}

/*!
 * \brief utils::MongoGridFSDriver::removeGrid
 * \param databseName
 * \param name
 * \param ok
 */
void utils::MongoGridFSDriver::removeGrid(const std::string &databseName, const std::string &name, bool* ok)
{
  if ( replicaSet->isStillConnected() ) {

    mongo::GridFS gridFS( *replicaSet, databseName );
    gridFS.removeFile( name );

    if ( ok != nullptr ) {
      *ok = true;
    }
  }
  else {
    if ( ok != nullptr ) {
      *ok = false;
    }
  }
}

/*!
 * \brief utils::MongoGridFSDriver::removeGrids
 * \param databseName
 * \param names
 * \param ok
 */
void utils::MongoGridFSDriver::removeGrids(const std::string &databseName, const std::list<std::string> &names, bool* ok)
{
  if ( replicaSet->isStillConnected() ) {
    for ( const auto& name : names ) {
      removeGrid( databseName, name );
    }
    if ( ok != nullptr ) {
      *ok = true;
    }
  }
  else {
    if ( ok != nullptr ) {
      *ok = false;
    }
  }
}

/*!
 * \brief utils::MongoGridFSDriver::fileExists
 * \param databseName
 * \param name
 * \param ok
 * \return
 */
bool utils::MongoGridFSDriver::fileExists(const std::string &databseName, const std::string &name, bool* ok)
{
  bool exists = false;
  if ( replicaSet->isStillConnected() ) {

    mongo::GridFS gridFS( *replicaSet, databseName );
    exists = gridFS.findFile( name ).exists();

    if ( ok != nullptr ) {
      *ok = true;
    }
  }
  else {
    if ( ok != nullptr ) {
      *ok = false;
    }
  }

  return exists;
}
