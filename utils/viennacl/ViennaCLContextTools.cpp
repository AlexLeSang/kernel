#include "ViennaCLContextTools.hpp"

#include <../utils/ThreadSaveLogger.hpp>

namespace kernel {
  namespace server {
    namespace functions {
      namespace node {
        namespace opencl {

          /*
          mongo::BSONObj print_device_information_to_bson(const std::vector<viennacl::ocl::device> &devices, const viennacl::ocl::device &dRef)
          {
            mongo::BSONObjBuilder builder;
            builder.appendIntOrLL( "No", long( std::distance( devices.begin(), std::find(devices.begin(), devices.end(), dRef) ) ) );
            builder.append( "name", std::string( dRef.name() ) );
            builder.append( "vendor", dRef.vendor() );
            builder.append( "type", dRef.type() == CL_DEVICE_TYPE_CPU ? "CPU"
                                                                      : dRef.type() == CL_DEVICE_TYPE_GPU ? "GPU"
                                                                                                          : dRef.type() == CL_DEVICE_TYPE_ACCELERATOR ? "ACCELERATOR"
                                                                                                                                                      : "DEFAULT" );
            builder.append( "available", dRef.available() ? "true" : "false" );

            builder.appendIntOrLL( "max_compute_units", long( dRef.max_compute_units() ) );
            builder.appendIntOrLL( "max_constant_args", long( dRef.max_constant_args() ) );
            builder.appendIntOrLL( "max_constant_buffer_size", long( dRef.max_constant_buffer_size() ) );

            builder.appendIntOrLL( "max_work_group_size", long( dRef.max_work_group_size() ) );
            builder.appendIntOrLL( "max_work_item_dimensions", long( dRef.max_work_item_dimensions() ) );

            builder.appendIntOrLL( "global_mem_cache_size", long( dRef.global_mem_cache_size() ) );

            builder.append( "local_mem_type", dRef.local_mem_type() == CL_LOCAL ? "CL_LOCAL" : "CL_GLOBAL" );
            //builder.appendIntOrLL( "max_mem_alloc_size", long( dRef.max_mem_alloc_size() ) );
            builder.appendIntOrLL( "local_mem_size", long( dRef.local_mem_size() ) );
            builder.append( "host_unified_memory", dRef.host_unified_memory() ? "true" : "false" );
            return builder.obj();
          }
          */

          /*
          std::vector< mongo::BSONObj > print_devices_for_current_context_to_bson()
          {
            const auto& context = viennacl::ocl::current_context();
            const auto& devices = context.devices();
            std::vector< mongo::BSONObj > documentsVector;
            documentsVector.reserve( devices.size() );
            auto documentInserter = std::back_insert_iterator< decltype(documentsVector) >( documentsVector );
            std::for_each( devices.begin(), devices.end(), [&] (const viennacl::ocl::device &dRef) {
              const auto bsonObj = print_device_information_to_bson( devices, dRef );
              documentInserter = bsonObj;
            } );
            return documentsVector;
          }
          */

          /*!
           * \brief create_all_gpu_devices_context
           * \return
           */
          bool create_all_gpu_devices_context()
          {
            static bool initialized = false;
            static bool success = true;

            if ( ! initialized ) {
              std::vector< viennacl::ocl::device > devices;
              try {
                devices = viennacl::ocl::platform().devices( CL_DEVICE_TYPE_GPU );
              }
              catch ( std::exception& e ) {
                const auto qErrorString = QString( "create_all_gpu_devices_context exception: " ) + e.what();
                utils::ThreadSaveLogger::addLineStatic( qErrorString );
                success = false;
              }

              if ( success ) {
                std::vector< cl_device_id > devices_id( devices.size() );
                std::transform( devices.begin(), devices.end(), devices_id.begin(), []( const viennacl::ocl::device &dRef ) { return dRef.id(); } );

                viennacl::ocl::setup_context( ALL_GPU_DEVICES_CONTEXT, devices_id );
              }

              initialized = true;
            }
            return success;
          }

          /*!
           * \brief create_all_cpu_devices_context
           * \return
           */
          bool create_all_cpu_devices_context()
          {
            /* Non static version, gives nothing
            bool success = true;
            std::vector< viennacl::ocl::device > devices;
            try {
              devices = viennacl::ocl::platform().devices( CL_DEVICE_TYPE_CPU );
            }
            catch ( std::exception& e ) {
              const auto qErrorString = QString( "create_all_cpu_devices_context exception: " ) + e.what();
              utils::ThreadSaveLogger::addLineStatic( qErrorString );
              success = false;
            }

            if ( success ) {
              std::vector< cl_device_id > devices_id( devices.size() );
              std::transform( devices.begin(), devices.end(), devices_id.begin(), []( const viennacl::ocl::device &dRef ) { return dRef.id(); } );

              viennacl::ocl::setup_context( ALL_CPU_DEVICES_CONTEXT, devices_id );
            }

            return success;
            */

            //* Static version
            static bool initialized = false;
            static bool success = true;

            if ( ! initialized ) {
              std::vector< viennacl::ocl::device > devices;
              try {
                devices = viennacl::ocl::platform().devices( CL_DEVICE_TYPE_CPU );
              }
              catch ( std::exception& e ) {
                const auto qErrorString = QString( "create_all_cpu_devices_context exception: " ) + e.what();
                utils::ThreadSaveLogger::addLineStatic( qErrorString );
                success = false;
              }

              if ( success ) {
                std::vector< cl_device_id > devices_id( devices.size() );
                std::transform( devices.begin(), devices.end(), devices_id.begin(), []( const viennacl::ocl::device &dRef ) { return dRef.id(); } );

                viennacl::ocl::setup_context( ALL_CPU_DEVICES_CONTEXT, devices_id );
              }

              initialized = true;
            }
            return success;
            //*/
          }

          /*!
           * \brief create_all_devices_context
           * \return
           */
          bool create_all_devices_context()
          {
            static bool initialized = false;
            static bool success = true;

            if ( ! initialized ) {
              std::vector< viennacl::ocl::device > devices;
              try {
                devices = viennacl::ocl::platform().devices( CL_DEVICE_TYPE_ALL );
              }
              catch ( std::exception& e ) {
                const auto qErrorString = QString( "create_all_devices_context exception: " ) + e.what();
                utils::ThreadSaveLogger::addLineStatic( qErrorString );
                success = false;
              }

              if ( success ) {
                std::vector< cl_device_id > devices_id( devices.size() );
                std::transform( devices.begin(), devices.end(), devices_id.begin(), []( const viennacl::ocl::device &dRef ) { return dRef.id(); } );

                viennacl::ocl::setup_context( ALL_DEVICES_CONTEXT, devices_id );
              }

              initialized = true;
            }
            return success;
          }

          /*!
           * \brief print_platforms
           */
          void print_platforms()
          {
            auto plarforms = viennacl::ocl::get_platforms();
            std::for_each( plarforms.begin(), plarforms.end(), []( viennacl::ocl::platform &pRef ) {
              std::cout << "Platform Information\n" << pRef.info() << std::endl;
              {
                std::cout << "CPU devices:" << std::endl;
                std::vector< viennacl::ocl::device > devices;
                try {
                  devices = pRef.devices( CL_DEVICE_TYPE_CPU );
                }
                catch( viennacl::ocl::device_not_found & ) {
                  std::cerr << "No CPU devices was found" << std::endl;
                }
                std::for_each( devices.begin(), devices.end(), [&]( const viennacl::ocl::device &dRef ) {
                  print_device_information( std::cout, devices, dRef );
                } );
              }

              {
                std::cout << "GPU devices:" << std::endl;
                std::vector< viennacl::ocl::device > devices;
                try {
                  devices = pRef.devices( CL_DEVICE_TYPE_GPU );
                }
                catch( viennacl::ocl::device_not_found & ) {
                  std::cerr << "No GPU devices was found" << std::endl;
                }
                std::for_each( devices.begin(), devices.end(), [&]( const viennacl::ocl::device &dRef ) {
                  print_device_information( std::cout, devices, dRef );
                } );
              }

              {
                std::cout << "Accelerator devices:" << std::endl;
                std::vector< viennacl::ocl::device > devices;
                try {
                  devices = pRef.devices( CL_DEVICE_TYPE_ACCELERATOR );
                }
                catch( viennacl::ocl::device_not_found & ) {
                  std::cerr << "No accelerator devices was found" << std::endl;
                }
                std::for_each( devices.begin(), devices.end(), [&]( const viennacl::ocl::device &dRef ) {
                  print_device_information( std::cout, devices, dRef );
                } );
              }

            } );
            std::cout << std::endl;
          }

        }


      }
    }
  }
}


/*
bool test::ViennaCLContextDescriptionToBSON()
{
  using namespace kernel::server::functions::node;

  std::cout << "ViennaCL BSON test" << std::endl;

  if ( opencl::create_all_cpu_devices_context() ) {
    viennacl::ocl::switch_context( opencl::ALL_CPU_DEVICES_CONTEXT );
    std::cout << "All cpu context devices:\n";
    std::ostream_iterator< mongo::BSONObj > oit( std::cout, "\n" );
    const auto documents = opencl::print_devices_for_current_context_to_bson();
    std::copy( documents.begin(), documents.end(), oit );
    std::cout << std::endl;
  }

  if ( opencl::create_all_gpu_devices_context() ) {
    viennacl::ocl::switch_context( opencl::ALL_GPU_DEVICES_CONTEXT );
    std::cout << "All gpu context devices:\n";
    std::ostream_iterator< mongo::BSONObj > oit( std::cout, "\n" );
    const auto documents = opencl::print_devices_for_current_context_to_bson();
    std::copy( documents.begin(), documents.end(), oit );
    std::cout << std::endl;
  }

  if ( opencl::create_all_devices_context() ) {
    viennacl::ocl::switch_context( opencl::ALL_DEVICES_CONTEXT );
    std::cout << "All devices context devices:\n";
    std::ostream_iterator< mongo::BSONObj > oit( std::cout, "\n" );
    const auto documents = opencl::print_devices_for_current_context_to_bson();
    std::copy( documents.begin(), documents.end(), oit );
    std::cout << std::endl;
  }

  return true;
}
*/

bool test::ViennaCLContextTest()
{
  using namespace kernel::server::functions::node;

  std::cout << "ViennaCLContextTest" << std::endl;

  opencl::create_all_cpu_devices_context();
  opencl::create_all_gpu_devices_context();
  opencl::create_all_devices_context();

  /*
  viennacl::ocl::switch_context( opencl::ALL_CPU_DEVICES_CONTEXT );
  // viennacl::ocl::current_context().build_options("-cl-mad-enable -cl-fast-relaxed-math");
  // viennacl::ocl::current_context().build_options("-cl-opt-disable");
  opencl::print_devices_for_current_context( std::cout );
  opencl::benchmark<float>();
  */

  viennacl::ocl::switch_context( opencl::ALL_GPU_DEVICES_CONTEXT );
  // viennacl::ocl::current_context().build_options("-cl-mad-enable -cl-fast-relaxed-math");
  // viennacl::ocl::current_context().build_options("-cl-opt-disable");
  opencl::print_devices_for_current_context( std::cout );
  opencl::benchmark<float>( 4096 );

  /*
  viennacl::ocl::switch_context( opencl::ALL_DEVICES_CONTEXT );
  // viennacl::ocl::current_context().build_options("-cl-mad-enable -cl-fast-relaxed-math");
  // viennacl::ocl::current_context().build_options("-cl-opt-disable");
  opencl::print_devices_for_current_context( std::cout );
  opencl::benchmark<float>();
  */

  return true;
}


bool test::ViennaCLContextMultithreadTest()
{
  using namespace kernel::server::functions::node;

  std::cout << "ViennaCLContextMultithreadTest" << std::endl;

  auto cpu_thread_lamda = [](){

    constexpr int n_loop = 5;
    for ( auto i = 0; i < n_loop; ++ i ) {
      std::cout << "cpu_thread_lamda" << std::endl;

      if ( opencl::create_all_cpu_devices_context() ) {
        viennacl::ocl::switch_context( opencl::ALL_CPU_DEVICES_CONTEXT );
        opencl::print_devices_for_current_context( std::cout );
      }

      std::chrono::milliseconds duration( 1000 );
      std::this_thread::sleep_for( duration );
    }
  };
  std::future< void > cpu_res = std::async( std::launch::async, cpu_thread_lamda );
  cpu_res.wait();

  std::chrono::milliseconds duration( 2500 );
  std::this_thread::sleep_for( duration );

  // Raw OpenCL test
  {
    typedef float       ScalarType;

    const char * my_compute_program =
        "__kernel void elementwise_prod(\n"
        "          __global const float * vec1,\n"
        "          __global const float * vec2, \n"
        "          __global float * result,\n"
        "          unsigned int size) \n"
        "{ \n"
        "  for (unsigned int i = get_global_id(0); i < size; i += get_global_size(0))\n"
        "    result[i] = vec1[i] * vec2[i];\n"
        "};\n";

    //manually set up a custom OpenCL context:
    std::vector<cl_device_id> device_id_array;

    //get all available devices
    viennacl::ocl::platform pf;
    std::cout << "Platform info: " << pf.info() << std::endl;
    std::vector<viennacl::ocl::device> devices = pf.devices(CL_DEVICE_TYPE_DEFAULT);
    std::cout << devices[0].name() << std::endl;
    std::cout << "Number of devices for custom context: " << devices.size() << std::endl;

    //set up context using all found devices:
    for (std::size_t i=0; i<devices.size(); ++i)
    {
      device_id_array.push_back(devices[i].id());
    }

    std::cout << "Creating context..." << std::endl;
    cl_int err;
    cl_context my_context = clCreateContext(0, cl_uint(device_id_array.size()), &(device_id_array[0]), NULL, NULL, &err);
    VIENNACL_ERR_CHECK(err);


    //create two Vectors:
    unsigned int vector_size = 100;
    std::vector<ScalarType> vec1(vector_size);
    std::vector<ScalarType> vec2(vector_size);
    std::vector<ScalarType> result(vector_size);

    //
    // fill the operands vec1 and vec2:
    //
    for (unsigned int i=0; i<vector_size; ++i)
    {
      vec1[i] = static_cast<ScalarType>(i);
      vec2[i] = static_cast<ScalarType>(vector_size-i);
    }

    //
    // create memory in OpenCL context:
    //
    cl_mem mem_vec1 = clCreateBuffer(my_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, vector_size * sizeof(ScalarType), &(vec1[0]), &err);
    VIENNACL_ERR_CHECK(err);
    cl_mem mem_vec2 = clCreateBuffer(my_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, vector_size * sizeof(ScalarType), &(vec2[0]), &err);
    VIENNACL_ERR_CHECK(err);
    cl_mem mem_result = clCreateBuffer(my_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, vector_size * sizeof(ScalarType), &(result[0]), &err);
    VIENNACL_ERR_CHECK(err);

    //
    // create a command queue for each device:
    //

    std::vector<cl_command_queue> queues(devices.size());
    for (std::size_t i=0; i<devices.size(); ++i)
    {
      queues[i] = clCreateCommandQueue(my_context, devices[i].id(), 0, &err);
      VIENNACL_ERR_CHECK(err);
    }

    //
    // create and build a program in the context:
    //
    std::size_t source_len = std::string(my_compute_program).length();
    cl_program my_prog = clCreateProgramWithSource(my_context, 1, &my_compute_program, &source_len, &err);
    err = clBuildProgram(my_prog, 0, NULL, NULL, NULL, NULL);

    /*            char buffer[1024];
                cl_build_status status;
                clGetProgramBuildInfo(my_prog, devices[1].id(), CL_PROGRAM_BUILD_STATUS, sizeof(cl_build_status), &status, NULL);
                clGetProgramBuildInfo(my_prog, devices[1].id(), CL_PROGRAM_BUILD_LOG, sizeof(char)*1024, &buffer, NULL);
                std::cout << "Build Scalar: Err = " << err << " Status = " << status << std::endl;
                std::cout << "Log: " << buffer << std::endl;*/

    VIENNACL_ERR_CHECK(err);

    //
    // create a kernel from the program:
    //
    const char * kernel_name = "elementwise_prod";
    cl_kernel my_kernel = clCreateKernel(my_prog, kernel_name, &err);
    VIENNACL_ERR_CHECK(err);


    //
    // Execute elementwise_prod kernel on first queue: result = vec1 .* vec2;
    //
    err = clSetKernelArg(my_kernel, 0, sizeof(cl_mem), (void*)&mem_vec1);
    VIENNACL_ERR_CHECK(err);
    err = clSetKernelArg(my_kernel, 1, sizeof(cl_mem), (void*)&mem_vec2);
    VIENNACL_ERR_CHECK(err);
    err = clSetKernelArg(my_kernel, 2, sizeof(cl_mem), (void*)&mem_result);
    VIENNACL_ERR_CHECK(err);
    err = clSetKernelArg(my_kernel, 3, sizeof(unsigned int), (void*)&vector_size);
    VIENNACL_ERR_CHECK(err);
    std::size_t global_size = vector_size;
    std::size_t local_size = vector_size;
    err = clEnqueueNDRangeKernel(queues[0], my_kernel, 1, NULL, &global_size, &local_size, 0, NULL, NULL);
    VIENNACL_ERR_CHECK(err);


    //
    // Read and output result:
    //
    err = clEnqueueReadBuffer(queues[0], mem_vec1, CL_TRUE, 0, sizeof(ScalarType)*vector_size, &(vec1[0]), 0, NULL, NULL);
    VIENNACL_ERR_CHECK(err);
    err = clEnqueueReadBuffer(queues[0], mem_result, CL_TRUE, 0, sizeof(ScalarType)*vector_size, &(result[0]), 0, NULL, NULL);
    VIENNACL_ERR_CHECK(err);

    std::cout << "vec1  : ";
    for (std::size_t i=0; i<vec1.size(); ++i)
      std::cout << vec1[i] << " ";
    std::cout << std::endl;

    std::cout << "vec2  : ";
    for (std::size_t i=0; i<vec2.size(); ++i)
      std::cout << vec2[i] << " ";
    std::cout << std::endl;

    std::cout << "result: ";
    for (std::size_t i=0; i<result.size(); ++i)
      std::cout << result[i] << " ";
    std::cout << std::endl;

  }

  std::this_thread::sleep_for( duration );

  auto gpu_thread_lamda = [](){

    constexpr int n_loop = 5;
    for ( auto i = 0; i < n_loop; ++ i ) {

      opencl::create_all_cpu_devices_context();
      opencl::create_all_gpu_devices_context();
      opencl::create_all_devices_context();

      std::cout << "gpu_thread_lamda" << std::endl;

      viennacl::ocl::switch_context( opencl::ALL_GPU_DEVICES_CONTEXT );
      opencl::print_devices_for_current_context( std::cout );

      std::chrono::milliseconds duration( 1000 );
      std::this_thread::sleep_for( duration );
    }
  };
  std::future< void > gpu_res = std::async( std::launch::async, gpu_thread_lamda );
  gpu_res.wait();

  std::this_thread::sleep_for( duration );

  auto empty_thread_lamda = [](){

    constexpr int n_loop = 10;
    for ( auto i = 0; i < n_loop; ++ i ) {
      std::cout << "empty_thread_lamda" << std::endl;

      std::chrono::milliseconds duration( 1000 );
      std::this_thread::sleep_for( duration );
    }
  };
  std::future< void > empty_res = std::async( std::launch::async, empty_thread_lamda );
  empty_res.wait();


  return true;
}
