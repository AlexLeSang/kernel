#ifndef PINGPONGCOMMAND_HPP
#define PINGPONGCOMMAND_HPP

#include "Command.hpp"

namespace kernel {
  namespace server {
    namespace command {

      bool PingPongCommand(QTcpSocket &socket, std::atomic<bool> &interruptFlag);
    }
  }
}

#endif // PINGPONGCOMMAND_HPP
