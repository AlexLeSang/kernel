#ifndef STOPSERVERCOMMAND_HPP
#define STOPSERVERCOMMAND_HPP

#include "Command.hpp"
#include "KernelServer.hpp"

namespace kernel {
  namespace server {
    namespace command {

      bool StopServerCommand(QTcpSocket &, std::atomic<bool> &);
    }
  }
}


#endif // STOPSERVERCOMMAND_HPP
