#ifndef KERNELWORKGROUPMAINTENANCER_HPP
#define KERNELWORKGROUPMAINTENANCER_HPP

#include <QObject>
#include <QRunnable>
#include <QString>

#include <QMutex>
#include <QTimer>

#include <atomic>
#include <memory>

#include <functions/node/MongoDBMaintenancer.hpp>


namespace kernel {

  namespace client {
    class KernelClient;
  }

  namespace server {

    /*!
     * \brief database_collection
     */
    static const std::string database_collection = functions::node::kernel_nodes_ns;

    /*!
     * \brief workgroup_edges_collection
     */
    static const std::string workgroup_edges_collection = functions::node::kernel_workgroup_graph_ns;

    /*!
     * \brief update_interval
     */
    // static const quint32 update_interval = functions::node::kernel_nodes_ttl * 0.75;
    static const quint32 update_interval = 2;


    /*!
     * \brief The KernelWorkgroupMaintenancer class
     */
    class KernelWorkgroupMaintenancer : public QObject, public QRunnable
    {
      Q_OBJECT

    public:
      explicit KernelWorkgroupMaintenancer(QObject* parent, const QHostAddress &serverAddress = QHostAddress::LocalHost, const quint32 serverPort = 31017, const QHostAddress &mongoAddress = QHostAddress::LocalHost, const quint32 mongoPort = 27017, QString workgroupTag = "unspacified");


      QHostAddress mongodbServerAddress() const;
      void setMongodbServerAddress(const QHostAddress &mongodbServerAddress);

      quint32 mongodbPortNumber() const;
      void setMongodbPortNumber(const quint32 &mongodbPortNumber);

      QString workgroupTag() const;
      void setWorkgroupTag(const QString &workgroupTag);

      // QRunnable interface
      virtual void run();

      void interrupt();

      void addEdgeObject(mongo::BSONObj objFrom, std::shared_ptr< mongo::DBClientReplicaSet > mongoConnection, const mongo::BSONObj edgeObj, mongo::BSONObj objTo);
    private:
      bool getAndSetWorkgroupInfo();

      mongo::BSONObj createNodeDescription();

    private:
      QHostAddress serverAddress_;
      quint32 serverPort_;

      // MongoDB relater info
      bool mongodbParametersChanged;
      QHostAddress mongodbServerAddress_;
      quint32 mongodbPortNumber_;
      QString workgroupTag_;
      std::unique_ptr< functions::node::MongoDBMaintenancer > mongodbMaintenancerPtr_;

      // Client related info
      client::KernelClient* kernelClient;
      std::atomic_bool interruptFlag;


    };

  }
}

#endif // KERNELWORKGROUPMAINTENANCER_HPP
