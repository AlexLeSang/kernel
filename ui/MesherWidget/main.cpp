#include <MesherWidget.hpp>
#include <QApplication>
#include <vtkXMLPolyDataReader.h>
#include <vtkSTLReader.h>

#include <QDesktopWidget>

int main(int argc, char *argv[])
{
  assert( argc == 2 );
  QApplication a( argc, argv );

  //  vtkSmartPointer< vtkXMLPolyDataReader > reader = vtkSmartPointer< vtkXMLPolyDataReader >::New();
  //  reader->SetFileName( argv[ 1 ] );
  //  reader->Update();
  //  vtkSmartPointer< vtkPolyData >  polydata = reader->GetOutput();

  vtkSmartPointer< vtkSTLReader > reader = vtkSmartPointer< vtkSTLReader >::New();
  reader->SetFileName( argv[ 1 ] );
  reader->Update();
  vtkSmartPointer< vtkPolyData >  polydata = reader->GetOutput();

  assert( polydata->GetNumberOfCells() );
  assert( polydata->GetNumberOfPoints() );

  MesherWidget widget;
  widget.setGeometry( polydata );
  widget.show();
  widget.move( QApplication::desktop()->screen()->rect().center() - widget.rect().center() );

  return a.exec();
}
