#ifndef GMRESILUTPSLAESOLVER_HPP
#define GMRESILUTPSLAESOLVER_HPP

#include <slae_solvers/AbstractSLAESolver.hpp>

class GmresILUTPSLAESolver : public AbstractSLAESolver
{
  // AbstractSLAESolver interface
public:
  virtual bool operator ()(getfem::model &model, gmm::iteration &iter);
};

#endif // GMRESILUTPSLAESOLVER_HPP
