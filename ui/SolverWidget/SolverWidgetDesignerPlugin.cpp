#include "SolverWidgetDesignerPlugin.hpp"

#include <SolverWidget.hpp>

#include <QtPlugin>

SolverWidgetDesignerPlugin::SolverWidgetDesignerPlugin(QObject *parent) : QObject( parent ), initialized( false )
{}

QString SolverWidgetDesignerPlugin::name() const
{
  return "SolverWidget";
}

QString SolverWidgetDesignerPlugin::group() const
{
  return "QVTK";
}

QString SolverWidgetDesignerPlugin::toolTip() const
{
  return "Solver widget";
}

QString SolverWidgetDesignerPlugin::whatsThis() const
{
  return "Solver widget";
}

QString SolverWidgetDesignerPlugin::includeFile() const
{
  return "SolverWidget.hpp";
}

QIcon SolverWidgetDesignerPlugin::icon() const
{
  return QIcon();
}

bool SolverWidgetDesignerPlugin::isContainer() const
{
  return false;
}

QWidget *SolverWidgetDesignerPlugin::createWidget(QWidget *parent)
{
  return new SolverWidget( parent );
}

bool SolverWidgetDesignerPlugin::isInitialized() const
{
  return initialized;
}

void SolverWidgetDesignerPlugin::initialize(QDesignerFormEditorInterface *)
{
  if ( initialized ) {
    return;
  }

  initialized = true;
}

QString SolverWidgetDesignerPlugin::domXml() const
{
  return "<ui language=\"c++\">\n"
              " <widget class=\"SolverWidget\" name=\"solverWidget\">\n"
              "  <property name=\"geometry\">\n"
              "   <rect>\n"
              "    <x>0</x>\n"
              "    <y>0</y>\n"
              "    <width>1024</width>\n"
              "    <height>768</height>\n"
              "   </rect>\n"
              "  </property>\n"
              "  <property name=\"toolTip\" >\n"
              "   <string>Solver widget</string>\n"
              "  </property>\n"
              "  <property name=\"whatsThis\" >\n"
              "   <string>Solver widget</string>\n"
              "  </property>\n"
              " </widget>\n"
              "<resources/>\n"
              "<connections/>\n"
              "<slots>\n"
              " <slot>addSource()</slot>\n"
              " <slot>selectActor(QModelIndex)</slot>\n"
              "</slots>\n"
              "</ui>\n";
}

QString SolverWidgetDesignerPlugin::codeTemplate() const
{
  return "";
}

Q_EXPORT_PLUGIN2( serverstatuswidget, SolverWidgetDesignerPlugin )
