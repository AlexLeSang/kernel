#include "KernelServer.hpp"

#include <cassert>

#include <QObject>
#include <QtCore>
#include <QThreadPool>

#include "KernelExecutor.hpp"
#include "ThreadSaveLogger.hpp"

#include <KernelWorkgroupMaintenancer.hpp>

#include <../utils/viennacl/ViennaCLContextTools.hpp>

namespace kernel {
  namespace server {

    bool KernelServer::activated = false;

    QHostAddress KernelServer::serverAddress_ = QHostAddress::LocalHost;
    quint32 KernelServer::portNumber_ = 31017;

    QHostAddress KernelServer::mongodbServerAddress_ = QHostAddress::LocalHost;
    quint32 KernelServer::mongodbPortNumber_ = 27017;
    QString KernelServer::workgroupTag_ = "unspecified";

    KernelWorkgroupMaintenancer* KernelServer::workgroupMaintenancerPtr = nullptr;

    /*!
     * \brief KernelServer::getInstance
     * \return
     */
    KernelServer* KernelServer::getInstance()
    {
      static KernelServer instance;
      if ( !activated ) {
        activated = true;
        instance.listen( serverAddress_, portNumber_ );
        utils::ThreadSaveLogger::addLineStatic( QString("Server started at: ") + serverAddress_.toString() + ":" + QString::number( portNumber_ ) + ", pid: " + QString::number( qApp->applicationPid() ) );

      }

      // QThread pool configuration
      QThreadPool::globalInstance()->setMaxThreadCount( QThread::idealThreadCount() * 100 );

      instance.setMaxPendingConnections( QThreadPool::globalInstance()->maxThreadCount() - 2 );

      // Init mongodb instance
      workgroupMaintenancerPtr = new KernelWorkgroupMaintenancer( &instance, serverAddress_, portNumber_, mongodbServerAddress_, mongodbPortNumber_, workgroupTag_ );

      if ( ! QThreadPool::globalInstance()->tryStart( workgroupMaintenancerPtr ) ) {
        utils::ThreadSaveLogger::addLineStatic( QString( "Unable to start a task in the thread pool") );
      }

      return &instance;
    }

    /*!
     * \brief KernelServer::connections
     * \return
     */
    quint64 KernelServer::connections() const
    {
      return connections_;
    }

    /*!
     * \brief KernelServer::stopServer
     */
    void KernelServer::stopServer()
    {
      emit signal_interrupt();

      workgroupMaintenancerPtr->interrupt();

      utils::ThreadSaveLogger::addLineStatic( QString("Server stopped at: ") + serverAddress_.toString() + ":" + QString::number( portNumber_ ) + ", pid: " + QString::number( qApp->applicationPid() ) );
      utils::ThreadSaveLogger::addLineStatic( "Connections processed: " + QString::number( connections_ ) );
      close();
      qApp->exit();
    }

    /*!
     * \brief KernelServer::incomingConnection
     * \param socketDescriptor
     */
    void KernelServer::incomingConnection(int socketDescriptor)
    {
      ++connections_;
      KernelExecutor *executorPtr = new KernelExecutor( socketDescriptor, this );
      QObject::connect( this, SIGNAL( signal_interrupt() ), executorPtr, SLOT( interrupt() ), Qt::DirectConnection );

      if ( ! QThreadPool::globalInstance()->tryStart( executorPtr ) ) {
        utils::ThreadSaveLogger::addLineStatic( QString( "Unable to start an executor in the thread pool" ) );
      }

    }

    /*!
     * \brief KernelServer::serverAddress
     * \return
     */
    QHostAddress KernelServer::serverAddress()
    {
      return serverAddress_;
    }

    /*!
     * \brief KernelServer::setServerAddress
     * \param serverAddress
     */
    void KernelServer::setServerAddress(const QHostAddress &serverAddress)
    {
      assert( !activated );
      serverAddress_ = serverAddress;
    }

    /*!
     * \brief KernelServer::portNumber
     * \return
     */
    quint32 KernelServer::portNumber()
    {
      return portNumber_;
    }

    /*!
     * \brief KernelServer::setPortNumber
     * \param portNumber
     */
    void KernelServer::setPortNumber(const quint32 &portNumber)
    {
      assert( !activated );
      portNumber_ = portNumber;
    }

    /*!
     * \brief KernelServer::mongodbServerAddress
     * \return
     */
    QHostAddress KernelServer::mongodbServerAddress()
    {
      return mongodbServerAddress_;
    }

    /*!
     * \brief KernelServer::setMongodbServerAddress
     * \param mongodbServerAddress
     */
    void KernelServer::setMongodbServerAddress(const QHostAddress &mongodbServerAddress)
    {
      mongodbServerAddress_ = mongodbServerAddress;
      workgroupMaintenancerPtr->setMongodbServerAddress( mongodbServerAddress );
    }

    /*!
     * \brief KernelServer::mongodbPortNumber
     * \return
     */
    quint32 KernelServer::mongodbPortNumber()
    {
      return mongodbPortNumber_;
    }

    /*!
     * \brief KernelServer::setMongodbPortNumber
     * \param mongodbPortNumber
     */
    void KernelServer::setMongodbPortNumber(const quint32 &mongodbPortNumber)
    {
      mongodbPortNumber_ = mongodbPortNumber;
      workgroupMaintenancerPtr->setMongodbPortNumber( mongodbPortNumber_ );
    }

    /*!
     * \brief KernelServer::getWorkgroupTag
     * \return
     */
    QString KernelServer::getWorkgroupTag()
    {
      return workgroupTag_;
    }

    /*!
     * \brief KernelServer::setWorkgroupTag
     * \param value
     */
    void KernelServer::setWorkgroupTag(const QString &value)
    {
      workgroupTag_ = value;
      workgroupMaintenancerPtr->setWorkgroupTag( value );
    }

  }
}


