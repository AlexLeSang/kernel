#include "MesherWidget.hpp"
#include "ui_MesherWidget.h"

#include <vtkBoundingBox.h>
#include <vtkRendererCollection.h>

/*!
 * \brief MesherWidget::MesherWidget
 * \param parent
 */
MesherWidget::MesherWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::MesherWidget),
  mesherTread( nullptr )
{
  ui->setupUi(this);

  mainRenderer = vtkSmartPointer< vtkRenderer >::New();
  mainRenderer->GradientBackgroundOn();
  mainRenderer->SetBackground( 0.0, 0.0, 0.0 );
  mainRenderer->SetBackground2( 0.0, 0.7, 0.1);

  ui->qvtkMainWidget->GetRenderWindow()->AddRenderer( mainRenderer );

  additionalRenderer = vtkSmartPointer< vtkRenderer >::New();
  additionalRenderer->GradientBackgroundOn();
  additionalRenderer->SetBackground( 0.0, 0.0, 0.0 );
  additionalRenderer->SetBackground2( 0.0, 0.7, 0.1 );

  ui->qvtkAdditionalWidget->GetRenderWindow()->AddRenderer( additionalRenderer );

  vtkSmartPointer< vtkInteractorStyleTrackballCamera > mainStyle = vtkSmartPointer< vtkInteractorStyleTrackballCamera >::New();
  mainStyle->SetDefaultRenderer( mainRenderer );
  ui->qvtkMainWidget->GetInteractor()->SetInteractorStyle( mainStyle );

  vtkSmartPointer< vtkInteractorStyleTrackballCamera > additionalStyle = vtkSmartPointer< vtkInteractorStyleTrackballCamera >::New();
  additionalStyle->SetDefaultRenderer( additionalRenderer );
  ui->qvtkAdditionalWidget->GetInteractor()->SetInteractorStyle( additionalStyle );
}

/*!
 * \brief MesherWidget::~MesherWidget
 */
MesherWidget::~MesherWidget()
{
  delete ui;
}

/*!
 * \brief MesherWidget::changeEvent
 * \param e
 */
void MesherWidget::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}

/*!
 * \brief MesherWidget::getGeometry
 * \return
 */
vtkSmartPointer<vtkPolyData> MesherWidget::getGeometry() const
{
  return geometry;
}

/*!
 * \brief MesherWidget::setGeometry
 * \param value
 */
void MesherWidget::setGeometry(const vtkSmartPointer<vtkPolyData> &value)
{
  geometry = value;

  mainRenderer->RemoveAllViewProps();
  additionalRenderer->RemoveAllViewProps();

  vtkSmartPointer<vtkDataSetMapper> mapper = vtkSmartPointer<vtkDataSetMapper>::New();
  mapper->SetInput( geometry );
  mapper->ScalarVisibilityOn();
  mapper->SetScalarModeToUsePointData();

  vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
  actor->SetMapper( mapper );
  // actor->GetProperty()->EdgeVisibilityOn();

  additionalRenderer->AddActor( actor );
  additionalRenderer->ResetCamera();
  ui->qvtkAdditionalWidget->update();


  vtkBoundingBox box( value->GetBounds() );
  const double maxLength = box.GetMaxLength();
  std::cout << "maxLength: " << maxLength << std::endl;
  ui->cellSizeSpinBox->setMaximum( maxLength * 3.0 );
  ui->cellSizeSpinBox->setValue( maxLength );

  ui->meshButton->setEnabled( true );
  ui->qvtkAdditionalWidget->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->ResetCamera();
  ui->qvtkMainWidget->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->ResetCamera();
}

/*!
 * \brief MesherWidget::setGeometryToMesher
 * \param polyData
 * \param fileName
 */
void MesherWidget::setGeometryToMesher(vtkSmartPointer<vtkPolyData> polyData, const QString fileName)
{
  setGeometry( polyData );
  ui->outputFileNameLineEdit->setText( fileName );
}

/*!
 * \brief MesherWidget::enhancingIsPossible
 * \return
 */
const bool MesherWidget::enhancingIsPossible()
{
  const bool possibilty = grid &&
                          ( prevCellSize > ui->cellSizeSpinBox->value() ) &&
                          ( prevMaxRadiusEdgeRatio > ui->radiusEdgeRatioSpinBox->value() );
  return possibilty;
}

/*!
 * \brief MesherWidget::assignGeometryToMesher
 * \param mesher
 */
void MesherWidget::assignGeometryToMesher(std::shared_ptr< kernel::utils::mesh::ViennaMeshMesher> mesher)
{
  vtkSmartPointer< vtkPolyData > surface;

  if ( enhancingIsPossible() ) {
    std::cout << "\nEnhancing\n" << std::endl;
    vtkSmartPointer< vtkDataSetSurfaceFilter > surfaceFilter = vtkSmartPointer< vtkDataSetSurfaceFilter >::New();
    surfaceFilter->AddInput( grid );
    surfaceFilter->Update();
    surface = surfaceFilter->GetOutput();
  }
  else {
    surface = geometry;
  }
  mesher->setGeometry( surface );
}

/*!
 * \brief MesherWidget::loadGeneratedMesh
 */
void MesherWidget::loadGeneratedMesh()
{
  vtkSmartPointer< vtkXMLUnstructuredGridReader > reader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
  reader->SetFileName( prevFileName.c_str() );
  reader->Update();

  grid = reader->GetOutput();
}

/*!
 * \brief MesherWidget::setMesherParameters
 * \param mesher
 */
void MesherWidget::setMesherParameters(std::shared_ptr< kernel::utils::mesh::ViennaMeshMesher> mesher)
{
  auto suffix = ".vtu";
  prevCellSize = ui->cellSizeSpinBox->value();
  prevFileName = ui->outputFileNameLineEdit->text().toStdString() + suffix;
  prevMaxRadiusEdgeRatio = ui->radiusEdgeRatioSpinBox->value();

  mesher->setCellSize( ui->cellSizeSpinBox->value() );
  mesher->setMaxRadiusEdgeRatio( prevMaxRadiusEdgeRatio );
  mesher->setMinDihedralAngle( ui->minDihedralAngleSpinBox->value() );
  mesher->setOutputFileName( prevFileName );
}

/*!
 * \brief MesherWidget::visualizeMesh
 */
void MesherWidget::visualizeMesh()
{
  mainRenderer->RemoveAllViewProps();

  vtkSmartPointer<vtkDataSetMapper> mapper = vtkSmartPointer<vtkDataSetMapper>::New();
  mapper->SetInput( grid );
  mapper->ScalarVisibilityOn();
  mapper->SetScalarModeToUsePointData();

  vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
  actor->SetMapper( mapper );
  actor->GetProperty()->EdgeVisibilityOn();
  mainRenderer->AddActor( actor );
  mainRenderer->ResetCamera();
  ui->qvtkMainWidget->update();
}

/*!
 * \brief MesherWidget::updateMeshStatistics
 */
void MesherWidget::updateMeshStatistics()
{
  QString pointsInfo( QObject::tr( "Mesh consists of " ) + QString::number( grid->GetNumberOfPoints() ) + QObject::tr( " points" ) + "\n" );
  QString tetraedtaInfo( QObject::tr( "Mesh consists of " ) + QString::number( grid->GetNumberOfCells() ) + QObject::tr( " tetraedra" ) + "\n" );
  ui->meshStatisticsTextEdit->setText( pointsInfo + tetraedtaInfo );
}

/*!
 * \brief MesherWidget::startMesherThread
 * \param mesher
 */
void MesherWidget::startMesherThread(std::shared_ptr< kernel::utils::mesh::ViennaMeshMesher> mesher)
{
  assert( mesherTread == nullptr );
  mesherTread = new MesherThread( mesher, this );

  QObject::connect( mesherTread, SIGNAL( meshInitialized() ), this, SLOT( slot_meshInitialized() ) );
  QObject::connect( mesherTread, SIGNAL( meshCreated() ), this, SLOT( slot_meshCreated() ) );
  QObject::connect( mesherTread, SIGNAL( meshWrited() ), this, SLOT( slot_meshWrited() ) );
  QObject::connect( mesherTread, SIGNAL( destroyed() ), this, SLOT( slot_meshTreadDeleted() ) );

  QObject::connect( mesherTread, SIGNAL( finished() ), mesherTread, SLOT( deleteLater() ) );
  QObject::connect( mesherTread, SIGNAL( terminated() ), mesherTread, SLOT( deleteLater() ) );
  mesherTread->start();
}

/*!
 * \brief MesherWidget::on_meshButton_clicked
 */
void MesherWidget::on_meshButton_clicked()
{
  boost::timer::auto_cpu_timer t;
  auto mesher = std::make_shared<kernel::utils::mesh::ViennaMeshMesher>();
  assignGeometryToMesher( mesher );
  setMesherParameters( mesher );
  startMesherThread( mesher );
  ui->meshButton->setEnabled( false );
  ui->terminatePushButton->setEnabled( true );
}

/*!
 * \brief MesherWidget::slot_meshInitialized
 */
void MesherWidget::slot_meshInitialized()
{
  std::cout << "\nMesh initialized\n" << std::endl;
}

/*!
 * \brief MesherWidget::slot_meshCreated
 */
void MesherWidget::slot_meshCreated()
{
  std::cout << "\nMesh Created\n" << std::endl;
}

/*!
 * \brief MesherWidget::slot_meshWrited
 */
void MesherWidget::slot_meshWrited()
{
  std::cout << "\nMesh Writed\n" << std::endl;
  loadGeneratedMesh();
  visualizeMesh();
  updateMeshStatistics();
  ui->acceptPushButton->setEnabled( true );
}

/*!
 * \brief MesherWidget::slot_meshTreadDeleted
 */
void MesherWidget::slot_meshTreadDeleted()
{
  mesherTread = nullptr;
  ui->meshButton->setEnabled( true );
  ui->terminatePushButton->setEnabled( false );
}

/*!
 * \brief MesherWidget::invalidateAccept
 */
void MesherWidget::invalidateAccept()
{
  ui->acceptPushButton->setEnabled( false );
}

/*!
 * \brief MesherWidget::on_terminatePushButton_clicked
 */
void MesherWidget::on_terminatePushButton_clicked()
{
  // you can try to terminate it
  mesherTread->terminate();
}

/*!
 * \brief MesherWidget::on_acceptPushButton_clicked
 */
void MesherWidget::on_acceptPushButton_clicked()
{
  emit sendMeshToGeometry( grid, QString( prevFileName.c_str() ) );
  emit sendMeshToBoundaryConditions( grid );
}
