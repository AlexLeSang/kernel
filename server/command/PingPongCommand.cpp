#include "PingPongCommand.hpp"

#include <cassert>
#include <../utils/ThreadSaveLogger.hpp>

namespace kernel {
  namespace server {
    namespace command {

      bool PingPongCommand(QTcpSocket &socket, std::atomic<bool> &interruptFlag)
      {
        // utils::ThreadSaveLogger::addLineStatic( QString("PingPongCommand") );

        if ( interruptFlag.load() ) {
          utils::ThreadSaveLogger::addLineStatic( QString("PingPongCommand: interrupted") );
          return false;
        }

        quint32 numberOfPackets = 0;
        quint32 packetSize = 0;

        {
          QDataStream in( &socket );
          in.setVersion( QDataStream::Qt_4_8 );
          in >> numberOfPackets;
          in >> packetSize;
          // qWarning() << "numberOfPackets: " << numberOfPackets;
          // qWarning() << "packetSize: " << packetSize;
        }

        for ( quint32 packetCounter = 0; packetCounter < numberOfPackets; ++ packetCounter ) {
          // qWarning() <<  "packetCounter: " << packetCounter;

          if ( interruptFlag.load() ) {
            utils::ThreadSaveLogger::addLineStatic( QString("PingPongCommand: interrupted") );
            return false;
          }

          quint32 receivedPacketCounter = 0;
          QByteArray receivedRandomArr;

          // Receive packed
          {
            // Receive from server
            while ( socket.bytesAvailable() < (qint64)sizeof(quint32) ) {
              if ( (! socket.waitForReadyRead( Timeout ) ) && ( socket.state() != QAbstractSocket::SocketState::ConnectedState ) ) {
                utils::ThreadSaveLogger::addLineStatic( QString("PingPongCommand: ") + socket.errorString() );
                return false;
              }
            }

            quint32 blockSize;
            QDataStream in( &socket );
            in.setVersion( QDataStream::Qt_4_8 );
            in >> blockSize;

            while ( socket.bytesAvailable() < blockSize ) {
              if ( ! socket.waitForReadyRead( Timeout ) ) {
                utils::ThreadSaveLogger::addLineStatic( QString("PingPongCommand: ") + socket.errorString() );
                return false;
              }
            }

            in >> receivedPacketCounter;
            // qWarning() << "receivedPacketCounter: " << receivedPacketCounter;

            in >> receivedRandomArr;
            // qWarning() << "receivedRandomArr.size(): " << receivedRandomArr.size();
          }

          assert( packetCounter == receivedPacketCounter );

          // Send packet back to sender
          {
            // Write to server
            {
              QByteArray block;
              QDataStream out( &block, QIODevice::WriteOnly );
              out.setVersion( QDataStream::Qt_4_8 );
              out << (quint32)0;
              out << receivedPacketCounter;
              out << receivedRandomArr;
              out.device()->seek(0);
              out << (quint32)( block.size() - sizeof(quint32) );

              socket.write( block );

              if ( ! socket.waitForBytesWritten() ) {
                utils::ThreadSaveLogger::addLineStatic( QString("PingPongCommand: ") + socket.errorString() );
                return false;
              }
            }
          }

        }

        return true;
      }
    }
  }
}
