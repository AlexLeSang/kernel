#ifndef VTKACTOREDITORDIALOG_HPP
#define VTKACTOREDITORDIALOG_HPP

#include <QDialog>

#include "vtkSmartPointer.h"
#include "vtkActor.h"

namespace Ui {
  class VTKActorEditorDialog;
}

/*!
 * \brief The VTKActorEditorDialog class
 */
class VTKActorEditorDialog : public QDialog
{
  Q_OBJECT

public:
  explicit VTKActorEditorDialog(QWidget *parent = 0);
  ~VTKActorEditorDialog();

  void setActor(vtkActor* newActorPtr);
  vtkActor* actor();

signals:
  void editingFinished();

protected:
  void changeEvent(QEvent *e);

private slots:
  void on_interpolationComboBox_currentIndexChanged(int index);
  void on_representationComboBox_currentIndexChanged(int index);
  void on_edgeVisibilityCheckBox_clicked(bool checked);
  void on_opacitySlider_valueChanged(int value);
  void restoreAndReject();

private:
  Ui::VTKActorEditorDialog *ui; 

  vtkSmartPointer< vtkRenderer > renderer;
  vtkActor* actorPtr;
  vtkSmartPointer< vtkProperty > oldPropetry;
};

#endif // VTKACTOREDITORDIALOG_HPP
