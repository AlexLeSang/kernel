#ifndef DirihletBoundaryCondition_HPP
#define DirihletBoundaryCondition_HPP

#include <boundary_condition/AbstractBoundaryCondition.hpp>
#include <QDataStream>

/*!
 * \brief The DirihletBoundaryCondition class
 */
class DirihletBoundaryCondition : public AbstractBoundaryCondition
{
public:
  DirihletBoundaryCondition() : AbstractBoundaryCondition( BOUNDARY_CONDITION_TYPE::DIRICHLET_BOUNDARY_CONDITION ), condition( 0.0 ) {}
  bool operator==(const AbstractBoundaryCondition& rhs) const;
  bool operator!=(const AbstractBoundaryCondition& rhs) const;

  virtual double appyBoundaryCondition(const bgeot::base_node&) const;
  double getCondition() const;
  void setCondition(double value);

private:
  double condition;
};

QDataStream &operator<<(QDataStream &ds, const DirihletBoundaryCondition &bc);
QDataStream &operator>>(QDataStream &ds, DirihletBoundaryCondition &bc);


namespace test {
  bool DirihletBoundaryConditionSerializationTest(int argc, char *argv[]);
}

#endif // DirihletBoundaryCondition_HPP
