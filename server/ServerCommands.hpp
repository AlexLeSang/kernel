#ifndef SERVERCOMMANDS_HPP
#define SERVERCOMMANDS_HPP

#include <QString>
#include <QtNetwork/QTcpSocket>
#include <QObject>
#include <QDebug>

namespace kernel {
  namespace server {

    const quint32 Timeout = 100; // 0.1 sec

    /*!
     * \brief The ServerCommands enum
     */
    enum struct ServerCommands : quint32
    {
      FRAMEWORK_TEST_COMMAND = 0,
      STOP_SERVER = 1,
      GET_SERVER_STATUS = 2,
      PING_PONG = 3,

      // TEST_TIME, // Test command, prints time every 1 ms and send to the client
      // SERVER_APPEARS,
      // Math command
      // CONJUGATE_GRADIENT,

      // ACKNOWLEDGE,
      // ARGUMENTS,
      // RESULTS,
      // STILL_ALIVE
    };
  }
}

#endif // SERVERCOMMANDS_HPP
