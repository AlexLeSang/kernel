#ifndef VIENNACLCONTEXTTOOLS_HPP
#define VIENNACLCONTEXTTOOLS_HPP

#include <iostream>

#include <viennacl/scalar.hpp>
#include <viennacl/vector.hpp>

#include <viennacl/matrix.hpp>
#include <viennacl/matrix_proxy.hpp>

#include <viennacl/linalg/prod.hpp>
#include <viennacl/linalg/lu.hpp>

#include <viennacl/ocl/platform.hpp>
#include <viennacl/ocl/kernel.hpp>

#include <viennacl/ocl/device.hpp>
#include <viennacl/ocl/context.hpp>


#include <boost/timer/timer.hpp>

//#include <mongo/bson/bson.h>

namespace kernel {
  namespace server {
    namespace functions {
      namespace node {

        namespace opencl {

          inline void init()
          {
            static bool init = false;
            if (!init)
            {
              srand( (unsigned int)time(NULL) );
              init = true;
            }
          }

          template<class T>
          T random();

          template<>
          inline double random<double>()
          {
            init();
            return static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
          }

          template<>
          inline float random<float>()
          {
            init();
            return static_cast<float>(random<double>());
          }


#ifdef _WIN32

#define WINDOWS_LEAN_AND_MEAN
#include <windows.h>
#undef min
#undef max

          class Timer
          {
          public:

            Timer()
            {
              QueryPerformanceFrequency(&freq);
            }

            void start()
            {
              QueryPerformanceCounter((LARGE_INTEGER*) &start_time);
            }

            double get() const
            {
              LARGE_INTEGER  end_time;
              QueryPerformanceCounter((LARGE_INTEGER*) &end_time);
              return (static_cast<double>(end_time.QuadPart) - static_cast<double>(start_time.QuadPart)) / static_cast<double>(freq.QuadPart);
            }


          private:
            LARGE_INTEGER freq;
            LARGE_INTEGER start_time;
          };

#else

#include <sys/time.h>

          class Timer
          {
          public:

            Timer() : ts(0)
            {}

            void start()
            {
              struct timeval tval;
              gettimeofday(&tval, NULL);
              ts = static_cast<double>(tval.tv_sec * 1000000 + tval.tv_usec);
            }

            double get() const
            {
              struct timeval tval;
              gettimeofday(&tval, NULL);
              double end_time = static_cast<double>(tval.tv_sec * 1000000 + tval.tv_usec);

              return static_cast<double>(end_time-ts) / 1000000.0;
            }

          private:
            double ts;
          };


#endif




          template < typename OStream >
          inline void print_device_information(OStream& stream, const std::vector< viennacl::ocl::device>& devices, const viennacl::ocl::device& dRef)
          {
            stream << "  No.:              " << std::distance( devices.begin(), std::find(devices.begin(), devices.end(), dRef) ) << std::endl;
            stream << "  Name:             " << dRef.name() << std::endl;
            stream << "  Compute Units:    " << dRef.max_compute_units() << std::endl;
            stream << "  Workgroup Size:   " << dRef.max_work_group_size() << std::endl;
            stream << "  Global Memory:    " << dRef.global_mem_size()/(1024*1024) << " MB" << std::endl;
            stream << "  Local Memory:     " << dRef.local_mem_size()/1024 << " KB" << std::endl;
            stream << "  Max-alloc Memory: " << dRef.max_mem_alloc_size()/(1024*1024) << " MB" << std::endl;
            stream << "  Double Support:   " << dRef.double_support() << std::endl;
            stream << "  Driver Version:   " << dRef.driver_version() << std::endl;
            stream << std::endl;
          }


          template< typename OStream >
          inline void print_devices_for_current_context(OStream& stream)
          {
            const auto& context = viennacl::ocl::current_context();
            stream << "Current context devices:" << std::endl;
            const auto& devices = context.devices();
            std::for_each( devices.begin(), devices.end(), [&] ( const viennacl::ocl::device &dRef) {
              print_device_information( stream, devices, dRef );
            } );
          }

          // mongo::BSONObj print_device_information_to_bson(const std::vector< viennacl::ocl::device>& devices, const viennacl::ocl::device& dRef);

          // std::vector<mongo::BSONObj> print_devices_for_current_context_to_bson();

          const long ALL_GPU_DEVICES_CONTEXT = 128;
		  const long ALL_CPU_DEVICES_CONTEXT = 256;
          const long ALL_DEVICES_CONTEXT = 512;

          bool create_all_gpu_devices_context();

          bool create_all_cpu_devices_context();

          bool create_all_devices_context();

          template < typename Scalar >
          inline void benchmark(const long MATRIX_SIZE = 2048)
          {
            // const long MATRIX_SIZE = 4096;
            // const long MATRIX_SIZE = 2048;
            // const long MATRIX_SIZE = 1024;
            // const long MATRIX_SIZE = 512;
            Timer timer;
            double exec_time;

            std::vector< Scalar > stl_A( MATRIX_SIZE * MATRIX_SIZE );
            std::vector< Scalar > stl_B( MATRIX_SIZE * MATRIX_SIZE );
            std::vector< Scalar > stl_C( MATRIX_SIZE * MATRIX_SIZE );

            for (unsigned int i = 0; i < MATRIX_SIZE; ++i)
              for (unsigned int j = 0; j < MATRIX_SIZE; ++j)
                stl_A[ i*MATRIX_SIZE + j ] = random< Scalar >();

            for (unsigned int i = 0; i < MATRIX_SIZE; ++i)
              for (unsigned int j = 0; j < MATRIX_SIZE; ++j)
                stl_B[i + j*MATRIX_SIZE ] = random< Scalar >();


            viennacl::matrix< Scalar > vcl_A( MATRIX_SIZE, MATRIX_SIZE );
            viennacl::matrix< Scalar > vcl_B( MATRIX_SIZE, MATRIX_SIZE );
            viennacl::matrix< Scalar > vcl_C( MATRIX_SIZE, MATRIX_SIZE );


            std::cout << " ------ Benchmark Matrix-Matrix product ------ " << std::endl;

            viennacl::fast_copy( &(stl_A[0]), &(stl_A[0]) + stl_A.size(), vcl_A );
            viennacl::fast_copy( &(stl_B[0]), &(stl_B[0]) + stl_B.size(), vcl_B );
            vcl_C = viennacl::linalg::prod( vcl_A, vcl_B );
            viennacl::backend::finish();
            timer.start();
            vcl_C = viennacl::linalg::prod( vcl_A, vcl_B );
            viennacl::backend::finish();
            exec_time = timer.get();
            std::cout << " - Execution time on device (no setup time included): " << exec_time << std::endl;

            std::cout << " - GFLOPs (counting multiply&add as separate operations): " <<
                         2.0 * (vcl_A.size1() / 1000.0) *
                         (vcl_A.size2() / 1000.0) *
                         (vcl_B.size2() / 1000.0) / exec_time << std::endl;

            std::cout << std::endl;

            std::cout << " ------ Benchmark LU factorization ------ " << std::endl;

            viennacl::fast_copy( &(stl_A[0]), &(stl_A[0]) + stl_A.size(), vcl_A );
            viennacl::linalg::lu_factorize(vcl_A);
            viennacl::backend::finish();
            timer.start();
            viennacl::linalg::lu_factorize(vcl_A);
            viennacl::backend::finish();
            exec_time = timer.get();
            std::cout << " - Execution time on device (no setup time included): " << exec_time << std::endl;

            std::cout << " - GFLOPs (counting multiply&add as separate operations): " <<
                         2.0 * (vcl_A.size1() / 1000.0) *
                         (vcl_A.size2() / 1000.0) *
                         (vcl_A.size2() / 1000.0) / exec_time << std::endl;

            std::cout << std::endl;

          }

          /*!
           * \brief print_platforms
           */
          void print_platforms();
        }


      }
    }
  }
}



#include <iostream>

#include <thread>
#include <future>
#include <chrono>

namespace test {
  bool ViennaCLContextMultithreadTest();
  bool ViennaCLContextTest();
  // bool ViennaCLContextDescriptionToBSON();
}


#endif // VIENNACLCONTEXTTOOLS_HPP
