#ifndef VTKACTORIDELEGATE_HPP
#define VTKACTORIDELEGATE_HPP

#include <QItemDelegate>

/*!
 * \brief The VTKActorDelegate class
 */
class VTKActorDelegate : public QItemDelegate
{
  Q_OBJECT
public:
  explicit VTKActorDelegate(QObject *parent = 0) : QItemDelegate( parent ) {}
  QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &/*option*/, const QModelIndex &/*index*/) const;
  void setEditorData(QWidget *editor, const QModelIndex &index) const;
  void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

private slots:
  void commitAndCloseEditor();

};

#endif // VTKACTORIDELEGATE_HPP
