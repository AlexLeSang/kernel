#ifndef MESHERTHREAD_HPP
#define MESHERTHREAD_HPP

#include <QObject>
#include <QThread>
#include <../utils/mesh/ViennaMeshMesher.hpp>

/*!
 * \brief The MesherThread class
 */
class MesherThread : public QThread
{
  Q_OBJECT
public:
  MesherThread(std::shared_ptr< kernel::utils::mesh::ViennaMeshMesher > mesher, QObject* parent = nullptr);

signals:
  void meshInitialized();
  void meshCreated();
  void meshWrited();

private:
  void run();

private:
  std::shared_ptr< kernel::utils::mesh::ViennaMeshMesher > mesher;
};

#endif // MESHERTHREAD_HPP
