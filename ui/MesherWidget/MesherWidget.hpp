#ifndef MESHERWIDGET_HPP
#define MESHERWIDGET_HPP

#include <QWidget>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkDataSetMapper.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkDataSetSurfaceFilter.h>

#include <boost/timer/timer.hpp>

#include <../utils/mesh/ViennaMeshMesher.hpp>
#include <MesherThread.hpp>

namespace Ui {
  class MesherWidget;
}

/*!
 * \brief The MesherWidget class
 */
class MesherWidget : public QWidget
{
  Q_OBJECT

public:
  explicit MesherWidget(QWidget *parent = 0);
  ~MesherWidget();

  vtkSmartPointer<vtkPolyData> getGeometry() const;
  void setGeometry(const vtkSmartPointer<vtkPolyData> &value);

signals:
  void sendMeshToGeometry(vtkSmartPointer<vtkUnstructuredGrid>, const QString);
  void sendMeshToBoundaryConditions(vtkSmartPointer<vtkUnstructuredGrid>);

public slots:
  void setGeometryToMesher(vtkSmartPointer<vtkPolyData> polyData, const QString fileName);

protected:
  void changeEvent(QEvent *e);

private slots:
  void updateMeshStatistics();
  void on_meshButton_clicked();

  void slot_meshInitialized();
  void slot_meshCreated();
  void slot_meshWrited();
  void slot_meshTreadDeleted();
  void invalidateAccept();

  void on_terminatePushButton_clicked();

  void on_acceptPushButton_clicked();

private:
  const bool enhancingIsPossible();
  void assignGeometryToMesher(std::shared_ptr<kernel::utils::mesh::ViennaMeshMesher> mesher);
  void setMesherParameters(std::shared_ptr<kernel::utils::mesh::ViennaMeshMesher> mesher);
  void startMesherThread(std::shared_ptr<kernel::utils::mesh::ViennaMeshMesher> mesher);
  void loadGeneratedMesh();
  void visualizeMesh();

private:
  Ui::MesherWidget *ui;
  vtkSmartPointer< vtkRenderer > mainRenderer;
  vtkSmartPointer< vtkRenderer > additionalRenderer;

  vtkSmartPointer< vtkPolyData > geometry;

  vtkSmartPointer< vtkUnstructuredGrid > grid;

  double prevMaxRadiusEdgeRatio;
  double prevCellSize;
  std::string prevFileName;

  MesherThread* mesherTread;
};

#endif // MESHERWIDGET_HPP
