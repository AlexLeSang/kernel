#ifndef CREATEBOUNDARYCONDITIONDIALOG_HPP
#define CREATEBOUNDARYCONDITIONDIALOG_HPP

#include <QDialog>
#include <QColor>
#include <QPalette>

class AbstractTreeItem;

namespace Ui {
  class CreateBoundaryConditionDialog;
}


/*!
 * \brief The CreateBoundaryConditionDialog class
 */
class CreateBoundaryConditionDialog : public QDialog
{
  Q_OBJECT

public:
  explicit CreateBoundaryConditionDialog(QWidget *parent = 0, AbstractTreeItem* item = 0);
  ~CreateBoundaryConditionDialog();

  AbstractTreeItem* treeItem();
  void setTreeItem(AbstractTreeItem* item);

  QColor getColor() const;
  void setColor(const QColor &value);

protected:
  void changeEvent(QEvent *e);

private:
  void applyPickedColor();
  void showNeumannUI();
  void hideNeumannUI();

private slots:
  void on_typeComboBox_activated(int index);
  void on_invertPushButton_clicked();
  void on_colorPushButton_clicked();

private:
  Ui::CreateBoundaryConditionDialog *ui;

  QColor color;
};


bool CreateBoundaryConditionDialogTest(int argc, char *argv[]);


#endif // CREATEBOUNDARYCONDITIONDIALOG_HPP
