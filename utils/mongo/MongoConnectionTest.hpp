#ifndef MONGOCONNECTIONTEST_HPP
#define MONGOCONNECTIONTEST_HPP

#include <iostream>

#include <mongo/client/dbclient.h>
#include <thread>
#include <future>
#include <memory>
#include <chrono>
#include <sstream>


namespace test {

  bool mongotest();
  bool namedFileGridFSTest();
  bool bufferGridFSTest(int argc, char * argv[]);
  bool classTest(int argc, char * argv[]);
}

#endif // MONGOCONNECTIONTEST_HPP
