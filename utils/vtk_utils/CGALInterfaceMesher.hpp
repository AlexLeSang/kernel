#ifndef CGALINTERFACEMESHER_HPP
#define CGALINTERFACEMESHER_HPP

#include <map>
#include <cassert>

#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkType.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>


namespace CGAL {

  template <typename CDT>
  vtkPolyData* output_interface_X_to_vtk_polydata(const CDT& cdt, const double x = 0.0, vtkPolyData* polydata = 0)
  {
    typedef typename CDT::Vertex_handle Vertex_handle;

    vtkPoints* const vtk_points = vtkPoints::New();
    vtkCellArray* const vtk_cells = vtkCellArray::New();

    vtk_points->Allocate(cdt.number_of_vertices());
    vtk_cells->Allocate(cdt.number_of_faces());

    std::map<Vertex_handle, vtkIdType> V;
    vtkIdType inum = 0;

    for (auto vit = cdt.finite_vertices_begin(), end = cdt.finite_vertices_end(); vit != end; ++vit) {
      typedef typename CDT::Point Point;
      const Point& p = vit->point();
      vtk_points->InsertNextPoint( x, p.x(), p.y() );
      V[vit] = inum++;
    }

    for (auto fit = cdt.finite_faces_begin(), end = cdt.finite_faces_end(); fit != end; ++fit) {
      if( fit->is_in_domain() ) {
        vtkIdType cell[3];
        for ( auto i = 0; i < 3; ++ i ) {
          cell[ i ] = V[ (*fit).vertex(i) ];
        }
        vtk_cells->InsertNextCell(3, cell);
      }
    }
    if(!polydata) {
      polydata = vtkPolyData::New();
    }

    polydata->SetPoints(vtk_points);
    vtk_points->Delete();

    polydata->SetPolys(vtk_cells);
    vtk_cells->Delete();
    return polydata;
  }


  template <typename CDT>
  vtkPolyData* output_interface_Y_to_vtk_polydata(const CDT& cdt, const double y = 0.0, vtkPolyData* polydata = 0)
  {
    typedef typename CDT::Vertex_handle Vertex_handle;

    vtkPoints* const vtk_points = vtkPoints::New();
    vtkCellArray* const vtk_cells = vtkCellArray::New();

    vtk_points->Allocate(cdt.number_of_vertices());
    vtk_cells->Allocate(cdt.number_of_faces());

    std::map<Vertex_handle, vtkIdType> V;
    vtkIdType inum = 0;

    for (auto vit = cdt.finite_vertices_begin(), end = cdt.finite_vertices_end(); vit != end; ++vit) {
      typedef typename CDT::Point Point;
      const Point& p = vit->point();
      vtk_points->InsertNextPoint( p.x(), y, p.y() );
      V[vit] = inum++;
    }

    for (auto fit = cdt.finite_faces_begin(), end = cdt.finite_faces_end(); fit != end; ++fit) {
      if( fit->is_in_domain() ) {
        vtkIdType cell[3];
        for ( auto i = 0; i < 3; ++ i ) {
          cell[ i ] = V[ (*fit).vertex(i) ];
        }
        vtk_cells->InsertNextCell(3, cell);
      }
    }
    if(!polydata) {
      polydata = vtkPolyData::New();
    }

    polydata->SetPoints(vtk_points);
    vtk_points->Delete();

    polydata->SetPolys(vtk_cells);
    vtk_cells->Delete();
    return polydata;
  }


  template <typename CDT>
  vtkPolyData* output_interface_Z_to_vtk_polydata(const CDT& cdt, const double z = 0.0, vtkPolyData* polydata = 0)
  {
    typedef typename CDT::Vertex_handle Vertex_handle;

    vtkPoints* const vtk_points = vtkPoints::New();
    vtkCellArray* const vtk_cells = vtkCellArray::New();

    vtk_points->Allocate(cdt.number_of_vertices());
    vtk_cells->Allocate(cdt.number_of_faces());

    std::map<Vertex_handle, vtkIdType> V;
    vtkIdType inum = 0;

    for (auto vit = cdt.finite_vertices_begin(), end = cdt.finite_vertices_end(); vit != end; ++vit) {
      typedef typename CDT::Point Point;
      const Point& p = vit->point();
      vtk_points->InsertNextPoint( p.x(), p.y(), z );
      V[vit] = inum++;
    }

    for (auto fit = cdt.finite_faces_begin(), end = cdt.finite_faces_end(); fit != end; ++fit) {
      if( fit->is_in_domain() ) {
        vtkIdType cell[3];
        for ( auto i = 0; i < 3; ++ i ) {
          cell[ i ] = V[ (*fit).vertex(i) ];
        }
        vtk_cells->InsertNextCell(3, cell);
      }
    }
    if(!polydata) {
      polydata = vtkPolyData::New();
    }

    polydata->SetPoints(vtk_points);
    vtk_points->Delete();

    polydata->SetPolys(vtk_cells);
    vtk_cells->Delete();
    return polydata;
  }

} // end namespace CGAL


template < typename CDT >
void print_cdt(const CDT& cdt)
{
  std::cout << "Number of vertices: " << cdt.number_of_vertices() << std::endl;
  for ( auto it = cdt.vertices_begin(); it != cdt.vertices_end(); ++ it ) {
    auto vertex = *it;
    std::cout << vertex << std::endl;
  }
  std::cout << std::endl;

  std::cout << "Number of faces: " << cdt.number_of_faces() << std::endl;
  for ( auto it = cdt.faces_begin(); it != cdt.faces_end(); ++ it) {
    std::cout << "*(it->vertex( 0 )): " << *(it->vertex( 0 )) << std::endl;
    std::cout << "*(it->vertex( 1 )): " << *(it->vertex( 1 )) << std::endl;
    std::cout << "*(it->vertex( 2 )): " << *(it->vertex( 2 )) << std::endl;

    std::cout << std::endl;
  }
}

namespace kernel {
  namespace utils {
    namespace vtk_utils {

      // TODO mesh vtkPolyData interface with a defined parameters
      vtkSmartPointer< vtkPolyData > triangulate_vtk_polydata_interfaseX(vtkPolyData* interfase, const double B = 0.125, const double S = 0.0);

      vtkSmartPointer< vtkPolyData > triangulate_vtk_polydata_interfaseY(vtkPolyData* interfase, const double B = 0.125, const double S = 0.0);

      vtkSmartPointer< vtkPolyData > triangulate_vtk_polydata_interfaseZ(vtkPolyData* interfase, const double B = 0.125, const double S = 0.0);

      std::vector< std::tuple< double, double > >
      find_seeds_X(vtkSmartPointer< vtkPolyData > boundaryEdges);

      std::vector< std::tuple< double, double > >
      find_seeds_Y(vtkSmartPointer< vtkPolyData > boundaryEdges);

      std::vector< std::tuple< double, double > >
      find_seeds_Z(vtkSmartPointer< vtkPolyData > boundaryEdges);

    }
  }
}


namespace test {
  bool CGALMeshInterfaceTest();
}

#endif // CGALINTERFACEMESHER_HPP
