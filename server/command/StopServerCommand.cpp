#include "StopServerCommand.hpp"

#include "ThreadSaveLogger.hpp"

namespace kernel {
  namespace server {
    namespace command {

      /*!
       * \brief StopServerCommand
       * \return
       */
      bool StopServerCommand(QTcpSocket &, std::atomic<bool> &)
      {
        utils::ThreadSaveLogger::addLineStatic( QString("StopServerCommand") );
        KernelServer::getInstance()->stopServer();
        return true;
      }
    }
  }
}
