#ifndef CGILDTSLAESOLVER_HPP
#define CGILDTSLAESOLVER_HPP

#include <slae_solvers/AbstractSLAESolver.hpp>

class CgILDTSLAESolver : public AbstractSLAESolver
{
  // AbstractSLAESolver interface
public:
  virtual bool operator ()(getfem::model &model, gmm::iteration &iter);
};

#endif // CGILDTSLAESOLVER_HPP
