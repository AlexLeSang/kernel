#include "ViennaMeshMesher.hpp"


#include <vtkSmartPointer.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkSTLReader.h>
#include <vtkPolyData.h>
#include <vtkIdList.h>
#include <vtkCellArray.h>

#include <MeshAppender.hpp>

#include <boost/timer/timer.hpp>

namespace kernel {
  namespace utils {
    namespace mesh {

      /*!
       * \brief ViennaMeshMesher::ViennaMeshMesher
       */
      ViennaMeshMesher::ViennaMeshMesher() : maxRadiusEdgeRatio( 1.5 ), minDihedralAngle( 0.17 ),
        mesher( new viennamesh::tetgen::make_mesh() ),
        writer( new viennamesh::io::mesh_writer() )
      {
        geometryHandle = viennamesh::make_parameter<GeometryMeshType>();

        mesher->set_input( "mesh", geometryHandle );

        writer->set_default_source( mesher );
      }

      /*!
       * \brief ViennaMeshMesher::getGeometry
       * \return
       */
      vtkSmartPointer<vtkPolyData> ViennaMeshMesher::getGeometry() const
      {
        return geometry;
      }

      /*!
       * \brief ViennaMeshMesher::setGeometry
       * \param value
       */
      void ViennaMeshMesher::setGeometry(const vtkSmartPointer<vtkPolyData> &value)
      {
        assert( value );
        assert( value->GetNumberOfPoints() );
        assert( value->GetNumberOfCells() );
        geometry = value;
      }

      /*!
       * \brief ViennaMeshMesher::init
       */
      void ViennaMeshMesher::init()
      {
        fillGeometry();
        setMeshParameters();
        setOutputParameters();
      }

      /*!
       * \brief ViennaMeshMesher::createMesh
       */
      void ViennaMeshMesher::createMesh()
      {
        try {
          mesher->run();
        }
        catch (std::exception& e) {
          std::cerr << "ViennaMeshMesher::createMesh: std mesher exception: " << e.what() << std::endl;
        }
        catch (int& e) {
          std::cerr << "ViennaMeshMesher::createMesh: int mesher exception: " << e << std::endl;
        }
        catch(...) {
          std::cerr << "Catch something" << std::endl;
        }
      }

      /*!
       * \brief ViennaMeshMesher::writeMesh
       */
      void ViennaMeshMesher::writeMesh()
      {
        writer->run();
      }

      /*!
       * \brief ViennaMeshMesher::getCellSize
       * \return
       */
      double ViennaMeshMesher::getCellSize() const
      {
        return cellSize;
      }

      /*!
       * \brief ViennaMeshMesher::setCellSize
       * \param value
       */
      void ViennaMeshMesher::setCellSize(double value)
      {
        cellSize = value;
      }

      /*!
       * \brief ViennaMeshMesher::getMaxRadiusEdgeRatio
       * \return
       */
      double ViennaMeshMesher::getMaxRadiusEdgeRatio() const
      {
        return maxRadiusEdgeRatio;
      }

      /*!
       * \brief ViennaMeshMesher::setMaxRadiusEdgeRatio
       * \param value
       */
      void ViennaMeshMesher::setMaxRadiusEdgeRatio(double value)
      {
        maxRadiusEdgeRatio = value;
      }

      /*!
       * \brief ViennaMeshMesher::getMinDihedralAngle
       * \return
       */
      double ViennaMeshMesher::getMinDihedralAngle() const
      {
        return minDihedralAngle;
      }

      /*!
       * \brief ViennaMeshMesher::setMinDihedralAngle
       * \param value
       */
      void ViennaMeshMesher::setMinDihedralAngle(double value)
      {
        minDihedralAngle = value;
      }

      /*!
       * \brief ViennaMeshMesher::getOutputFileName
       * \return
       */
      std::string ViennaMeshMesher::getOutputFileName() const
      {
        return outputFileName;
      }

      /*!
       * \brief ViennaMeshMesher::setOutputFileName
       * \param value
       */
      void ViennaMeshMesher::setOutputFileName(const std::string &value)
      {
        outputFileName = value;
      }

      /*!
       * \brief ViennaMeshMesher::fillGeometry
       */
      void ViennaMeshMesher::fillGeometry()
      {
        GeometryMeshType& meshGeometry = geometryHandle();

        const auto numberOfPoints = geometry->GetNumberOfPoints();
        const auto numberOfTriangles = geometry->GetNumberOfCells();

        std::vector< GeometryVertexHandle > vertices;
        vertices.reserve( numberOfPoints );
        for ( auto i = 0; i < numberOfPoints; ++ i ) {
          double p[ 3 ];
          geometry->GetPoint( i, p );
          GeometryVertexHandle handle = viennagrid::make_vertex( meshGeometry, PointType( p [ 0 ], p[ 1 ], p[ 2 ] ) );
          vertices.push_back( handle );
        }

        for ( auto c = 0; c < numberOfTriangles; ++c ) {
          vtkSmartPointer< vtkIdList > cellPoints = vtkIdList::New();
          geometry->GetCellPoints( c, cellPoints );
          assert( cellPoints->GetNumberOfIds() == 3 );
          GeometryVertexHandle vh0 = vertices[ cellPoints->GetId( 0 ) ];
          GeometryVertexHandle vh1 = vertices[ cellPoints->GetId( 1 ) ];
          GeometryVertexHandle vh2 = vertices[ cellPoints->GetId( 2 ) ];

          GeometryLineHandle lines[ 3 ];
          lines[ 0 ] = viennagrid::make_line( meshGeometry, vh0, vh1 );
          lines[ 1 ] = viennagrid::make_line( meshGeometry, vh1, vh2 );
          lines[ 2 ] = viennagrid::make_line( meshGeometry, vh2, vh0 );
          viennagrid::make_plc( meshGeometry, lines + 0, lines + 3 );
        }

      }

      /*!
       * \brief ViennaMeshMesher::setMeshParameters
       */
      void ViennaMeshMesher::setMeshParameters()
      {
        mesher->set_input( "cell_size", cellSize );
        mesher->set_input( "max_radius_edge_ratio", maxRadiusEdgeRatio );
        mesher->set_input( "min_dihedral_angle", minDihedralAngle );
      }

      /*!
       * \brief ViennaMeshMesher::setOutputParameters
       */
      void ViennaMeshMesher::setOutputParameters()
      {
        assert( !outputFileName.empty() );
        writer->set_input( "filename", outputFileName.c_str() );
      }

    }
  }
}

#include <vtkUnstructuredGrid.h>
#include <vtkMeshQuality.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkDoubleArray.h>
#include <vtkCellData.h>
#include <vtkCleanPolyData.h>
#include <vtkFeatureEdges.h>

void MeasureGridQuality(vtkSmartPointer< vtkUnstructuredGrid > grid)
{
  {
    vtkSmartPointer<vtkMeshQuality> qualityFilter = vtkSmartPointer<vtkMeshQuality>::New();
    qualityFilter->SetInputConnection( grid->GetProducerPort() );
    qualityFilter->SetTetQualityMeasureToShapeAndSize();
    qualityFilter->Update();
    vtkSmartPointer<vtkDoubleArray> qualityArray = vtkDoubleArray::SafeDownCast( qualityFilter->GetOutput()->GetCellData()->GetArray("Quality") );

    std::cout << "\n===============================================================================\n" << std::endl;
    std::cout << "grid->GetNumberOfPoints(): " << grid->GetNumberOfPoints() << std::endl;
    std::cout << "grid->GetNumberOfCells(): " << grid->GetNumberOfCells() << std::endl;
    std::cout << "There are " << qualityArray->GetNumberOfTuples() << " values." << std::endl;
    for(vtkIdType i = 0; i < qualityArray->GetNumberOfTuples(); i++) {
      double val = qualityArray->GetValue(i);
      std::cout << "value " << i << " : " << val << std::endl;
    }
  }
}

void AnalyzeCreatedMesh(const std::string& destinationFileName)
{
  vtkSmartPointer< vtkXMLUnstructuredGridReader > reader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
  reader->SetFileName( destinationFileName.c_str() );
  reader->Update();

  vtkSmartPointer< vtkUnstructuredGrid > grid = reader->GetOutput();
  assert( grid->GetNumberOfPoints() );
  assert( grid->GetNumberOfCells() );

  test::VisualizeVtkUnstructutedGrid( grid );
  // MeasureGridQuality( grid );
}

bool test::ViennaMeshMesherTest(int argc, char **argv)
{
  assert( argc == 4 );

  std::cout << "ViennaMeshMesherTest" << std::endl;

  const std::string sourceFile = argv[ 1 ];
  const auto cellSize = atof( argv[ 2 ] );
  const std::string destinationFileName = argv[ 3 ];

  //  vtkSmartPointer< vtkXMLPolyDataReader > reader = vtkSmartPointer< vtkXMLPolyDataReader >::New();
  vtkSmartPointer< vtkSTLReader > reader = vtkSmartPointer< vtkSTLReader >::New();
  reader->SetFileName( sourceFile.c_str() );
  reader->Update();
  vtkSmartPointer< vtkCleanPolyData > cleanPolyData =  vtkSmartPointer< vtkCleanPolyData >::New();
  cleanPolyData->SetInputConnection( reader->GetOutputPort()  );
  cleanPolyData->ConvertLinesToPointsOn();
  cleanPolyData->ConvertPolysToLinesOn();
  cleanPolyData->ConvertStripsToPolysOn();
  cleanPolyData->Update();

  vtkSmartPointer< vtkPolyData >  polydata = cleanPolyData->GetOutput();

  using namespace kernel::utils::mesh;
  ViennaMeshMesher mesher;
  mesher.setGeometry( polydata );
  mesher.setCellSize( cellSize );
  mesher.setOutputFileName( destinationFileName );

  mesher.init();
  mesher.createMesh();
  mesher.writeMesh();

  //  AnalyzeCreatedMesh( destinationFileName );

  std::cout << "ViennaMeshMesherTest end" << std::endl;
  return true;
}
