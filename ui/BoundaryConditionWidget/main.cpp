#include <QApplication>
#include <BoundaryConditionWidget.hpp>

#include <VisibleSelectionInteractorStyle.hpp>
#include <TreeModel.hpp>
#include <CreateBoundaryConditionDialog.hpp>

// Main
/*
int main(int argc, char *argv[])
{
  QApplication a( argc, argv );
  return a.exec();
}
*/

// VTK test
int main(int argc, char *argv[])
{
  //  if ( ! VisibleSelectionInteractorStyleTest( argc, argv ) ) { return EXIT_FAILURE; }
  //  if ( ! TreeModelTest( argc, argv ) ) { return EXIT_FAILURE; }
  //  if ( ! CreateBoundaryConditionDialogTest( argc, argv ) ) { return EXIT_FAILURE; }
  if ( ! BoundaryConditionWidgetTest( argc, argv ) ) { return EXIT_FAILURE; }
  return EXIT_SUCCESS;
}
