#ifndef ABSTRACTSLAESOLVER_HPP
#define ABSTRACTSLAESOLVER_HPP

#include <getfem/getfem_model_solvers.h>

enum SOLVER_TYPE
{
  STANDARD = 0,
  CG_ILDT = 1,
  GMRES_ILU = 2,
  GMRES_ILUT = 3,
  GMRES_ILUTP = 4,
  clCG = 5
};

class AbstractSLAESolver
{
public:
  virtual bool operator()(getfem::model& model, gmm::iteration& iter) = 0;
  virtual ~AbstractSLAESolver() {}
};

#endif // ABSTRACTSLAESOLVER_HPP
