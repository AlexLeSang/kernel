#ifndef VTKPOLYDATASLICER_HPP
#define VTKPOLYDATASLICER_HPP

#include <tuple>

#include <vtkSmartPointer.h>
#include <vtkClipPolyData.h>
#include <vtkFloatArray.h>
#include <vtkReverseSense.h>
#include <vtkAppendPolyData.h>
#include <vtkPlane.h>
#include <vtkCutter.h>

#include <vtk_includes/vtkContourTriangulator.hpp>

namespace kernel {
  namespace utils {
    namespace vtk_utils {

      enum CLIP_COORDINATE
      {
        X_CLIP,
        Y_CLIP,
        Z_CLIP
      };

      /*!
       * \brief SliceVTKPolyData
       * \param inputPolyData
       * \param clipCoordinate
       * \param center
       * \return X:(interface, right, left), Y:(interface, above, below), Z:(interface, front, back)
       */
      std::tuple< vtkSmartPointer< vtkPolyData > /*interface*/, vtkSmartPointer< vtkPolyData > /*part*/, vtkSmartPointer< vtkPolyData > /*part*/ >
      SliceVTKPolyData(vtkSmartPointer< vtkPolyData > inputPolyData, CLIP_COORDINATE clipCoordinate, double* center = nullptr);
    }
  }
}


namespace test {
  bool VTKSlicerXTest();
  bool VTKSlicerYTest();
  bool VTKSlicerZTest();
  bool VTKSlicerXYZTest();
  bool VTKSlicerXYZTestGeometry();
  bool VTKSlicerXYZTestLargeGeometry();
  bool VTKSlicerExtractBoundaryEdges();
  bool VTKComplexInterface();
  bool SliceAndSave(int argc, char* argv[]);
}

#endif // VTKPOLYDATASLICER_HPP
