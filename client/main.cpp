///* The network communication test
#include <QCoreApplication>
#include <QTime>

#include <tuple>
#include <functional>
#include <algorithm>
#include <vector>
#include <iterator>

#include "KernelClient.hpp"

#include "ClientCommand.hpp"
#include "StopServerClientCommand.hpp"
#include "ServerStatusClientCommand.hpp"
#include "PingPongClientCommand.hpp"


int main(int argc, char *argv[])
{
  Q_UNUSED(argc);
  Q_UNUSED(argv);

  QTime timer;
  timer.start();

  // TODO test set
  // if ( ! test::PingPongPingPongResult() ) { return EXIT_FAILURE; }
  // if ( ! test::FrameworkTestCommandTest() ) { return EXIT_FAILURE; }
  // if ( ! test::PingPongClientCommandTest() ) { return EXIT_FAILURE; }
  // if ( ! test::ParallelPingPongCommandTest() ) { return EXIT_FAILURE; }
  if ( ! test::ServerStatusClientCommandTest() ) { return EXIT_FAILURE; }

  qWarning() << "Total time: " << timer.elapsed() << " ms";

  return EXIT_SUCCESS;
}
//*/


