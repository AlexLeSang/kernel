#ifndef RANDOMUTILS_HPP
#define RANDOMUTILS_HPP

#include <random>
#include <cassert>
#include <vector>
#include <unordered_set>

namespace kernel {
  namespace utils {
    namespace random {
      /*!
       * \brief uniform_real_rand returns a random real number from range [start, end) based on a uniform distribution
       * \param start
       * \param end
       * \return
       */
      inline std::double_t uniform_real_rand(const std::double_t start, const std::double_t end)
      {
        assert( start < end );
        static std::random_device rd;
        static std::mt19937 gen( rd() );
        std::uniform_real_distribution<> dis( start, end );
        return dis( gen );
      }

      /*!
       * \brief uniform_int_rand returns a random integer number from range [start, end) based on a uniform distribution
       * \param start
       * \param end
       * \return
       */
      inline int uniform_int_rand(const int start, const int end)
      {
        static std::random_device rd;
        static std::mt19937 gen( rd() );
        std::uniform_int_distribution<> dis( start, end - 1 );
        return dis( gen );
      }

      /*!
       * \brief indeces_in_range generates an unsorted vector on numbers from the range [start; end) of size n
       * \param start start of range
       * \param end end of range
       * \param n number of numbers
       * \return
       */
      inline std::vector< std::size_t > indeces_in_range(const std::size_t start, const std::size_t end, const std::size_t n)
      {
        assert( start <= end );
        std::vector< std::size_t > result;
        const auto elements_number = std::min( n, (end - start) );
        if ( elements_number == 0 ) { return result; }
        result.reserve( elements_number );
        static std::random_device rd;
        static std::mt19937 gen( rd() );
        std::uniform_int_distribution<> dis( start, end - 1 );
        std::unordered_set< std::size_t > rand_set;
        while ( rand_set.size() != elements_number ) { rand_set.insert( dis( gen ) ); }
        std::copy( rand_set.begin(), rand_set.end(), std::back_inserter( result ) );
        return result;
      }
    }
  }
}
#endif // RANDOMUTILS_HPP
