#include "SettingsDialog.hpp"
#include "ui_SettingsDialog.h"

#include <QDebug>
#include <cassert>

SettingsDialog::SettingsDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::SettingsDialog)
{
  ui->setupUi(this);
  dataStep = ( ui->timeStepSpinBox->value() );
}

SettingsDialog::~SettingsDialog()
{
  delete ui;
}

void SettingsDialog::setDataTimeStep(const double step)
{
  dataStep = step;
}

void SettingsDialog::setTimeStep(const double value)
{
  ui->timeStepSpinBox->setMinimum( qMin( ui->timeStepSpinBox->minimum(), value ) );
  ui->timeStepSpinBox->setMaximum( qMax( ui->timeStepSpinBox->maximum(), value ) );
  ui->timeStepSpinBox->setValue( value );
}

double SettingsDialog::getTimeStep() const
{
  return ui->timeStepSpinBox->value();
}

void SettingsDialog::setLooped(const bool value)
{
  ui->loopedCheckBox->setChecked( value );
}

bool SettingsDialog::getLooped() const
{
  return ui->loopedCheckBox->isChecked();
}

void SettingsDialog::changeEvent(QEvent *e)
{
  QDialog::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}


void SettingsDialog::on_comboBox_activated(int index)
{
  if ( index == 0 ) { // User defined
    ui->timeStepSpinBox->setEnabled( true );
    ui->timeStepSpinBox->setValue( ui->timeStepSpinBox->minimum() );
  }

  if ( index == 1 ) { // Data based
    ui->timeStepSpinBox->setEnabled( false );
    ui->timeStepSpinBox->setValue( dataStep );
  }
}
