#ifndef VISUALISATIONWIDGET_HPP
#define VISUALISATIONWIDGET_HPP

#include <QWidget>
#include <QMainWindow>
#include <QVector>
#include <QItemSelection>
#include <QMap>

#include <tuple>

#include <vtkAlgorithmOutput.h>
#include <vtkSmartPointer.h>

#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkDoubleClickPickerTrackballCameraInterractor.hpp>

#include <VTKActorListModel.hpp>


namespace Ui {
  class VisualisationWidget;
}

/*!
 * \brief The VisualisationWidget class
 */
class VisualisationWidget : public QWidget
{
  Q_OBJECT

public:
  explicit VisualisationWidget(QWidget *parent = 0);
  virtual ~VisualisationWidget();

public slots:
  void addMapper(vtkSmartPointer<vtkMapper> mapper, const ActorType actorType, const QString name = "");
  void resetCamera();

  void setMeshToGeometry(vtkSmartPointer<vtkUnstructuredGrid> grid, const QString fileName);

signals:
  void signal_actorSelected(vtkActor *actor);
  void signal_actorDeselected(vtkActor *actor);
  void signal_addOrRemoveBoundingBoxForActor(vtkActor *actor);

  void sendGeometryToMesher(vtkSmartPointer< vtkPolyData >, const QString);
  void sendMeshToBoundaryConditions(vtkSmartPointer< vtkUnstructuredGrid >);

protected:
  void changeEvent(QEvent *e);

private slots:
  void slot_actorSelected(vtkActor *selectedActor);
  void slot_actorDeselected(vtkActor *deselectedActor);
  void selectActor(QItemSelection selected, QItemSelection deselected);
  void removeSelectedRows();
  void hideShowSelectedRows();
  void actorListViewContextMenu(const QPoint& pos);
  void moveActorsToNewWVisualisationWidget();
  void addOrRemoveBoundingBox();
  void applyProcessing();

private:
  Ui::VisualisationWidget *ui;

  vtkSmartPointer< vtkRenderer > renderer;
  vtkSmartPointer< vtkDoubleClickPickerTrackballCameraInterractor > style;
  VTKActorListModel* actorListModelPtr;
};


namespace test {
  bool VisualisationWidgetTest(int argc, char *argv[]);
}

#endif // VISUALISATIONWIDGET_HPP
