#ifndef STANDARDSLAESOLVER_HPP
#define STANDARDSLAESOLVER_HPP

#include <slae_solvers/AbstractSLAESolver.hpp>

class StandardSLAESolver : public AbstractSLAESolver
{
public:
  virtual bool operator ()(getfem::model &model, gmm::iteration &iter);
};

#endif // STANDARDSLAESOLVER_HPP
