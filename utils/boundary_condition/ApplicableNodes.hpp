#ifndef APPLICABLENODES_HPP
#define APPLICABLENODES_HPP

#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>

#include <getfem/getfem_mesh.h>
#include <gmm/gmm.h>
#include <gmm/gmm_ref.h>

#include <unordered_set>
#include <cmath>

#include <boost/functional/hash.hpp>

/*!
 * \brief The BaseNodeHash struct
 */
struct BaseNodeHash
{
  std::size_t operator()(const bgeot::base_node& node) const
  {
    assert( node.size() == 3 );
    std::size_t seed = 0;
    boost::hash_combine( seed, node[0] );
    boost::hash_combine( seed, node[1] );
    boost::hash_combine( seed, node[2] );
    return seed;
  }
};

namespace bgeot {
  inline bool operator ==(const bgeot::base_node& n1, const bgeot::base_node& n2)
  {
    return ( n1[0] == n2[0] ) && ( n1[1] == n2[1] ) && ( n1[2] == n2[2] );
  }
}


/*!
 * \brief The ApplicableNodes class
 */
class ApplicableNodes
{
public:
  ApplicableNodes(vtkSmartPointer< vtkUnstructuredGrid > grid);
  bool applicableToFace(const std::vector<bgeot::base_node> &facePoints) const;

private:
    std::unordered_set< bgeot::base_node, BaseNodeHash > applicableNodes;
};


namespace test {
  bool ApplicableNodesContainerTest();
}

#endif // APPLICABLENODES_HPP
