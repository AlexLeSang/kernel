#include "VisualisationWidgetDesignerPlugin.hpp"

#include "VisualisationWidget.hpp"

#include <QtPlugin>

VisualisationWidgetDesignerPlugin::VisualisationWidgetDesignerPlugin(QObject *parent) : QObject( parent ), initialized( false )
{

}

QString VisualisationWidgetDesignerPlugin::name() const
{
  return "VisualisationWidget";
}

QString VisualisationWidgetDesignerPlugin::group() const
{
  return "QVTK";
}

QString VisualisationWidgetDesignerPlugin::toolTip() const
{
  return "This widget manage visualisation";
}

QString VisualisationWidgetDesignerPlugin::whatsThis() const
{
  return "VTK visualisation widget";
}

QString VisualisationWidgetDesignerPlugin::includeFile() const
{
  return "VisualisationWidget.hpp";
}

QIcon VisualisationWidgetDesignerPlugin::icon() const
{
  return QIcon();
}

bool VisualisationWidgetDesignerPlugin::isContainer() const
{
  return false;
}

QWidget *VisualisationWidgetDesignerPlugin::createWidget(QWidget *parent)
{
  return new VisualisationWidget( parent );
}

bool VisualisationWidgetDesignerPlugin::isInitialized() const
{
  return initialized;
}

void VisualisationWidgetDesignerPlugin::initialize(QDesignerFormEditorInterface *)
{
  if ( initialized ) {
    return;
  }

  initialized = true;
}

QString VisualisationWidgetDesignerPlugin::domXml() const
{
  /*
  return
  "<ui language=\"c++\>\n"
   "<widget class=\"QWidget\" name=\"visualisationWidget\">\n"
    "<property name=\"geometry\">\n"
     "<rect>\n"
      "<x>0</x>\n"
      "<y>0</y>\n"
      "<width>1024</width>\n"
      "<height>768</height>\n"
     "</rect>\n"
    "</property>\n"
    "<property name=\"sizePolicy\">\n"
     "<sizepolicy hsizetype=\"MinimumExpanding\" vsizetype=\"MinimumExpanding\">\n"
      "<horstretch>0</horstretch>\n"
      "<verstretch>0</verstretch>\n"
     "</sizepolicy>\n"
    "</property>\n"
    "<property name=\"minimumSize\">\n"
     "<size>\n"
      "<width>1024</width>\n"
      "<height>768</height>\n"
     "</size>\n"
    "</property>\n"
   "</widget>\n"
   "</ui>\n";
   */

  return "<ui language=\"c++\">\n"
              " <widget class=\"VisualisationWidget\" name=\"visualisationWidget\">\n"
              "  <property name=\"geometry\">\n"
              "   <rect>\n"
              "    <x>0</x>\n"
              "    <y>0</y>\n"
              "    <width>1024</width>\n"
              "    <height>768</height>\n"
              "   </rect>\n"
              "  </property>\n"
              "  <property name=\"toolTip\" >\n"
              "   <string>This widget manage visualisation</string>\n"
              "  </property>\n"
              "  <property name=\"whatsThis\" >\n"
              "   <string>VTK visualisation widget</string>\n"
              "  </property>\n"
              " </widget>\n"
              "<resources/>\n"
              "<connections/>\n"
              "<slots>\n"
              " <slot>addSource()</slot>\n"
              " <slot>selectActor(QModelIndex)</slot>\n"
              "</slots>\n"
              "</ui>\n";
}

QString VisualisationWidgetDesignerPlugin::codeTemplate() const
{
  return "";
}

Q_EXPORT_PLUGIN2( visualisationwidget, VisualisationWidgetDesignerPlugin )
