#include <QApplication>
#include <VisualisationWidget.hpp>

int main(int argc, char *argv[])
{
  if ( ! test::VisualisationWidgetTest( argc, argv ) ) { return EXIT_FAILURE; }
  // if ( ! test::VTKMeshViewerDialogTest( argc, argv ) ) { return EXIT_FAILURE; }
  return EXIT_SUCCESS;
}

