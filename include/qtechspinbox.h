#ifndef QTECHSPINBOX_H
#define QTECHSPINBOX_H

#include <QtGui/qabstractspinbox.h>
#ifdef QT_PLUGIN
#include <QtDesigner/QDesignerExportWidget>
#endif

class
#ifdef QT_PLUGIN
      QDESIGNER_WIDGET_EXPORT
#endif
      QTechSpinBox : public QAbstractSpinBox {
   Q_OBJECT

   Q_ENUMS(Roundingrule)
   Q_PROPERTY(unsigned int predigits READ predigits WRITE setPredigits)
   Q_PROPERTY(unsigned int postdigits READ postdigits WRITE setPostdigits)
   Q_PROPERTY(double minimum READ minimum WRITE setMinimum)
   Q_PROPERTY(double maximum READ maximum WRITE setMaximum)
   Q_PROPERTY(unsigned int singlestep READ singlestep WRITE setSinglestep)
   Q_PROPERTY(QString baseunit READ baseunit WRITE setBaseunit)
   Q_PROPERTY(Roundingrule roundingrule READ roundingrule WRITE setRoundingrule)
   Q_PROPERTY(double value READ value WRITE setValue)
   Q_PROPERTY(bool unitmodifiers READ unitmodifiers WRITE setUnitmodifiers)
public:
   enum Roundingrule {Cut, BankersRule, Arithmetic, Symmetric};
   explicit QTechSpinBox(QWidget *parent = 0);
   double value() const {return d_value;}
   unsigned short int singlestep() const {return singlestepInt;}
   void setSinglestep(const unsigned short int &singlestep);
   unsigned short int predigits() const {return predigitsInt;}
   void setPredigits(const unsigned short int &predigits);
   unsigned short int postdigits() const {return postdigitsInt;}
   void setPostdigits(const unsigned short int &postdigits);
   QString baseunit() const {return baseunitString;}
   void setBaseunit(const QString &baseunit);
   double minimum() const {return d_minimum;}
   void setMinimum(double min);
   double maximum() const {return d_maximum;}
   void setMaximum(double max);
   Roundingrule roundingrule() const {return roundingruleEnum;}
   void setRoundingrule(const Roundingrule& roundingrule);
   bool unitmodifiers() const {return unitmodifiersBool;}
   void setUnitmodifiers(bool useModifiers);
   virtual double valueFromText(const QString &text) const;
   virtual QString textFromValue(double val) const;
   virtual void fixup(QString &str) const;
   virtual QValidator::State validate(QString &input, int &pos) const;
public slots:
   void setValue(double val);
protected slots:
   void setValueFromText();
signals:
   void valueChanged(double);
   void valueChanged(const QString &);
protected:
   virtual void stepBy (int steps);
   virtual StepEnabled stepEnabled() const;
   void setRegExpValidator();
   void updateDisplay();
   bool isNumeric(QChar oneChar) const;
private:
   bool unitmodifiersBool;
   double d_minimum;
   double d_maximum;
   double d_value;
   unsigned short int singlestepInt;   // **factor** for multiplication or division
   QString baseunitString;
   unsigned short int postdigitsInt;
   unsigned short int predigitsInt;
   Roundingrule roundingruleEnum;
//   QChar signMinus;
//   QChar delimiter;
//   QRegExpValidator* regExpValidator;
//   QString allNumerics;
};

#endif // QTECHSPINBOX_H
