#ifndef BSONCONVERTERS_HPP
#define BSONCONVERTERS_HPP

#include <mongo/bson/bson.h>
#include <mongo/bson/bsonobjbuilder.h>

#include <vtkCellType.h>
#include <vtkCell.h>
#include <vtkCellArray.h>
#include <vtkIdList.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellType.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkDataSetMapper.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkUnstructuredGridWriter.h>
#include <vtkUnstructuredGridReader.h>

#include <boost/timer/timer.hpp>

#include <fstream>
#include <cassert>
#include <string>

namespace kernel {
  namespace utils {
    namespace bson_converters {

      /*!
       * \brief vtk_unstructured_grid_to_bson
       * \param gridPtr
       * \param grid_id
       * \return
       */
      std::pair<std::vector<mongo::BSONObj>, std::vector<mongo::BSONObj> >
      vtk_unstructured_grid_to_bson(vtkUnstructuredGrid * const gridPtr, const std::string &grid_id);


      /*!
       * \brief bson_to_vtk_unstructured_grid
       * \param obj
       * \return pointer to vtkUnstructuredGrid. Caller should take care of this pointer
       */
      vtkUnstructuredGrid* bson_to_vtk_unstructured_grid(const mongo::BSONObj& obj);

    }
  }
}


namespace boost {
  namespace serialization {

    template<class Archive>
    void save(Archive& ar, vtkUnstructuredGrid* const gridPtr, const unsigned int version)
    {

      // INFO remove debug output
      std::cout << "save(Archive& ar, vtkUnstructuredGrid* const gridPtr, const unsigned int version)" << std::endl;

      // Serialize points
      vtkPoints* const points_array = gridPtr->GetPoints();
      const auto number_of_points = gridPtr->GetNumberOfPoints();
      std::cout << "number_of_points: " << number_of_points << std::endl;
      // Save number of points
      ar & number_of_points;
      {
        // boost::timer::auto_cpu_timer t_points;
        double xyz[3];
        for ( auto i = 0; i < number_of_points; ++ i ) {
          points_array->GetPoint( i, xyz );
          ar & xyz[ 0 ];
          ar & xyz[ 1 ];
          ar & xyz[ 2 ];
        }
      }

      // Serialize point data
      vtkPointData* const point_data = gridPtr->GetPointData();
      if ( point_data != nullptr ) {
        std::cout << "Point data with " << point_data->GetNumberOfArrays() << " arrays." << std::endl;

        for ( auto i = 0; i < point_data->GetNumberOfArrays(); ++ i ) {
          std::cout << "#: " << i << " " << ( point_data->GetArrayName(i) ? point_data->GetArrayName(i) : "NULL" ) << std::endl;
        }
      }


      // Serialize cells
      vtkCellArray* const cells_array = gridPtr->GetCells(); // Structure: [n, ind1, ind2, ..., n,...]
      const auto number_of_cells = gridPtr->GetNumberOfCells();
      // Save number of cells
      ar & number_of_cells;
      std::cout << "number_of_cells: " << number_of_cells << std::endl;
      {
        // boost::timer::auto_cpu_timer t_cells;
        for ( auto i = 0; i < number_of_cells; ++ i ) {
          vtkSmartPointer< vtkIdList > id_list_prt = vtkSmartPointer< vtkIdList >::New();
          cells_array->GetCell( i, id_list_prt.GetPointer() );
          // Save cell type
          int cell_type = gridPtr->GetCellType( i );
          assert( cell_type == VTK_TETRA );

          ar & cell_type;

          // Save number of ids in the cell
          const auto number_of_ids = id_list_prt->GetNumberOfIds();
          ar & number_of_ids;
          // std::cout << "i: " << i << " cell type:" << cell_type << " number_of_ids: " << number_of_ids << std::endl;
          for ( auto j = 0; j < number_of_ids; ++ j ) {
            auto id = id_list_prt->GetId( j );
            ar & id;
          }
        }
      }

    }

    template<class Archive>
    void load(Archive& ar, vtkUnstructuredGrid* const gridPtr, const unsigned int /*version*/)
    {
      // Serialize points

      // Get number of points
      vtkIdType number_of_points = 0;
      ar & number_of_points;
      std::cout << "number_of_points: " << number_of_points << std::endl;

      vtkPoints* const vtk_points = vtkPoints::New();
      vtk_points->Allocate( number_of_points );
      {
        boost::timer::auto_cpu_timer t_points;
        double xyz[ 3 ];
        for ( auto i = 0; i < number_of_points; ++ i ) {
          ar & xyz[ 0 ];
          ar & xyz[ 1 ];
          ar & xyz[ 2 ];
          vtk_points->InsertNextPoint( xyz );
        }
      }

      // Deserialize cells
      // Get number of cells
      vtkIdType number_of_cells = 0;
      ar & number_of_cells;
      std::cout << "number_of_cells: " << number_of_cells << std::endl;

      vtkCellArray* const vtk_cells = vtkCellArray::New(); // Structure: [n, ind1, ind2, ..., n,...]
      vtk_cells->Allocate( number_of_cells );
      {
        boost::timer::auto_cpu_timer t_cells;

        for ( auto i = 0; i < number_of_cells; ++ i ) {

          // Get cell type
          int cell_type = 0;
          ar & cell_type;

          vtkIdType number_of_ids = 0;
          ar & number_of_ids;
          // std::cout << "i: " << i << " cell type:" << cell_type << " number_of_ids: " << number_of_ids << std::endl;

          vtkSmartPointer< vtkIdList > id_list_prt = vtkSmartPointer< vtkIdList >::New();

          id_list_prt->Allocate( number_of_ids );
          for ( auto j = 0; j < number_of_ids; ++ j ) {
            vtkIdType id = 0;
            ar & id;
            id_list_prt->SetId( j, id );
          }

          vtk_cells->InsertNextCell( id_list_prt );

        }
      }


      gridPtr->SetPoints( vtk_points );
      //vtk_points->Delete();

      gridPtr->SetCells( VTK_TETRA, vtk_cells );
      //vtk_cells->Delete();
    }



  }
}

namespace test {
  bool vtkReadWriteTest();
  bool VTKSerializationTest();
  bool BSONVTKUnstructuredGridTest();
}

#endif // BSONCONVERTERS_HPP
