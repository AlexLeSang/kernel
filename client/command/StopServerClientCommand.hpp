#ifndef STOPSERVERCLIENTCOMMAND_HPP
#define STOPSERVERCLIENTCOMMAND_HPP

#include "ClientCommand.hpp"

namespace kernel {
  namespace client {
    namespace command {

      class StopServerClientCommand : public ClientCommand
      {
      public:
        virtual bool operator ()();
        virtual server::ServerCommands commandName() const;
      };

    }
  }
}

#include "KernelClient.hpp"
inline bool StopServerClientCommandTest()
{
  /* TODO
  kernel::client::KernelClient* client = kernel::client::KernelClient::getInstance();
  auto commandProcessor = kernel::client::command::CommandProcessor( kernel::server::ServerCommands::STOP_SERVER );
  QScopedPointer< kernel::client::command::ClientCommand > sPtr( commandProcessor );
  if ( ! (*client).executeAndWait( commandProcessor ) ) {
    qWarning() << "StopServer failed";
    return false;
  }
  */
  return true;
}


#endif // STOPSERVERCLIENTCOMMAND_HPP
