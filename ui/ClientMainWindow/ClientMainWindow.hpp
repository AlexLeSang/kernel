#ifndef CLIENTMAINWINDOW_HPP
#define CLIENTMAINWINDOW_HPP

#include <QMainWindow>

namespace Ui {
  class ClientMainWindow;
}

/*!
 * \brief The ClientMainWindow class
 */
class ClientMainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit ClientMainWindow(QWidget *parent = 0);
  ~ClientMainWindow();

protected:
  void changeEvent(QEvent *e);

private slots:
  void open();

  void activateMesher();
  void activateGeometry();
  void activateBoundaryConditions();
  void activateSolver();
  void activateResults();

private:
  void processFileNameLists(const QStringList &fileNameList);
  void processFileName(const QString &fileName);
  void addVtkUnstructuredGrid(const QString &fileName);
  void addXmlPolyData(const QString& fileName);
  void addPolyData(const QString &fileName);
  void addStlPolyData(const QString &fileName);
  void addPlyPolyData(const QString &fileName);

private:
  Ui::ClientMainWindow *ui;
};

#endif // CLIENTMAINWINDOW_HPP
