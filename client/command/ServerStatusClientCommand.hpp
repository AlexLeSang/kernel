#ifndef SERVERSTATUSCLIENTCOMMAND_HPP
#define SERVERSTATUSCLIENTCOMMAND_HPP

#include <QtCore>

#include "ClientCommand.hpp"

namespace kernel {

  namespace server {
    namespace command {
      class ServerStatus;
    }
  }

  namespace client {
    namespace command {

      class ServerStatusClientCommand : public ClientCommand
      {
      public:
        ServerStatusClientCommand() : ClientCommand(), serverStatusPtr( nullptr ) {}

        virtual bool operator ()();
        virtual server::ServerCommands commandName() const;

        void setServerStatusPtr(server::command::ServerStatus *value);

      private:
        server::command::ServerStatus* serverStatusPtr;
      };
    }
  }
}


#include <iostream>
#include <KernelClient.hpp>
#include <../server/command/GetServerStatusCommand.hpp>

namespace test {

  inline bool ServerStatusClientCommandTest()
  {
    std::cout << "ServerStatusClientCommandTest()" << std::endl;

    kernel::client::KernelClient* client = kernel::client::KernelClient::getInstance();
    kernel::client::ServerDescription sd;
    sd.address = QHostAddress( QHostAddress::LocalHost );
    // sd.address = QHostAddress( "192.168.0.9" );
    // sd.address = QHostAddress( "192.168.0.10" );
    sd.port = 3101;
    client->setServerDescription( sd );

    kernel::server::command::ServerStatus serverStatus;

    auto result = client->serverStatus( &serverStatus );

    qWarning() << QThread::currentThread() << " is going to wait for result";
    result.waitForFinished();
    const bool commandSuccessful = result.result();
    if ( commandSuccessful ) {
      qWarning() << "serverStatus =" << serverStatus;
    }

    return true;
  }
}

#endif // SERVERSTATUSCLIENTCOMMAND_HPP
