#include "NeumannTreeItem.hpp"

#include <QDebug>

/*!
 * \brief NeumannTreeItem::NeumannTreeItem
 * \param name
 * \param xvalue
 * \param yvalue
 * \param zvalue
 * \param color
 * \param parent
 */
NeumannTreeItem::NeumannTreeItem(const QString &name, qreal xvalue, qreal yvalue, qreal zvalue, const QColor &color, RootTreeItem *parent) :
  AbstractTreeItem(parent )
{
  std::shared_ptr< AbstractBoundaryCondition > boundaryCondition( new NeumannBoundaryCondition );
  static_cast<NeumannBoundaryCondition*>( boundaryCondition.get() )->setCondition( xvalue, 0 );
  static_cast<NeumannBoundaryCondition*>( boundaryCondition.get() )->setCondition( yvalue, 1 );
  static_cast<NeumannBoundaryCondition*>( boundaryCondition.get() )->setCondition( zvalue, 2 );
  applyedCondition.setBoundaryCondition( boundaryCondition );
  applyedCondition.setName( name );
  applyedCondition.setColor( color );
}

/*!
 * \brief NeumannTreeItem::columnCount
 * \return
 */
int NeumannTreeItem::columnCount() const
{
  return 6;
}

/*!
 * \brief NeumannTreeItem::data
 * \param column
 * \return
 */
QVariant NeumannTreeItem::data(int column) const
{
  auto boundaryCondition = applyedCondition.getBoundaryCondition();
  switch ( column ) {
    case 0:
      return qVariantFromValue<QString>( applyedCondition.getName() );
      break;

    case 1:
      return QVariant();
      break;

    case 2:
      return qVariantFromValue<qreal>( static_cast<NeumannBoundaryCondition*>( boundaryCondition.get() )->getCondition( 0 ) );
      break;

    case 3:
      return qVariantFromValue<qreal>( static_cast<NeumannBoundaryCondition*>( boundaryCondition.get() )->getCondition( 1 ) );
      break;

    case 4:
      return qVariantFromValue<qreal>( static_cast<NeumannBoundaryCondition*>( boundaryCondition.get() )->getCondition( 2 ) );
      break;

    case 5:
      return qVariantFromValue<QColor>( applyedCondition.getColor() );
      break;

    default:
      return QVariant();
      break;
  }

}

/*!
 * \brief NeumannTreeItem::setData
 * \param column
 * \param value
 * \return
 */
bool NeumannTreeItem::setData(int column, const QVariant &value)
{
  auto boundaryCondition = applyedCondition.getBoundaryCondition();

  switch ( column ) {
    case 0:
      {
        applyedCondition.setName( value.value<QString>() );
        return true;
        break;
      }

    case 1:
      {
        return false;
        break;
      }

    case 2:
      {
        static_cast<NeumannBoundaryCondition*>( boundaryCondition.get() )->setCondition( value.value<qreal>(), 0 );
        return true;
        break;
      }


    case 3:
      {
        static_cast<NeumannBoundaryCondition*>( boundaryCondition.get() )->setCondition( value.value<qreal>(), 1 );
        return true;
        break;
      }


    case 4:
      {
        static_cast<NeumannBoundaryCondition*>( boundaryCondition.get() )->setCondition( value.value<qreal>(), 2 );
        return true;
        break;
      }

    case 5:
      {
        applyedCondition.setColor( value.value<QColor>() );
        return true;
        break;
      }

    default:
      return false;
      break;
  }
}

/*!
 * \brief NeumannTreeItem::getBcXvalue
 * \return
 */
qreal NeumannTreeItem::getBcXvalue() const
{
  auto boundaryCondition = applyedCondition.getBoundaryCondition();
  return static_cast<NeumannBoundaryCondition*>( boundaryCondition.get() )->getCondition( 0 );
}

/*!
 * \brief NeumannTreeItem::setBcXvalue
 * \param value
 */
void NeumannTreeItem::setBcXvalue(const qreal &value)
{
  auto boundaryCondition = applyedCondition.getBoundaryCondition();
  static_cast<NeumannBoundaryCondition*>( boundaryCondition.get() )->setCondition( value, 0 );
}

/*!
 * \brief NeumannTreeItem::getBcYvalue
 * \return
 */
qreal NeumannTreeItem::getBcYvalue() const
{
  auto boundaryCondition = applyedCondition.getBoundaryCondition();
  return static_cast<NeumannBoundaryCondition*>( boundaryCondition.get() )->getCondition( 1 );
}

/*!
 * \brief NeumannTreeItem::setBcYvalue
 * \param value
 */
void NeumannTreeItem::setBcYvalue(const qreal &value)
{
  auto boundaryCondition = applyedCondition.getBoundaryCondition();
  static_cast<NeumannBoundaryCondition*>( boundaryCondition.get() )->setCondition( value, 1 );
}

/*!
 * \brief NeumannTreeItem::getBcZvalue
 * \return
 */
qreal NeumannTreeItem::getBcZvalue() const
{
  auto boundaryCondition = applyedCondition.getBoundaryCondition();
  return static_cast<NeumannBoundaryCondition*>( boundaryCondition.get() )->getCondition( 2 );
}

/*!
 * \brief NeumannTreeItem::setBcZvalue
 * \param value
 */
void NeumannTreeItem::setBcZvalue(const qreal &value)
{
  auto boundaryCondition = applyedCondition.getBoundaryCondition();
  static_cast<NeumannBoundaryCondition*>( boundaryCondition.get() )->setCondition( value, 2 );
}
