#include <SolverWidget.hpp>

#include <QApplication>
#include <QDesktopWidget>

/*
int main(int argc, char *argv[])
{
  QApplication a( argc, argv );

  SolverWidget widget;
  widget.show();
  widget.move( QApplication::desktop()->screen()->rect().center() - widget.rect().center() );

  return a.exec();
}
*/


int main(int argc, char *argv[])
{
  if ( !test::SolverWidgetTest( argc, argv ) ) { return EXIT_FAILURE; }

  return EXIT_SUCCESS;
}
