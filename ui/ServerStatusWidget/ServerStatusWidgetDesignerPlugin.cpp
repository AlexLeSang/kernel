#include "ServerStatusWidgetDesignerPlugin.hpp"

#include <ServerStatusWidget.hpp>

#include <QtPlugin>

ServerStatusWidgetDesignerPlugin::ServerStatusWidgetDesignerPlugin(QObject *parent) : QObject( parent ), initialized( false )
{}

QString ServerStatusWidgetDesignerPlugin::name() const
{
  return "ServerStatusWidget";
}

QString ServerStatusWidgetDesignerPlugin::group() const
{
  return "QVTK";
}

QString ServerStatusWidgetDesignerPlugin::toolTip() const
{
  return "Server status widget";
}

QString ServerStatusWidgetDesignerPlugin::whatsThis() const
{
  return "Server visualisation widget";
}

QString ServerStatusWidgetDesignerPlugin::includeFile() const
{
  return "ServerStatusWidget.hpp";
}

QIcon ServerStatusWidgetDesignerPlugin::icon() const
{
  return QIcon();
}

bool ServerStatusWidgetDesignerPlugin::isContainer() const
{
  return false;
}

QWidget *ServerStatusWidgetDesignerPlugin::createWidget(QWidget *parent)
{
  return new ServerStatusWidget( parent );
}

bool ServerStatusWidgetDesignerPlugin::isInitialized() const
{
  return initialized;
}

void ServerStatusWidgetDesignerPlugin::initialize(QDesignerFormEditorInterface *)
{
  if ( initialized ) {
    return;
  }

  initialized = true;
}

QString ServerStatusWidgetDesignerPlugin::domXml() const
{
  /*
  return
  "<ui language=\"c++\>\n"
   "<widget class=\"QWidget\" name=\"visualisationWidget\">\n"
    "<property name=\"geometry\">\n"
     "<rect>\n"
      "<x>0</x>\n"
      "<y>0</y>\n"
      "<width>1024</width>\n"
      "<height>768</height>\n"
     "</rect>\n"
    "</property>\n"
    "<property name=\"sizePolicy\">\n"
     "<sizepolicy hsizetype=\"MinimumExpanding\" vsizetype=\"MinimumExpanding\">\n"
      "<horstretch>0</horstretch>\n"
      "<verstretch>0</verstretch>\n"
     "</sizepolicy>\n"
    "</property>\n"
    "<property name=\"minimumSize\">\n"
     "<size>\n"
      "<width>1024</width>\n"
      "<height>768</height>\n"
     "</size>\n"
    "</property>\n"
   "</widget>\n"
   "</ui>\n";
   */

  return "<ui language=\"c++\">\n"
              " <widget class=\"ServerStatusWidget\" name=\"serverStatusWidget\">\n"
              "  <property name=\"geometry\">\n"
              "   <rect>\n"
              "    <x>0</x>\n"
              "    <y>0</y>\n"
              "    <width>1024</width>\n"
              "    <height>768</height>\n"
              "   </rect>\n"
              "  </property>\n"
              "  <property name=\"toolTip\" >\n"
              "   <string>Server status widget</string>\n"
              "  </property>\n"
              "  <property name=\"whatsThis\" >\n"
              "   <string>Server visualisation widget</string>\n"
              "  </property>\n"
              " </widget>\n"
              "<resources/>\n"
              "<connections/>\n"
              "<slots>\n"
              " <slot>addSource()</slot>\n"
              " <slot>selectActor(QModelIndex)</slot>\n"
              "</slots>\n"
              "</ui>\n";
}

QString ServerStatusWidgetDesignerPlugin::codeTemplate() const
{
  return "";
}

Q_EXPORT_PLUGIN2( serverstatuswidget, ServerStatusWidgetDesignerPlugin )
