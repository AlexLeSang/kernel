#include "BoundaryConditionWidgetDesignerPlugin.hpp"

#include <BoundaryConditionWidget.hpp>

#include <QtPlugin>

BoundaryConditionWidgetDesignerPlugin::BoundaryConditionWidgetDesignerPlugin(QObject *parent) : QObject( parent ), initialized( false )
{}

QString BoundaryConditionWidgetDesignerPlugin::name() const
{
  return "BoundaryConditionWidget";
}

QString BoundaryConditionWidgetDesignerPlugin::group() const
{
  return "QVTK";
}

QString BoundaryConditionWidgetDesignerPlugin::toolTip() const
{
  return "Boundary condition widget";
}

QString BoundaryConditionWidgetDesignerPlugin::whatsThis() const
{
  return "Boundary condition widget";
}

QString BoundaryConditionWidgetDesignerPlugin::includeFile() const
{
  return "BoundaryConditionWidget.hpp";
}

QIcon BoundaryConditionWidgetDesignerPlugin::icon() const
{
  return QIcon();
}

bool BoundaryConditionWidgetDesignerPlugin::isContainer() const
{
  return false;
}

QWidget *BoundaryConditionWidgetDesignerPlugin::createWidget(QWidget *parent)
{
  return new BoundaryConditionWidget( parent );
}

bool BoundaryConditionWidgetDesignerPlugin::isInitialized() const
{
  return initialized;
}

void BoundaryConditionWidgetDesignerPlugin::initialize(QDesignerFormEditorInterface *)
{
  if ( initialized ) {
    return;
  }

  initialized = true;
}

QString BoundaryConditionWidgetDesignerPlugin::domXml() const
{
  /*
  return
  "<ui language=\"c++\>\n"
   "<widget class=\"QWidget\" name=\"visualisationWidget\">\n"
    "<property name=\"geometry\">\n"
     "<rect>\n"
      "<x>0</x>\n"
      "<y>0</y>\n"
      "<width>1024</width>\n"
      "<height>768</height>\n"
     "</rect>\n"
    "</property>\n"
    "<property name=\"sizePolicy\">\n"
     "<sizepolicy hsizetype=\"MinimumExpanding\" vsizetype=\"MinimumExpanding\">\n"
      "<horstretch>0</horstretch>\n"
      "<verstretch>0</verstretch>\n"
     "</sizepolicy>\n"
    "</property>\n"
    "<property name=\"minimumSize\">\n"
     "<size>\n"
      "<width>1024</width>\n"
      "<height>768</height>\n"
     "</size>\n"
    "</property>\n"
   "</widget>\n"
   "</ui>\n";
   */

  return "<ui language=\"c++\">\n"
              " <widget class=\"BoundaryConditionWidget\" name=\"boundaryConditionWidget\">\n"
              "  <property name=\"geometry\">\n"
              "   <rect>\n"
              "    <x>0</x>\n"
              "    <y>0</y>\n"
              "    <width>1024</width>\n"
              "    <height>768</height>\n"
              "   </rect>\n"
              "  </property>\n"
              "  <property name=\"toolTip\" >\n"
              "   <string>Boundary condition widget</string>\n"
              "  </property>\n"
              "  <property name=\"whatsThis\" >\n"
              "   <string>Boundary condition widget</string>\n"
              "  </property>\n"
              " </widget>\n"
              "<resources/>\n"
              "<connections/>\n"
              "<slots>\n"
              " <slot>addSource()</slot>\n"
              " <slot>selectActor(QModelIndex)</slot>\n"
              "</slots>\n"
              "</ui>\n";
}

QString BoundaryConditionWidgetDesignerPlugin::codeTemplate() const
{
  return "";
}

Q_EXPORT_PLUGIN2( boundaryconditionwidget, BoundaryConditionWidgetDesignerPlugin )
