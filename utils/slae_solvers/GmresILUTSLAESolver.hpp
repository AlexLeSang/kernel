#ifndef GMRESILUTSLAESOLVER_HPP
#define GMRESILUTSLAESOLVER_HPP

#include <slae_solvers/AbstractSLAESolver.hpp>

class GmresILUTSLAESolver : public AbstractSLAESolver
{
  // AbstractSLAESolver interface
public:
  virtual bool operator ()(getfem::model &model, gmm::iteration &iter);
};

#endif // GMRESILUTSLAESOLVER_HPP
