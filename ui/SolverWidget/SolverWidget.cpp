#include "SolverWidget.hpp"
#include "ui_SolverWidget.h"

#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkRenderWindow.h>
#include <cassert>
#include <vtkDataSetMapper.h>
#include <vtkScalarBarActor.h>
#include <vtkPointData.h>
#include <vtkDataArray.h>
#include <vtkLookupTable.h>
#include <vtkProperty.h>
#include <vtkTextProperty.h>
#include <vtkSmartPointer.h>
#include <vtkXMLUnstructuredGridReader.h>

#include <QShortcut>
#include <QDebug>
#include <QDesktopWidget>

#include <QThreadPool>

#include <slae_solvers/AbstractSLAESolver.hpp>
#include <slae_solvers/StandardSLAESolver.hpp>
#include <slae_solvers/CgILDTSLAESolver.hpp>
#include <slae_solvers/GmresILUSLAESolver.hpp>
#include <slae_solvers/GmresILUTSLAESolver.hpp>
#include <slae_solvers/GmresILUTPSLAESolver.hpp>
#include <slae_solvers/clCGSLAESolver.hpp>

/*!
 * \brief The SolverRunner class
 */
class SolverRunner : public QRunnable
{
public:
  SolverRunner(kernel::utils::fem_solvers::HeatFemSolver& solver) : solver( solver ) {}

  void run()
  {
    const bool result = solver.solve();
    std::cout << std::boolalpha << "Solution result: " << result << std::endl;
  }

private:
  kernel::utils::fem_solvers::HeatFemSolver& solver;
};


/*!
 * \brief SolverWidget::SolverWidget
 * \param parent
 */
SolverWidget::SolverWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::SolverWidget)
{
  ui->setupUi(this);

  grid = vtkSmartPointer< vtkUnstructuredGrid >::New();

  renderer = vtkSmartPointer< vtkRenderer >::New();
  renderer->GradientBackgroundOn();
  renderer->SetBackground( 0.0, 0.0, 0.0 );
  renderer->SetBackground2( 0.7, 0.2, 0.7 );
  ui->qvtkWidget->GetRenderWindow()->AddRenderer( renderer );

  vtkSmartPointer< vtkInteractorStyleTrackballCamera > style = vtkSmartPointer< vtkInteractorStyleTrackballCamera >::New();
  style->SetDefaultRenderer( renderer );

  ui->qvtkWidget->GetInteractor()->SetInteractorStyle( style );

  QObject::connect( &solver, SIGNAL( finished() ), this, SLOT( solverFinished() ) );
  QObject::connect( &solver, SIGNAL( interrupted() ), this, SLOT( solverInterrupted() ) );
  QObject::connect( &solver, SIGNAL( timeStepProcessed(double) ), this, SLOT( solverTimeStepProcessed(double) ) );
  QObject::connect( this, SIGNAL( stopSolver() ), &solver, SLOT( stop() ) );
}

/*!
 * \brief SolverWidget::~SolverWidget
 */
SolverWidget::~SolverWidget()
{
  delete ui;
}

/*!
 * \brief SolverWidget::changeEvent
 * \param e
 */
void SolverWidget::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}

/*!
 * \brief SolverWidget::on_startPushButton_clicked
 */
void SolverWidget::on_startPushButton_clicked()
{
  solver.clear();
  solver.setInputGrid( grid );
  solver.addBoundaryCondition( *applyedConditions.get() );

  solver.setThermalDiffusivity( ui->thermalDiffusivitySpinBox->value() );
  solver.setInitialTemperature( ui->initialTemperatureSpinBox->value() );
  solver.setResidual( ui->precisionSpinBox->value() );
  totalSolutionTime = ui->totalSolutionTimeSpinBox->value();
  solver.setTotalSolutionTime( totalSolutionTime );
  solver.setTimeStep( ui->timeStepSpinBox->value() );

  std::shared_ptr< AbstractSLAESolver > slaeSolver;

  switch ( ui->solverTypeComboBox->currentIndex() ) {
    case SOLVER_TYPE::STANDARD:
      slaeSolver = std::shared_ptr< AbstractSLAESolver >( new StandardSLAESolver() );
      break;

    case SOLVER_TYPE::CG_ILDT:
      slaeSolver = std::shared_ptr< AbstractSLAESolver >( new CgILDTSLAESolver() );
      break;

    case SOLVER_TYPE::GMRES_ILU:
      slaeSolver = std::shared_ptr< AbstractSLAESolver >( new GmresILUSLAESolver() );
      break;

    case SOLVER_TYPE::GMRES_ILUT:
      slaeSolver = std::shared_ptr< AbstractSLAESolver >( new GmresILUTSLAESolver() );
      break;

    case SOLVER_TYPE::GMRES_ILUTP:
      slaeSolver = std::shared_ptr< AbstractSLAESolver >( new GmresILUTPSLAESolver() );
      break;

    case SOLVER_TYPE::clCG:
      slaeSolver = std::shared_ptr< AbstractSLAESolver >( new clCGSLAESolver() );
      break;

    default:
      break;
  }

  solver.setSlaeSolver( slaeSolver );

  solver.init();

  SolverRunner* runner = new SolverRunner( solver );
  QThreadPool::globalInstance()->start( runner );
  ui->startPushButton->setEnabled( false );
  ui->stopPushButton->setEnabled( true );
  ui->progressBar->setValue( 0 );
}

/*!
 * \brief SolverWidget::on_stopPushButton_clicked
 */
void SolverWidget::on_stopPushButton_clicked()
{
  emit stopSolver();
}

/*!
 * \brief SolverWidget::solverFinished
 */
void SolverWidget::solverFinished()
{
  ui->progressBar->setValue( 100 );
  ui->startPushButton->setEnabled( true );
  ui->stopPushButton->setEnabled( false );
  emit solved();

  auto timeGrid = std::make_shared< TimeGrid >( solver.getOutputGrids() );
  emit sendResults( timeGrid );
}

/*!
 * \brief SolverWidget::solverInterrupted
 */
void SolverWidget::solverInterrupted()
{
  ui->startPushButton->setEnabled( true );
  ui->stopPushButton->setEnabled( false );
}

/*!
 * \brief SolverWidget::solverTimeStepProcessed
 * \param step
 */
void SolverWidget::solverTimeStepProcessed(double step)
{
  ui->progressBar->setValue( ( step / totalSolutionTime ) * 100 );
}

/*!
 * \brief SolverWidget::getGrid
 * \return
 */
vtkSmartPointer<vtkUnstructuredGrid> SolverWidget::getGrid() const
{
  return grid;
}

/*!
 * \brief SolverWidget::visualizeGrid
 */
void SolverWidget::visualizeGrid()
{
  renderer->RemoveAllViewProps();

  vtkSmartPointer<vtkDataSetMapper> mapper = vtkSmartPointer<vtkDataSetMapper>::New();
  mapper->SetInput( grid );
  mapper->ScalarVisibilityOn();
  mapper->SetScalarModeToUsePointData();

  vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
  actor->SetMapper( mapper );
  actor->GetProperty()->EdgeVisibilityOn();
  renderer->AddActor( actor );
  renderer->ResetCamera();
  ui->qvtkWidget->update();
}

/*!
 * \brief SolverWidget::getApplyedConditions
 * \return
 */
std::shared_ptr<std::list<AppliedBoundaryCondition> > SolverWidget::getApplyedConditions()
{
  return applyedConditions;
}

/*!
 * \brief SolverWidget::setApplyedConditions
 * \param conditions
 */
void SolverWidget::setApplyedConditions(std::shared_ptr<std::list<AppliedBoundaryCondition> > conditions)
{
  applyedConditions = conditions;
}

/*!
 * \brief SolverWidget::setGridToSolver
 * \param grid
 */
void SolverWidget::setGridToSolver(vtkSmartPointer<vtkUnstructuredGrid> grid)
{
  setGrid( grid );
}

/*!
 * \brief SolverWidget::setApplyedBoundaryConditions
 * \param applyedConditions
 */
void SolverWidget::setApplyedBoundaryConditions(std::shared_ptr<std::list<AppliedBoundaryCondition> > applyedConditions)
{
  qDebug() << "SolverWidget::setApplyedBoundaryConditions: applyedConditions->size(): " << applyedConditions->size();
  setApplyedConditions( applyedConditions );
}

/*!
 * \brief SolverWidget::setGrid
 * \param value
 */
void SolverWidget::setGrid(const vtkSmartPointer<vtkUnstructuredGrid> &value)
{
  grid = value;
  visualizeGrid();
  ui->startPushButton->setEnabled( true );
}





bool test::SolverWidgetTest(int argc, char *argv[])
{
  assert( argc > 2 );
  QApplication a( argc, argv );
  SolverWidget widget;

  const std::string gridFileName = argv[ 1 ];

  {
    vtkSmartPointer< vtkXMLUnstructuredGridReader > reader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
    reader->SetFileName( gridFileName.c_str() );
    reader->Update();

    vtkSmartPointer< vtkUnstructuredGrid > grid( reader->GetOutput() );
    std::cout << "grid->GetNumberOfCells(): " << grid->GetNumberOfCells() << std::endl;

    widget.setGrid( grid );
  }

  auto conditions = std::make_shared< std::list< AppliedBoundaryCondition > >( std::list< AppliedBoundaryCondition >() );
  for ( auto i = 2; i < argc; ++ i ) {
    const auto bcFileName = argv[ i ];
    auto bc = AppliedBoundaryCondition::readFromFile( bcFileName );
    conditions->push_back( bc );
  }
  std::cout << "conditions->size(): " << conditions->size() << std::endl;
  widget.setApplyedConditions( conditions );


  widget.show();
  widget.move( QApplication::desktop()->screen()->rect().center() - widget.rect().center() );
  a.exec();

  return true;
}
