#include "MesherThread.hpp"

MesherThread::MesherThread(std::shared_ptr<kernel::utils::mesh::ViennaMeshMesher> mesher, QObject *parent) : QThread( parent ), mesher( mesher ) {}

void MesherThread::run()
{
  mesher->init();
  emit meshInitialized();

  mesher->createMesh();
  emit meshCreated();

  mesher->writeMesh();
  emit meshWrited();
}
