#include "ClientMainWindow.hpp"
#include "ui_ClientMainWindow.h"

#include <QFileDialog>
#include <QVTKWidget.h>

#include <QMessageBox>

#include <vtkDataSetMapper.h>
#include <vtkPolyDataMapper.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkXMLStructuredGridReader.h>
#include <vtkStructuredGridGeometryFilter.h>
#include <vtkXMLRectilinearGridReader.h>
#include <vtkXMLImageDataReader.h>
#include <vtkStructuredGridReader.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkPolyDataReader.h>
#include <vtkSTLReader.h>
#include <vtkPLYReader.h>
#include <vtkSmartPointer.h>

/*!
 * \brief ClientMainWindow::ClientMainWindow
 * \param parent
 */
ClientMainWindow::ClientMainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::ClientMainWindow)
{
  ui->setupUi(this);
  // showMaximized();

  QObject::connect( ui->visualisationWidget, SIGNAL( sendGeometryToMesher(vtkSmartPointer<vtkPolyData>,QString) ),
                    ui->mesherWidget, SLOT( setGeometryToMesher(vtkSmartPointer<vtkPolyData>,QString) ) );

  QObject::connect( ui->visualisationWidget, SIGNAL( sendGeometryToMesher(vtkSmartPointer<vtkPolyData>,QString) ),
                    this, SLOT( activateMesher() ) );


  QObject::connect( ui->mesherWidget, SIGNAL( sendMeshToGeometry(vtkSmartPointer<vtkUnstructuredGrid>,QString) ),
                    ui->visualisationWidget, SLOT( setMeshToGeometry(vtkSmartPointer<vtkUnstructuredGrid>,QString) ) );

  QObject::connect( ui->mesherWidget, SIGNAL( sendMeshToGeometry(vtkSmartPointer<vtkUnstructuredGrid>,QString) ),
                    this, SLOT( activateBoundaryConditions() ) );

  QObject::connect( ui->mesherWidget, SIGNAL( sendMeshToGeometry(vtkSmartPointer<vtkUnstructuredGrid>,QString) ),
                    ui->boundaryConditionWidget, SLOT( fromMesherOrGeometry(vtkSmartPointer<vtkUnstructuredGrid>) ) );


  QObject::connect( ui->visualisationWidget, SIGNAL( sendMeshToBoundaryConditions(vtkSmartPointer<vtkUnstructuredGrid>) ),
                    ui->boundaryConditionWidget, SLOT( fromMesherOrGeometry(vtkSmartPointer<vtkUnstructuredGrid>) ) );

  QObject::connect( ui->visualisationWidget, SIGNAL( sendMeshToBoundaryConditions(vtkSmartPointer<vtkUnstructuredGrid>) ),
                    ui->solverWidget, SLOT( setGridToSolver(vtkSmartPointer<vtkUnstructuredGrid>) ) );

  QObject::connect( ui->visualisationWidget, SIGNAL( sendMeshToBoundaryConditions(vtkSmartPointer<vtkUnstructuredGrid>) ),
                    this, SLOT( activateBoundaryConditions() ) );

  QObject::connect( ui->mesherWidget, SIGNAL( sendMeshToGeometry(vtkSmartPointer<vtkUnstructuredGrid>,QString) ),
                    ui->solverWidget, SLOT( setGridToSolver(vtkSmartPointer<vtkUnstructuredGrid>) ) );


  QObject::connect( ui->boundaryConditionWidget, SIGNAL( sendBoundaryConditionsToSolver(std::shared_ptr<std::list<AppliedBoundaryCondition> >) ),
                    ui->solverWidget, SLOT( setApplyedBoundaryConditions(std::shared_ptr<std::list<AppliedBoundaryCondition> >) ) );

  QObject::connect( ui->boundaryConditionWidget, SIGNAL( sendBoundaryConditionsToSolver(std::shared_ptr<std::list<AppliedBoundaryCondition> >) ),
                    this, SLOT( activateSolver() ) );

  QObject::connect( ui->solverWidget, SIGNAL( sendResults(std::shared_ptr<TimeGrid>) ),
                    ui->resultsViewWidget, SLOT(setResults(std::shared_ptr<TimeGrid>) ) );

  QObject::connect( ui->solverWidget, SIGNAL( sendResults(std::shared_ptr<TimeGrid>) ),
                    this, SLOT( activateResults() ) );
}

/*!
 * \brief ClientMainWindow::~ClientMainWindow
 */
ClientMainWindow::~ClientMainWindow()
{
  delete ui;
}

/*!
 * \brief ClientMainWindow::changeEvent
 * \param e
 */
void ClientMainWindow::changeEvent(QEvent *e)
{
  QMainWindow::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}

/*!
 * \brief ClientMainWindow::addVtkUnstructuredGrid
 * \param fileName
 */
void ClientMainWindow::addVtkUnstructuredGrid(const QString& fileName)
{
  vtkSmartPointer< vtkXMLUnstructuredGridReader > reader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
  reader->SetFileName( fileName.toUtf8().constData() );
  reader->Update();

  vtkSmartPointer< vtkDataSetMapper > mapper = vtkSmartPointer< vtkDataSetMapper >::New();
  mapper->SetInputConnection( reader->GetOutputPort() );

  ui->visualisationWidget->addMapper( mapper, MESH_ACTOR, QString( fileName ).remove( 0, fileName.lastIndexOf( '/' ) + 1 ) );
  ui->visualisationWidget->resetCamera();
}

/*!
 * \brief ClientMainWindow::addXmlPolyData
 * \param fileName
 */
void ClientMainWindow::addXmlPolyData(const QString& fileName)
{
  vtkSmartPointer< vtkXMLPolyDataReader > reader = vtkSmartPointer< vtkXMLPolyDataReader >::New();
  reader->SetFileName( fileName.toUtf8().constData() );
  reader->Update();

  vtkSmartPointer< vtkPolyDataMapper > mapper = vtkSmartPointer< vtkPolyDataMapper >::New();
  mapper->SetInputConnection( reader->GetOutputPort() );

  ui->visualisationWidget->addMapper( mapper, GEOMETRY_ACTOR, QString( fileName ).remove( 0, fileName.lastIndexOf( '/' ) + 1 ) );
  ui->visualisationWidget->resetCamera();
}

/*!
 * \brief ClientMainWindow::addPolyData
 * \param fileName
 */
void ClientMainWindow::addPolyData(const QString& fileName)
{
  vtkSmartPointer< vtkPolyDataReader > reader = vtkSmartPointer< vtkPolyDataReader >::New();
  reader->SetFileName( fileName.toUtf8().constData() );
  reader->Update();

  vtkSmartPointer< vtkPolyDataMapper > mapper = vtkSmartPointer< vtkPolyDataMapper >::New();
  mapper->SetInputConnection( reader->GetOutputPort() );

  ui->visualisationWidget->addMapper( mapper, GEOMETRY_ACTOR, QString( fileName ).remove( 0, fileName.lastIndexOf( '/' ) + 1 ) );
  ui->visualisationWidget->resetCamera();
}

/*!
 * \brief ClientMainWindow::addStlPolyData
 * \param fileName
 */
void ClientMainWindow::addStlPolyData(const QString& fileName)
{
  vtkSmartPointer< vtkSTLReader > reader = vtkSmartPointer< vtkSTLReader >::New();
  reader->SetFileName( fileName.toUtf8().constData() );
  reader->Update();

  vtkSmartPointer< vtkPolyDataMapper > mapper = vtkSmartPointer< vtkPolyDataMapper >::New();
  mapper->SetInputConnection( reader->GetOutputPort() );

  ui->visualisationWidget->addMapper( mapper, GEOMETRY_ACTOR, QString( fileName ).remove( 0, fileName.lastIndexOf( '/' ) + 1 ) );
  ui->visualisationWidget->resetCamera();
}

/*!
 * \brief ClientMainWindow::addPlyPolyData
 * \param fileName
 */
void ClientMainWindow::addPlyPolyData(const QString& fileName)
{
  vtkSmartPointer< vtkPLYReader > reader = vtkSmartPointer< vtkPLYReader >::New();
  reader->SetFileName( fileName.toUtf8().constData() );
  reader->Update();

  vtkSmartPointer< vtkPolyDataMapper > mapper = vtkSmartPointer< vtkPolyDataMapper >::New();
  mapper->SetInputConnection( reader->GetOutputPort() );

  ui->visualisationWidget->addMapper( mapper, GEOMETRY_ACTOR, QString( fileName ).remove( 0, fileName.lastIndexOf( '/' ) + 1 ) );
  ui->visualisationWidget->resetCamera();
}

/*!
 * \brief ClientMainWindow::processFileName
 * \param fileName
 */
void ClientMainWindow::processFileName(const QString& fileName)
{
  if ( fileName.isEmpty() ) return;

  const QString extension = fileName.mid( fileName.lastIndexOf( "." ), fileName.size() );

  if (extension == ".vtu")
  {
    addVtkUnstructuredGrid( fileName );
  }
  else if (extension == ".vtp")
  {
    addXmlPolyData( fileName );
  }
  else if (extension == ".vtk")
  {
    addPolyData( fileName );
  }
  else if (extension == ".stl")
  {
    addStlPolyData( fileName );
  }
  else if (extension == ".ply")
  {
    addPlyPolyData( fileName );
  }
  else
  {
    QMessageBox::warning( this, tr( "Unsupported file format" ), tr( "Unsupported file format: " ) + extension );
  }
}

/*!
 * \brief ClientMainWindow::processFileNameLists
 * \param fileNameList
 */
void ClientMainWindow::processFileNameLists(const QStringList& fileNameList)
{
  if ( fileNameList.empty() ) return;
  std::for_each( fileNameList.begin(), fileNameList.end(), [&](const QString& fileName) { processFileName( fileName ); } );
}

void ClientMainWindow::open()
{
  const auto fileNameList = QFileDialog::getOpenFileNames( this, tr("Open geometry file"), "", tr( "Geometry files(*.vtp *.vtk *.stl *.ply);;Mesh files (*.vtu)" ) );

  processFileNameLists( fileNameList );
}

/*!
 * \brief ClientMainWindow::slotFromGemetryToMesher
 */
void ClientMainWindow::activateMesher()
{
  ui->tabWidget->setCurrentIndex( ui->tabWidget->indexOf( ui->mesherWidget->parentWidget() ) );
}

/*!
 * \brief ClientMainWindow::selectGeometry
 */
void ClientMainWindow::activateGeometry()
{
  ui->tabWidget->setCurrentIndex( ui->tabWidget->indexOf( ui->visualisationWidget->parentWidget() ) );
}

/*!
 * \brief ClientMainWindow::activateBoundaryConditions
 */
void ClientMainWindow::activateBoundaryConditions()
{
  ui->tabWidget->setCurrentIndex( ui->tabWidget->indexOf( ui->boundaryConditionWidget->parentWidget() ) );
}

/*!
 * \brief ClientMainWindow::activateSolver
 */
void ClientMainWindow::activateSolver()
{
  ui->tabWidget->setCurrentIndex( ui->tabWidget->indexOf( ui->solverWidget->parentWidget() ) );
}

/*!
 * \brief ClientMainWindow::activateResults
 */
void ClientMainWindow::activateResults()
{
  ui->tabWidget->setCurrentIndex( ui->tabWidget->indexOf( ui->resultsViewWidget->parentWidget() ) );
}
