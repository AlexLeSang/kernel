#include "AbstractTreeItem.hpp"

#include <QDebug>

#include <vtkCell.h>
#include <vtkCellArray.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkSmartPointer.h>
#include <../utils/vtk_utils/vtk_includes/vtkBooleanOperationPolyDataFilter.hpp>
#include <../utils/vtk_utils/vtk_includes/vtkIntersectionPolyDataFilter.hpp>
#include <numeric>

/*!
 * \brief AbstractTreeItem::AbstractTreeItem
 * \param name
 * \param color
 * \param parent
 */
AbstractTreeItem::AbstractTreeItem(RootTreeItem *parent) :
  parentItem( parent )
{
}

/*!
 * \brief AbstractTreeItem::~AbstractTreeItem
 */
AbstractTreeItem::~AbstractTreeItem()
{}

/*!
 * \brief AbstractTreeItem::parent
 * \return
 */
RootTreeItem *AbstractTreeItem::parent()
{
  return parentItem;
}

/*!
 * \brief AbstractTreeItem::setParent
 * \param parent
 */
void AbstractTreeItem::setParent(RootTreeItem *parent)
{
  parentItem = parent;
}

/*!
 * \brief AbstractTreeItem::getName
 * \return
 */
QString AbstractTreeItem::getName() const
{
  return applyedCondition.getName();
}

/*!
 * \brief AbstractTreeItem::setName
 * \param name
 */
void AbstractTreeItem::setName(const QString &name)
{
  applyedCondition.setName( name );
}

/*!
 * \brief AbstractTreeItem::getColor
 * \return
 */
QColor AbstractTreeItem::getColor() const
{
  return applyedCondition.getColor();
}

/*!
 * \brief AbstractTreeItem::setColor
 * \param value
 */
void AbstractTreeItem::setColor(const QColor &value)
{
  applyedCondition.setColor( value );
}

/*!
 * \brief AbstractTreeItem::getIntemType
 * \return
 */
BOUNDARY_CONDITION_TYPE AbstractTreeItem::getIntemType() const
{
  return applyedCondition.getConditionType();
}

/*!
 * \brief AbstractTreeItem::getBoundaryCondition
 * \return
 */
std::weak_ptr<AbstractBoundaryCondition> AbstractTreeItem::getBoundaryCondition() const
{
  return applyedCondition.getBoundaryCondition();
}

/*!
 * \brief AbstractTreeItem::setBoundaryCondition
 * \param value
 */
void AbstractTreeItem::setBoundaryCondition(std::shared_ptr<AbstractBoundaryCondition> &value)
{
  applyedCondition.setBoundaryCondition( value );
}

/*!
 * \brief AbstractTreeItem::saveSelectedGrid
 * \param grid
 */
void AbstractTreeItem::saveSelectedGrid(vtkSmartPointer<vtkUnstructuredGrid> grid)
{
  applyedCondition.setSelectedGrid( grid );
}


/*!
 * \brief AbstractTreeItem::loadSelectedGrid
 * \param polyData
 * \return
 */
vtkSmartPointer<vtkUnstructuredGrid> AbstractTreeItem::loadSelectedGrid(vtkPolyData *polyData)
{
  assert( polyData );
  if ( applyedCondition.checkCompatibility( polyData ) ) {
    return applyedCondition.getSelectedGrid();
  }
  else {
    vtkSmartPointer< vtkUnstructuredGrid > grid = vtkSmartPointer< vtkUnstructuredGrid >::New();
    return grid;
  }
}

/*!
 * \brief AbstractTreeItem::getApplyedCondition
 * \return
 */
AppliedBoundaryCondition& AbstractTreeItem::getApplyedCondition()
{
  return applyedCondition;
}

/*!
 * \brief AbstractTreeItem::setApplyedCondition
 * \param value
 */
void AbstractTreeItem::setApplyedCondition(const AppliedBoundaryCondition &value)
{
  applyedCondition = value;
}


