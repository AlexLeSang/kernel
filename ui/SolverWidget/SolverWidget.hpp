#ifndef SOLVERWIDGET_HPP
#define SOLVERWIDGET_HPP

#include <QWidget>

#include <vtkSmartPointer.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkScalarBarActor.h>

#include <../utils/fem_solvers/HeatFemSolver.hpp>

namespace Ui {
  class SolverWidget;
}

typedef std::unordered_map< double, vtkSmartPointer< vtkUnstructuredGrid > > TimeGrid;

class SolverWidget : public QWidget
{
  Q_OBJECT

public:
  explicit SolverWidget(QWidget *parent = 0);
  ~SolverWidget();

  vtkSmartPointer<vtkUnstructuredGrid> getGrid() const;
  void setGrid(const vtkSmartPointer<vtkUnstructuredGrid> &value);

  std::shared_ptr< std::list<AppliedBoundaryCondition> > getApplyedConditions();
  void setApplyedConditions(std::shared_ptr< std::list<AppliedBoundaryCondition> > conditions);

public slots:
  void setGridToSolver(vtkSmartPointer< vtkUnstructuredGrid > grid);
  void setApplyedBoundaryConditions(std::shared_ptr< std::list<AppliedBoundaryCondition> > applyedConditions);

signals:
  void solved();
  void stopSolver();
  void sendResults(std::shared_ptr<TimeGrid>);

protected:
  void changeEvent(QEvent *e);

private slots:
  void on_startPushButton_clicked();
  void on_stopPushButton_clicked();
  void solverFinished();
  void solverInterrupted();
  void solverTimeStepProcessed(double step);

private:
  void visualizeGrid();

private:
  Ui::SolverWidget *ui;
  vtkSmartPointer< vtkRenderer > renderer;

  vtkSmartPointer< vtkUnstructuredGrid > grid;
  std::shared_ptr< std::list<AppliedBoundaryCondition> > applyedConditions;

  kernel::utils::fem_solvers::HeatFemSolver solver;
  double totalSolutionTime;
};

namespace test {
  bool SolverWidgetTest(int argc, char* argv[]);
}


#endif // SOLVERWIDGET_HPP
