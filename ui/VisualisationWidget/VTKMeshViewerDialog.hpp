#ifndef VTKMESHVIEVERDIALOD_HPP
#define VTKMESHVIEVERDIALOD_HPP

#include <QDialog>

#include <vtkActor.h>
#include <vtkSmartPointer.h>
#include <vtkRenderer.h>
#include <vtkDelaunay3D.h>

namespace Ui {
  class VTKMeshViewerDialog;
}

// TODO make meshing here

/*!
 * \brief The VTKMeshViewerDialog class
 */
class VTKMeshViewerDialog : public QDialog
{
  Q_OBJECT

public:
  explicit VTKMeshViewerDialog(QWidget *parent, vtkActor* actor);
  ~VTKMeshViewerDialog();

protected:
  void changeEvent(QEvent *e);

private slots:
  void on_pushButton_clicked();
  void on_xMinDoubleSpinBox_valueChanged(double arg1);
  void on_xMaxDoubleSpinBox_valueChanged(double arg1);
  void on_yMinDoubleSpinBox_valueChanged(double arg1);
  void on_yMaxDoubleSpinBox_valueChanged(double arg1);
  void on_zMinDoubleSpinBox_valueChanged(double arg1);
  void on_zMaxDoubleSpinBox_valueChanged(double arg1);

  void on_clearPushButton_clicked();

private:
  Ui::VTKMeshViewerDialog *ui;

  vtkActor* actorPtr;
  vtkActor* meshedActorPtr;
  vtkSmartPointer< vtkRenderer > originalActorRenderer;

  double xMin;
  double xMax;
  double yMin;
  double yMax;
  double zMin;
  double zMax;
};


namespace test {
  bool VTKMeshViewerDialogTest(int argc, char *argv[]);
}


#endif // VTKMESHVIEVERDIALOD_HPP
