#include "GraphInveractor.hpp"

#include <WorkgroupGraphVisualizer.hpp>


vtkStandardNewMacro(GraphInveractor);


/*!
 * \brief GraphInveractor::OnLeftButtonDown
 */
void GraphInveractor::OnLeftButtonDown()
{
  /*
    int pickPosition[ 2 ];
    this->GetInteractor()->GetEventPosition( pickPosition );

    std::cout << "pickPosition: [" << pickPosition[ 0 ] << "," << pickPosition[ 1 ] << "]" << std::endl;

    vtkSmartPointer< vtkCellPicker > picker = vtkSmartPointer< vtkCellPicker >::New();
    picker->SetTolerance( 0.005 );

    // Pick from this location.
    picker->Pick( pickPosition[ 0 ], pickPosition[ 1 ], 0, graphLayoutView->GetRenderer() );
    auto pickedActor = picker->GetActor();

    if ( pickedActor != nullptr ) {
      addOrRemoveFromSelected( pickedActor );
    }
    else {
      cleanSelection();
    }
    */

  vtkInteractorStyleTrackballCamera::OnLeftButtonDown();
}

/*!
 * \brief GraphInveractor::OnKeyPress
 */
void GraphInveractor::OnKeyPress()
{
  vtkRenderWindowInteractor *iren = this->Interactor;
  const std::string sym = iren->GetKeySym();
  if ( "u" == sym ) {
    workgroupGraphVisualizer->update();
  }
}

/*!
 * \brief GraphInveractor::Execute
 * \param vtkNotUsed
 * \param eventId
 * \param vtkNotUsed
 */
void GraphInveractor::Execute(vtkObject *vtkNotUsed(caller), unsigned long eventId, void *vtkNotUsed(callData))
{
  if (vtkCommand::TimerEvent == eventId) {
    workgroupGraphVisualizer->update();
  }
}

/*!
 * \brief GraphInveractor::SetWorkgroupGraphVisualizer
 * \param visualizer
 */
void GraphInveractor::SetWorkgroupGraphVisualizer(WorkgroupGraphVisualizer *visualizer)
{
  assert( visualizer != nullptr );
  workgroupGraphVisualizer = visualizer;
}


/*!
 * \brief GraphInveractor::addSelected
 * \param pickedActor
 */
void GraphInveractor::addSelected(vtkActor *pickedActor)
{
  vtkSmartPointer< vtkActor > selectedActor( pickedActor );

  vtkSmartPointer< vtkProperty > oldProperty = vtkSmartPointer< vtkProperty >::New();
  oldProperty->DeepCopy( pickedActor->GetProperty() );
  selectedActorList.push_back( std::make_pair( selectedActor, oldProperty ) );

  pickedActor->GetProperty()->EdgeVisibilityOn();
  pickedActor->GetProperty()->SetEdgeColor( 0.75, 0.75, 0.75 );
  pickedActor->GetProperty()->SetLineWidth( 1.15 );
}

/*!
 * \brief GraphInveractor::removeSelected
 * \param pickedActor
 */
void GraphInveractor::removeSelected(vtkActor *pickedActor)
{
  for ( auto it = selectedActorList.begin(); it != selectedActorList.end();  ++it ) {
    if ( ( it->first.GetPointer() ) == pickedActor ) {
      it->first->GetProperty()->DeepCopy( it->second );
      selectedActorList.erase( it );
      break;
    }
  }
}

/*!
 * \brief GraphInveractor::cleanSelection
 */
void GraphInveractor::cleanSelection()
{
  auto copiedList = selectedActorList;
  std::for_each( copiedList.begin(), copiedList.end(), [&](std::pair< vtkSmartPointer< vtkActor >, vtkSmartPointer< vtkProperty > >& val) {
    vtkActor* actorPtr = std::get<0>( val );
    removeSelected( actorPtr );
    // emit signal_actorDeselected( actorPtr );
  } );

}

/*!
 * \brief GraphInveractor::addOrRemoveFromSelected
 * \param pickedActor
 */
void GraphInveractor::addOrRemoveFromSelected(vtkActor *pickedActor)
{
  bool found = false;
  for ( auto it = selectedActorList.begin(); it != selectedActorList.end();  ++it ) {
    if ( ( it->first.GetPointer() ) == pickedActor ) {
      found = true;
      break;
    }
  }

  if ( found ) {
    removeSelected( pickedActor );
    // emit signal_actorDeselected( pickedActor );
  }
  else {
    addSelected( pickedActor );
    // emit signal_actorSelected( pickedActor );
  }
}
