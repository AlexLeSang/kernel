#ifndef COMMAND_HPP
#define COMMAND_HPP

#include <atomic>

#include <QtNetwork/QTcpSocket>

#include "../server/ServerCommands.hpp"

namespace kernel {
  namespace server {
    namespace command {

      /*!
       * \return true - operation has finished, false - operation was interrupted
       */
      typedef bool (*Command)(QTcpSocket &, std::atomic<bool> &);

      bool DumbCommand(QTcpSocket &, std::atomic<bool> &);

      Command CommandProcessor(const ServerCommands &command);
    }
  }
}


#endif // COMMAND_HPP
