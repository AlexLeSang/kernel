#ifndef BOUNDARYCONDITIONWIDGET_HPP
#define BOUNDARYCONDITIONWIDGET_HPP

#include <QItemSelection>
#include <QWidget>
#include <VisibleSelectionInteractorStyle.hpp>
#include <vtkAxesActor.h>

class TreeModel;
class ColorColumnDelegate;

namespace Ui {
  class BoundaryConditionWidget;
}


/*!
 * \brief The BoundaryConditionWidget class
 */
class BoundaryConditionWidget : public QWidget
{
  const char* boundaryConditionExtension = ".kkbc";

Q_OBJECT

public:
  explicit BoundaryConditionWidget(QWidget *parent = 0);
  ~BoundaryConditionWidget();

  void setGrid(vtkSmartPointer<vtkUnstructuredGrid> grid);
  void setGlyphCoefficient(double coefficient);

  // Test function
  void addTestConditions(const std::size_t numberOfGenerators = 2);

public slots:
  void fromMesherOrGeometry(vtkSmartPointer<vtkUnstructuredGrid> grid);

signals:
  void sendBoundaryConditionsToSolver(std::shared_ptr< std::list<AppliedBoundaryCondition> >);

protected:
  void changeEvent(QEvent *e);

private slots:
  void on_pushButton_clicked();
  void on_pushButton_2_clicked();
  void selectionChanged(QModelIndex, QModelIndex);
  void updateAppearance();
  void switchState();
  void onCustomContextMenu(const QPoint& point);
  void loadBCAction();
  void saveBCAction();
  void removeCurrentItem();
  void displayOnItemContextMenu(const QPoint& point, QModelIndex index);
  void displayOnEmptyPlace(const QPoint& point);

  void on_acceptPushButton_clicked();

private:
  void calculateGlyphCoefficient(vtkSmartPointer<vtkUnstructuredGrid> &value);
  vtkSmartPointer<vtkPolyData> getSurfacePolyData(vtkSmartPointer<vtkUnstructuredGrid> &value);

private:
  Ui::BoundaryConditionWidget *ui;
  ColorColumnDelegate* delegate;

  TreeModel* treeModel;

  vtkSmartPointer< vtkActor > gridActor;
  vtkSmartPointer< vtkRenderer > renderer;
  vtkSmartPointer< VisibleSelectionInteractorStyle > style;
  vtkSmartPointer< vtkAxesActor > axesActor;
};


bool BoundaryConditionWidgetTest(int argc, char *argv[]);

#endif // BOUNDARYCONDITIONWIDGET_HPP
