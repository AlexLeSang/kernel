#ifndef MESHAPPENDER_HPP
#define MESHAPPENDER_HPP

#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>

#include <list>

namespace kernel {
  namespace utils {
    namespace mesh {

      /*!
       * \brief AppendMeshes
       * \param grids
       * \return
       */
      vtkSmartPointer< vtkUnstructuredGrid > AppendMeshes(const std::list<vtkSmartPointer<vtkUnstructuredGrid> > &grids);

    }
  }
}


namespace test {
  void VisualizeVtkUnstructutedGrid(vtkSmartPointer< vtkUnstructuredGrid > grid);
  bool AppendMeshesTest(int argc, char *argv[]);
}

#endif // MESHAPPENDER_HPP
