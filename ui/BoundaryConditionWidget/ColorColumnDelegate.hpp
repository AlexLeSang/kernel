#ifndef ColorColumnDelegate_HPP
#define ColorColumnDelegate_HPP

#include <QStyledItemDelegate>

/*!
 * \brief The ColorColumnDelegate class
 */
class ColorColumnDelegate : public QStyledItemDelegate
{
  Q_OBJECT

public:
  ColorColumnDelegate(QObject *parent = 0);

  void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

  QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &) const;

  void setEditorData(QWidget *editor, const QModelIndex &index) const;
  void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

  void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &) const;

signals:
  void changeAppearance() const;

};

#endif // ColorColumnDelegate_HPP
