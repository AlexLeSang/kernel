#include "ServerStatusWidget.hpp"
#include "ui_ServerStatusWidget.h"

#include <ServerDescription.hpp>

#include <QHostInfo>
#include <QMessageBox>
#include <QTimer>

#include <command/PingPongClientCommand.hpp>
#include <command/GetServerStatusCommand.hpp>

/*!
 * \brief ServerStatusWidget::fillUpdateIntervalList
 */
void ServerStatusWidget::fillUpdateIntervalList()
{
  auto fibLambda = [](const quint32 minValue = 500 /*ms*/, const quint32 maxValue = 20*1000 /*ms*/)
  {
    QVector< quint32 > sequence;
    sequence.push_back( 1 );
    sequence.push_back( 1 );
    while ( sequence.last() < maxValue ) {
      const auto prev = *(sequence.end() - 1);
      const auto prevPrev = *(sequence.end() - 2);
      sequence.push_back( prev + prevPrev );
    }

    const int removeDistance = std::distance( sequence.begin(), std::find_if( sequence.begin(), sequence.end(), [&](const quint32& val) { return val > minValue; } ) );
    sequence.remove( 0, removeDistance );

    return sequence;
  };

  const QVector< quint32 > sequence = fibLambda();

  std::for_each( sequence.constBegin(), sequence.constEnd(), [&](const quint32 number) {
    ui->updateIntervalComboBox->addItem( QString::number( number ) + " ms", QVariant( number ) );
  } );

}

/*!
 * \brief ServerStatusWidget::ServerStatusWidget
 * \param parent
 */
ServerStatusWidget::ServerStatusWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::ServerStatusWidget),
  kernelClient( nullptr ),
  updateTimerPtr( new QTimer( this ) ),
  producer( new WorkgroupGraphProducer() )
{
  ui->setupUi(this);
  ui->portLineEdit->setValidator( new QIntValidator( 1024, 65535, ui->portLineEdit ) );
  kernelClient =  kernel::client::KernelClient::getInstance();

  fillUpdateIntervalList();
  ui->disconnectPushButton->hide();
  ui->visualizePushButton->hide();

  connect( updateTimerPtr, SIGNAL(timeout()), this, SLOT(on_updateTimer_timeout()) );
}

/*!
 * \brief ServerStatusWidget::~ServerStatusWidget
 */
ServerStatusWidget::~ServerStatusWidget()
{
  delete ui;
}

/*!
 * \brief ServerStatusWidget::changeEvent
 * \param e
 */
void ServerStatusWidget::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}

/*!
 * \brief ServerStatusWidget::on_testPushButton_clicked
 */
void ServerStatusWidget::on_testPushButton_clicked()
{
  using namespace kernel::client;

  ui->infoLabel->clear();

  const QString addressStr = ui->addressLineEdit->text();
  QHostInfo hostInfo = QHostInfo::fromName( addressStr );
  if ( hostInfo.error() != QHostInfo::NoError ) {
    if ( hostInfo.error() == QHostInfo::HostNotFound ) {
      const auto errStr = tr("Host not found");
      ui->infoLabel->setText( errStr );
      // QMessageBox::warning( this, tr( "Test connection" ), errStr );
      return;
    }
    if ( hostInfo.error() == QHostInfo::UnknownError ) {
      const auto errStr = tr("An unknown error occured");
      ui->infoLabel->setText( errStr );
      // QMessageBox::warning( this, tr( "Test connection" ), errStr );
      return;
    }
    return;
  }

  QHostAddress address( hostInfo.addresses().first() );

  const QString portStr = ui->portLineEdit->text();
  bool ok = false;
  const int portNumber = portStr.toInt( &ok );
  if ( !ok ) {
    QMessageBox::warning( this, tr( "Test connection" ), tr("Bad port") );
    return;
  }

  ServerDescription sd;
  sd.address = address;
  sd.port = portNumber;
  KernelClient* localClient = KernelClient::getInstance();
  localClient->setServerDescription( sd );
  kernel::client::command::PingPongResult pingPongResult;
  auto pingResult = localClient->pingHost( &pingPongResult );

  if ( pingResult ) {
    const auto str = QString( tr( "Success! Avg ping to ") + address.toString() + tr( " is " ) + QString::number( pingPongResult.getAvg() ) );
    ui->infoLabel->setText( str );
  }
  else {
    const auto str = QString( tr( "Fail! Node ") + address.toString() + tr( " is unreachable" ) );
    ui->infoLabel->setText( str );
  }

  return;
}

/*!
 * \brief ServerStatusWidget::on_connectPushButton_clicked
 */
void ServerStatusWidget::on_connectPushButton_clicked()
{
  const auto currentIndex = ui->updateIntervalComboBox->currentIndex();
  bool intervalOk = false;
  const auto updateInterval = ui->updateIntervalComboBox->itemData( currentIndex ).toInt( &intervalOk );
  if ( !intervalOk ) {
    // TODO log it
    qWarning() << "ServerStatusWidget::on_connectPushButton_clicked(): cannot convert the interval value";
    return;
  }

  const QString addressStr = ui->addressLineEdit->text();
  QHostInfo hostInfo = QHostInfo::fromName( addressStr );
  if ( hostInfo.error() != QHostInfo::NoError ) {
    if ( hostInfo.error() == QHostInfo::HostNotFound ) {
      const auto errStr = tr("Host not found");
      ui->infoLabel->setText( errStr );
      // QMessageBox::warning( this, tr( "Test connection" ), errStr );
      return;
    }
    if ( hostInfo.error() == QHostInfo::UnknownError ) {
      const auto errStr = tr("An unknown error occured");
      ui->infoLabel->setText( errStr );
      // QMessageBox::warning( this, tr( "Test connection" ), errStr );
      return;
    }
    return;
  }

  QHostAddress address( hostInfo.addresses().first() );

  const QString portStr = ui->portLineEdit->text();
  bool ok = false;
  const int portNumber = portStr.toInt( &ok );
  if ( !ok ) {
    QMessageBox::warning( this, tr( "Test connection" ), tr("Bad port") );
    return;
  }

  kernel::client::ServerDescription sd;
  sd.address = address;
  sd.port = portNumber;
  kernelClient->setServerDescription( sd );

  updateTimerPtr->start( updateInterval );
  ui->connectPushButton->hide();
  ui->disconnectPushButton->show();
  ui->infoLabel->setText( tr( "Connected" ) );
  producer->setDone( false );
  ui->visualizePushButton->show();
}

/*!
 * \brief ServerStatusWidget::on_disconnectPushButton_clicked
 */
void ServerStatusWidget::on_disconnectPushButton_clicked()
{
  updateTimerPtr->stop();
  ui->connectPushButton->show();
  ui->disconnectPushButton->hide();
  ui->infoLabel->setText( tr( "Disconnected" ) );
  producer->setDone( true );
  ui->visualizePushButton->hide();
}

/*!
 * \brief ServerStatusWidget::on_updateTimer_timeout
 */
void ServerStatusWidget::on_updateTimer_timeout()
{
  //qDebug() << "ServerStatusWidget::on_updateTimer_event()";

  kernel::server::command::ServerStatus serverStatus;

  auto result = kernelClient->serverStatus( &serverStatus );

  result.waitForFinished();
  const bool commandSuccessful = result.result();
  if ( commandSuccessful ) {
    const auto  qMongoDBString = serverStatus.getMongodbAddress().address.toString() + ":" + QString::number( serverStatus.getMongodbAddress().port );
    ui->mongoDBAddressLabel->setText( qMongoDBString );

    ui->workgroupTagLabel->setText( serverStatus.getWorkgroupTag() );

    ui->workgroupSizeLabel->setText( QString::number( serverStatus.getWorkgroupGraph().size() ) ); // TODO replace with size
    producer->setGraph( serverStatus.getWorkgroupGraph() );
  }

}


/*!
 * \brief ServerStatusWidget::on_updateIntervalComboBox_activated
 * \param index
 */
void ServerStatusWidget::on_updateIntervalComboBox_activated(int index)
{
  bool ok = false;
  const auto updateInterval = ui->updateIntervalComboBox->itemData( index ).toInt( &ok );
  if ( !ok ) {
    // TODO log it
    qWarning() << "ServerStatusWidget::on_updateIntervalComboBox_activated(): cannot convert the interval value";
    return;
  }

  // qDebug() << "new updateInterval: " << updateInterval;
  updateTimerPtr->setInterval( updateInterval );
}


/*!
 * \brief WorkgroupGraphProducer::done
 * \return
 */
bool WorkgroupGraphProducer::done()
{
  return getDone();
}

/*!
 * \brief WorkgroupGraphProducer::produce
 * \return
 */
QList<kernel::server::command::ServerStatus::WorkgroupGraphEdge> WorkgroupGraphProducer::produce()
{
  return getGraph();
}

/*!
 * \brief WorkgroupGraphProducer::getGraph
 * \return
 */
QList<kernel::server::command::ServerStatus::WorkgroupGraphEdge> WorkgroupGraphProducer::getGraph() const
{
  QMutexLocker l( &graphMutex );
  return graph;
}

/*!
 * \brief WorkgroupGraphProducer::setGraph
 * \param value
 */
void WorkgroupGraphProducer::setGraph(const QList<kernel::server::command::ServerStatus::WorkgroupGraphEdge> &value)
{
  QMutexLocker l( &graphMutex );
  graph = value;
}


/*!
 * \brief WorkgroupGraphProducer::getDone
 * \return
 */
bool WorkgroupGraphProducer::getDone() const
{
  QMutexLocker l( &graphMutex );
  return done_;
}

/*!
 * \brief WorkgroupGraphProducer::setDone
 * \param val
 */
void WorkgroupGraphProducer::setDone(const bool val)
{
  QMutexLocker l( &graphMutex );
  done_ = val;
}

/*!
 * \brief ServerStatusWidget::on_visualizePushButton_clicked
 */
void ServerStatusWidget::on_visualizePushButton_clicked()
{
  const auto currentIndex = ui->updateIntervalComboBox->currentIndex();
  bool intervalOk = false;
  const auto updateInterval = ui->updateIntervalComboBox->itemData( currentIndex ).toInt( &intervalOk );
  if ( !intervalOk ) {
    // TODO log it
    qWarning() << "ServerStatusWidget::on_visualizePushButton_clicked(): cannot convert the interval value";
    return;
  }

  WorkgroupGraphVisualizer* visualizer = new WorkgroupGraphVisualizer( producer, "VisualizeWorkgroupGraphTest", updateInterval );
  if ( ! QThreadPool::globalInstance()->tryStart( visualizer ) ) {
    // TODO log it
    qWarning() << "ServerStatusWidget::on_visualizePushButton_clicked(): unable to start WorkgroupGraphVisualizer";
  }

}
