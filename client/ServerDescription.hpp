#ifndef SERVERDESCRIPTION_HPP
#define SERVERDESCRIPTION_HPP

#include <QtNetwork/QHostAddress>

namespace kernel {
  namespace client {

    /*!
     * \brief The ServerDescription struct
     */
    struct ServerDescription
    {
      ServerDescription() : address(QHostAddress::Null), port(0) {}

      ServerDescription(const QHostAddress address, quint32 port) : address( address ), port( port ) {}

      QHostAddress address;
      quint32 port;

    };

    /*!
     * \brief operator <<
     * \param d
     * \param sd
     * \return
     */
    inline QDebug operator << (QDebug d, const ServerDescription &sd)
    {
      d << sd.address << ":";
      d << sd.port;
      return d;
    }

    /*!
     * \brief operator <<
     * \param stream
     * \param hostDescription
     * \return
     */
    inline QDataStream & operator<< (QDataStream& stream, const ServerDescription& hostDescription)
    {
      stream << hostDescription.address;
      stream << hostDescription.port;
      return stream;
    }

    /*!
     * \brief operator >>
     * \param stream
     * \param hostDescription
     * \return
     */
    inline QDataStream & operator>> (QDataStream& stream, ServerDescription& hostDescription)
    {
      stream >> hostDescription.address;
      stream >> hostDescription.port;
      return stream;
    }


  }
}

#endif // SERVERDESCRIPTION_HPP
