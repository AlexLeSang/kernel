#include "VTKActorListModel.hpp"

#include "vtkProperty.h"

#include <QPixmap>

#include <cassert>

#include "QVTKWidget.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"

/*!
 * \brief VTKActorListModel::VTKActorListModel
 * \param parent
 */
VTKActorListModel::VTKActorListModel(QObject *parent, vtkRenderer *_rendererPtr) : QAbstractListModel( parent ), rendererPtr( _rendererPtr )
{
}

/*!
 * \brief VTKActorListModel::data
 * \param index
 * \param role
 * \return
 */
QVariant VTKActorListModel::data(const QModelIndex &index, int role) const
{
  if ( !index.isValid() ) {
    return QVariant();
  }

  if ( role == Qt::DisplayRole ) {
    const QString ptrStr = std::get<2>( actorList.value( index.row() ) ).isEmpty() ?
                             QString( "0x%1" ).arg( (quintptr) std::get<0>( actorList.value( index.row() ) ).GetPointer() , QT_POINTER_SIZE * 2, 16, QChar( '0' ) )
                           : std::get<2>( actorList.value( index.row() ) );
    if ( std::get<1>( actorList.value( index.row() ) ) == MESH_ACTOR ) {
      return QVariant( tr("Mesh: ") + ptrStr );
    }
    else if ( std::get<1>( actorList.value( index.row() ) ) == GEOMETRY_ACTOR ) {
      return QVariant( tr("Geometry: ") + ptrStr );
    }
    else if ( std::get<1>( actorList.value( index.row() ) ) == RESULT_ACTOR ) {
      return QVariant( tr("Result: ") + ptrStr );
    }
  }

  if ( role == Qt::EditRole ) {
    const auto actorTypePair = actorList.at( index.row() );
    vtkActor* pointer = std::get<0>( actorTypePair );
    emit deselectActor( pointer );
    return qVariantFromValue( actorTypePair );
  }

  if ( role == Qt::CheckStateRole ) {
    bool visible = false;
    {
      vtkActor* pointer = std::get<0>( actorList.at( index.row() ) ).GetPointer();
      vtkActorCollection* collectionPtr = rendererPtr->GetActors();
      visible = collectionPtr->IsItemPresent( pointer );
    }
    if ( visible ) {
      return Qt::Checked;
    }
    else {
      return Qt::Unchecked;
    }
  }

  return QVariant();
}

/*!
 * \brief VTKActorListModel::setData
 * \param index
 * \param value
 * \param role
 * \return
 */
bool VTKActorListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  Q_UNUSED( value );
  if ( role == Qt::EditRole ) {
    vtkActor* pointer = std::get<0>( actorList.at( index.row() ) ).GetPointer();
    emit dataChanged( index, index );
    emit selectActor( pointer );
    return true;
  }

  if ( role == Qt::CheckStateRole ) {
    bool visible = false;
    vtkActor* pointer = std::get<0>( actorList.at( index.row() ) ).GetPointer();
    {
      vtkActorCollection* collectionPtr = rendererPtr->GetActors();
      visible = collectionPtr->IsItemPresent( pointer );
    }
    if ( visible ) {
      rendererPtr->RemoveActor( pointer );
    }
    else {
      rendererPtr->AddActor( pointer );
    }

    emit dataChanged( index, index );
    return true;
  }
  return true;
}

/*!
 * \brief VTKActorListModel::flags
 * \param index
 * \return
 */
Qt::ItemFlags VTKActorListModel::flags(const QModelIndex &index) const
{
  if ( index.isValid() ) {
    return ( Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsUserCheckable );
  }

  return Qt::ItemIsDropEnabled;
}

/*!
 * \brief VTKActorListModel::rowCount
 * \param parent
 * \return
 */
int VTKActorListModel::rowCount(const QModelIndex &parent) const
{
  if ( parent.isValid() ) {
    return 0;
  }
  else {
    return actorList.size();
  }
}

/*!
 * \brief VTKActorListModel::addActor
 * \param actor
 * \param actorType
 * \param name
 */
void VTKActorListModel::addActor(vtkSmartPointer<vtkActor> actor, const ActorType actorType, const QString name)
{
  const int row = actorList.size();

  auto iter = std::find_if( actorList.begin(), actorList.end(), [&]( const ActorDescriptionType& actorDescription ) {
    const ActorType& type = std::get<1>( actorDescription );
    const QString& localName = std::get<2>( actorDescription );
    return (type == actorType) && (localName == name);
  } );

  if ( iter == actorList.end() ) {
    beginInsertRows( QModelIndex(), row, row );
    actorList.insert( row, std::make_tuple( actor, actorType, name ) );
    endInsertRows();
  }
  else {
    std::get<0>( *iter ) = actor;
  }
}

/*!
 * \brief VTKActorListModel::removeRows
 * \param row
 * \param count
 * \param parent
 * \return
 */
bool VTKActorListModel::removeRows(int row, int count, const QModelIndex &parent)
{
  if ( parent.isValid() ) {
    return false;
  }

  if ( (row >= actorList.size()) || (row + count <= 0) ) {
    return false;
  }

  int beginRow = qMax( 0, row );
  const int endRow = qMin( (row + count - 1), (actorList.size() - 1) );

  beginRemoveRows( parent, beginRow, endRow );

  while ( beginRow <= endRow ) {
    actorList.removeAt( beginRow );
    ++beginRow;
  }

  endRemoveRows();
  return true;
}
