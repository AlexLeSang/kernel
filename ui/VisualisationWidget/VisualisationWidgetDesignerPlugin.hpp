#ifndef VISUALISATIONWIDGETDESIGNERPLUGIN_HPP
#define VISUALISATIONWIDGETDESIGNERPLUGIN_HPP

#include <QDesignerCustomWidgetInterface>

class VisualisationWidgetDesignerPlugin : public QObject, public QDesignerCustomWidgetInterface
{
  Q_OBJECT
  Q_INTERFACES( QDesignerCustomWidgetInterface )

public:
  VisualisationWidgetDesignerPlugin(QObject* parent = 0);

  QString name() const;
  QString group() const;
  QString toolTip() const;
  QString whatsThis() const;
  QString includeFile() const;
  QIcon icon() const;
  bool isContainer() const;
  QWidget *createWidget(QWidget *parent);
  bool isInitialized() const;
  void initialize(QDesignerFormEditorInterface */*core*/);
  QString domXml() const;
  QString codeTemplate() const;

private:
  bool initialized;
};

#endif // VISUALISATIONWIDGETDESIGNERPLUGIN_HPP
