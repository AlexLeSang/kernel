#ifndef BOUNDARYCONDITIONWIDGETDESIGNERPLUGIN_HPP
#define BOUNDARYCONDITIONWIDGETDESIGNERPLUGIN_HPP

#include <QDesignerCustomWidgetInterface>

class BoundaryConditionWidgetDesignerPlugin : public QObject, public QDesignerCustomWidgetInterface
{
  Q_OBJECT
  Q_INTERFACES( QDesignerCustomWidgetInterface )

public:
  BoundaryConditionWidgetDesignerPlugin(QObject* parent = 0);

  QString name() const;
  QString group() const;
  QString toolTip() const;
  QString whatsThis() const;
  QString includeFile() const;
  QIcon icon() const;
  bool isContainer() const;
  QWidget *createWidget(QWidget *parent);
  bool isInitialized() const;
  void initialize(QDesignerFormEditorInterface */*core*/);
  QString domXml() const;
  QString codeTemplate() const;

private:
  bool initialized;
};

#endif // BOUNDARYCONDITIONWIDGETDESIGNERPLUGIN_HPP

