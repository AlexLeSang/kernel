#include "GetFemLaplacianTest.hpp"

#include <boost/timer/timer.hpp>

using std::endl; using std::cout; using std::cerr;
using std::ends; using std::cin;


namespace laplacian {


  /* some Getfem++ types that we will be using */
  using bgeot::base_small_vector; /* special class for small (dim<16) vectors */
  using bgeot::base_node; /* geometrical nodes (derived from base_small_vector)*/
  using bgeot::scalar_type; /* = double */
  using bgeot::size_type;   /* = unsigned long */

  /* definition of some matrix/vector types. These ones are built
 * using the predefined types in Gmm++
 */
  typedef gmm::rsvector<scalar_type> sparse_vector_type;
  typedef gmm::row_matrix<sparse_vector_type> sparse_matrix_type;
  typedef gmm::col_matrix<sparse_vector_type> col_sparse_matrix_type;
  typedef std::vector<scalar_type> plain_vector;

  /* Definitions for the exact solution of the Laplacian problem,
 *  i.e. Delta(sol_u) + sol_f = 0
 */

  base_small_vector sol_K; /* a coefficient */

  /* exact solution */
  scalar_type sol_u(const base_node &x)
  {
    auto res = sin(gmm::vect_sp(sol_K, x));
    std::cout << "sol_u(): res: " << res << std::endl;
    return res;
  }


  /* righ hand side */
  scalar_type sol_f(const base_node &x)
  {
    auto res = gmm::vect_sp(sol_K, sol_K) * sin(gmm::vect_sp(sol_K, x));
    std::cout << "sol_f(): res: " << res << std::endl;
    return res;
  }

  /* gradient of the exact solution */
  base_small_vector sol_grad(const base_node &x)
  {
    auto res = sol_K * cos(gmm::vect_sp(sol_K, x));
    std::cout << "sol_grad: res: " << res << std::endl;
    return res;
  }

  /* custom BCs */


  /* Permitivity */
  scalar_type permittivity(const base_node &x)
  {
    scalar_type p( 2.0 );
    return p;
  }

  /* Dirihlet BC */
  scalar_type dirihlet(const base_node &x)
  {
    scalar_type t( 5 );
    return t;
  }

  /* Neumann BC */
  base_small_vector neumann(const base_node &x)
  {
    base_small_vector b( -1.0, -4.0, -2.0 );
    return b;
  }




  /*
  structure for the Laplacian problem
  (not mandatory, just to gather the variables)
*/
  struct laplacian_problem {

    enum { DIRICHLET_BOUNDARY_NUM = 0, NEUMANN_BOUNDARY_NUM = 1};
    enum { DIRICHLET_WITH_MULTIPLIERS = 0, DIRICHLET_WITH_PENALIZATION = 1};
    getfem::mesh mesh;        /* the mesh */
    getfem::mesh_im mim;      /* the integration methods. */
    getfem::mesh_fem mf_u;    /* the main mesh_fem, for the Laplacian solution */
    getfem::mesh_fem mf_rhs;  /* the mesh_fem for the right hand side(f(x),..) */

    scalar_type residual;        /* max residual for the iterative solvers     */
    size_type N;
    size_type dirichlet_version;
    scalar_type dirichlet_coefficient; /* Penalization parameter.              */
    plain_vector U;

    std::string datafilename;
    bgeot::md_param PARAM;

    vtkSmartPointer< vtkUnstructuredGrid > grid;

    bool solve(void);
    void init(void);
    void compute_error();
    laplacian_problem(void) : mim(mesh), mf_u(mesh), mf_rhs(mesh) {}
  };

  /* Read parameters from the .param file, build the mesh, set finite element
 * and integration methods and selects the boundaries.
 */
  void laplacian_problem::init(void) {

    std::string MESH_TYPE = PARAM.string_value("MESH_TYPE","Mesh type ");
    std::string FEM_TYPE  = PARAM.string_value("FEM_TYPE","FEM name");
    std::string INTEGRATION = PARAM.string_value("INTEGRATION",
                                                 "Name of integration method");
    cout << "MESH_TYPE=" << MESH_TYPE << "\n";
    cout << "FEM_TYPE="  << FEM_TYPE << "\n";
    cout << "INTEGRATION=" << INTEGRATION << "\n";

    /* First step : build the mesh */
    bgeot::pgeometric_trans pgt = bgeot::geometric_trans_descriptor( MESH_TYPE );
    N = pgt->dim();


    // Read vtkUnstructuredGrid
    {
      assert( grid );

      const auto numberOfPoints = grid->GetNumberOfPoints();
      const auto numberOfCells = grid->GetNumberOfCells();

      assert( numberOfPoints );
      assert( numberOfCells );

      std::vector< size_type > indecies;
      indecies.reserve( numberOfPoints );

      for ( auto p = 0; p < numberOfPoints; ++p ) {
        double point[3];
        grid->GetPoint( p, point );
        indecies.push_back( mesh.add_point( bgeot::base_node( point[ 0 ], point[ 1 ], point[ 2 ] ) ) );
      }


      for ( auto c = 0; c < numberOfCells; ++c ) {
        vtkSmartPointer< vtkIdList > cell_points = vtkSmartPointer< vtkIdList >::New();
        grid->GetCellPoints( c, cell_points );
        size_t num_cell_points = cell_points->GetNumberOfIds();
        assert( num_cell_points == 4 );

        std::vector< size_type > convex( 4 );
        convex[ 0 ] = indecies[ cell_points->GetId( 0 ) ];
        convex[ 1 ] = indecies[ cell_points->GetId( 1 ) ];
        convex[ 2 ] = indecies[ cell_points->GetId( 2 ) ];
        convex[ 3 ] = indecies[ cell_points->GetId( 3 ) ];

        mesh.add_convex( pgt, convex.begin() );
      }

    }

    // Generate regular grid
    /*
    {
      std::vector<size_type> nsubdiv( N );
      std::fill( nsubdiv.begin(), nsubdiv.end(), PARAM.int_value("NX", "Number of space steps") );
      getfem::regular_unit_mesh( mesh, nsubdiv, pgt, PARAM.int_value("MESH_NOISED") != 0 );
    }
    */

    /*
    bgeot::base_matrix M( N, N );
    for ( size_type i = 0; i < N; ++i ) {
      static const char *t[] = {"LX","LY","LZ"};
      M( i, i ) = ( i < 3 ) ? PARAM.real_value( t[ i ], t[ i ] ) : 1.0;
    }
    if ( N > 1 ) {
      M( 0, 1 ) = PARAM.real_value("INCLINE") * PARAM.real_value("LY");
    }

    // scale the unit mesh to [LX,LY,..] and incline it
    mesh.transformation( M );
    */

    datafilename = PARAM.string_value("ROOTFILENAME","Base name of data files.");
    scalar_type FT = PARAM.real_value("FT", "parameter for exact solution");
    residual = PARAM.real_value("RESIDUAL");
    dirichlet_version = PARAM.int_value("DIRICHLET_VERSION", "Type of Dirichlet contion");

    if (dirichlet_version == 1) {
      dirichlet_coefficient = PARAM.real_value("DIRICHLET_COEFFICIENT", "Penalization coefficient for " "Dirichlet condition");
    }

    if (residual == 0.) {
      residual = 1e-10;
    }

    sol_K.resize(N);
    /*
    for (size_type j = 0; j < N; j++)
      sol_K[j] = ((j & 1) == 0) ? FT : -FT;
    */
    sol_K.fill( FT );

    /* set the finite element on the mf_u */
    getfem::pfem pf_u = getfem::fem_descriptor(FEM_TYPE);
    getfem::pintegration_method ppi = getfem::int_method_descriptor(INTEGRATION);

    mim.set_integration_method( mesh.convex_index(), ppi );
    mf_u.set_finite_element( mesh.convex_index(), pf_u );

    /* set the finite element on mf_rhs (same as mf_u is DATA_FEM_TYPE is
     not used in the .param file */
    std::string data_fem_name = PARAM.string_value("DATA_FEM_TYPE");

    if (data_fem_name.size() == 0) {
      GMM_ASSERT1(pf_u->is_lagrange(), "You are using a non-lagrange FEM. " << "In that case you need to set " << "DATA_FEM_TYPE in the .param file");
      mf_rhs.set_finite_element( mesh.convex_index(), pf_u );
    }
    else {
      mf_rhs.set_finite_element( mesh.convex_index(), getfem::fem_descriptor(data_fem_name) );
    }

    /* set boundary conditions
   * (Neuman on the upper face, Dirichlet elsewhere) */
    cout << "Selecting Neumann and Dirichlet boundaries\n";
    getfem::mesh_region border_faces;
    getfem::outer_faces_of_mesh( mesh, border_faces );
    for (getfem::mr_visitor i(border_faces); !i.finished(); ++i) {
      assert(i.is_face());

      std::cout << "Convex: " << i.cv() << " ";
      std::cout << "Face: " << i.f() << " ";

      base_node un = mesh.normal_of_face_of_convex(i.cv(), i.f());

      std::cout << "un: " << un << " ";

      un /= gmm::vect_norm2(un);
      if(gmm::abs( un[N - 3] + 1.0 ) < 1.0E-7) { // new Neumann face X

        std::cout << "is Neumann fase" << std::endl;

        mesh.region(NEUMANN_BOUNDARY_NUM).add(i.cv(), i.f());
      }
      /*
    else
      if(gmm::abs( un[N - 2] - 1.0 ) < 1.0E-7) { // new Neumann face Y

        std::cout << "is Neumann fase" << std::endl;

        mesh.region(NEUMANN_BOUNDARY_NUM).add(i.cv(), i.f());
      }
      else
      if(gmm::abs( un[N - 1] - 1.0 ) < 1.0E-7) { // new Neumann face Z

        std::cout << "is Neumann fase" << std::endl;

        mesh.region(NEUMANN_BOUNDARY_NUM).add(i.cv(), i.f());
      }
    */
      else {

        std::cout << "is Dirichlet fase" << std::endl;

        mesh.region(DIRICHLET_BOUNDARY_NUM).add(i.cv(), i.f());
      }
    }
  }

  bool laplacian_problem::solve(void) {

    getfem::model model;

    // Main unknown of the problem
    model.add_fem_variable("u", mf_u);

    // Laplacian term on u.
    getfem::add_Laplacian_brick(model, mim, "u");

    // Volumic source term. 1 val
    std::vector<scalar_type> F( mf_rhs.nb_dof() );
    getfem::interpolation_function( mf_rhs, F, permittivity );
    model.add_initialized_fem_data( "VolumicData", mf_rhs, F );
    getfem::add_source_term_brick( model, mim, "u", "VolumicData" );

    ///*
    gmm::resize( F, mf_rhs.nb_dof() * N );
    //  getfem::interpolation_function( mf_rhs, F, sol_grad, NEUMANN_BOUNDARY_NUM );
    getfem::interpolation_function( mf_rhs, F, neumann, NEUMANN_BOUNDARY_NUM );
    model.add_initialized_fem_data( "NeumannData", mf_rhs, F );
    getfem::add_normal_source_term_brick( model, mim, "u", "NeumannData", NEUMANN_BOUNDARY_NUM );
    //*/

    // Dirichlet condition.
    gmm::resize( F, mf_rhs.nb_dof() );
    //  getfem::interpolation_function( mf_rhs, F, sol_u );
    getfem::interpolation_function( mf_rhs, F, dirihlet );
    model.add_initialized_fem_data( "DirichletData", mf_rhs, F );


    if (dirichlet_version == DIRICHLET_WITH_MULTIPLIERS) {
      getfem::add_Dirichlet_condition_with_multipliers( model, mim, "u", mf_u, DIRICHLET_BOUNDARY_NUM, "DirichletData" );
    }
    else {
      getfem::add_Dirichlet_condition_with_penalization(model, mim, "u", dirichlet_coefficient, DIRICHLET_BOUNDARY_NUM, "DirichletData" );
    }

    model.listvar(cout);

    gmm::iteration iter(residual, 1, 40000);
    getfem::standard_solve(model, iter);

    gmm::resize(U, mf_u.nb_dof());
    gmm::copy(model.real_variable("u"), U);

    return (iter.converged());
  }

  /* compute the error with respect to the exact solution */
  void laplacian_problem::compute_error() {
    plain_vector V(mf_rhs.nb_basic_dof());
    getfem::interpolation(mf_u, mf_rhs, U, V);
    for (size_type i = 0; i < mf_rhs.nb_basic_dof(); ++i)
      V[i] -= sol_u(mf_rhs.point_of_basic_dof(i));
    cout.precision(16);
    cout << "L2 error = " << getfem::asm_L2_norm(mim, mf_rhs, V) << endl
         << "H1 error = " << getfem::asm_H1_norm(mim, mf_rhs, V) << endl
         << "Linfty error = " << gmm::vect_norminf(V) << endl;
  }

}


bool test::GetFemLaplacianTest(int argc, char **argv)
{
  GMM_SET_EXCEPTION_DEBUG; // Exceptions make a memory fault, to debug.
  FE_ENABLE_EXCEPT;        // Enable floating point exception for Nan.

  try {
    boost::timer::auto_cpu_timer t;

    laplacian::laplacian_problem p;
    p.PARAM.read_command_line(argc, argv);

    {
      vtkSmartPointer< vtkXMLUnstructuredGridReader > reader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
      reader->SetFileName( "left_meshed_tetgen.vtu" );
      reader->Update();

      vtkSmartPointer< vtkUnstructuredGrid > grid( reader->GetOutput() );
      p.grid = grid;
    }

    p.init();
    // p.mesh.write_to_file(p.datafilename + ".mesh");
    if (!p.solve()) GMM_ASSERT1(false, "Solve procedure has failed");
    {
      getfem::vtk_export exp("getfem_laplacian_test_output.vtk");
      exp.exporting( p.mf_u );
      // exp.write_point_data(mfp, P, "pressure"); // write a scalar field
      exp.write_point_data( p.mf_u, p.U, "Laplacian_result"); // write a vector field
    }
    // p.compute_error();
  }
  GMM_STANDARD_CATCH_ERROR;



  // Visualize
  {
    vtkSmartPointer<vtkUnstructuredGridReader> reader =
        vtkSmartPointer<vtkUnstructuredGridReader>::New();
    reader->SetFileName( "getfem_laplacian_test_output.vtk" );
    reader->Update();


    std::string results_name;
    double range[ 2 ];

    vtkSmartPointer< vtkUnstructuredGrid > grid( reader->GetOutput() );
    {

      vtkSmartPointer< vtkPointData > pd( grid->GetPointData() );
      if ( pd )
      {
        std::cout << "Contains point data with " << pd->GetNumberOfArrays() << " arrays." << std::endl;
        for (int i = 0; i < pd->GetNumberOfArrays(); i++)
        {
          std::cout << "\tArray " << i << " is named " << (pd->GetArrayName(i) ? pd->GetArrayName(i) : "NULL") << std::endl;
          results_name = pd->GetArrayName(i);
          //*
          vtkSmartPointer< vtkDataArray > arr( pd->GetArray( i ) );
          assert( arr );
          const auto arrNumOfTuples = arr->GetNumberOfTuples();
          std::cout << "arrNumOfTuples: " << arrNumOfTuples << std::endl;
          arr->GetRange( range );
          std::cout << "[" << range[ 0 ] << ", " << range[ 1 ] << "]" << std::endl;
          /*
          for ( auto j = 0; j < arrNumOfTuples; ++ j ) {
            std::cout << arr->GetComponent( VTK_DOUBLE, j ) << "\n";
          }
          */
          std::cout << std::endl;
          //*/
        }
      }


      /*
      vtkSmartPointer< vtkCellData > cd( grid->GetCellData() );
      if ( cd )
      {
        std::cout << "Contains cell data with " << cd->GetNumberOfArrays() << " arrays." << std::endl;
        for (int i = 0; i < cd->GetNumberOfArrays(); i++)
        {
          std::cout << "\tArray " << i << " is named " << (cd->GetArrayName(i) ? cd->GetArrayName(i) : "NULL") << std::endl;
        }
      }
      */

    }

    vtkSmartPointer<vtkDataSetMapper> mapper =
        vtkSmartPointer<vtkDataSetMapper>::New();
    mapper->SetInputConnection( reader->GetOutputPort() );
    mapper->ScalarVisibilityOn();
    mapper->SetScalarModeToUsePointData();
    // mapper->SetColorModeToMapScalars();

    vtkSmartPointer<vtkScalarBarActor> scalarBar =
        vtkSmartPointer<vtkScalarBarActor>::New();
    scalarBar->SetLookupTable(mapper->GetLookupTable());
    scalarBar->SetTitle( results_name.c_str() );
    scalarBar->SetNumberOfLabels( 5 );

    // Create a lookup table to share between the mapper and the scalarbar
    vtkSmartPointer<vtkLookupTable> hueLut =
        vtkSmartPointer<vtkLookupTable>::New();
    hueLut->SetTableRange( range );
    // hueLut->SetHueRange(0, 1);
    // hueLut->SetSaturationRange(1, 1);
    // hueLut->SetValueRange(1, 1);
    hueLut->SetNumberOfColors( 1024 );
    hueLut->Build();

    mapper->SetLookupTable( hueLut );
    mapper->SetScalarRange( range );
    scalarBar->SetLookupTable( hueLut );


    vtkSmartPointer<vtkActor> actor =
        vtkSmartPointer<vtkActor>::New();
    actor->SetMapper(mapper);
    actor->GetProperty()->EdgeVisibilityOn();

    vtkSmartPointer<vtkRenderer> renderer =
        vtkSmartPointer<vtkRenderer>::New();
    vtkSmartPointer<vtkRenderWindow> renderWindow =
        vtkSmartPointer<vtkRenderWindow>::New();
    renderWindow->AddRenderer(renderer);
    renderWindow->SetSize( 1024, 768 );
    vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
        vtkSmartPointer<vtkRenderWindowInteractor>::New();
    renderWindowInteractor->SetRenderWindow(renderWindow);

    vtkSmartPointer<vtkInteractorStyleTrackballCamera> style =
        vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New(); //like paraview

    renderWindowInteractor->SetInteractorStyle( style );

    renderer->AddActor(actor);
    renderer->AddActor2D(scalarBar);
    renderer->SetBackground(.0, .0, .0); // Background color green

    renderWindow->Render();
    renderWindowInteractor->Start();
  }

  return true;
}
