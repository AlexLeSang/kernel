#ifndef ABSTRACTBOUNDARYCONDITIONTREEITEM_HPP
#define ABSTRACTBOUNDARYCONDITIONTREEITEM_HPP

#include <TreeItems/RootTreeItem.hpp>
#include <QColor>

#include <vtkUnstructuredGrid.h>
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>

#include <../../utils/boundary_condition/AbstractBoundaryCondition.hpp>
#include <../../utils/boundary_condition/AppliedBoundaryCondition.hpp>

/*!
 * \brief The AbstractTreeItem class
 */
class AbstractTreeItem
{
public:
  AbstractTreeItem(RootTreeItem* parent = 0);
  virtual ~AbstractTreeItem();

  RootTreeItem *parent();
  void setParent(RootTreeItem* parent);

  QString getName() const;
  void setName(const QString& name);

  virtual int columnCount() const = 0;
  virtual QVariant data(int column) const = 0;
  virtual bool setData(int column, const QVariant &value) = 0;

  QColor getColor() const;
  void setColor(const QColor &value);

  BOUNDARY_CONDITION_TYPE getIntemType() const;

  std::weak_ptr<AbstractBoundaryCondition> getBoundaryCondition() const;
  void setBoundaryCondition(std::shared_ptr<AbstractBoundaryCondition> &value);

  void saveSelectedGrid(vtkSmartPointer<vtkUnstructuredGrid> grid);
  vtkSmartPointer< vtkUnstructuredGrid > loadSelectedGrid(vtkPolyData* polyData);

  AppliedBoundaryCondition& getApplyedCondition();
  void setApplyedCondition(const AppliedBoundaryCondition &value);

protected:
  AppliedBoundaryCondition applyedCondition;

private:
  RootTreeItem* parentItem;
};

#endif // ABSTRACTBOUNDARYCONDITIONTREEITEM_HPP
