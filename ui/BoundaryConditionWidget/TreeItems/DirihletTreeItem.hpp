#ifndef DirihletTreeItem_HPP
#define DirihletTreeItem_HPP

#include <QString>
#include "AbstractTreeItem.hpp"
#include <../../utils/boundary_condition/DirihletBoundaryCondition.hpp>

/*!
 * \brief The DirihletTreeItem class
 */
class DirihletTreeItem : public AbstractTreeItem
{
public:
  DirihletTreeItem(const QString& name = QString(), qreal value = 0.0, const QColor color = Qt::white, RootTreeItem* parent = 0);

  // AbstractBoundaryConditionTreeItem interface
public:
  virtual int columnCount() const;
  virtual QVariant data(int column) const;
  virtual bool setData(int column, const QVariant &value);

  qreal getBcValue() const;
  void setBcValue(const qreal &value);
};

#endif // DirihletTreeItem_HPP
