#ifndef MONGOGRIDFSDRIVER_HPP
#define MONGOGRIDFSDRIVER_HPP

#include <memory>
#include <fstream>
#include <string>
#include <list>

#include <mongo/client/dbclient.h>

#include <vtkSmartPointer.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkUnstructuredGridWriter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridReader.h>

namespace utils {

  class MongoGridFSDriver
  {
  public:
    MongoGridFSDriver(std::shared_ptr< mongo::DBClientReplicaSet > replicaSet) : replicaSet( replicaSet ) {}

    std::list< std::string > files(const std::string &databseName, bool *ok = nullptr) const;
    vtkSmartPointer< vtkUnstructuredGrid > readGrid(const std::string &databseName, const std::string& name, bool *ok = nullptr);
    bool saveGrid(vtkUnstructuredGrid* grid, const std::string& databseName, const std::string& name, bool *ok = nullptr);
    bool saveGridToFile(const std::string& databseName, const std::string& name, const std::string& fileName, bool *ok = nullptr);
    bool saveGridFromFile(const std::string& fileName, const std::string& databseName, const std::string& name, bool *ok = nullptr);

    void removeGrid(const std::string& databseName, const std::string& name, bool *ok = nullptr);
    void removeGrids(const std::string& databseName, const std::list< std::string >& names, bool *ok = nullptr);
    bool fileExists(const std::string& databseName, const std::string& name, bool *ok = nullptr);

  private:
    std::shared_ptr< mongo::DBClientReplicaSet > replicaSet;
  };
}



#endif // MONGOGRIDFSDRIVER_HPP
