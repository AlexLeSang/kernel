#include "VTKActorEditorDialog.hpp"
#include "ui_VTKActorEditorDialog.h"

#include "vtkRenderWindow.h"
#include "vtkRenderer.h"
#include "vtkProperty.h"

/*!
 * \brief VTKActorEditorDialog::VTKActorEditorDialog
 * \param parent
 */
VTKActorEditorDialog::VTKActorEditorDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::VTKActorEditorDialog),
  actorPtr( nullptr )
{
  ui->setupUi(this);
  setModal( true );
  oldPropetry = vtkSmartPointer< vtkProperty >::New();

  renderer = vtkSmartPointer< vtkRenderer >::New();
  ui->qvtkWidget->GetRenderWindow()->AddRenderer( renderer );

  QObject::connect( this, SIGNAL( rejected() ), this, SIGNAL( editingFinished() ) );
}

/*!
 * \brief VTKActorEditorDialog::~VTKActorEditorDialog
 */
VTKActorEditorDialog::~VTKActorEditorDialog()
{
  delete ui;
}

/*!
 * \brief VTKActorEditorDialog::setActor
 * \param newActorPtr
 */
void VTKActorEditorDialog::setActor(vtkActor *newActorPtr)
{
  actorPtr = newActorPtr;
  oldPropetry->DeepCopy( actorPtr->GetProperty() );
  renderer->AddActor( actorPtr );
  ui->edgeVisibilityCheckBox->setChecked( actorPtr->GetProperty()->GetEdgeVisibility() );
  ui->interpolationComboBox->setCurrentIndex( actorPtr->GetProperty()->GetInterpolation() );
  ui->representationComboBox->setCurrentIndex( actorPtr->GetProperty()->GetRepresentation() );
  ui->opacitySlider->setValue( (actorPtr->GetProperty()->GetOpacity() * 100 - 1) );
}

/*!
 * \brief VTKActorEditorDialog::actor
 * \return
 */
vtkActor *VTKActorEditorDialog::actor()
{
  return actorPtr;
}

/*!
 * \brief VTKActorEditorDialog::changeEvent
 * \param e
 */
void VTKActorEditorDialog::changeEvent(QEvent *e)
{
  QDialog::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}

/*!
 * \brief VTKActorEditorDialog::on_interpolationComboBox_currentIndexChanged
 * \param index
 */
void VTKActorEditorDialog::on_interpolationComboBox_currentIndexChanged(int index)
{
  actorPtr->GetProperty()->SetInterpolation( index );
  ui->qvtkWidget->update();
}

/*!
 * \brief VTKActorEditorDialog::on_representationComboBox_currentIndexChanged
 * \param index
 */
void VTKActorEditorDialog::on_representationComboBox_currentIndexChanged(int index)
{
  actorPtr->GetProperty()->SetRepresentation( index );
  ui->qvtkWidget->update();
}


/*!
 * \brief VTKActorEditorDialog::on_edgeVisibilityCheckBox_clicked
 * \param checked
 */
void VTKActorEditorDialog::on_edgeVisibilityCheckBox_clicked(bool checked)
{
  actorPtr->GetProperty()->SetEdgeVisibility( checked );
  ui->qvtkWidget->update();
}

/*!
 * \brief VTKActorEditorDialog::on_opacitySlider_valueChanged
 * \param value
 */
void VTKActorEditorDialog::on_opacitySlider_valueChanged(int value)
{
  actorPtr->GetProperty()->SetOpacity( (value + 1) / 100.0 );
  ui->qvtkWidget->update();
}

/*!
 * \brief VTKActorEditorDialog::restoreAndReject
 */
void VTKActorEditorDialog::restoreAndReject()
{
  actorPtr->SetProperty( oldPropetry );
  reject();
}
