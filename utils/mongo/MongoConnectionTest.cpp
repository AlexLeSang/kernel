#include "MongoConnectionTest.hpp"

#include <vtkSmartPointer.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkUnstructuredGridWriter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridReader.h>
#include <fstream>

#include <MongoGridFSDriver.hpp>

namespace test {

  bool mongotest()
  {
    const std::string ns( "test.threads" );
    // const std::string replSet("NULP/192.168.0.9:27017,192.168.0.10:27017");
    // const std::string replSet("NULP/192.168.0.10:27017");
    constexpr int concurrency_level = 10;


    const std::string replica_set_name = "NULP";
    std::vector< mongo::HostAndPort > hostAndPortVector;

    hostAndPortVector.push_back( mongo::HostAndPort( "192.168.0.10", 27017 ) );
    hostAndPortVector.push_back( mongo::HostAndPort( "192.168.0.9", 27017 ) );

    std::ostringstream os;
    os << replica_set_name;
    os << "/";
    os << hostAndPortVector.front().host();
    os << ":";
    os << hostAndPortVector.front().port();

    for ( auto it = hostAndPortVector.begin() + 1; it != hostAndPortVector.end(); ++ it ) {
      os << ",";
      os << it->host();
      os << ":";
      os << it->port();
    }

    const std::string replSet = os.str();

    std::cerr << "replSet: " << replSet << std::endl;

    std::vector< std::future< void > > futures;
    futures.reserve( concurrency_level );

    auto conn_lambda = [](std::unique_ptr< mongo::ScopedDbConnection > scopedConnection){
      const std::string ns( "test.threads" );
      constexpr unsigned int n_repeat = 4;
      constexpr unsigned int ms_wait = 500;

      auto tick = n_repeat;
      while ( -- tick ) {

        std::ostringstream ss;
        ss << std::this_thread::get_id();
        const std::string idstr = ss.str();

        (*scopedConnection)->insert( ns, BSON( "thread" << idstr << "tick" << tick ) );

        const auto count = (*scopedConnection)->count( ns );

        {
          static std::mutex m;
          std::lock_guard< std::mutex > guard( m );
          std::cerr << "thread: " << idstr << " tick: " << tick << " count: " << count << std::endl;
        }

        //        auto cursor = scopedConnection->query( ns, mongo::BSONObj() );

        //        while ( cursor->more() ) {
        //          auto obj = cursor->next();
        //          std::cerr << "\tobj: " << obj << std::endl;
        //        }

        scopedConnection->done();

        std::this_thread::sleep_for( std::chrono::milliseconds( ms_wait ) );
      }

    };


    for ( auto i = 0; i < concurrency_level; ++ i ) {
      std::unique_ptr< mongo::ScopedDbConnection > scopedConnection( new mongo::ScopedDbConnection( replSet ) );
      futures.push_back( std::async( std::launch::async, conn_lambda, std::move( scopedConnection ) ) );
    }

    std::for_each( futures.begin(), futures.end(), [](std::future< void >& val ) {
      val.wait();
    } );

    // Cleanup
    {
      mongo::ScopedDbConnection scopedConnection( replSet );
      scopedConnection->remove( ns, mongo::BSONObj() );
      scopedConnection.done();
    }

    std::cerr << "All is OK, bye" << std::endl;
    
    return true;
  }

  bool namedFileGridFSTest()
  {
    const std::string replica_set_name = "NULP";
    const char * fileName = "simple_cube.vtu";
    const char * newFileName = "simple_cube_gridFS.vtu";

    std::vector< mongo::HostAndPort > hostAndPortVector;
    hostAndPortVector.push_back( mongo::HostAndPort( "192.168.0.10", 27017 ) );
    hostAndPortVector.push_back( mongo::HostAndPort( "192.168.0.9", 27017 ) );

    mongo::DBClientReplicaSet replicaSet( replica_set_name, hostAndPortVector );

    try {
      const bool connResult = replicaSet.connect();
      if ( !connResult ) {
        std::cerr << "Connection failed, " << replicaSet.getLastError() << std::endl;
        return false;
      }
    }
    catch( const mongo::DBException &e ) {
      std::cerr << "mongo::DBException: " << e.what() << std::endl;
      return false;
    }

    mongo::GridFS gridFS( replicaSet, "gridFsDB" );

    auto printList = [&]() {
      std::auto_ptr< mongo::DBClientCursor > cursor = gridFS.list();
      while ( cursor->more() ) {
        mongo::BSONObj obj = cursor->next();
        std::cout << obj << std::endl;
        std::string filename = obj.getStringField( "filename" );
        std::cout << "filename: " << filename << std::endl;
      }
    };

    std::cout << "\nBefore insertion: " << std::endl;
    printList();

    std::cout << "Insert..." << std::endl;
    gridFS.storeFile( fileName );

    std::cout << "\nAfter insertion: " << std::endl;
    printList();

    std::cout << "Retreive..." << std::endl;
    mongo::GridFile gridFile = gridFS.findFile( fileName );

    std::cout << "Save..." << std::endl;
    gridFile.write( newFileName );

    std::cout << "Remove..." << std::endl;
    gridFS.removeFile( fileName );

    std::cout << "\nAfter removing: " << std::endl;
    printList();

    return true;
  }


  bool bufferGridFSTest(int argc, char * argv[])
  {
    assert( argc == 3 );

    {
      vtkSmartPointer< vtkXMLUnstructuredGridReader > reader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
      reader->SetFileName( argv[ 1 ] );
      reader->Update();
      vtkSmartPointer< vtkUnstructuredGrid > grid = reader->GetOutput();

      const auto numberOfPoints = grid->GetNumberOfPoints();
      const auto numberOfCells = grid->GetNumberOfCells();

      vtkSmartPointer< vtkUnstructuredGridWriter > stringWriter = vtkSmartPointer< vtkUnstructuredGridWriter >::New();
      stringWriter->SetInput( grid );
      stringWriter->WriteToOutputStringOn();
      stringWriter->Write();
      const char* gridString = stringWriter->GetOutputString();
      const std::size_t gridStringSize = stringWriter->GetOutputStringLength();

      std::cout << "outputStringSize: " << gridStringSize << std::endl;

      const std::string replica_set_name = "NULP";
      std::vector< mongo::HostAndPort > hostAndPortVector;
      hostAndPortVector.push_back( mongo::HostAndPort( "192.168.0.10", 27017 ) );
      hostAndPortVector.push_back( mongo::HostAndPort( "192.168.0.9", 27017 ) );

      mongo::DBClientReplicaSet replicaSet( replica_set_name, hostAndPortVector );

      try {
        const bool connResult = replicaSet.connect();
        if ( !connResult ) {
          std::cerr << "Connection failed, " << replicaSet.getLastError() << std::endl;
          return false;
        }
      }
      catch( const mongo::DBException &e ) {
        std::cerr << "mongo::DBException: " << e.what() << std::endl;
        return false;
      }

      mongo::GridFS gridFS( replicaSet, "gridFsDB" );

      auto printList = [&]() {
        std::auto_ptr< mongo::DBClientCursor > cursor = gridFS.list();
        while ( cursor->more() ) {
          mongo::BSONObj obj = cursor->next();
          std::cout << obj << std::endl;
          std::string filename = obj.getStringField( "filename" );
          std::cout << "filename: " << filename << std::endl;
        }
      };

      std::cout << "\nBefore insertion: " << std::endl;
      printList();

      std::cout << "Insert..." << std::endl;
      gridFS.storeFile( gridString, gridStringSize, argv[ 1 ] );

      std::cout << "\nAfter insertion: " << std::endl;
      printList();

      std::cout << "Retreive..." << std::endl;
      mongo::GridFile gridFile = gridFS.findFile( argv[ 1 ] );

      std::cout << "Load from grid..." << std::endl;

      std::ostringstream buf;
      gridFile.write( buf );
      const auto str = buf.str();
      std::cout << "str.size(): " << str.size() << std::endl;
      assert( str.size() == gridStringSize );
      const char* obtainedString = str.c_str();

      std::cout << "Remove..." << std::endl;
      gridFS.removeFile( argv[ 1 ] );

      std::cout << "\nAfter removing: " << std::endl;
      printList();

      vtkSmartPointer< vtkUnstructuredGridReader > stringReader = vtkSmartPointer< vtkUnstructuredGridReader >::New();
      stringReader->ReadFromInputStringOn();
      stringReader->SetInputString( obtainedString, gridStringSize );
      stringReader->Update();

      vtkSmartPointer< vtkUnstructuredGrid > readGrid( stringReader->GetOutput() );
      assert( readGrid->GetNumberOfPoints() == numberOfPoints );
      assert( readGrid->GetNumberOfCells() == numberOfCells );

      vtkSmartPointer< vtkXMLUnstructuredGridWriter > writer = vtkSmartPointer< vtkXMLUnstructuredGridWriter >::New();
      writer->SetFileName( argv[ 2 ] );
      writer->SetInput( readGrid );
      writer->Write();
    }

    return true;
  }



  bool classTest(int argc, char * argv[])
  {
    assert( argc == 3 );
    const std::string replica_set_name = "NULP";
    std::vector< mongo::HostAndPort > hostAndPortVector;
    hostAndPortVector.push_back( mongo::HostAndPort( "192.168.0.10", 27017 ) );
    hostAndPortVector.push_back( mongo::HostAndPort( "192.168.0.9", 27017 ) );

    const std::string database_name = "gridFsDB";

    const std::string input_filename = argv[ 1 ];
    const std::string output_filename = argv[ 2 ];

    auto print_list = [&](const std::list< std::string >& list) {
      std::cout << "list.size(): " << list.size() << std::endl;
      for (const auto& name : list) {
        std::cout << name << std::endl;
      }
    };

    std::shared_ptr< mongo::DBClientReplicaSet > replicaSet( new mongo::DBClientReplicaSet( replica_set_name, hostAndPortVector ) );

    try {
      const bool connResult = replicaSet->connect();
      if ( !connResult ) {
        std::cerr << "Connection failed, " << replicaSet->getLastError() << std::endl;
        return false;
      }
    }
    catch( const mongo::DBException &e ) {
      std::cerr << "mongo::DBException: " << e.what() << std::endl;
      return false;
    }


    utils::MongoGridFSDriver driver( replicaSet );
    bool ok = false;
    {
      assert( ! driver.fileExists( database_name, input_filename, &ok ) );
      assert( ok );

    }

    {
      driver.saveGridFromFile( input_filename, database_name, input_filename, &ok );
      assert( driver.fileExists( database_name, input_filename, &ok ) );
      assert( ok );

      auto list = driver.files( database_name, &ok );
      assert( ok );
      assert( !list.empty() );

      driver.removeGrid( database_name, input_filename, &ok );
      assert( ok );
    }

    {
      driver.saveGridFromFile( input_filename, database_name, input_filename, &ok );
      assert( ok );

      assert( driver.fileExists( database_name, input_filename, &ok ) );
      assert( ok );

      assert( driver.saveGridToFile( database_name, input_filename, output_filename, &ok ) );
      assert( ok );

      driver.removeGrid( database_name, input_filename, &ok );
      assert( ok );
    }

    {
      vtkSmartPointer< vtkXMLUnstructuredGridReader > reader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
      reader->SetFileName( input_filename.c_str() );
      reader->Update();
      vtkSmartPointer< vtkUnstructuredGrid > grid = reader->GetOutput();

      driver.saveGrid( grid, database_name, input_filename, &ok );
      assert( driver.fileExists( database_name, input_filename, &ok ) );
      assert( ok );

      driver.removeGrid( database_name, input_filename, &ok );
      assert( ok );
    }

    {
      driver.saveGridFromFile( input_filename, database_name, input_filename, &ok );
      assert( ok );

      assert( driver.fileExists( database_name, input_filename, &ok ) );
      assert( ok );

      vtkSmartPointer< vtkUnstructuredGrid > grid = driver.readGrid( database_name, input_filename, &ok );
      assert( ok );
      assert( grid->GetNumberOfPoints() );
      assert( grid->GetNumberOfCells() );

      driver.removeGrid( database_name, input_filename, &ok );
      assert( ok );
    }

    return true;
  }


}
