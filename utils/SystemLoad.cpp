#include "SystemLoad.hpp"

#include <QDebug>

namespace kernel {
  namespace utils {

    /*!
     * \brief SystemLoad::currentCPULoad
     * \return
     */
    qreal SystemLoad::currentCPULoad()
    {
#ifdef Q_OS_LINUX

      const TimeList timeList = getTimeList();
      ThreadSleep::msleep( SLEEP_INTERVAL );
      const TimeList newTimedList = getTimeList();

      // Take delta with the previous timing
      int sum  = 0;
      int idle = 0;
      int iowait = 0;
      int cpu = 0;

      for ( int i = 2; i < 7 && i < newTimedList.length(); ++i ) {
        sum += newTimedList.at(i).toInt() - timeList.at(i).toInt();
      }

      idle = newTimedList.at(5).toInt() - timeList.at(5).toInt();
      iowait = newTimedList.at(6).toInt() - timeList.at(6).toInt();

      cpu = sum - idle - iowait;
      Q_UNUSED(cpu);

      // Calculate load
      return ( 100.0 - (100.0 * idle / sum) );
#else
      return 0;
#endif
    }

    /*!
     * \brief SystemLoad::totalRAM
     * \return
     */
    quint64 SystemLoad::totalRAM()
    {
      QProcess p;
      QString memory;
      p.start("awk", QStringList() << "/MemTotal/ { print $2 }" << "/proc/meminfo");
      p.waitForFinished();
      memory = p.readAllStandardOutput();
      p.close();

      return memory.toLong();
    }

    /*!
     * \brief SystemLoad::freeRAM
     * \return
     */
    quint64 SystemLoad::freeRAM()
    {
      QProcess p;
      QString memory;
      p.start("awk", QStringList() << "/MemFree/ { print $2 }" << "/proc/meminfo");
      p.waitForFinished();
      memory = p.readAllStandardOutput();
      p.close();

      return memory.toLong();
    }

    /*!
     * \brief SystemLoad::getTimeList
     * \return
     */
    SystemLoad::TimeList SystemLoad::getTimeList()
    {
#ifdef Q_OS_LINUX
      QFile fin( "/proc/stat" );
      if ( fin.open( QIODevice::ReadOnly | QIODevice::Text) ) {
        return fin.readLine().split(' ');
      }
      else {
        qWarning() << "Cannot open stat file";
      }
#endif

      return TimeList();
    }

  }
}
