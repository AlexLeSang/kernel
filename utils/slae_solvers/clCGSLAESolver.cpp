#include "clCGSLAESolver.hpp"

#include <ViennaCLContextTools.hpp>

#include <boost/numeric/ublas/matrix_sparse.hpp>
#include <boost/numeric/ublas/vector_sparse.hpp>
#include <viennacl/compressed_matrix.hpp>
#include <viennacl/linalg/cg.hpp>

#include <gmm/gmm_matrix.h>
#include <gmm/gmm_vector.h>
#include <gmm/gmm_sub_vector.h>

bool clCGSLAESolver::operator ()(getfem::model &model, gmm::iteration &iter)
{
  using VECTOR = std::remove_const< std::remove_reference< decltype( model.real_rhs() )>::type >::type;
  using MATRIX = std::remove_const< std::remove_reference< decltype( model.real_tangent_matrix() )>::type >::type;

  using ScalarType = bgeot::scalar_type;

  std::cout << "clCGSLAESolver::operator ()" << std::endl;
  VECTOR state( model.nb_dof() );
  model.assembly( getfem::model::BUILD_ALL );
  model.from_variables( state );
  {
    //    dal::shared_ptr< getfem::abstract_linear_solver< MATRIX, VECTOR > > p( new getfem::linear_solver_gmres_preconditioned_ilu<MATRIX, VECTOR> );
    //    (*p)( model.real_tangent_matrix(), state, model.real_rhs(), iter );

    // typedef gmm::rsvector<scalar_type> model_real_sparse_vector;
    // typedef gmm::col_matrix<model_real_sparse_vector> model_real_sparse_matrix;

    using namespace kernel::server::functions::node;
    using namespace boost::numeric;


    opencl::create_all_cpu_devices_context();
    // opencl::create_all_gpu_devices_context();
    // opencl::create_all_devices_context();

    viennacl::ocl::switch_context( opencl::ALL_CPU_DEVICES_CONTEXT );
    // opencl::print_devices_for_current_context( std::cout );

    const std::size_t vcl_size = state.size();

    ublas::compressed_matrix<ScalarType> ublas_matrix( vcl_size, vcl_size );
    ublas::vector<ScalarType> rhs( vcl_size );
    ublas::vector<ScalarType> result( vcl_size );

    // GMM to UBLAS
    {
      const MATRIX& K = model.real_tangent_matrix();
      {
        boost::timer::auto_cpu_timer t;
        auto it = gmm::mat_col_const_begin( K );
        auto ite = gmm::mat_col_const_end( K );

        for (; it != ite; ++it ) {
          typename gmm::linalg_traits<MATRIX>::const_sub_col_type col = gmm::linalg_traits<MATRIX>::col( it );
          auto vit = gmm::vect_const_begin( col );
          auto vite = gmm::vect_const_end( col );

          for (; vit != vite; ++ vit ) {
            ublas_matrix.insert_element( vit.index(), std::distance( it, ite ), *vit );
          }
        }

        std::cout << "GMM to UBLAS matrix transfer completed" << std::endl;
      }

      {
        boost::timer::auto_cpu_timer t;
        const VECTOR& gmm_rhs = model.real_rhs();
        for ( std::size_t i = 0; i < gmm_rhs.size(); ++ i ) {
          rhs[ i ] = gmm_rhs[ i ];
        }
        std::cout << "GMM to UBLAS vector transfer completed" << std::endl;
      }

    }

    viennacl::compressed_matrix<ScalarType> vcl_compressed_matrix;
    viennacl::vector<ScalarType> vcl_rhs( vcl_size );
    viennacl::vector<ScalarType> vcl_result( vcl_size );

    // UBLAS to ViennaCL
    {
      boost::timer::auto_cpu_timer t;
      viennacl::copy( rhs.begin(), rhs.end(), vcl_rhs.begin() );
      viennacl::copy( ublas_matrix, vcl_compressed_matrix );
      std::cout << "UBLAS to ViennaCL matrix transfer completed" << std::endl;
    }

    // Solve
    {
      boost::timer::auto_cpu_timer t;
      vcl_result = viennacl::linalg::solve( vcl_compressed_matrix, vcl_rhs, viennacl::linalg::cg_tag() );
      std::cout << "ViennaCL solution completed" << std::endl;
    }

    // From ViennaCL to UBLAS
    {
      boost::timer::auto_cpu_timer t;
      viennacl::copy( vcl_result.begin(), vcl_result.end(), result.begin() );
      std::cout << "ViennaCL to UBLAS matrix transfer completed" << std::endl;
    }

    // UBLAS to GMM
    {
      for ( std::size_t i = 0; i < result.size(); ++ i ) {
        state[ i ] = result[ i ];
      }
      std::cout << "UBLAS to GMM matrix transfer completed" << std::endl;
    }


  }
  model.to_variables( state );
  return iter.converged();
}
