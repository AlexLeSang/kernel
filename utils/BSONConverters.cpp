#include "BSONConverters.hpp"

#include <iostream>


namespace kernel {
  namespace utils {
    namespace bson_converters {

      std::pair< std::vector< mongo::BSONObj >, std::vector< mongo::BSONObj > >
      vtk_unstructured_grid_to_bson(vtkUnstructuredGrid* const gridPtr, const std::string& grid_id)
      {
        assert( gridPtr != nullptr );
        boost::timer::auto_cpu_timer t;

        // INFO remove debug output
        // gridPtr->Print( std::cout );
        // std::cout << std::endl;

        // get number of elements
        const auto number_of_points = gridPtr->GetNumberOfPoints();

        std::vector< mongo::BSONObj > points;
        points.reserve( number_of_points );
        vtkPoints* const points_array = gridPtr->GetPoints();
        {
          boost::timer::auto_cpu_timer t_points;
          double xyz[3];
          for ( auto i = 0; i < number_of_points; ++ i ) {
            points_array->GetPoint( i, xyz );

            // INFO remove debug output
            // std::cout << i << ": " << xyz[0] << "," << xyz[1] << "," << xyz[2] << std::endl;
            auto point_obj = BSON( "#" << i << "xyz" << BSON_ARRAY( xyz[0] << xyz[1] << xyz[2] ) << "grid_id" << grid_id );

            // INFO remove debug output
            // std::cout << "point_obj[" << i << "]: " << point_obj << std::endl;
            points.push_back( std::move(point_obj) );
          }
        }

        // INFO remove debug output
        // for ( auto it = int_string_map.begin(); it != int_string_map.end(); ++ it ) { std::cout << it->first << " " << it->second << std::endl; }

        // INFO remove debug output
        std::cout << "number_of_points = " << number_of_points << std::endl;

        const auto number_of_cells = gridPtr->GetNumberOfCells();
        std::vector< mongo::BSONObj > cells;
        cells.reserve( number_of_cells );

        vtkCellArray* const cells_array = gridPtr->GetCells(); // Structure: [n, ind1, ind2, ..., n,...]

        std::list< int > cell_types;
        //*/
        {
          boost::timer::auto_cpu_timer t_cells;

          for ( auto i = 0; i < number_of_cells; ++ i ) {
            vtkSmartPointer< vtkIdList> id_list_prt = vtkSmartPointer< vtkIdList >::New();
            cells_array->GetCell( i, id_list_prt.GetPointer() );

            // INFO remove debug output
            std::cout << i << ": [ " << id_list_prt->GetNumberOfIds() << " ";

            mongo::BSONArrayBuilder ids_array_builder;
            const auto number_of_ids = id_list_prt->GetNumberOfIds();
            for ( auto j = 0; j < number_of_ids; ++ j ) {
              ids_array_builder.append( id_list_prt->GetId( j ) );
            }

            auto cell_obj = BSON( "#" << i << "type" << gridPtr->GetCellType( i ) << "ids" << ids_array_builder.arr() );

            cell_types.push_back( gridPtr->GetCellType( i ) );

            // INFO remove debug output
            std::cout << "cell_obj[" << i << "]: " << cell_obj << "\n" << std::endl;
            cells.push_back( std::move( cell_obj ) );
          }
        }

        //* INFO remove debug output
        std::cout << "cell types: \n";
        auto last = std::unique( cell_types.begin(), cell_types.end() );
        for ( auto it = cell_types.begin(); it != last; ++ it ) {
          std::cout << *it << std::endl;
        }
        //*/


        // INFO remove debug output
        std::cout << "number_of_cells = " << number_of_cells << std::endl;

        return std::make_pair( std::move( points ), std::move( cells ) );
      }

      vtkUnstructuredGrid *bson_to_vtk_unstructured_grid(const mongo::BSONObj &obj)
      {
        const std::string document_type = obj.getStringField( "document_type" );
        assert( document_type == "vtkUnstructuredGrid" );
        // TODO fill vtkUnstructuredGrid parts

        vtkUnstructuredGrid* grid = vtkUnstructuredGrid::New();
        return grid;
      }

    }
  }
}


#include <vtkSmartPointer.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXMLUnstructuredGridWriter.h>

namespace test {

  bool BSONVTKUnstructuredGridTest()
  {
    std::cout << "BSONVTKUnstructuredGridTest" << std::endl;

    const std::string fileName( "grid.vtu" );
    const std::string modifiedFileName( "modified_grid.vtu" );
    // read grid
    vtkSmartPointer< vtkXMLUnstructuredGridReader > reader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
    reader->SetFileName( fileName.c_str() );
    reader->Update();

    // INFO remove debug output
    // reader->Print( std::cout );
    // std::cout << std::endl;

    vtkSmartPointer< vtkUnstructuredGrid > grid( reader->GetOutput() );

    // convert to BSON
    auto points_cells = kernel::utils::bson_converters::vtk_unstructured_grid_to_bson( grid.GetPointer(), fileName );

    // INFO remove debug output
    // std::cout << "bsonObj\n";
    // std::cout << bsonObj << std::endl;

    // convert to grid
    // const vtkSmartPointer< vtkUnstructuredGrid > convertedGrid( kernel::utils::bson_converters::bson_to_vtk_unstructured_grid( bsonObj ) );
    // vtkSmartPointer< vtkXMLUnstructuredGridWriter > writer = vtkSmartPointer< vtkXMLUnstructuredGridWriter >::New();
    // writer->SetInput( convertedGrid );
    // writer->SetFileName( modifiedFileName.c_str() );
    // writer->Write();
    // check similarity
    return true;
  }


  bool vtkReadWriteTest()
  {
    boost::timer::auto_cpu_timer t_cells;
    std::cout << "vtkReadWriteTest()" << std::endl;
    const std::string fileName( "grid.vtu" );
    // read grid
    vtkSmartPointer< vtkXMLUnstructuredGridReader > reader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
    reader->SetFileName( fileName.c_str() );
    reader->Update();

    vtkSmartPointer< vtkUnstructuredGrid > grid( reader->GetOutput() );

    // Write to string
    vtkSmartPointer< vtkUnstructuredGridWriter > writer = vtkSmartPointer< vtkUnstructuredGridWriter >::New();
    writer->SetInput( grid );
    writer->WriteToOutputStringOn();
    writer->Write();
    std::string grid_str( writer->GetOutputString(), writer->GetOutputStringLength() );

    // INFO remove debug output
    std::cout << "grid_string.length() = " << grid_str.length() << std::endl;


    // Read from grid string
    vtkSmartPointer< vtkUnstructuredGridReader > string_reader = vtkSmartPointer< vtkUnstructuredGridReader >::New();
    string_reader->ReadFromInputStringOn();
    string_reader->SetInputString( grid_str.c_str() );
    string_reader->Update();

    vtkSmartPointer< vtkUnstructuredGrid > read_grid( string_reader->GetOutput() );

    // Save read_grid
    const std::string readGridFileName( "read_grid.vtu" );
    vtkSmartPointer< vtkXMLUnstructuredGridWriter > read_grid_writer = vtkSmartPointer< vtkXMLUnstructuredGridWriter >::New();
    read_grid_writer->SetInput( read_grid );
    read_grid_writer->SetFileName( readGridFileName.c_str() );
    read_grid_writer->Write();

    return true;
  }


}

/*
#include <iostream>
int main(int argc, char *argv[])
{
  std::cout << "utils test" << std::endl;
  std::cout << "Current directory: " << argv[ 0 ] << std::endl;
  test::vtkReadWriteTest();
  // test::BSONVTKUnstructuredGridTest();
  // test::VTKSerializationTest();

  return 0;
}
*/
