#ifndef APPLYEDBOUNDARYCONDITION_HPP
#define APPLYEDBOUNDARYCONDITION_HPP

#include <memory>
#include <QColor>
#include <QString>

#include <boundary_condition/AbstractBoundaryCondition.hpp>
#include <boundary_condition/DirihletBoundaryCondition.hpp>
#include <boundary_condition/NeumannBoundaryCondition.hpp>

#include <vtkUnstructuredGrid.h>
#include <vtkSmartPointer.h>
#include <boundary_condition/AbstractBoundaryCondition.hpp>

#include <vtkUnstructuredGridReader.h>
#include <vtkUnstructuredGridWriter.h>
#include <vtkPolyData.h>

#include <fstream>

#include <boundary_condition/ApplicableNodes.hpp>

/*!
 * \brief The AppliedBoundaryCondition class
 */
class AppliedBoundaryCondition
{
public:
  AppliedBoundaryCondition();

  BOUNDARY_CONDITION_TYPE getConditionType() const;

  QString getName() const;
  void setName(const QString &value);

  vtkSmartPointer<vtkUnstructuredGrid> getSelectedGrid() const;
  void setSelectedGrid(const vtkSmartPointer<vtkUnstructuredGrid> &value);

  QColor getColor() const;
  void setColor(const QColor &value);

  std::shared_ptr<AbstractBoundaryCondition> getBoundaryCondition() const;
  void setBoundaryCondition(std::shared_ptr<AbstractBoundaryCondition> &value);

  bool checkCompatibility(vtkPolyData *polyData);

  static AppliedBoundaryCondition readFromFile(const QString& fileName);
  static bool writeToFile(const AppliedBoundaryCondition &condition, const QString& fileName);

  ApplicableNodes getApplicableNodes() const;

private:
  std::shared_ptr<AbstractBoundaryCondition> boundaryCondition;
  QColor color;
  vtkSmartPointer<vtkUnstructuredGrid> selectedGrid;
};

QDataStream &operator<<(QDataStream &ds, const vtkSmartPointer< vtkUnstructuredGrid > & grid);
QDataStream &operator>>(QDataStream &ds, vtkSmartPointer< vtkUnstructuredGrid > & grid);

QDataStream &operator<<(QDataStream &ds, const AppliedBoundaryCondition & condition);
QDataStream &operator>>(QDataStream &ds, AppliedBoundaryCondition & condition);

namespace test {
  bool ApplyedBoundaryConditionTest(int argc, char* argv[]);
  bool ApplyedBoundaryConditionReadTest(int argc, char* argv[]);
  bool ApplyedBoundaryConditionWriteTest(int argc, char* argv[]);
}

#endif // APPLYEDBOUNDARYCONDITION_HPP
