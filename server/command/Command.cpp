#include "Command.hpp"

#include "StopServerCommand.hpp"
#include "GetServerStatusCommand.hpp"
#include "PingPongCommand.hpp"


namespace kernel {
  namespace server {
    namespace command {

      /*!
       * \brief CommandProcessor
       * \param command
       * \return
       */
      Command CommandProcessor(const kernel::server::ServerCommands &command)
      {
        switch ( command ) {
          case ServerCommands::STOP_SERVER:
            return &StopServerCommand;
            break;

          case ServerCommands::GET_SERVER_STATUS:
            return &GetServerStatusCommand;
            break;

          case ServerCommands::PING_PONG:
            return &PingPongCommand;
            break;

          default:
            // Empty
            break;
        }

        return &DumbCommand;
      }

      /*!
       * \brief DumbCommand
       * \return
       */
      bool DumbCommand(QTcpSocket &, std::atomic<bool> &)
      {
        qDebug() << "DumbCommand";
        return false;
      }
    }
  }
}
