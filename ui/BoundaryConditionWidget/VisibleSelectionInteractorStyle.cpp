#include "VisibleSelectionInteractorStyle.hpp"

#include <vtkAxesActor.h>
#include <vtkGeometryFilter.h>

#include <QDebug>

vtkStandardNewMacro(VisibleSelectionInteractorStyle);

/*!
 * \brief VisibleSelectionInteractorStyle::VisibleSelectionInteractorStyle
 */
VisibleSelectionInteractorStyle::VisibleSelectionInteractorStyle() : vtkInteractorStyleRubberBandPick()
{
  this->SelectedGrid = vtkSmartPointer< vtkUnstructuredGrid >::New();

  this->SelectedMapper = vtkSmartPointer< vtkDataSetMapper >::New();
  this->SelectedActor = vtkSmartPointer< vtkActor >::New();
  this->SelectedActor->SetMapper( SelectedMapper );

  this->SelectedGridMapper = vtkSmartPointer< vtkDataSetMapper >::New();
  this->SelectedGridActor = vtkSmartPointer< vtkActor >::New();
  this->SelectedGridActor->SetMapper( SelectedGridMapper );

  this->PrevSelectedMapper = vtkSmartPointer< vtkDataSetMapper >::New();
  this->PrevSelectedActor = vtkSmartPointer< vtkActor >::New();
  this->PrevSelectedActor->SetMapper( PrevSelectedMapper );


  this->PrevSelectedGridMapper = vtkSmartPointer< vtkDataSetMapper >::New();
  this->PrevSelectedGridActor = vtkSmartPointer< vtkActor >::New();
  this->PrevSelectedGridActor->SetMapper( PrevSelectedGridMapper );

  this->selection = vtkSmartPointer< vtkSelection >::New();

  this->GlyphScaleCoefficient = 1.0;
  this->prevSelectionColor = QColor( 128, 128, 128 );
  CurrentTreeItem = nullptr;
}

/*!
 * \brief VisibleSelectionInteractorStyle::appendSelectedGrid
 * \param selected
 */
void VisibleSelectionInteractorStyle::appendSelectedGrid(vtkSmartPointer< vtkUnstructuredGrid > selected)
{
  vtkSmartPointer< vtkAppendFilter > appendFilter = vtkSmartPointer< vtkAppendFilter >::New();
  appendFilter->AddInput( SelectedGrid );
  appendFilter->AddInput( selected );
  appendFilter->Update();
  SelectedGrid = appendFilter->GetOutput();
}

/*!
 * \brief VisibleSelectionInteractorStyle::replaceSelectedGrid
 * \param selected
 */
void VisibleSelectionInteractorStyle::replaceSelectedGrid(vtkSmartPointer< vtkUnstructuredGrid > selected)
{
  SelectedGrid = selected;
}

/*!
 * \brief VisibleSelectionInteractorStyle::getGlyphFigure
 * \param boundaryConditionType
 * \return
 */
vtkSmartPointer< vtkPolyData > VisibleSelectionInteractorStyle::getGlyphFigure(BOUNDARY_CONDITION_TYPE boundaryConditionType)
{
  vtkSmartPointer< vtkPolyData > glyphFigure;
  switch ( boundaryConditionType ) {
    case BOUNDARY_CONDITION_TYPE::DIRICHLET_BOUNDARY_CONDITION:
      {
        vtkSmartPointer< vtkSphereSource > sphereSource = vtkSmartPointer< vtkSphereSource >::New();
        sphereSource->SetRadius( 0.1 );
        sphereSource->SetPhiResolution( 7 );
        sphereSource->SetThetaResolution( 7 );
        sphereSource->Update();
        glyphFigure = sphereSource->GetOutput();
      }
      break;

    case BOUNDARY_CONDITION_TYPE::NEUMANN_BOUNDARY_CONDITION:
      {
        vtkSmartPointer< vtkArrowSource > arrowSource = vtkSmartPointer< vtkArrowSource >::New();
        arrowSource->InvertOn();
        arrowSource->SetTipResolution( 10 );
        arrowSource->SetShaftResolution( 10 );
        arrowSource->Update();
        glyphFigure = arrowSource->GetOutput();
      }
      break;

    case BOUNDARY_CONDITION_TYPE::NON_CONDITION:
      {
        vtkSmartPointer< vtkCubeSource > cubeSource = vtkSmartPointer< vtkCubeSource >::New();
        cubeSource->SetXLength( 0.1 );
        cubeSource->SetYLength( 0.1 );
        cubeSource->SetZLength( 0.1 );
        cubeSource->Update();
        glyphFigure = cubeSource->GetOutput();
      }
      break;

    default:
      break;

  }

  return glyphFigure;
}

/*!
 * \brief VisibleSelectionInteractorStyle::getGlyph3D
 * \param cellCentersPolyData
 * \param glyphFigure
 * \return
 */
vtkSmartPointer<vtkPolyData> VisibleSelectionInteractorStyle::getGlyph3D(vtkSmartPointer< vtkPolyData > cellCentersPolyData, vtkSmartPointer< vtkPolyData > glyphFigure)
{
  vtkSmartPointer< vtkGlyph3D > glyph3D = vtkSmartPointer< vtkGlyph3D >::New();
  glyph3D->SetSource( glyphFigure );
  glyph3D->SetInput( cellCentersPolyData );
  glyph3D->OrientOn();
  glyph3D->SetVectorModeToUseNormal();
  glyph3D->SetScaleModeToDataScalingOff();
  glyph3D->SetColorModeToColorByScalar();
  glyph3D->SetScaleFactor( GlyphScaleCoefficient );
  glyph3D->Update();

  return glyph3D->GetOutput();
}

/*!
 * \brief VisibleSelectionInteractorStyle::extractSelected
 * \return
 */
vtkSmartPointer<vtkUnstructuredGrid> VisibleSelectionInteractorStyle::extractSelected()
{
  vtkSmartPointer< vtkExtractSelection > extractSelection = vtkSmartPointer< vtkExtractSelection >::New();
  extractSelection->SetInput( 0, PolyData );
  extractSelection->SetInput( 1, selection );
  extractSelection->Update();

  vtkSmartPointer< vtkUnstructuredGrid > selected = vtkUnstructuredGrid::SafeDownCast( extractSelection->GetOutput() );

  return selected;
}


/*!
 * \brief VisibleSelectionInteractorStyle::getGlyphColors
 * \param selectedGrid
 * \param glyphColor
 * \return
 */
vtkSmartPointer< vtkUnsignedCharArray > VisibleSelectionInteractorStyle::getGlyphColors(vtkUnstructuredGrid* selectedGrid, QColor glyphColor)
{
  vtkSmartPointer< vtkUnsignedCharArray > colors = vtkSmartPointer< vtkUnsignedCharArray >::New();
  colors->SetName( "Colors" );
  colors->SetNumberOfComponents( 3 );
  colors->Allocate( selectedGrid->GetNumberOfCells() );
  colors->SetNumberOfValues( selectedGrid->GetNumberOfCells() * 3 );
  colors->FillComponent( 0, (unsigned char) ( glyphColor.red() ) );
  colors->FillComponent( 1, (unsigned char) ( glyphColor.green() ) );
  colors->FillComponent( 2, (unsigned char) ( glyphColor.blue() ) );
  return colors;
}

/*!
 * \brief VisibleSelectionInteractorStyle::getCellNormals
 * \param selectedGrid
 * \return
 */
vtkSmartPointer< vtkDoubleArray > VisibleSelectionInteractorStyle::getCellNormals(vtkUnstructuredGrid* selectedGrid)
{
  vtkSmartPointer< vtkDoubleArray > cellNormals = vtkSmartPointer< vtkDoubleArray >::New();
  cellNormals->SetName( "Normals" );
  cellNormals->SetNumberOfComponents( 3 );
  cellNormals->Allocate( selectedGrid->GetNumberOfCells() );

  for ( auto c = 0; c < selectedGrid->GetNumberOfCells(); ++ c ) {
    vtkCell* cell = selectedGrid->GetCell( c );
    assert( cell->GetNumberOfPoints() == 3 );
    vtkTriangle* triangle = vtkTriangle::SafeDownCast( cell );
    assert( triangle );
    double p1[ 3 ];
    double p2[ 3 ];
    double p3[ 3 ];
    double n[ 3 ];
    selectedGrid->GetPoint( triangle->GetPointId( 0 ), p1 );
    selectedGrid->GetPoint( triangle->GetPointId( 1 ), p2 );
    selectedGrid->GetPoint( triangle->GetPointId( 2 ), p3 );
    triangle->ComputeNormal( p1, p2, p3, n );
    cellNormals->InsertNextTuple( n );
  }

  return cellNormals;
}

/*!
 * \brief VisibleSelectionInteractorStyle::getSelectedGridWithGlyphs
 * \return
 */
vtkSmartPointer<vtkPolyData> VisibleSelectionInteractorStyle::getSelectedGridWithGlyphs()
{
  vtkSmartPointer< vtkCellCenters > cellCenters = vtkSmartPointer< vtkCellCenters >::New();
  cellCenters->SetInput( SelectedGrid );
  cellCenters->Update();

  vtkSmartPointer< vtkPolyData > cellCentersPolyData = cellCenters->GetOutput();
  cellCentersPolyData->GetPointData()->SetNormals( getCellNormals( SelectedGrid ) );
  cellCentersPolyData->GetPointData()->SetScalars( getGlyphColors( SelectedGrid, CurrentTreeItem->getColor() ) );

  vtkSmartPointer< vtkPolyData > polyDataWithGlyphs = getGlyph3D( cellCentersPolyData, getGlyphFigure( CurrentTreeItem->getIntemType() ) );

  return polyDataWithGlyphs;
}

/*!
 * \brief VisibleSelectionInteractorStyle::reduceColor
 * \param color
 * \return
 */
std::tuple< float, float, float > VisibleSelectionInteractorStyle::reduceColor(const QColor& color)
{
  std::tuple< float, float, float > selectionColor;

  auto reduceBrightness = [&](unsigned char c) {
    return c / ( 2.5 * 255.0 );
  };

  std::get<0>( selectionColor ) = reduceBrightness( (unsigned char) ( color.red() ) );
  std::get<1>( selectionColor ) = reduceBrightness( (unsigned char) ( color.green() ) );
  std::get<2>( selectionColor ) = reduceBrightness( (unsigned char) ( color.blue() ) );
  return selectionColor;
}

/*!
 * \brief VisibleSelectionInteractorStyle::removeActors
 */
void VisibleSelectionInteractorStyle::removeActors()
{
  this->GetInteractor()->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->RemoveActor( SelectedActor );
  this->GetInteractor()->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->RemoveActor( SelectedGridActor );
}

/*!
 * \brief VisibleSelectionInteractorStyle::addPrevSelectedActor
 */
void VisibleSelectionInteractorStyle::addPrevSelectedActor()
{
  this->GetInteractor()->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor( PrevSelectedActor );
  this->GetInteractor()->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor( PrevSelectedGridActor );
}

/*!
 * \brief VisibleSelectionInteractorStyle::removePrevSelectedActor
 */
void VisibleSelectionInteractorStyle::removePrevSelectedActor()
{
  this->GetInteractor()->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->RemoveActor( PrevSelectedActor );
  this->GetInteractor()->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->RemoveActor( PrevSelectedGridActor );
}

/*!
 * \brief VisibleSelectionInteractorStyle::addActors
 */
void VisibleSelectionInteractorStyle::addActors()
{
  this->GetInteractor()->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor( SelectedActor );
  this->GetInteractor()->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->AddActor( SelectedGridActor );
}

/*!
 * \brief VisibleSelectionInteractorStyle::highlightSelectedGrid
 */
void VisibleSelectionInteractorStyle::highlightSelectedGrid()
{
  auto selectionColor = reduceColor( CurrentTreeItem->getColor() );
  this->SelectedGridActor->GetProperty()->SetColor( std::get<0>( selectionColor ), std::get<1>( selectionColor ), std::get<2>( selectionColor ) );
}

/*!
 * \brief VisibleSelectionInteractorStyle::highlightPrevSelectedGrid
 */
void VisibleSelectionInteractorStyle::highlightPrevSelectedGrid()
{
  auto color = reduceColor( prevSelectionColor );
  this->PrevSelectedGridActor->GetProperty()->SetColor( std::get<0>( color ), std::get<1>( color ), std::get<2>( color ) );
}

/*!
 * \brief VisibleSelectionInteractorStyle::displaySelectedGrid
 */
void VisibleSelectionInteractorStyle::displaySelectedGrid()
{
  this->SelectedMapper->SetInput( getSelectedGridWithGlyphs() );
  this->SelectedGridMapper->SetInput( SelectedGrid );
  highlightSelectedGrid();
  addActors();
}

/*!
 * \brief VisibleSelectionInteractorStyle::processSelection
 */
void VisibleSelectionInteractorStyle::processSelection()
{
  updateSelection();

  if ( selection->GetNumberOfNodes() ) {
    removeActors();

    if ( this->GetInteractor()->GetShiftKey() ) {
      appendSelectedGrid( extractSelected() );
    }
    else {
      replaceSelectedGrid( extractSelected() );
    }

    displaySelectedGrid();
  }
  else {
    SelectedGrid = vtkSmartPointer< vtkUnstructuredGrid >::New();
    removeActors();
  }
  this->GetInteractor()->GetRenderWindow()->Render();
  CurrentTreeItem->saveSelectedGrid( SelectedGrid );
}

/*!
 * \brief VisibleSelectionInteractorStyle::GetCurrentTreeItem
 * \return
 */
AbstractTreeItem *VisibleSelectionInteractorStyle::GetCurrentTreeItem() const
{
  return CurrentTreeItem;
}

/*!
 * \brief VisibleSelectionInteractorStyle::SetCurrentTreeItem
 * \param value
 */
void VisibleSelectionInteractorStyle::SetCurrentTreeItem(AbstractTreeItem *value)
{
  // assert( value );
  CurrentTreeItem = value;
  if ( CurrentTreeItem ) {
    SelectedGrid = CurrentTreeItem->loadSelectedGrid( PolyData );
    displaySelectedGrid();
  }
  else {
    SelectedGrid = vtkSmartPointer< vtkUnstructuredGrid >::New();
    removeActors();
  }
  this->GetInteractor()->GetRenderWindow()->Render();
}

/*!
 * \brief VisibleSelectionInteractorStyle::SetPrevSelected
 * \param itemsList
 */
void VisibleSelectionInteractorStyle::SetPrevSelected(QList<AbstractTreeItem *> itemsList)
{
  if ( !itemsList.empty() ) {
    removePrevSelectedActor();

    vtkSmartPointer< vtkUnstructuredGrid > commonSelection = vtkSmartPointer< vtkUnstructuredGrid >::New();

    for ( auto it = itemsList.begin(); it != itemsList.end(); ++ it ) {
      AbstractTreeItem* item = *it;

      vtkSmartPointer<vtkUnstructuredGrid> loadedGrid = item->loadSelectedGrid( PolyData );

      vtkSmartPointer< vtkAppendFilter > appendFilter = vtkSmartPointer< vtkAppendFilter >::New();
      appendFilter->AddInput( commonSelection );
      appendFilter->AddInput( loadedGrid );
      appendFilter->Update();
      commonSelection = appendFilter->GetOutput();
    }

    vtkSmartPointer< vtkCellCenters > cellCenters = vtkSmartPointer< vtkCellCenters >::New();
    cellCenters->SetInput( commonSelection );
    cellCenters->Update();

    vtkSmartPointer< vtkPolyData > cellCentersPolyData = cellCenters->GetOutput();
    cellCentersPolyData->GetPointData()->SetNormals( getCellNormals( commonSelection ) );
    cellCentersPolyData->GetPointData()->SetScalars( getGlyphColors( commonSelection, prevSelectionColor ) );

    vtkSmartPointer< vtkPolyData > polyDataWithGlyphs = getGlyph3D( cellCentersPolyData, getGlyphFigure( BOUNDARY_CONDITION_TYPE::NON_CONDITION ) );

    PrevSelectedMapper->SetInput( polyDataWithGlyphs );
    PrevSelectedGridMapper->SetInput( commonSelection );

    addPrevSelectedActor();
    highlightPrevSelectedGrid();
  }
  else {
    removePrevSelectedActor();
  }
  this->GetInteractor()->GetRenderWindow()->Render();

}

/*!
 * \brief VisibleSelectionInteractorStyle::selectionMakesSence
 * \return
 */
bool VisibleSelectionInteractorStyle::selectionMakesSence()
{
  return (this->CurrentMode == VTKISRBP_SELECT) && (CurrentTreeItem != nullptr);
}

/*!
 * \brief VisibleSelectionInteractorStyle::OnLeftButtonUp
 */
void VisibleSelectionInteractorStyle::OnLeftButtonUp()
{
  if ( selectionMakesSence() ) {
    processSelection();
  }
  // Forward events
  vtkInteractorStyleRubberBandPick::OnLeftButtonUp();
}

/*!
 * \brief VisibleSelectionInteractorStyle::OnKeyPress
 */
void VisibleSelectionInteractorStyle::OnKeyPress()
{
  vtkRenderWindowInteractor *rwi = this->Interactor;
  std::string key = rwi->GetKeySym();

  if( key == "r" ) {
    emit switchState();
  }

  vtkInteractorStyleTrackballCamera::OnKeyPress();
}

/*!
 * \brief VisibleSelectionInteractorStyle::SetPolyData
 * \param polyData
 */
void VisibleSelectionInteractorStyle::SetPolyData(vtkSmartPointer<vtkPolyData> polyData)
{
  this->PolyData = polyData;
}

/*!
 * \brief VisibleSelectionInteractorStyle::GetGlyphScaleCoefficient
 * \return
 */
double VisibleSelectionInteractorStyle::GetGlyphScaleCoefficient() const
{
  return GlyphScaleCoefficient;
}

/*!
 * \brief VisibleSelectionInteractorStyle::SetGlyphScaleCoefficient
 * \param value
 */
void VisibleSelectionInteractorStyle::SetGlyphScaleCoefficient(double value)
{
  GlyphScaleCoefficient = value;
}

/*!
 * \brief VisibleSelectionInteractorStyle::updateSelection
 */
void VisibleSelectionInteractorStyle::updateSelection()
{
  vtkSmartPointer< vtkHardwareSelector > selector = vtkSmartPointer< vtkHardwareSelector >::New();
  selector->SetRenderer( this->GetInteractor()->GetRenderWindow()->GetRenderers()->GetFirstRenderer() );

  unsigned int windowSize[ 4 ];
  windowSize[ 0 ] = std::min( StartPosition[ 0 ], EndPosition[ 0 ] );
  windowSize[ 1 ] = std::min( StartPosition[ 1 ], EndPosition[ 1 ] );

  windowSize[ 2 ] = std::max( StartPosition[ 0 ], EndPosition[ 0 ] );
  windowSize[ 3 ] = std::max( StartPosition[ 1 ], EndPosition[ 1 ] );

  selector->SetArea( windowSize );
  selector->SetFieldAssociation( vtkDataObject::FIELD_ASSOCIATION_CELLS );

  selection = selector->Select();
}

/*!
 * \brief VisibleSelectionInteractorStyleTest
 * \param argc
 * \param argv
 * \return
 */
bool VisibleSelectionInteractorStyleTest(int argc, char* argv[])
{
  assert( argc == 2 );

  vtkSmartPointer< vtkXMLUnstructuredGridReader > reader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
  reader->SetFileName( argv[ 1 ] );
  reader->Update();
  vtkSmartPointer< vtkUnstructuredGrid > grid = reader->GetOutput();

  vtkSmartPointer<vtkDataSetSurfaceFilter> surfaceFilter = vtkSmartPointer< vtkDataSetSurfaceFilter >::New();
  surfaceFilter->SetInput( grid );
  surfaceFilter->Update();

  vtkSmartPointer< vtkPolyDataMapper > mapper = vtkSmartPointer< vtkPolyDataMapper >::New();
  mapper->SetInput( surfaceFilter->GetOutput() );

  vtkSmartPointer< vtkActor > actor = vtkSmartPointer< vtkActor >::New();
  actor->SetMapper( mapper );
  actor->GetProperty()->EdgeVisibilityOn();

  vtkSmartPointer< vtkRenderer > renderer = vtkSmartPointer< vtkRenderer >::New();
  vtkSmartPointer< vtkRenderWindow > renderWindow = vtkSmartPointer< vtkRenderWindow >::New();
  renderWindow->AddRenderer( renderer );
  renderWindow->SetSize( 800, 600 );

  vtkSmartPointer< vtkAreaPicker > areaPicker = vtkSmartPointer< vtkAreaPicker >::New();
  vtkSmartPointer< vtkRenderWindowInteractor > renderWindowInteractor = vtkSmartPointer< vtkRenderWindowInteractor >::New();
  renderWindowInteractor->SetPicker( areaPicker );
  renderWindowInteractor->SetRenderWindow( renderWindow );

  vtkSmartPointer< VisibleSelectionInteractorStyle > style = vtkSmartPointer< VisibleSelectionInteractorStyle >::New();
  style->SetPolyData( surfaceFilter->GetOutput() );
  style->SetGlyphScaleCoefficient( 0.09 );

  renderWindowInteractor->SetInteractorStyle(style);
  style->SetCurrentRenderer(renderer);

  renderer->AddActor( actor );

  renderWindow->Render();
  renderWindowInteractor->Start();

  return true;
}
