#ifndef GETFEMLAPLACIANTEST_HPP
#define GETFEMLAPLACIANTEST_HPP

#include <getfem/getfem_model_solvers.h>
#include <getfem/getfem_export.h>
#include <getfem/getfem_regular_meshes.h>
#include <gmm/gmm.h>

#include <vtkPolyDataMapper.h>
#include <vtkSmartPointer.h>
#include <vtkStructuredGridGeometryFilter.h>
#include <vtkStructuredGridReader.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkGenericDataObjectReader.h>
#include <vtkPolyDataReader.h>
#include <vtkDataSetMapper.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkProperty.h>
#include <vtkScalarBarActor.h>
#include <vtkLookupTable.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#include <vtkDoubleArray.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkIdList.h>


namespace test {
  bool GetFemLaplacianTest(int argc, char **argv);
}

#endif // GETFEMLAPLACIANTEST_HPP
