#ifndef NEUMANNBOUNDARYCONDITIONTREEITEM_HPP
#define NEUMANNBOUNDARYCONDITIONTREEITEM_HPP

#include <AbstractTreeItem.hpp>
#include <../../utils/boundary_condition/NeumannBoundaryCondition.hpp>

/*!
 * \brief The NeumannTreeItem class
 */
class NeumannTreeItem : public AbstractTreeItem
{
public:
  NeumannTreeItem(const QString& name = QString(), qreal xvalue = 0.0, qreal yvalue = 0.0, qreal zvalue = 0.0, const QColor& color = Qt::white, RootTreeItem* parent = 0);

  // AbstractBoundaryConditionTreeItem interface
public:
  virtual int columnCount() const;
  virtual QVariant data(int column) const;
  virtual bool setData(int column, const QVariant &value);

  qreal getBcXvalue() const;
  void setBcXvalue(const qreal &value);

  qreal getBcYvalue() const;
  void setBcYvalue(const qreal &value);

  qreal getBcZvalue() const;
  void setBcZvalue(const qreal &value);
};

#endif // NEUMANNBOUNDARYCONDITIONTREEITEM_HPP
