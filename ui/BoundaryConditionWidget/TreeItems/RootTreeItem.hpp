#ifndef BOUNDARYCONDITIONTREEITEM_HPP
#define BOUNDARYCONDITIONTREEITEM_HPP

#include <QList>
#include <QVariant>
#include <QStringList>

class AbstractTreeItem;

/*!
 * \brief The RootTreeItem class
 */
class RootTreeItem
{
public:
    RootTreeItem(const QStringList &name);
    ~RootTreeItem();

    void appendChild(AbstractTreeItem *item);

    AbstractTreeItem *child(int row);
    QList< AbstractTreeItem* > children();
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    int row() const;
    RootTreeItem *parent();
    void setParent(RootTreeItem* parent);

    void insertChild(int position, AbstractTreeItem *item);
    void removeChild(int position);

private:
    QList< AbstractTreeItem* > childItems;
    RootTreeItem *parentItem;

    QStringList headerData;
};

#endif // BOUNDARYCONDITIONTREEITEM_HPP
