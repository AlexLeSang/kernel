#ifndef SETTINGSDIALOG_HPP
#define SETTINGSDIALOG_HPP

#include <QDialog>

namespace Ui {
  class SettingsDialog;
}

struct Settings
{
  double dataTimeStep;
  double timeStep;
  bool looped;
};


class SettingsDialog : public QDialog
{
  Q_OBJECT

public:
  explicit SettingsDialog(QWidget *parent = 0);
  ~SettingsDialog();

  void setDataTimeStep(const double step);

  void setTimeStep(const double value);
  double getTimeStep() const;

  void setLooped(const bool value);
  bool getLooped() const;


protected:
  void changeEvent(QEvent *e);

private slots:
  void on_comboBox_activated(int index);

private:
  Ui::SettingsDialog *ui;

  double dataStep;
};

#endif // SETTINGSDIALOG_HPP
