#include "MesherWidgetDesignerPlugin.hpp"

#include <MesherWidget.hpp>

#include <QtPlugin>

MesherWidgetDesignerPlugin::MesherWidgetDesignerPlugin(QObject *parent) : QObject( parent ), initialized( false )
{}

QString MesherWidgetDesignerPlugin::name() const
{
  return "MesherWidget";
}

QString MesherWidgetDesignerPlugin::group() const
{
  return "QVTK";
}

QString MesherWidgetDesignerPlugin::toolTip() const
{
  return "Mesher widget";
}

QString MesherWidgetDesignerPlugin::whatsThis() const
{
  return "Mesher widget";
}

QString MesherWidgetDesignerPlugin::includeFile() const
{
  return "MesherWidget.hpp";
}

QIcon MesherWidgetDesignerPlugin::icon() const
{
  return QIcon();
}

bool MesherWidgetDesignerPlugin::isContainer() const
{
  return false;
}

QWidget *MesherWidgetDesignerPlugin::createWidget(QWidget *parent)
{
  return new MesherWidget( parent );
}

bool MesherWidgetDesignerPlugin::isInitialized() const
{
  return initialized;
}

void MesherWidgetDesignerPlugin::initialize(QDesignerFormEditorInterface *)
{
  if ( initialized ) {
    return;
  }

  initialized = true;
}

QString MesherWidgetDesignerPlugin::domXml() const
{
  /*
  return
  "<ui language=\"c++\>\n"
   "<widget class=\"QWidget\" name=\"visualisationWidget\">\n"
    "<property name=\"geometry\">\n"
     "<rect>\n"
      "<x>0</x>\n"
      "<y>0</y>\n"
      "<width>1024</width>\n"
      "<height>768</height>\n"
     "</rect>\n"
    "</property>\n"
    "<property name=\"sizePolicy\">\n"
     "<sizepolicy hsizetype=\"MinimumExpanding\" vsizetype=\"MinimumExpanding\">\n"
      "<horstretch>0</horstretch>\n"
      "<verstretch>0</verstretch>\n"
     "</sizepolicy>\n"
    "</property>\n"
    "<property name=\"minimumSize\">\n"
     "<size>\n"
      "<width>1024</width>\n"
      "<height>768</height>\n"
     "</size>\n"
    "</property>\n"
   "</widget>\n"
   "</ui>\n";
   */

  return "<ui language=\"c++\">\n"
              " <widget class=\"MesherWidget\" name=\"mesherWidget\">\n"
              "  <property name=\"geometry\">\n"
              "   <rect>\n"
              "    <x>0</x>\n"
              "    <y>0</y>\n"
              "    <width>1024</width>\n"
              "    <height>768</height>\n"
              "   </rect>\n"
              "  </property>\n"
              "  <property name=\"toolTip\" >\n"
              "   <string>Mesher widget</string>\n"
              "  </property>\n"
              "  <property name=\"whatsThis\" >\n"
              "   <string>Mesher widget</string>\n"
              "  </property>\n"
              " </widget>\n"
              "<resources/>\n"
              "<connections/>\n"
              "<slots>\n"
              " <slot>addSource()</slot>\n"
              " <slot>selectActor(QModelIndex)</slot>\n"
              "</slots>\n"
              "</ui>\n";
}

QString MesherWidgetDesignerPlugin::codeTemplate() const
{
  return "";
}

Q_EXPORT_PLUGIN2( Mesherwidget, MesherWidgetDesignerPlugin )
