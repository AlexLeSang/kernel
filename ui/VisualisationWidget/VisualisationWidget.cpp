#include "VisualisationWidget.hpp"
#include "ui_VisualisationWidget.h"

#include "vtkRenderWindow.h"
#include "vtkRenderer.h"
#include "vtkProperty.h"

#include "VTKActorDelegate.hpp"

#include "vtkAxesActor.h"

#include <QShortcut>
#include <QDesktopWidget>
#include <QMenu>

#include "VTKMeshViewerDialog.hpp"

#include <vtkUnstructuredGrid.h>

#include <QDebug>

VisualisationWidget::VisualisationWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::VisualisationWidget),
  actorListModelPtr( nullptr )
{
  ui->setupUi(this);

  renderer = vtkSmartPointer< vtkRenderer >::New();
  renderer->GradientBackgroundOn();
  renderer->SetBackground( 0.0, 0.0, 0.0 );
  renderer->SetBackground2( 0.7, 0.1, 0.0 );

  ui->qvtkWidget->GetRenderWindow()->AddRenderer( renderer );

  style = vtkSmartPointer< vtkDoubleClickPickerTrackballCameraInterractor >::New();
  style->SetDefaultRenderer( renderer );

  ui->qvtkWidget->GetInteractor()->SetInteractorStyle( style );

  auto axesActor = vtkSmartPointer< vtkAxesActor >::New();
  axesActor->AxisLabelsOff();

  axesActor->SetConeRadius( 0.1 );

  //  double axesLenght[3] = { 1., 1., 1. };
  //  axesActor->SetTotalLength( axesLenght );
  //  renderer->AddActor( axesActor );

  actorListModelPtr = new VTKActorListModel( this, renderer.GetPointer() );
  ui->actorsListView->setModel( actorListModelPtr );
  ui->actorsListView->setItemDelegate( new VTKActorDelegate() );
  QShortcut* deleteShortcut = new QShortcut( QKeySequence( Qt::Key_Delete ), ui->actorsListView );
  deleteShortcut->setContext( Qt::WidgetShortcut );
  QObject::connect( deleteShortcut, SIGNAL( activated() ), this, SLOT( removeSelectedRows() ) );

  QObject::connect( actorListModelPtr, SIGNAL( dataChanged( QModelIndex,QModelIndex ) ), ui->qvtkWidget, SLOT( update() ) );

  QObject::connect( actorListModelPtr, SIGNAL( selectActor(vtkActor*) ), this, SIGNAL( signal_actorSelected(vtkActor*) ) );
  QObject::connect( actorListModelPtr, SIGNAL( deselectActor(vtkActor*) ), this, SIGNAL( signal_actorDeselected(vtkActor*) ) );

  QObject::connect( style.GetPointer(), SIGNAL( signal_actorSelected(vtkActor*) ), this, SLOT( slot_actorSelected(vtkActor*) ) );
  QObject::connect( style.GetPointer(), SIGNAL( signal_actorDeselected(vtkActor*) ), this, SLOT( slot_actorDeselected(vtkActor*) ) );

  QObject::connect( this, SIGNAL( signal_actorSelected(vtkActor*) ), style.GetPointer(), SLOT( slot_addToSelected(vtkActor*) ) );
  QObject::connect( this, SIGNAL( signal_actorDeselected(vtkActor*) ), style.GetPointer(), SLOT( slot_removeFromSelected(vtkActor*) ) );

  QObject::connect( ui->actorsListView->selectionModel(), SIGNAL( selectionChanged( QItemSelection, QItemSelection ) ), this, SLOT( selectActor( QItemSelection, QItemSelection ) ) );

  QObject::connect( ui->actorsListView, SIGNAL( customContextMenuRequested(QPoint) ), this, SLOT( actorListViewContextMenu(QPoint) ) );
  //  QObject::connect( ui->qvtkWidget, SIGNAL( customContextMenuRequested(QPoint) ), this, SLOT( actorListViewContextMenu(QPoint) ) );
  QObject::connect( this, SIGNAL( customContextMenuRequested(QPoint) ), this, SLOT( actorListViewContextMenu(QPoint) ) );

  QObject::connect( this, SIGNAL( signal_addOrRemoveBoundingBoxForActor(vtkActor*) ), style.GetPointer(), SLOT( slot_addOrRemoveBoundingBoxForActor(vtkActor*) ) );
}

/*!
 * \brief VisualisationWidget::~VisualisationWidget
 */
VisualisationWidget::~VisualisationWidget()
{
  delete ui;
}

/*!
 * \brief VisualisationWidget::addMapper
 * \param mapper
 */
void VisualisationWidget::addMapper(vtkSmartPointer< vtkMapper > mapper, const ActorType actorType, const QString name)
{
  auto newActor = vtkSmartPointer< vtkActor >::New();

  vtkSmartPointer< vtkProperty > backFaces = vtkSmartPointer< vtkProperty >::New();
  backFaces->SetSpecular( 0.0 );
  backFaces->SetDiffuse( 0.0 );
  backFaces->SetAmbient( 1.0 );
  backFaces->SetAmbientColor( 1.0000, 0.3882, 0.2784 );

  newActor->SetBackfaceProperty( backFaces );

  newActor->SetMapper( mapper );
  renderer->AddActor( newActor );
  actorListModelPtr->addActor( newActor, actorType, name );
}

/*!
 * \brief VisualisationWidget::resetCamera
 */
void VisualisationWidget::resetCamera()
{
  ui->qvtkWidget->GetRenderWindow()->GetRenderers()->GetFirstRenderer()->ResetCamera();
  ui->qvtkWidget->repaint();
}

/*!
 * \brief VisualisationWidget::setMeshToGeometry
 * \param grid
 * \param fileName
 */
void VisualisationWidget::setMeshToGeometry(vtkSmartPointer<vtkUnstructuredGrid> grid, const QString fileName)
{
  style->slot_cleanSelection();
  ui->actorsListView->clearSelection();
  vtkSmartPointer< vtkDataSetMapper > mapper = vtkSmartPointer< vtkDataSetMapper >::New();
  mapper->SetInput( grid );

  addMapper( mapper, MESH_ACTOR, fileName );
  resetCamera();
}

/*!
 * \brief VisualisationWidget::changeEvent
 * \param e
 */
void VisualisationWidget::changeEvent(QEvent *e)
{
  QWidget::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}

/*!
 * \brief VisualisationWidget::slot_actorSelected
 * \param selectedActor
 */
void VisualisationWidget::slot_actorSelected(vtkActor *selectedActor)
{
  //  std::cerr << "VisualisationWidget::slot_actorSelected: " << selectedA.org/ctor << std::endl;
  int row = 0;
  for ( auto it = actorListModelPtr->begin(); it != actorListModelPtr->end(); ++ it, ++ row ) {
    if ( std::get<0>( *it ).GetPointer() == selectedActor ) {
      break;
    }
  }

  QModelIndex index = actorListModelPtr->index( row );
  ui->actorsListView->selectionModel()->select( index, QItemSelectionModel::Select );
}

/*!
 * \brief VisualisationWidget::slot_actorDeselected
 * \param deselectedActor
 */
void VisualisationWidget::slot_actorDeselected(vtkActor *deselectedActor)
{
  //  std::cerr << "VisualisationWidget::slot_actorDeselected: " << deselectedActor << std::endl;
  int row = 0;
  for ( auto it = actorListModelPtr->begin(); it != actorListModelPtr->end(); ++ it, ++ row ) {
    if ( std::get<0>( *it ).GetPointer() == deselectedActor ) {
      break;
    }
  }

  QModelIndex index = actorListModelPtr->index( row );
  ui->actorsListView->selectionModel()->select( index, QItemSelectionModel::Deselect );
}

/*!
 * \brief VisualisationWidget::selectActor
 * \param selected
 * \param deselected
 */
void VisualisationWidget::selectActor(QItemSelection selected, QItemSelection deselected)
{
  const auto selectedList = selected.indexes();
  std::for_each( selectedList.begin(), selectedList.end(), [&]( const QModelIndex& index ) {
    //    vtkActor* actorPtr = (vtkActor *) index.data( Qt::EditRole ).value< void* >();
    const auto actorTypePair = index.data( Qt::EditRole ).value< ActorDescriptionType >();
    vtkActor* actorPtr = std::get<0>( actorTypePair );
    //    std::cerr << "VisualisationWidget::selectActor: select " << actorPtr << std::endl;
    emit signal_actorSelected( actorPtr );
  } );

  const auto deselectedList = deselected.indexes();
  std::for_each( deselectedList.begin(), deselectedList.end(), [&]( const QModelIndex& index ) {
    //    vtkActor* actorPtr = (vtkActor *) index.data( Qt::EditRole ).value< void* >();
    const auto actorTypePair = index.data( Qt::EditRole ).value< ActorDescriptionType >();
    vtkActor* actorPtr = std::get<0>( actorTypePair );
    //    std::cerr << "VisualisationWidget::selectActor: deselect " << actorPtr << std::endl;
    emit signal_actorDeselected( actorPtr );
  } );

  ui->qvtkWidget->update();
}

/*!
 * \brief VisualisationWidget::removeSelectedRows
 */
void VisualisationWidget::removeSelectedRows()
{
  auto indexes = ui->actorsListView->selectionModel()->selectedIndexes();
  qSort( indexes.begin(), indexes.end(), qGreater< QModelIndex >() );
  for ( auto iter = indexes.constBegin(); iter != indexes.constEnd(); ++iter ) {
    //    vtkActor* actorPtr = (vtkActor *) (*iter).data( Qt::EditRole ).value< void* >();
    const auto actorTypePair = (*iter).data( Qt::EditRole ).value< ActorDescriptionType >();
    vtkActor* actorPtr = std::get<0>( actorTypePair );
    emit signal_actorDeselected( actorPtr );
    renderer->RemoveActor( actorPtr );
    actorListModelPtr->removeRow( (*iter).row(), (*iter).parent() );
  }

  ui->qvtkWidget->update();
}

/*!
 * \brief VisualisationWidget::hideShowSelectedRows
 */
void VisualisationWidget::hideShowSelectedRows()
{
  auto indexes = ui->actorsListView->selectionModel()->selectedIndexes();
  for ( auto iter = indexes.constBegin(); iter != indexes.constEnd(); ++iter ) {
    //    vtkActor* pointer = (vtkActor *) (*iter).data( Qt::EditRole ).value< void* >();
    const auto actorTypePair = (*iter).data( Qt::EditRole ).value< ActorDescriptionType >();
    vtkActor* actorPtr = std::get<0>( actorTypePair );
    bool visible = false;
    {
      vtkActorCollection* collectionPtr = renderer->GetActors();
      visible = collectionPtr->IsItemPresent( actorPtr );
    }
    if ( visible ) {
      renderer->RemoveActor( actorPtr );
      emit signal_actorDeselected( actorPtr );
    }
    else {
      renderer->AddActor( actorPtr );
      emit signal_actorSelected( actorPtr );
    }
  }
  ui->qvtkWidget->update();
}

/*!
 * \brief VisualisationWidget::actorListViewContextMenu
 * \param pos
 */
void VisualisationWidget::actorListViewContextMenu(const QPoint &pos)
{
  if ( ui->actorsListView->selectionModel()->selectedIndexes().empty() ) { return; }

  QMenu contextMenu( tr("Context menu"), this );

  QAction* hideShowActionPtr = contextMenu.addAction( tr("&Visibility") );
  QObject::connect( hideShowActionPtr, SIGNAL( triggered() ), this, SLOT( hideShowSelectedRows() ) );

  QAction* deleteActionPtr = contextMenu.addAction( tr("&Delete") );
  QObject::connect( deleteActionPtr, SIGNAL( triggered() ), this, SLOT( removeSelectedRows() ) );

  QAction* moveToNewWidget = contextMenu.addAction( tr( "Move to &new visualisation widget" ) );
  QObject::connect( moveToNewWidget, SIGNAL( triggered() ), this, SLOT( moveActorsToNewWVisualisationWidget() ) );

  QAction* addOrRemoveBoundingBox = contextMenu.addAction( tr( "Add or remove &bounding box") );
  QObject::connect( addOrRemoveBoundingBox, SIGNAL( triggered() ), this, SLOT( addOrRemoveBoundingBox() ) );

  const auto actorType = std::get<1>( ui->actorsListView->selectionModel()->selectedIndexes().first().data( Qt::EditRole ).value< ActorDescriptionType >() );

  switch ( actorType ) {
    case GEOMETRY_ACTOR:
      {
        QAction* geometryProcessing = contextMenu.addAction( tr( "&Geometry processing" ) );
        QObject::connect( geometryProcessing, SIGNAL( triggered() ), this, SLOT( applyProcessing() ) );
      }
      break;

    case MESH_ACTOR:
      {
        QAction* meshProcessing = contextMenu.addAction( tr( "&Mesh processing" ) );
        QObject::connect( meshProcessing, SIGNAL( triggered() ), this, SLOT( applyProcessing() ) );
      }
      break;

    case RESULT_ACTOR:
      {
        QAction* resultProcessing = contextMenu.addAction( tr( "&Result processing" ) );
        QObject::connect( resultProcessing, SIGNAL( triggered() ), this, SLOT( applyProcessing() ) );
      }
      break;

    default:
      break;
  }

  const QPoint globalPos = ui->actorsListView->mapToGlobal( pos );
  contextMenu.exec( globalPos );
}

/*!
 * \brief VisualisationWidget::moveActorsToNewWVisualisationWidget
 */
void VisualisationWidget::moveActorsToNewWVisualisationWidget()
{
  auto indexes = ui->actorsListView->selectionModel()->selectedIndexes();
  qSort( indexes.begin(), indexes.end(), qGreater< QModelIndex >() );
  QList< std::pair< vtkSmartPointer < vtkMapper >, const ActorType > > mapperList;
  for ( auto iter = indexes.constBegin(); iter != indexes.constEnd(); ++iter ) {
    const auto actorTypePair = (*iter).data( Qt::EditRole ).value< ActorDescriptionType >();
    vtkActor* actorPtr = std::get<0>( actorTypePair );
    emit signal_actorDeselected( actorPtr );
    vtkSmartPointer< vtkMapper > mapperPointer( actorPtr->GetMapper() );
    mapperList.push_back( std::make_pair( mapperPointer, std::get<1>( actorTypePair ) ) );
  }

  VisualisationWidget* newVisualisationWidget = new VisualisationWidget();
  std::for_each( mapperList.begin(), mapperList.end(), [&](std::pair< vtkSmartPointer < vtkMapper >, const ActorType > &ref) {
    newVisualisationWidget->addMapper( vtkSmartPointer< vtkMapper >( ref.first.GetPointer() ), ref.second );
  } );

  newVisualisationWidget->show();
}

/*!
 * \brief VisualisationWidget::addOrRemoveBoundingBox
 */
void VisualisationWidget::addOrRemoveBoundingBox()
{
  auto indexes = ui->actorsListView->selectionModel()->selectedIndexes();
  qSort( indexes.begin(), indexes.end(), qGreater< QModelIndex >() );
  for ( auto iter = indexes.constBegin(); iter != indexes.constEnd(); ++iter ) {
    //    vtkActor* actorPtr = (vtkActor *) (*iter).data( Qt::EditRole ).value< void* >();
    const auto actorTypePair = (*iter).data( Qt::EditRole ).value< ActorDescriptionType >();
    vtkActor* actorPtr = std::get<0>( actorTypePair );
    emit signal_addOrRemoveBoundingBoxForActor( actorPtr );
  }

  ui->qvtkWidget->update();
}

void VisualisationWidget::applyProcessing()
{
  auto indexes = ui->actorsListView->selectionModel()->selectedIndexes();
  qSort( indexes.begin(), indexes.end(), qGreater< QModelIndex >() );
  // vtkActor* actorPtr = (vtkActor *) indexes.first().data( Qt::EditRole ).value< void* >();
  const auto actorTypePair = indexes.first().data( Qt::EditRole ).value< ActorDescriptionType >();
  vtkActor* actorPtr = std::get<0>( actorTypePair );
  const ActorType actorType = std::get<1>( actorTypePair );
  const QString fileName = std::get<2>( actorTypePair );

  switch (actorType) {
    case GEOMETRY_ACTOR:
      {
        vtkSmartPointer< vtkPolyData > polyData = (vtkPolyData*)actorPtr->GetMapper()->GetInput();
        QString newName( fileName );
        newName.chop( fileName.size() - fileName.lastIndexOf( "." ) );
        emit sendGeometryToMesher( polyData, newName );
      }
      break;

    case MESH_ACTOR:
      {
        //        VTKMeshViewerDialog dialog( this, actorPtr );
        //        /*const auto val = */dialog.exec();
        vtkSmartPointer< vtkUnstructuredGrid > grid = (vtkUnstructuredGrid*)actorPtr->GetMapper()->GetInput();
        emit sendMeshToBoundaryConditions( grid );
      }
      break;

    case RESULT_ACTOR:
      {
        // TODO
      }
      break;

    default:
      break;
  }

}


#include "vtkSphereSource.h"
#include "vtkCubeSource.h"

#include "vtkDataSetMapper.h"

#include "vtkXMLUnstructuredGridReader.h"

#include "RandomUtils.hpp"

#include <tuple>


constexpr auto sphereCount = 10;
constexpr auto xDeviation = std::make_pair( -3.0, 3.0 );
constexpr auto yDeviation = std::make_pair( -3.0, 3.0 );
constexpr auto zDeviation = std::make_pair( -3.0, 3.0 );
constexpr auto rDeviation = std::make_pair( 0.2, 0.5 );

void cubeSpheres(VisualisationWidget& visualisationWidget)
{
  {
    vtkSmartPointer< vtkCubeSource > cubeSource = vtkSmartPointer< vtkCubeSource >::New();
    cubeSource->Update();
    cubeSource->SetCenter( 4, 4, 4 );
    cubeSource->SetXLength( 2.0 );
    cubeSource->SetYLength( 2.0 );
    cubeSource->SetZLength( 2.0 );

    vtkSmartPointer< vtkPolyDataMapper > newMapper = vtkSmartPointer< vtkPolyDataMapper >::New();
    newMapper->SetInputConnection( cubeSource->GetOutputPort() );

    visualisationWidget.addMapper( newMapper, GEOMETRY_ACTOR, "Cube 1" );
  }

  {
    // Sphere 1
    vtkSmartPointer< vtkSphereSource > sphereSource1 = vtkSmartPointer<vtkSphereSource>::New();
    sphereSource1->SetPhiResolution( 40 );
    sphereSource1->SetThetaResolution( 40 );
    sphereSource1->Update();
    sphereSource1->SetCenter( -0.5, 0.5, 0.5 );

    vtkSmartPointer< vtkPolyDataMapper > newMapper = vtkSmartPointer< vtkPolyDataMapper >::New();
    newMapper->SetInputConnection( sphereSource1->GetOutputPort() );

    visualisationWidget.addMapper( newMapper, GEOMETRY_ACTOR, "Sphere 1" );
  }

  {
    // Sphere 2
    vtkSmartPointer<vtkSphereSource> sphereSource2 = vtkSmartPointer< vtkSphereSource >::New();
    sphereSource2->Update();
    sphereSource2->SetCenter( 0.5, -0.5, 0.5 );

    vtkSmartPointer< vtkPolyDataMapper > newMapper = vtkSmartPointer< vtkPolyDataMapper >::New();
    newMapper->SetInputConnection( sphereSource2->GetOutputPort() );

    visualisationWidget.addMapper( newMapper, GEOMETRY_ACTOR, "Sphere 2" );
  }
}


void spheres(VisualisationWidget& visualisationWidget)
{
  std::vector< double > radiuses( sphereCount );
  std::generate( radiuses.begin(), radiuses.end(), [](){ return kernel::utils::random::uniform_real_rand( rDeviation.first, rDeviation.second ); } );

  std::vector< double > xVector( sphereCount );
  std::generate( xVector.begin(), xVector.end(), [](){ return kernel::utils::random::uniform_real_rand( xDeviation.first, xDeviation.second ); } );

  std::vector< double > yVector( sphereCount );
  std::generate( yVector.begin(), yVector.end(), [](){ return kernel::utils::random::uniform_real_rand( yDeviation.first, yDeviation.second ); } );

  std::vector< double > zVector( sphereCount );
  std::generate( zVector.begin(), zVector.end(), [](){ return kernel::utils::random::uniform_real_rand( zDeviation.first, zDeviation.second ); } );

  for ( auto i = 0; i < sphereCount; ++ i ) {
    vtkSmartPointer< vtkSphereSource > sphereSource = vtkSmartPointer<vtkSphereSource>::New();
    sphereSource->SetPhiResolution( 15 );
    sphereSource->SetThetaResolution( 15 );
    sphereSource->Update();
    sphereSource->SetRadius( radiuses[ i ] );
    sphereSource->SetCenter( xVector[ i ], yVector[ i ], zVector[ i ] );

    //    std::cout << "x: " << xVector[ i ] << " y: " << yVector[ i ] << " z: " << zVector[ i ] << " r: " << radiuses[ i ] << std::endl;

    vtkSmartPointer< vtkPolyDataMapper > newMapper = vtkSmartPointer< vtkPolyDataMapper >::New();
    newMapper->SetInputConnection( sphereSource->GetOutputPort() );

    visualisationWidget.addMapper( newMapper, GEOMETRY_ACTOR );
  }
}


#include <vtkOBBDicer.h>
#include <vtkThreshold.h>
#include <vtkGeometryFilter.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkDoubleArray.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkFieldData.h>
#include <vtkUnstructuredGrid.h>


bool OBBDicerTest(VisualisationWidget& visualisationWidget)
{

  // OBBDicer
  {
    vtkSmartPointer< vtkSphereSource > sphereSource = vtkSmartPointer<vtkSphereSource>::New();
    sphereSource->SetRadius( 4 * M_PI );
    sphereSource->SetPhiResolution( 30 );
    sphereSource->SetThetaResolution( 30 );
    sphereSource->Update();
    sphereSource->SetCenter( -0.5, 0.5, 0.5 );

    vtkSmartPointer< vtkPolyData > polyData = vtkSmartPointer< vtkPolyData >::New();
    polyData->DeepCopy( sphereSource->GetOutput() );

    vtkSmartPointer< vtkOBBDicer > dicer = vtkSmartPointer< vtkOBBDicer >::New();

    dicer->SetInput( polyData );
    dicer->SetNumberOfPieces( 64 );
    dicer->SetDiceModeToSpecifiedNumberOfPieces();
    dicer->Update();

    const auto numberOfPieces = dicer->GetNumberOfActualPieces();

    std::cout << "Asked for: " << dicer->GetNumberOfPieces() << " pieces, got: " << numberOfPieces << std::endl;

    // Put the original diced object
    {
      vtkSmartPointer< vtkPolyDataMapper > newMapper = vtkSmartPointer< vtkPolyDataMapper >::New();
      newMapper->SetInputConnection( dicer->GetOutputPort() );
      newMapper->SetScalarRange( 0, dicer->GetNumberOfActualPieces() );
      visualisationWidget.addMapper( newMapper, GEOMETRY_ACTOR );
    }

    // Print all
    {
      vtkSmartPointer< vtkPointData > pd( dicer->GetOutput()->GetPointData() );
      if ( pd )
      {
        std::cout << " contains point data with " << pd->GetNumberOfArrays() << " arrays." << std::endl;
        for (int i = 0; i < pd->GetNumberOfArrays(); i++)
        {
          std::cout << "Array " << i << " is named " << (pd->GetArrayName(i) ? pd->GetArrayName(i) : "NULL") << std::endl;

          vtkSmartPointer< vtkDataArray > arr( pd->GetArray( i ) );
          const auto arrNumOfTuples = arr->GetNumberOfTuples();
          std::cout << "arrNumOfTuples: " << arrNumOfTuples << std::endl;
          for ( auto j = 0; j < arrNumOfTuples; ++ j ) {
            std::cout << arr->GetComponent( VTK_INT, j ) << " ";
          }
          std::cout << std::endl;

        }
      }

      vtkSmartPointer< vtkCellData > cd( dicer->GetOutput()->GetCellData() );
      if ( cd )
      {
        std::cout << " contains cell data with " << cd->GetNumberOfArrays() << " arrays." << std::endl;
        for (int i = 0; i < cd->GetNumberOfArrays(); i++)
        {
          std::cout << "\tArray " << i << " is named " << (cd->GetArrayName(i) ? cd->GetArrayName(i) : "NULL") << std::endl;
        }
      }

      vtkSmartPointer< vtkFieldData > fd( dicer->GetOutput()->GetFieldData() );
      if ( fd )
      {
        std::cout << " contains field data with " << fd->GetNumberOfArrays() << " arrays." << std::endl;
        for (int i = 0; i < fd->GetNumberOfArrays(); i++)
        {
          std::cout << "\tArray " << i << " is named " << fd->GetArray(i)->GetName() << std::endl;
        }
      }

      // return EXIT_SUCCESS;
    }

    for ( auto th = 0; th < numberOfPieces; ++ th ) {

      vtkSmartPointer< vtkThreshold > threshold = vtkSmartPointer< vtkThreshold >::New();

      threshold->SetInput( dicer->GetOutput() );
      threshold->ThresholdBetween( th - 0.5, th + 0.5 );

      vtkSmartPointer< vtkDataSetSurfaceFilter > surfaceFilter =  vtkSmartPointer< vtkDataSetSurfaceFilter >::New();
      surfaceFilter->SetInput( threshold->GetOutput() );
      surfaceFilter->Update();

      vtkSmartPointer< vtkPolyDataMapper > newMapper = vtkSmartPointer< vtkPolyDataMapper >::New();

      newMapper->SetInputConnection( surfaceFilter->GetOutputPort() );

      newMapper->SetScalarRange( 0, dicer->GetNumberOfActualPieces() );

      visualisationWidget.addMapper( newMapper, GEOMETRY_ACTOR );
    }

  }

  return true;
}


bool test::VisualisationWidgetTest(int argc, char *argv[])
{
  QApplication a( argc, argv );

  VisualisationWidget widget;
  widget.show();
  widget.move( QApplication::desktop()->screen()->rect().center() - widget.rect().center() );

  //  spheres( visualisationWidget );
  //  cubeSpheres( visualisationWidget );
  //  OBBDicerTest( visualisationWidget );

  return !a.exec();
}
