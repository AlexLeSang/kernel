#include "RootTreeItem.hpp"

#include <QDebug>
#include "AbstractTreeItem.hpp"

/*!
 * \brief RootTreeItem::RootTreeItem
 * \param name
 */
RootTreeItem::RootTreeItem(const QStringList &name)
{
  headerData = name;
}

/*!
 * \brief RootTreeItem::~RootTreeItem
 */
RootTreeItem::~RootTreeItem()
{
  qDeleteAll( childItems );
}

/*!
 * \brief RootTreeItem::appendChild
 * \param item
 */
void RootTreeItem::appendChild(AbstractTreeItem *item)
{
  childItems.append( item );
  item->setParent( parentItem );
}

/*!
 * \brief RootTreeItem::child
 * \param row
 * \return
 */
AbstractTreeItem *RootTreeItem::child(int row)
{
  return childItems.value( row );
}

/*!
 * \brief RootTreeItem::children
 * \return
 */
QList<AbstractTreeItem *> RootTreeItem::children()
{
  return childItems;
}

/*!
 * \brief RootTreeItem::childCount
 * \return
 */
int RootTreeItem::childCount() const
{
  return childItems.count();
}

/*!
 * \brief RootTreeItem::columnCount
 * \return
 */
int RootTreeItem::columnCount() const
{
  return headerData.size();
}

/*!
 * \brief RootTreeItem::data
 * \param column
 * \return
 */
QVariant RootTreeItem::data(int column) const
{
  return headerData.value( column );
}

/*!
 * \brief RootTreeItem::row
 * \return
 */
int RootTreeItem::row() const
{
  return 0;
}

/*!
 * \brief RootTreeItem::parent
 * \return
 */
RootTreeItem *RootTreeItem::parent()
{
  return parentItem;
}

/*!
 * \brief RootTreeItem::setParent
 * \param parent
 */
void RootTreeItem::setParent(RootTreeItem *parent)
{
  parentItem = parent;
}

/*!
 * \brief RootTreeItem::insertChild
 * \param position
 * \param item
 */
void RootTreeItem::insertChild(int position, AbstractTreeItem* item)
{
  Q_ASSERT( !( position < 0 || position > childItems.size() ) );
  childItems.insert( position, item );
  item->setParent( parentItem );
}

/*!
 * \brief RootTreeItem::removeChild
 * \param position
 */
void RootTreeItem::removeChild(int position)
{
  Q_ASSERT( !( position < 0 || position > childItems.size() ) );
  delete childItems.takeAt( position );
}
