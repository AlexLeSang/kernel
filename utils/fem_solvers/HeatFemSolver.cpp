#include "HeatFemSolver.hpp"

#include <getfem/getfem_model_solvers.h>
#include <gmm/gmm_iter_solvers.h>
#include <type_traits>



auto equalDouble = [&](const double& d1, const double& d2)
{
  return ( std::fabs( d1 - d2 ) < std::numeric_limits<double>::epsilon() );
};


namespace kernel {
  namespace utils {
    namespace fem_solvers {

      /*!
       * \brief HeatFemSolver::stop
       */
      void HeatFemSolver::stop()
      {
        stopFlag.store( true, std::memory_order_relaxed );
      }

      /*!
       * \brief HeatFemSolver::setInputGrid
       * \param value
       */
      void HeatFemSolver::setInputGrid(const vtkSmartPointer<vtkUnstructuredGrid> &value)
      {
        inputGrid = value;
        vtkPointIndexToGetFemDofIndex.clear();
      }

      /*!
       * \brief HeatFemSolver::getInputGrid
       * \return
       */
      vtkSmartPointer<vtkUnstructuredGrid> HeatFemSolver::getInputGrid() const
      {
        return inputGrid;
      }

      /*!
       * \brief HeatFemSolver::setResidual
       * \param value
       */
      void HeatFemSolver::setResidual(const double &value)
      {
        residual = value;
      }

      /*!
       * \brief HeatFemSolver::getResidual
       * \return
       */
      double HeatFemSolver::getResidual() const
      {
        return residual;
      }

      /*!
       * \brief HeatFemSolver::getSlaeSolver
       * \return
       */
      std::shared_ptr<AbstractSLAESolver> HeatFemSolver::getSlaeSolver() const
      {
        return slaeSolver;
      }

      /*!
       * \brief HeatFemSolver::setSlaeSolver
       * \param value
       */
      void HeatFemSolver::setSlaeSolver(const std::shared_ptr<AbstractSLAESolver> &value)
      {
        slaeSolver = value;
      }

      /*!
       * \brief HeatFemSolver::setTotalSolutionTime
       * \param value
       */
      void HeatFemSolver::setTotalSolutionTime(const double &value)
      {
        totalSolutionTime = value;
      }

      /*!
       * \brief HeatFemSolver::getTotalSolutionTime
       * \return
       */
      double HeatFemSolver::getTotalSolutionTime() const
      {
        return totalSolutionTime;
      }

      /*!
       * \brief HeatFemSolver::setTimeStep
       * \param value
       */
      void HeatFemSolver::setTimeStep(const double &value)
      {
        timeStep = value;
      }

      /*!
       * \brief HeatFemSolver::getTimeStep
       * \return
       */
      double HeatFemSolver::getTimeStep() const
      {
        return timeStep;
      }

      /*!
       * \brief HeatFemSolver::setTheta
       * \param value
       */
      void HeatFemSolver::setTheta(const double &value)
      {
        theta = value;
      }

      /*!
       * \brief HeatFemSolver::getTheta
       * \return
       */
      double HeatFemSolver::getTheta() const
      {
        return theta;
      }

      /*!
       * \brief HeatFemSolver::setThermalDiffusivity
       * \param value
       */
      void HeatFemSolver::setThermalDiffusivity(const double &value)
      {
        thermalDiffusivity = value;
      }

      /*!
       * \brief HeatFemSolver::getThermalDiffusivity
       * \return
       */
      double HeatFemSolver::getThermalDiffusivity() const
      {
        return thermalDiffusivity;
      }

      /*!
       * \brief HeatFemSolver::setSolutionTime
       * \param value
       */
      void HeatFemSolver::setSolutionTime(const double &value)
      {
        solutionTime = value;
      }

      /*!
       * \brief HeatFemSolver::getSolutionTime
       * \return
       */
      double HeatFemSolver::getSolutionTime() const
      {
        return solutionTime;
      }

      /*!
       * \brief HeatFemSolver::setInitialTemperature
       * \param value
       */
      void HeatFemSolver::setInitialTemperature(const double &value)
      {
        initialTemperature = value;
      }

      /*!
       * \brief HeatFemSolver::getInitialTemperature
       * \return
       */
      double HeatFemSolver::getInitialTemperature() const
      {
        return initialTemperature;
      }

      /*!
       * \brief HeatFemSolver::addBoundaryCondition
       * \param applyedConditions
       */
      void HeatFemSolver::addBoundaryCondition(const std::list<AppliedBoundaryCondition> &applyedConditions)
      {
        for ( auto& c : applyedConditions ) {
          addBoundaryCondition( c );
        }
      }

      /*!
       * \brief HeatFemSolver::addBoundaryCondition
       * \param applyedCondition
       */
      void HeatFemSolver::addBoundaryCondition(const AppliedBoundaryCondition &applyedCondition)
      {
        ApplicableNodes applicableNodes = applyedCondition.getApplicableNodes();
        std::shared_ptr<AbstractBoundaryCondition> condition = applyedCondition.getBoundaryCondition();
        applicableNodesAndCondition.push_back( std::make_pair( applicableNodes, condition ) );
      }

      /*!
       * \brief HeatFemSolver::getOutputGrids
       * \return
       */
      std::unordered_map<double, vtkSmartPointer<vtkUnstructuredGrid> > HeatFemSolver::getOutputGrids()
      {
        std::unordered_map< double, vtkSmartPointer< vtkUnstructuredGrid > > outputGrids;

        for ( auto it = solutionResultsForTime.begin(); it != solutionResultsForTime.end(); ++ it ) {
          const double time = (*it).first;
          const std::vector<double>& results = (*it).second;
          vtkSmartPointer< vtkUnstructuredGrid > newGrid = vtkSmartPointer< vtkUnstructuredGrid >::New();
          newGrid->DeepCopy( inputGrid );
          actualizeIndexMapping();
          setResultIntoGrid( newGrid, results );
          outputGrids.insert( std::make_pair( time, newGrid ) );
        }

        return outputGrids;
      }

      /*!
       * \brief HeatFemSolver::init
       */
      void HeatFemSolver::init()
      {
        gmm::set_warning_level( 1 );
        gmm::set_traces_level( 1 );
        convertGrid();
        setFEM();
        discoverOutherFacesOfMesh();
      }

      /*!
       * \brief HeatFemSolver::clear
       */
      void HeatFemSolver::clear()
      {
        stopFlag.store( false, std::memory_order_relaxed );
        solutionTime = 0.0;

        model.clear();
        mesh.clear();
        U.clear();
        neumannData.clear();
        dirihletData.clear();

        inputGrid = vtkSmartPointer< vtkUnstructuredGrid >::New();
        solutionResultsForTime.clear();
        vtkPointIndexToGetFemDofIndex.clear();
        applicableNodesAndCondition.clear();
        nodeDirihletMap.clear();
        nodeNeumannMap.clear();
      }

      /*!
       * \brief HeatFemSolver::solve
       * \return
       */
      bool HeatFemSolver::solve()
      {
        assert( slaeSolver );

        addMainUnknown();
        appLaplacianTerm();
        assignNeumannCondtion();
        assignDirihletCondition();
        addTransientPart();

        gmm::iteration iter( residual );

        model.first_iter();

        setInitialTemperature();
        storeResult();
        bool interruped = false;
        for ( double t = 0; t < totalSolutionTime; t += timeStep) {

          {
            if ( stopFlag.load( std::memory_order_relaxed ) ) {
              interruped = true;
              break;
            }

            emit timeStepProcessed( t );
          }

          updateSolutionTime( t );
          updateNeumannData();
          updateDirihletData();

          iter.init();

          {
            boost::timer::auto_cpu_timer t;
            std::cout << "model.real_tangent_matrix().ncols(): " << model.real_tangent_matrix().ncols() << std::endl;
            std::cout << "model.real_tangent_matrix().nrows(): " << model.real_tangent_matrix().nrows() << std::endl;
            std::cout << "model.real_rhs().size(): " << model.real_rhs().size() << std::endl;

            (*slaeSolver)( model, iter );
          }


          gmm::copy( model.real_variable( "u" ), U );

          storeResult();

          model.next_iter();
        }

        if ( !interruped ) {
          emit finished();
        }
        else {
          emit interrupted();
        }

        return ( iter.converged() );
      }

      /*!
       * \brief HeatFemSolver::actualizeIndexMapping
       */
      void HeatFemSolver::actualizeIndexMapping()
      {
        if ( vtkPointIndexToGetFemDofIndex.empty() ) {

          const auto totalNumberOfDOF = meshFemU.nb_basic_dof();
          std::size_t nodesFound = 0;
          for ( std::size_t dofIndex = 0; dofIndex < meshFemU.nb_basic_dof(); ++ dofIndex ) {
            const bgeot::base_node node = meshFemU.point_of_basic_dof( dofIndex );
            const auto vtkIndex = findVtkPointIndex( node );
            if ( vtkIndex != -1 ) {
              ++ nodesFound;
              vtkPointIndexToGetFemDofIndex.insert( std::make_pair( vtkIndex, dofIndex ) );
            }
          }
          assert( nodesFound == totalNumberOfDOF );
          std::cout << "Number of mapped DOF -> vtkPoints: (" << nodesFound << "/" << totalNumberOfDOF << ")" << std::endl;
        }
      }

      /*!
       * \brief HeatFemSolver::findVtkPointIndex
       * \param node
       * \return
       */
      const long HeatFemSolver::findVtkPointIndex(const bgeot::base_node &node)
      {
        long index = -1;
        const auto points = inputGrid->GetPoints();
        assert( points->GetNumberOfPoints() );
        const auto numberOfPoints = points->GetNumberOfPoints();

        for ( auto i = 0; i < numberOfPoints; ++ i ) {
          double p[ 3 ];
          points->GetPoint( i, p );
          if ( pointEqualsNode( p, node ) ) {
            index = i;
            break;
          }
        }

        return index;
      }

      /*!
       * \brief HeatFemSolver::setResultIntoGrid
       * \param newGrid
       * \param results
       */
      void HeatFemSolver::setResultIntoGrid(vtkSmartPointer<vtkUnstructuredGrid> &newGrid, const std::vector<double> &results)
      {
        vtkSmartPointer< vtkDoubleArray > vtkResultScalars = vtkSmartPointer< vtkDoubleArray >::New();
        vtkResultScalars->SetNumberOfComponents( 1 );
        vtkResultScalars->Allocate( results.size() );
        for ( std::size_t vtkIndex = 0; vtkIndex < results.size(); ++ vtkIndex )  {
          const double& v = results[ vtkPointIndexToGetFemDofIndex[ vtkIndex ] ];
          vtkResultScalars->InsertNextValue( v );
        }
        vtkResultScalars->SetName( "Temperature" );
        newGrid->GetPointData()->SetScalars( vtkResultScalars );
      }

      /*!
       * \brief HeatFemSolver::pointEqualsNode
       * \param p
       * \param node
       * \return
       */
      bool HeatFemSolver::pointEqualsNode(const double p[], const bgeot::base_node &node)
      {
        return equalDouble( p[ 0 ], node[ 0 ] ) && equalDouble( p[ 1 ], node[ 1 ] ) && equalDouble( p[ 2 ], node[ 2 ] );
      }

      /*!
       * \brief HeatFemSolver::convertGrid
       */
      void HeatFemSolver::convertGrid()
      {
        clearMeshDataStructures();
        const std::vector< size_type > baseNodeIndiciesInMesh = getNodeIndiciesInMesh();
        addSimplexesToMesh( baseNodeIndiciesInMesh );
      }

      /*!
       * \brief HeatFemSolver::clearMeshDataStructures
       */
      void HeatFemSolver::clearMeshDataStructures()
      {
        mesh.clear();
        vtkPointIndexToGetFemDofIndex.clear();
        solutionResultsForTime.clear();
      }

      /*!
       * \brief HeatFemSolver::getNodeIndiciesInMesh
       * \return
       */
      std::vector<size_type> HeatFemSolver::getNodeIndiciesInMesh()
      {
        std::vector< size_type > baseNodeIndeciesInMesh;
        const auto numberOfPoints = inputGrid->GetNumberOfPoints();
        assert( numberOfPoints );
        baseNodeIndeciesInMesh.reserve( numberOfPoints );
        for ( auto p = 0; p < numberOfPoints; ++p ) {
          double point[ 3 ];
          inputGrid->GetPoint( p, point );
          baseNodeIndeciesInMesh.push_back( mesh.add_point( bgeot::base_node( point[ 0 ], point[ 1 ], point[ 2 ] ) ) );
        }

        return baseNodeIndeciesInMesh;
      }

      /*!
       * \brief HeatFemSolver::addSimplexesToMesh
       * \param baseNodeIndiciesInMesh
       */
      void HeatFemSolver::addSimplexesToMesh(const std::vector<size_type> &baseNodeIndiciesInMesh)
      {
        const auto numberOfCells = inputGrid->GetNumberOfCells();
        assert( numberOfCells );
        for ( auto c = 0; c < numberOfCells; ++c ) {
          vtkSmartPointer< vtkIdList > cell_points = vtkSmartPointer< vtkIdList >::New();
          inputGrid->GetCellPoints( c, cell_points );
          assert( cell_points->GetNumberOfIds() == 4 );
          std::vector< size_type > convex( 4 );
          convex[ 0 ] = baseNodeIndiciesInMesh[ cell_points->GetId( 0 ) ];
          convex[ 1 ] = baseNodeIndiciesInMesh[ cell_points->GetId( 1 ) ];
          convex[ 2 ] = baseNodeIndiciesInMesh[ cell_points->GetId( 2 ) ];
          convex[ 3 ] = baseNodeIndiciesInMesh[ cell_points->GetId( 3 ) ];
          mesh.add_simplex( 3, convex.begin() );
        }
      }

      /*!
       * \brief HeatFemSolver::setFEM
       */
      void HeatFemSolver::setFEM()
      {
        //        getfem::pfem pf_u = getfem::fem_descriptor( "FEM_PK(3,1)" );
        //        getfem::pintegration_method ppi = getfem::int_method_descriptor( "IM_TETRAHEDRON(2)" );

        integrationMethod.set_integration_method( mesh.convex_index(), ppi );
        meshFemU.set_finite_element( mesh.convex_index(), pf_u );
        meshFemRhs.set_finite_element( mesh.convex_index(), pf_u );
      }

      /*!
       * \brief HeatFemSolver::discoverOutherFacesOfMesh
       */
      void HeatFemSolver::discoverOutherFacesOfMesh()
      {
        getfem::mesh_region border_faces;
        getfem::outer_faces_of_mesh( mesh, border_faces );

        for ( getfem::mr_visitor i( border_faces ); ! i.finished(); ++i ) {
          assert( i.is_face() );
          assignBCToOutherFace( i );
        }
      }

      /*!
       * \brief HeatFemSolver::assignBCToOutherFace
       * \param face
       */
      void HeatFemSolver::assignBCToOutherFace(getfem::mr_visitor &face)
      {
        const bgeot::base_node normal = getFaceNormal( face );
        const std::vector< bgeot::base_node > facePoints = getFacePoints( face );

        for ( auto it = applicableNodesAndCondition.begin(); it != applicableNodesAndCondition.end(); ++ it ) {
          const ApplicableNodes& applicableNodes = it->first;

          if ( applicableNodes.applicableToFace( facePoints ) ) {
            std::shared_ptr<AbstractBoundaryCondition>& condition = it->second;
            const BOUNDARY_CONDITION_TYPE conditionType = condition->getBoundaryConditionType();
            assert( conditionType != BOUNDARY_CONDITION_TYPE::NON_CONDITION );
            mesh.region( static_cast< std::size_t >( conditionType ) ).add( face.cv(), face.f() );
            fillNodeConditionMaps( condition.get(), facePoints, normal );
          }
        }
      }

      /*!
       * \brief HeatFemSolver::getFaceNormal
       * \param face
       * \return
       */
      bgeot::base_node HeatFemSolver::getFaceNormal(getfem::mr_visitor &face)
      {
        bgeot::base_node normal = mesh.normal_of_face_of_convex( face.cv(), face.f() );
        normal /= gmm::vect_norm2( normal );
        return normal;
      }

      /*!
       * \brief HeatFemSolver::getFacePoints
       * \param face
       * \return
       */
      std::vector<bgeot::base_node> HeatFemSolver::getFacePoints(getfem::mr_visitor face)
      {
        const auto container = mesh.points_of_face_of_convex( face.cv(), face.f() );
        assert( container.size() == 3 );
        std::vector< bgeot::base_node > facePoints;
        facePoints.resize( container.size() );
        std::copy( container.begin(), container.end(), facePoints.begin() );
        return facePoints;
      }

      /*!
       * \brief HeatFemSolver::fillNodeConditionMaps
       * \param condition
       * \param facePoints
       * \param normal
       */
      void HeatFemSolver::fillNodeConditionMaps(AbstractBoundaryCondition* condition, const std::vector<bgeot::base_node> &facePoints, const bgeot::base_node &normal)
      {
        assert( facePoints.size() == 3 );

        const BOUNDARY_CONDITION_TYPE conditionType = condition->getBoundaryConditionType();

        for ( auto i = 0; i < 3; ++ i ) {
          const bgeot::base_node& node = facePoints[ i ];
          switch ( conditionType ) {
            case BOUNDARY_CONDITION_TYPE::DIRICHLET_BOUNDARY_CONDITION:
              {
                DirihletBoundaryCondition* bc = static_cast< DirihletBoundaryCondition* >( condition );
                nodeDirihletMap.insert( std::make_pair( node, bc->getCondition() ) );
                break;
              }

            case BOUNDARY_CONDITION_TYPE::NEUMANN_BOUNDARY_CONDITION:
              {
                NeumannBoundaryCondition* bc = static_cast< NeumannBoundaryCondition* >( condition );
                const bgeot::small_vector<double> conditionValue = bc->getCondition();
                const bgeot::small_vector<double> directedConditionValue = directConditionTowardNormal( conditionValue, normal );
                nodeNeumannMap.insert( std::make_pair( node, directedConditionValue ) );
                break;
              }
            default:
              break;
          }

        }
      }

      /*!
       * \brief HeatFemSolver::directConditionTowardNormal
       * \param condition
       * \param normal
       * \return
       */
      bgeot::small_vector<double> HeatFemSolver::directConditionTowardNormal(const bgeot::small_vector<double> &condition, const bgeot::base_node &normal)
      {
        assert( condition.size() == 3 );
        auto directedCondition = condition;
        for ( auto i = 0; i < 3; ++ i ) {
          directedCondition[ i ] *= -( std::signbit( normal[ 0 ] ) );
        }
        return directedCondition;
      }

      /*!
       * \brief HeatFemSolver::addMainUnknown
       */
      void HeatFemSolver::addMainUnknown()
      {
        // Main unknown of the problem
        model.add_fem_variable( "u", meshFemU, 2 );
      }

      /*!
       * \brief HeatFemSolver::appLaplacianTerm
       */
      void HeatFemSolver::appLaplacianTerm()
      {
        // Laplacian term on u.
        model.add_initialized_scalar_data( "h", thermalDiffusivity );
        transientBricks.add( getfem::add_generic_elliptic_brick( model, integrationMethod, "u", "h" ) );
      }

      /*!
       * \brief HeatFemSolver::assignNeumannCondtion
       */
      void HeatFemSolver::assignNeumannCondtion()
      {
        computeNeumannData();
        applyMidpointScheme();
      }

      /*!
       * \brief HeatFemSolver::computeNeumannData
       */
      void HeatFemSolver::computeNeumannData()
      {
        auto memberNeumann = [&](const bgeot::base_node &node) {
          return this->applyNeumann( node );
        };

        neumannData.clear();
        neumannData.resize( meshFemRhs.nb_dof() * dimension );
        getfem::interpolation_function( meshFemRhs, neumannData, memberNeumann, static_cast< std::size_t >( BOUNDARY_CONDITION_TYPE::NEUMANN_BOUNDARY_CONDITION ) );
      }

      /*!
       * \brief HeatFemSolver::applyNeumann
       * \param node
       * \return
       */
      bgeot::base_small_vector HeatFemSolver::applyNeumann(const bgeot::base_node &node)
      {
        //  assert( !nodeNeumannMap.empty() );
        //  auto search = nodeNeumannMap.find( node );
        //  assert( search != nodeNeumannMap.end() );
        //  return search->second;
        return nodeNeumannMap[ node ];
      }

      /*!
       * \brief HeatFemSolver::applyMidpointScheme
       */
      void HeatFemSolver::applyMidpointScheme()
      {
        // The two version of the data make only a difference for midpoint scheme
        model.add_fem_data( "NeumannData", meshFemRhs, bgeot::dim_type( dimension ), 2 );
        gmm::copy( neumannData, model.set_real_variable( "NeumannData", 0 ) );
        gmm::copy( neumannData, model.set_real_variable( "NeumannData", 1 ) );
        transientBricks.add( getfem::add_normal_source_term_brick( model, integrationMethod, "u", "NeumannData", static_cast< std::size_t >( BOUNDARY_CONDITION_TYPE::NEUMANN_BOUNDARY_CONDITION ) ) );
      }

      /*!
       * \brief HeatFemSolver::assignDirihletCondition
       */
      void HeatFemSolver::assignDirihletCondition()
      {
        computeDirihletData();
        model.add_initialized_fem_data( "DirichletData", meshFemRhs, dirihletData );
        getfem::add_Dirichlet_condition_with_multipliers( model, integrationMethod, "u", meshFemU,
                                                          static_cast< std::size_t >( BOUNDARY_CONDITION_TYPE::DIRICHLET_BOUNDARY_CONDITION ), "DirichletData" );
      }

      /*!
       * \brief HeatFemSolver::computeDirihletData
       */
      void HeatFemSolver::computeDirihletData()
      {
        auto memberDirihlet = [&](const bgeot::base_node &node) {
          return this->applyDirihlet( node );
        };

        dirihletData.clear();
        dirihletData.resize( meshFemRhs.nb_dof() );
        getfem::interpolation_function( meshFemRhs, dirihletData, memberDirihlet );
      }

      /*!
       * \brief HeatFemSolver::applyDirihlet
       * \param node
       * \return
       */
      double HeatFemSolver::applyDirihlet(const bgeot::base_node &node)
      {
        //  assert( !nodeDirihletMap.empty() );
        //  auto search = nodeDirihletMap.find( node );
        //  assert( search != nodeDirihletMap.end() );
        //  return search->second;
        return nodeDirihletMap[ node ];
      }

      /*!
       * \brief HeatFemSolver::addTransientPart
       */
      void HeatFemSolver::addTransientPart()
      {
        // transient part.
        model.add_initialized_scalar_data( "dt", timeStep );
        getfem::add_basic_d_on_dt_brick( model, integrationMethod, "u", "dt" );
        model.add_initialized_scalar_data( "theta", theta );
        getfem::add_theta_method_dispatcher( model, transientBricks, "theta" );
      }

      /*!
       * \brief HeatFemSolver::setInitialTemperature
       */
      void HeatFemSolver::setInitialTemperature()
      {
        gmm::resize( U, meshFemU.nb_dof() );
        gmm::clear( U );
        std::fill( U.begin(), U.end(), initialTemperature );
        gmm::copy( U, model.set_real_variable( "u", 1 ) );
      }

      /*!
       * \brief HeatFemSolver::storeResult
       */
      void HeatFemSolver::storeResult()
      {
        std::vector<double> V;
        V.resize( U.size() );
        gmm::copy( U, V );
        solutionResultsForTime.push_back( std::make_pair( solutionTime, V ) );
      }

      /*!
       * \brief HeatFemSolver::updateSolutionTime
       * \param t
       */
      void HeatFemSolver::updateSolutionTime(double t)
      {
        solutionTime = t + timeStep;
        std::cout << "solving for t = " << solutionTime << std::endl;
      }

      /*!
       * \brief HeatFemSolver::updateNeumannData
       */
      void HeatFemSolver::updateNeumannData()
      {
        computeNeumannData();
        gmm::copy( neumannData, model.set_real_variable( "NeumannData" ) );
      }

      /*!
       * \brief HeatFemSolver::updateDirihletData
       */
      void HeatFemSolver::updateDirihletData()
      {
        computeDirihletData();
        gmm::copy( dirihletData, model.set_real_variable( "DirichletData" ) );
      }





    }
  }
}

#include <vtkUnstructuredGridReader.h>
#include <vtkPointData.h>
#include <vtkDataSetMapper.h>
#include <vtkScalarBarActor.h>
#include <vtkLookupTable.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkProperty.h>
#include <vtkIdList.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkDoubleArray.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <boundary_condition/AppliedBoundaryCondition.hpp>

#include <ResultsViewWidget.hpp>

#include <QApplication>

/*
bool test::HeatFemSolverTest(int argc, char **argv)
{
  HeatFemSolver solver;

  try {
    boost::timer::auto_cpu_timer t;

    assert( argc == 3 );
    double totalSolutionTime = atof( argv[ 1 ] );
    double timeStep = atof( argv[ 2 ] );

    double bounds[ 6 ];
    {
      vtkSmartPointer< vtkXMLUnstructuredGridReader > reader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
      reader->SetFileName( "simple_cube.vtu" );
      reader->Update();

      vtkSmartPointer< vtkUnstructuredGrid > grid( reader->GetOutput() );
      solver.setInputGrid( grid );
      grid->GetBounds( bounds );
    }

    solver.setDiffusionCoefficient( 10 );
    solver.setThermalPermittivity( 1.0 ); // Iron
    solver.setInitialTemperature( 10 );
    solver.setTotalSolutionTime( totalSolutionTime );
    solver.setTimeStep( timeStep );

    solver.init();

    if ( ! solver.solve() ) GMM_ASSERT1( false, "Solve procedure has failed" );
  } GMM_STANDARD_CATCH_ERROR;


  // Postprocessing
  {
    const auto results = solver.getOutputGrids();

    // Save all results
    //    std::for_each( results.begin(), results.end(), []( const std::pair< double, vtkSmartPointer<vtkUnstructuredGrid> >& v ) {
    //      const auto& time = v.first;
    //      const auto& grid = v.second;

    //      const std::string prefix = "getfem_heat_equation_test_output_";
    //      const std::string suffix = ".vtu";

    //      std::ostringstream oss;
    //      // oss << std::setw( 10 ) << std::setfill( '0' ) << time;
    //      oss << time;
    //      const std::string number = oss.str();

    //      vtkSmartPointer< vtkXMLUnstructuredGridWriter > writer = vtkSmartPointer< vtkXMLUnstructuredGridWriter >::New();
    //      writer->SetInput( grid );
    //      const auto filename = prefix + number + suffix;
    //      std::cout << "Grid for time " << time << " saved to " << filename << std::endl;
    //      writer->SetFileName( filename.c_str() );
    //      writer->Write();
    //    } );

    // Visualization
    {
      QApplication a( argc, argv );

      ResultsViewWidget widget;
      widget.setTimeGrids( results );
      widget.show();

      return a.exec();
    }

  }

  return true;
}
*/


/*!
 * \brief test::HeatFemSolverTestBC
 * \param argc
 * \param argv
 * \return
 */
bool test::HeatFemSolverTestBC(int argc, char *argv[])
{
  using namespace kernel::utils::fem_solvers;
  HeatFemSolver solver;
  solver.clear();

  //try
  {
    boost::timer::auto_cpu_timer t;

    assert( argc == 6 );

    const std::string gridFileName = argv[ 1 ];
    const std::string bc1FileName = argv[ 2 ];
    const std::string bc2FileName = argv[ 3 ];

    const double totalSolutionTime = atof( argv[ 4 ] );
    const double timeStep = atof( argv[ 5 ] );

    {
      AppliedBoundaryCondition condition1 = AppliedBoundaryCondition::readFromFile( QString( bc1FileName.c_str() ) );
      solver.addBoundaryCondition( condition1 );
    }
    {
      AppliedBoundaryCondition condition2 = AppliedBoundaryCondition::readFromFile( QString( bc2FileName.c_str() ) );
      solver.addBoundaryCondition( condition2 );
    }

    {
      vtkSmartPointer< vtkXMLUnstructuredGridReader > reader = vtkSmartPointer< vtkXMLUnstructuredGridReader >::New();
      reader->SetFileName( gridFileName.c_str() );
      reader->Update();

      vtkSmartPointer< vtkUnstructuredGrid > grid( reader->GetOutput() );
      std::cout << "grid->GetNumberOfCells(): " << grid->GetNumberOfCells() << std::endl;
      solver.setInputGrid( grid );
    }

    solver.setThermalDiffusivity( 10 );
    solver.setInitialTemperature( 10 );
    solver.setTotalSolutionTime( totalSolutionTime );
    solver.setTimeStep( timeStep );

    solver.init();

    if ( ! solver.solve() ) GMM_ASSERT1( false, "Solve procedure has failed" );
  }
  //GMM_STANDARD_CATCH_ERROR;


  const auto results = solver.getOutputGrids();
  assert( !results.empty() );

  return EXIT_SUCCESS;
}
